#ifndef __SUNDIALS_MAIN_H__
#define __SUNDIALS_MAIN_H__

#include "common.h"

return_t sundials_init(simulator_t* simulator, model_t* model);

return_t sundials_solve(simulator_t* simulator, model_t* model, double t_start, double t_end, double t_step,
                        double* t_reached);

#endif