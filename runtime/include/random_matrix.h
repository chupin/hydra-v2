#include <stdint.h>

int8_t** gen_random_matrix(uint32_t size, double sparsity, int8_t max_idx);
void free_matrix(uint32_t size, int8_t** matrix);