#ifndef __RETURN_CODE_H__
#define __RETURN_CODE_H__

typedef enum ret {
    SUCCESS,
    ROOT_RET,
    SOLVER_ERROR,
    INDEX_REDUCTION_FAILED,
    CATASTROPHIC_ERROR,
} return_t;

char* error_code_to_string(return_t ret);

#define CHECK_RET(ret)                                                                                                 \
    if (ret != SUCCESS) {                                                                                              \
        debug("Simulation failed with error code %s\n", error_code_to_string(ret));                                    \
        return ret;                                                                                                    \
    }

#endif