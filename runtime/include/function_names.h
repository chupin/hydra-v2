#define RESIDUAL_FUN __sdebl_residual_fun
// #define RESIDUAL_JACOBIAN_FUN __sdebl_residual_jacobian_fun

#define INIT_RESIDUAL_FUN __sdebl_init_residual_fun
// #define INIT_JACOBIAN_FUN __sdebl_init_jacobian_fun

#define COUNT_NON_ZERO __sdebl_count_nonzero

#define ROOT_SETUP_FUN __sdebl_setup_roots
#define ROOT_FINDING_FUN __sdebl_find_roots

#define SIG_BUILDER_FUN __sdebl_build_sig

#define GLOBAL_USER_DATA __sdebl_user_data

#define HANDLE_SWITCH __sdebl_handle_switch