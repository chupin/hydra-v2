#ifndef __DUMMY_SELECTION_H__
#define __DUMMY_SELECTION_H__

#include <stdbool.h>
#include <stdint.h>

#include "integer_matrix.h"

// dummy_selection.c implements the dummy derivative method through the function
// below.
//
// The function takes as argument the HOD for the equations and for the
// variable. These two arrays cannot be modified.
//
// It then takes some storage indicating which equations and variables are
// currently under consideration. These are 2 i64 arrays, indicating which
// derivative is under consideration (0 means it's not considered). These arrays
// should be copies of equation_hod_array and variable_hod_array.
//
// A result array, which contains, for every variable, the actual derivative of
// the variables that will be considered states.

bool select_dummy(const matrix_t* matrix, uint64_t* active_equations, uint64_t* active_variables);

// // Returns the number of variables present in vars that appear in eqn
// uint32_t count_non_zero_entries(void* state, entries_t* vars, uint32_t eqn);
// // Sort the variables present in vars according to how favorable they appear in
// // eqn. The array sorted_variables should at least be of size
// // count_non_zero_entries(state, vars, eqn), it contains the index of the
// // varible into vars
// void sort_variables(void* state, entries_t* vars, uint32_t eqn, uint32_t* sorted_variables);
// void eliminate_pair(void* state, uint32_t eqn, uint32_t var);

#endif