#ifndef __INTEGER_MATRIX_H__
#define __INTEGER_MATRIX_H__

#include <stdbool.h>
#include <stdint.h>

#include "return_code.h"

typedef struct row {
    uint32_t  row_size;
    uint32_t* row_indices;
    uint8_t*  row_entries;
    double*   double_row_entries;
    bool*     linear_entry;
} row_t;

typedef struct matrix {
    uint32_t size;
    row_t*   rows;
} matrix_t;

// typedef struct matrix matrix_t;

uint32_t get_matrix_size(matrix_t* matrix);

bool get_matrix_index(matrix_t* matrix, uint32_t row_ct, uint32_t col_ct, uint8_t* result);

bool get_row_col_info(const row_t* row, uint32_t var, uint8_t* entry, double* double_entry, bool* appears_linearly);

double get_double_matrix_index(matrix_t* matrix, uint32_t row_ct, uint32_t col_ct);

uint32_t non_zero_row_length(matrix_t* matrix, uint32_t row_ct);

matrix_t* gen_random_matrix(uint32_t size, double sparsity, int8_t max_idx);

void free_matrix(matrix_t* matrix);

void print_as_pairs(matrix_t* matrix);
void print_matrix(matrix_t* matrix);

struct best {
    uint32_t best_object;
    double   best_value;
    uint32_t second_best_object;
    double   second_best_value;
};

struct best best_net_value(matrix_t* matrix, double* prices, uint32_t person);

return_t is_sane(matrix_t* sig_mat);

// matrix_t* test_matrix1_addr;

#endif