#ifndef __DEBUG_H__
#define __DEBUG_H__

void debug(const char* format, ...);
void debugln(const char* format, ...);
#endif