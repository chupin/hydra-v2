#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdbool.h>
#include <stdint.h>

#include "debug.h"
#include "integer_matrix.h"
#include "return_code.h"

#define max(a, b)                                                              \
    ({                                                                         \
        __typeof__(a) _a = (a);                                                \
        __typeof__(b) _b = (b);                                                \
        _a > _b ? _a : _b;                                                     \
    })

typedef struct methods {
    return_t (*constructor)(void* model_env, uint64_t* next_var,
                            void**    model_state_storage,
                            uint64_t* variable_count, uint64_t* relations_count,
                            uint64_t* init_relations_count,
                            uint64_t* entries_count, uint64_t* root_count);
    return_t (*signature)(void* model_env, void* model_state,
                          bool first_activation, uint64_t* next_var,
                          row_t** next_row, uint32_t** next_row_index,
                          int8_t** next_row_entry, double** next_double_entry,
                          bool** next_linearity_info_entry);
    return_t (*residual)(void* model_env, void* model_state,
                         bool first_activation, uint64_t* next_var,
                         bool at_init, uint64_t* equation_hod_array,
                         double** input_signals, double* workspace,
                         double**  result_ptr_storage,
                         uint64_t* equation_count_storage);
    return_t (*root_setup)(void* model_env, void* model_state,
                           bool first_activation, uint64_t* next_var,
                           int32_t** next_root);
    return_t (*compute_roots)(void* model_env, void* model_state,
                              bool first_activation, uint64_t* next_var,
                              double** input_signals, double** root_result_ptr);
    return_t (*event_handling)(void* model_env, void* model_state,
                               bool first_activation, uint64_t* next_var,
                               double** input_signals,
                               void** model_state_storage, int32_t** next_root,
                               bool* any_change, bool* structural_change,
                               uint64_t* variable_count,
                               uint64_t* relations_count,
                               uint64_t* init_relations_count,
                               uint64_t* entries_count, uint64_t* root_count);
    return_t (*set_signal_info)(void* model_env, void* model_state,
                                bool first_activation, uint64_t* next_var,
                                bool* reinit_signals, char** signal_names);
    return_t (*workspace_size)(void* model_env, void* model_state,
                               bool first_activation, uint64_t* next_var,
                               uint64_t* equation_hod_array,
                               uint64_t* equation_count,
                               uint64_t* current_request);
    return_t (*copy_inputs)(void* model_env, void* model_state,
                            bool first_activation, uint64_t* next_var,
                            bool existed, uint64_t* old_variable_hod_array,
                            uint64_t* current_variable_hod_array,
                            double**  old_input_signals,
                            double**  current_input_signals,
                            uint64_t* old_next_var_storage);
} methods_t;

typedef struct model {
    methods_t* methods;
    void*      environment;
} model_t;

typedef struct simulator {
    bool  first_activation;
    bool  any_change;
    bool  structural_change;
    void* model_state;

    uint64_t allocated_variables;
    uint64_t allocated_entries;
    uint64_t allocated_roots;

    bool*  reinit_signals;
    char** signal_names;

    uint64_t variable_count;
    uint64_t init_relations_count;
    uint64_t relations_count;
    uint64_t entries_count;
    uint64_t root_count;

    uint64_t allocated_input_slots;
    uint64_t input_signals_slot_count;
    double*  input_signals_slab;
    double** input_signals;

    uint64_t allocated_workspace;
    double*  workspace;

    row_t* rows;

    uint32_t* row_indices;
    int8_t*   row_entries;
    double*   row_double_entries;
    bool*     row_linearity_info_entries;

    uint64_t* equation_hod_array;
    uint64_t* variable_hod_array;
    uint64_t* state_variables;

    uint64_t  allocated_old_variables;
    uint64_t* old_variable_hod_array;
    uint64_t  allocated_old_input_signals_slots;
    double*   old_input_signals_slab;
    double**  old_input_signals;

    int* root_dir;
    int* root_info;
} simulator_t;

typedef enum obj_kind {
    MODEL = 0,
} obj_kind_t;

typedef struct obj {
    obj_kind_t kind;
    void*      payload;
} obj_t;

// typedef struct user_data {
//     uint32_t   output_var_ct;
//     char**     output_var_names;
//     uint64_t*  prev_var_offset;
//     uint64_t*  init_var_offset;
//     uint64_t*  var_offset;
//     uint64_t*  sol_offset;
//     bool       first_activation;
//     double*    sol_vector;
//     uint64_t*  head_var;
//     uint64_t*  var_hod;
//     bool*      reinit_array;
//     uint64_t*  eqn_hod;
//     switch_t** program_struct;
//     matrix_t*  signature_matrix;
//     int32_t*   root_dir_array;
//     int32_t*   root_array;
// } user_data_t;

#include "function_names.h"

// int RESIDUAL_FUN(double* ys, double** rs, user_data_t* user_data);
// // int RESIDUAL_JACOBIAN_FUN(double* ys, int64_t* iptrs, int64_t* vptrs,
// double*
// // rs, user_data_t* user_data);

// int INIT_RESIDUAL_FUN(double* iys, double** rs, user_data_t* user_data);
// // int INIT_JACOBIAN_FUN(double* ys, int64_t* iptrs, int64_t* vptrs, double*
// rs,
// // user_data_t* user_data);

// int COUNT_NON_ZERO(uint64_t* non_zero_residual, uint64_t* non_zero_init,
// user_data_t* user_data);

// uint64_t ROOT_SETUP_FUN(user_data_t* user_data);
// int      ROOT_FINDING_FUN(double* ys, double** gout, user_data_t* user_data);

// void SIG_BUILDER_FUN(uint32_t* eqn_count, uint32_t* init_eqn_count,
// user_data_t* user_data);

// void HANDLE_SWITCH(double* ys, int32_t* roots, user_data_t* user_data);

// extern user_data_t GLOBAL_USER_DATA;

#endif