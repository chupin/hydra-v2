#ifndef __INDEX_REDUCTION_H__
#define __INDEX_REDUCTION_H__

#include "common.h"
#include "integer_matrix.h"

#include <stdint.h>

return_t index_reduction(matrix_t* sig_mat, uint64_t* cs, uint64_t* ds);

uint64_t compute_degrees_of_freedom(uint64_t size, uint64_t* cs, uint64_t* ds);

#endif