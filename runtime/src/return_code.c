#include "return_code.h"

char* error_code_to_string(return_t ret) {
    switch (ret) {
    case SUCCESS:
        return "SUCCESS";
    case ROOT_RET:
        return "ROOT_RET";
    case SOLVER_ERROR:
        return "SOLVER_ERROR";
    case INDEX_REDUCTION_FAILED:
        return "INDEX_REDUCTION_FAILED";
    case CATASTROPHIC_ERROR:
        return "CATASTROPHIC_ERROR";
    default:
        return "UNKNOWN_ERROR";
    }
}