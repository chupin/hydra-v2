#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "debug.h"
#include "dummy_selection.h"
#include "integer_matrix.h"

// This file implements the dummy derivative method

// After sort_variables has been called, sorted_variables contain the variable
// numbers in order of preference for the equation represented by the row. The
// array is assumed to be uninitialised when the function is entered.
void sort_variables(const row_t* row, uint32_t* sorted_variables) {
    // Initialise sorted_variables with the entries in row
    for (uint64_t entry = 0; entry < row->row_size; ++entry) {
        sorted_variables[entry] = row->row_indices[entry];
    }

    // Below is an implementation of bubble sort. Yes it's probably not the best
    // but I can always change it later and I don't expect arrays with more that
    // 5 or 6 variables so who cares.
    bool no_change = false;
    int  stop      = row->row_size - 1;
    while (!no_change && stop > 0) {
        no_change = true;
        for (int var = 0; var < stop; ++var) {
            // Swap the two entries if row->linear_entry[entry + 1] is true but
            // not row->linear_entry[entry]
            bool var_appears_linearly;
            bool next_var_appears_linearly;
            bool ok = get_row_col_info(row, sorted_variables[var], NULL, NULL,
                                       &var_appears_linearly);
            bool ok_next =
                get_row_col_info(row, sorted_variables[var + 1], NULL, NULL,
                                 &next_var_appears_linearly);
            if (!ok || !ok_next) {
                printf("Something catastrophic happened\n");
                exit(-1);
            }

            if (!var_appears_linearly && next_var_appears_linearly) {
                uint32_t tmp_sorted_variables_var = sorted_variables[var];
                sorted_variables[var]             = sorted_variables[var + 1];
                sorted_variables[var + 1]         = tmp_sorted_variables_var;
                no_change                         = false;
            }
        }

        // Decrement stop
        --stop;
    }

    return;
}

// This function perfors one round of the dummy derivative method. Given a set
// of m active equations, it selects m active variables (whose entry is != 0 in
// the active_equations array), one for every equation.
//
// To do so, it runs over the list of actives equation. Once it's encountered
// one, it selects an active variable that appears in it. Currently, it selects
// any variable appearing linearly, the last one otherwise.
bool select_dummy_round(uint64_t next_equation, const matrix_t* matrix,
                        uint64_t* active_equations, uint64_t* active_variables,
                        bool* selected_variables) {

    bool no_equation = true;
    for (uint64_t eqn = next_equation; eqn < matrix->size; ++eqn) {
        // If the equation is not active, skip it. We don't need to check if
        // it's already been selected or not, because equations are selected in
        // order.
        if (active_equations[eqn] != 0) {
            debugln("Considering equation %lu(%lu)", eqn,
                    active_equations[eqn]);
            no_equation = false;
            // Retrieve the row corresponding to the equation in the signature
            // matrix
            row_t eqn_row = matrix->rows[eqn];

            // We wish to select an active variable appearing in the equation.
            // We start by sorting the variables that appear in the equation.
            //
            // NOTE: I think it should always be more or less OK to allocate the
            // array on the stack. It should never be more that a few entries
            // long.
            uint32_t* sorted_variables =
                malloc(eqn_row.row_size * sizeof(uint32_t));
            sort_variables(&eqn_row, sorted_variables);
            // sorted_variables now contains all the variables appearing in eqn,
            // sorted by order of preference for being selected as a dummy by
            // eqn.
            //
            // We run over the list of variables.
            for (uint64_t entry = 0; entry < eqn_row.row_size; ++entry) {
                uint64_t var = sorted_variables[entry];
                debugln("Considering variable %lu(%lu)", var,
                        active_variables[var]);
                // If the variable is active and has not already been selected,
                // we select it.
                if (active_variables[var] != 0 && !selected_variables[var]) {
                    debugln("Trying variable %lu", var);
                    selected_variables[var] = true;
                    // We set no_variable to false, because we have found at
                    // least one variable
                    bool success = select_dummy_round(
                        eqn + 1, matrix, active_equations, active_variables,
                        selected_variables);

                    if (success) {
                        debugln("Select variable %lu for equation %lu", var,
                                eqn);
                        // If the assignment was a success, we can decrease
                        // active_variable for var by 1, since its HOD has been
                        // selected as a dummy and is not to be considered a
                        // state. The entry in active_equation for eqn must also
                        // be decremented by 1. It's not necessary to check for
                        // underflow because we know that active_equations[var]
                        // is not 0
                        active_variables[var] -= 1;
                        active_equations[eqn] -= 1;
                        // Don't forget to free sorted_variables in case of
                        // early return. We also have to free it if we haven't
                        // found a good variable
                        free(sorted_variables);
                        return true;
                    } else {
                        debugln("Failed to assign variable %lu to equation %lu",
                                var, eqn);
                        // If the assignement has failed, we must report it in
                        // selected_variables and then we loop back to try to
                        // select another variable
                        selected_variables[var] = false;
                    }
                }
            }

            // Free sorted_variables if we have found no suitable variable
            free(sorted_variables);

            // If we've reached this point, this means that no variable could be
            // found for the equation and we must therefore bail out.
            return false;
        }
    }

    // If we reach this point, this means that we have not encountered any
    // equation and therefore we can terminate fine

    return true;
}

// select_dummy works as follow. It runs over the equations that appear
// differentiated (the ones which have a != 0 entry in active_equations). It
// selects the one with the least number of entries.
//
// Once it's done that, it selects a variable that must have a != entry in
// active_variables and appear in the selected equation.
bool select_dummy(const matrix_t* matrix, uint64_t* active_equations,
                  uint64_t* active_variables) {

    bool* selected_variables = malloc(matrix->size * sizeof(bool));

    while (true) {
        // Initialise the selected_variables array. Also check that there are
        // active equations. Use the boolean done for this
        bool no_active_equation = true;
        for (uint64_t var = 0; var < matrix->size; ++var) {
            selected_variables[var] = false;
            if (active_equations[var] != 0) {
                no_active_equation = false;
            }
        }

        // If there were no active equations, free selected_variables and return
        // success
        if (no_active_equation) {
            free(selected_variables);
            return true;
        }

        bool success = select_dummy_round(0, matrix, active_equations,
                                          active_variables, selected_variables);

        // If the assignment has failed, free selected_variables and return
        // failure
        if (!success) {
            free(selected_variables);
            return false;
        }

        // Otherwise, loop around
    }
}