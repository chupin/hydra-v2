#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "common.h"
#include "index_reduction.h"
#include "integer_matrix.h"

#define INF_BUMP ((10))

typedef struct assig {
    uint32_t  a_size;
    double    epsilon;
    uint32_t* assig; // For each person, stores the object it has been
                     // assigned too
    double*   price; // Current prices for the objects matrix_t*
    matrix_t* affinities;
} assig_t;

static void print_assig(assig_t*);

static void print_hod(uint64_t size, uint64_t* equation_hod_array,
                      uint64_t* variable_hod_array) {
    debug("EQN HOD: ");
    for (uint32_t eqn = 0; eqn < size; ++eqn) {
        debug(", %lu", equation_hod_array[eqn]);
    }
    debug("\nVAR HOD :");
    for (uint32_t var = 0; var < size; ++var) {
        debug(", %lu", variable_hod_array[var]);
    }
    debug("\n");
}

static bool possible_assignement(assig_t* assig) {
    // It is possible to find a suitable assignment if for every object there is
    // at least one person that desires it
    for (uint32_t object = 0; object < assig->a_size; ++object) {
        bool desired = false;
        for (uint32_t person = 0; person < assig->a_size; ++person) {
            uint8_t result;
            if (get_matrix_index(assig->affinities, person, object, &result)) {
                desired = true;
                break;
            }
        }
        if (!desired) {
            return false;
        }
    }

    return true;
}

static double net_value(assig_t* assig, uint32_t person, uint32_t object) {
    return get_double_matrix_index(assig->affinities, person, object) -
           assig->price[object];
}

static bool is_happy(assig_t* assig, uint32_t person,
                     struct best* best_values) {
    *best_values = best_net_value(assig->affinities, assig->price, person);
    uint32_t current_object = assig->assig[person];
    double   current_value  = net_value(assig, person, current_object);
    return (current_value >= best_values->best_value - assig->epsilon);
}

static bool bidding_round(assig_t* assig) {
    bool all_happy = false;
    while (!all_happy) {
        all_happy = true;
        for (uint32_t person = 0; person < assig->a_size; ++person) {
            struct best best_values;
            if (is_happy(assig, person, &best_values)) {
                continue;
            }

            double bump = best_values.best_value -
                          best_values.second_best_value + assig->epsilon;
            // Use this to have no infinite prices:
            /*
            if (bump == INFINITY) {
                bump = INF_BUMP * assig->epsilon;
            }
            */

            for (uint32_t other_person = 0; other_person < assig->a_size;
                 ++other_person) {
                if (assig->assig[other_person] == best_values.best_object) {
                    assig->assig[other_person] = assig->assig[person];
                    break;
                }
            }

            assig->price[best_values.best_object] += bump;
            assig->assig[person] = best_values.best_object;
            all_happy            = false;
        }
    }

    return true;
}

static bool solve_assignment(assig_t* assig) {
    if (!possible_assignement(assig)) {
        return false;
    }
    while (true) {
        if (!bidding_round(assig)) {
            return false;
        }

        if (assig->epsilon > 1.0 / (double)assig->a_size) {
            assig->epsilon /= 2.0;
            /*
            // Reset the prices to 0
            for (uint32_t object = 0; object < assig->a_size; ++object) {
                assig->price[object] = 0.0;
            }*/
        } else {
            break;
        }
    }

    // Check if all assignements are to values with positive affinities
    print_assig(assig);
    for (uint32_t person = 0; person < assig->a_size; ++person) {
        uint8_t result;
        if (!get_matrix_index(assig->affinities, person, assig->assig[person],
                              &result)) {
            return false;
        }
    }

    return true;
}

static void smallest_dual_variable(assig_t* assig, uint64_t* cs, uint64_t* ds) {
    memset(cs, 0, assig->a_size * sizeof(uint64_t));
    memset(ds, 0, assig->a_size * sizeof(uint64_t));

    bool unchanged;
    do {
        unchanged = true;
        for (uint32_t object = 0; object < assig->a_size; ++object) {
            bool    found_a_max = false;
            uint8_t max_value   = 0;
            for (uint32_t person = 0; person < assig->a_size; ++person) {
                uint8_t affinity;
                if (get_matrix_index(assig->affinities, person, object,
                                     &affinity)) {
                    if (affinity + cs[person] > max_value || !found_a_max) {
                        found_a_max = true;
                        max_value   = affinity + cs[person];
                    }
                }
            }

            assert(found_a_max);
            if (ds[object] != max_value) {
                ds[object] = max_value;
                unchanged  = false;
            }
        }

        for (uint32_t person = 0; person < assig->a_size; ++person) {
            uint32_t object = assig->assig[person];
            uint8_t  result;
            bool     finite =
                get_matrix_index(assig->affinities, person, object, &result);
            assert(finite);
            int8_t csp = ds[object] - result;
            if (cs[person] != (uint64_t)csp) {
                cs[person] = csp;
                unchanged  = false;
            }
        }
    } while (!unchanged);
}

// Init and free

static assig_t* init_assig(matrix_t* affinities) {
    uint32_t a_size   = get_matrix_size(affinities);
    assig_t* assig    = calloc(1, sizeof(assig_t));
    assig->a_size     = a_size;
    assig->epsilon    = 1.0;
    assig->assig      = calloc(a_size, sizeof(uint32_t));
    assig->price      = calloc(a_size, sizeof(double));
    assig->affinities = affinities;
    for (uint32_t i = 0; i < a_size; ++i) {
        assig->assig[i] = i;
    }

    return assig;
}

static void print_assig(assig_t* assig) {
#ifdef DEBUG_MODE
    for (uint32_t person = 0; person < assig->a_size; ++person) {
        debug("%d -> %d\n", person, assig->assig[person]);
    }
#else
    (void)assig;
#endif
}

static void free_assig(assig_t* assig) {
    free(assig->assig);
    free(assig->price);
    free(assig);
}

// static void set_head_vars(user_data_t* data) {
//     matrix_t* matrix    = data->signature_matrix;
//     uint64_t* head_vars = data->head_var;
//     memset(head_vars, 0, matrix->size * sizeof(uint64_t));
//     for (uint32_t eqn = 0; eqn < matrix->size; ++eqn) {
//         row_t row = matrix->rows[eqn];
//         for (uint32_t i = 0; i < row.row_size; ++i) {
//             uint32_t var   = row.row_indices[i];
//             head_vars[var] = max(head_vars[var], row.row_entries[i]);
//         }
//     }
// }

typedef struct {
    assig_t* assig;
} index_reduction_alloc_t;

static void cleanup_index_reduction_alloc(index_reduction_alloc_t* alloc) {
    if (alloc->assig != NULL) {
        free_assig(alloc->assig);
    }
}

return_t index_reduction(matrix_t* sig_mat, uint64_t* cs, uint64_t* ds) {

    index_reduction_alloc_t alloc = {.assig = NULL};

    // matrix_t* sig_mat = data->signature_matrix;
    // print_as_pairs(sig_mat);
    print_matrix(sig_mat);
    // uint64_t* cs              = data->eqn_hod;
    // uint64_t* ds              = data->var_hod;
    // uint64_t* var_offset      = data->var_offset;
    // uint64_t* init_var_offset = data->init_var_offset;

    assig_t* assig = init_assig(sig_mat);
    if (assig == NULL) {
        cleanup_index_reduction_alloc(&alloc);
        return CATASTROPHIC_ERROR;
    }
    alloc.assig = assig;
    bool solved = solve_assignment(assig);
    if (!solved) {
        cleanup_index_reduction_alloc(&alloc);
        return INDEX_REDUCTION_FAILED;
    }

    smallest_dual_variable(assig, cs, ds);

    print_hod(sig_mat->size, cs, ds);

    cleanup_index_reduction_alloc(&alloc);
    return SUCCESS;
}

uint64_t compute_degrees_of_freedom(uint64_t size, uint64_t* cs, uint64_t* ds) {
    uint64_t sum_ds = 0;
    uint64_t sum_cs = 0;
    for (uint32_t var = 0; var < size; ++var) {
        sum_ds += ds[var];
        sum_cs += cs[var];
    }

    assert(sum_ds >= sum_cs);

    return sum_ds - sum_cs;
}