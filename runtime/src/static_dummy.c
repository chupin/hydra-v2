#include "dummy_selection.h"
#include "integer_matrix.h"

typedef struct state {
    matrix_t* matrix;
} state_t;

// For dummy selection, the state is simply the matrix of affinities

uint32_t count_non_zero_entries(void* state, entries_t* vars, uint32_t eqn) {
    matrix_t* aff     = ((state_t*)state)->matrix;
    row_t     row     = aff->rows[eqn];
    uint32_t  count   = 0;
    uint32_t  row_idx = 0;
    uint32_t  var_idx = 0;
    while (var_idx < vars->entry_ct && row_idx < row.row_size) {
        uint32_t row_entry = row.row_indices[row_idx];
        uint32_t var_entry = vars->entries[var_idx].entry_idx;
        if (row_entry == var_entry) {
            ++count;
            ++row_idx;
            ++var_idx;
        } else if (row_entry > var_entry) {
            ++var_idx;
        } else {
            ++row_idx;
        }
    }

    return count;
}

void eliminate_pair(void* state, uint32_t eqn, uint32_t var) { return; }

void sort_variables(void* state, entries_t* vars, uint32_t eqn, uint32_t* sorted_variables) {
    matrix_t* aff = ((state_t*)state)->matrix;
    row_t     row = aff->rows[eqn];

    uint32_t row_idx = 0;
    uint32_t var_idx = 0;
    uint32_t count   = 0;
    while (var_idx < vars->entry_ct && row_idx < row.row_size) {
        uint32_t row_entry = row.row_indices[row_idx];
        uint32_t var_entry = vars->entries[var_idx].entry_idx;
        if (row_entry == var_entry) {
            // Store at count the index at which to find var if it appears in
            // vars and in the row
            sorted_variables[count] = var_idx;
            ++count;
            ++row_idx;
            ++var_idx;
        } else if (row_entry > var_entry) {
            ++var_idx;
        } else {
            ++row_idx;
        }
    }

    // No further sorting is done at this point. When we will get the
    // information on whether a variable appears linearly in an equation, we
    // will integrate the info.

    /*
    // We can now sort in place sorted_variables
    uint32_t start = 0;
    uint32_t end = count;
    uint32_t mid = end / 2;
    uint32_t start2 = mid + 1;
    */

    return;
}
