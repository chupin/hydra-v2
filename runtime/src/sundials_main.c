#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include <ida/ida.h>
#include <kinsol/kinsol.h>
#include <nvector/nvector_serial.h>
#include <sundials/sundials_linearsolver.h>
#include <sundials/sundials_nonlinearsolver.h>
#include <sunlinsol/sunlinsol_dense.h>
#include <sunlinsol/sunlinsol_klu.h>
#include <sunmatrix/sunmatrix_dense.h>
#include <sunmatrix/sunmatrix_sparse.h>
#include <sunnonlinsol/sunnonlinsol_newton.h>

#include "common.h"
#include "index_reduction.h"
#include "sundials_main.h"

// Holds all the allocation needed for the computation of initial conditions.
typedef struct {
    void*           kin_mem;
    N_Vector        iys;
    N_Vector        us;
    N_Vector        fs;
    SUNMatrix       jac;
    SUNLinearSolver ls;
} init_alloc_t;

typedef struct user_data {
    model_t*     model;
    simulator_t* simulator;
} user_data_t;

// Frees the pointers allocated for the calculation of initial condition. Will
// make sure to only free things that are non-null so it can be used for
// clean-up if one of the allocation fails.
static void cleanup_init_alloc(init_alloc_t* alloc) {
    if (alloc->kin_mem != NULL) {
        KINFree(&alloc->kin_mem);
    }

    if (alloc->iys != NULL) {
        N_VDestroy(alloc->iys);
    }

    if (alloc->us != NULL) {
        N_VDestroy(alloc->us);
    }

    if (alloc->fs != NULL) {
        N_VDestroy(alloc->fs);
    }

    if (alloc->jac != NULL) {
        SUNMatDestroy(alloc->jac);
    }

    if (alloc->ls != NULL) {
        SUNLinSolFree(alloc->ls);
    }
}

#define CHECK_KIN_CODE(alloc, ret, code, err_code)                             \
    if (ret != KIN_SUCCESS && ret != KIN_INITIAL_GUESS_OK) {                   \
        code = err_code;                                                       \
        cleanup_init_alloc(&alloc);                                            \
        return code;                                                           \
    }

#define CHECK_KIN_NON_NULL(alloc, ret, code)                                   \
    if (ret == NULL) {                                                         \
        code = CATASTROPHIC_ERROR;                                             \
        cleanup_init_alloc(&alloc);                                            \
        return code;                                                           \
    }

static void copy_from_flat_vec(simulator_t* simulator, double* iys) {
    uint64_t var_offset = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        for (uint64_t n = 0; n < simulator->variable_hod_array[var] + 1; ++n) {
            simulator->input_signals[var][n] = iys[var_offset];
            ++var_offset;
        }
    }
}

static void copy_from_initial(simulator_t* simulator, const double* iys) {
    // The variables that we need to compute the values for at initialisations
    // are:
    //
    //    * x^(0), x^(1), ..., x^(n) if x is reinitialised
    //    * x^(p), x^(p + 1), ..., x^(n) if x is not reinitialised and p is the
    //      entry for var in simulator->state_variables
    uint64_t var_offset = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        uint64_t start;
        if (simulator->reinit_signals[var]) {
            start = 0;
        } else {
            start = simulator->state_variables[var];
        }

        for (uint64_t n = start; n < simulator->variable_hod_array[var] + 1;
             ++n) {
            simulator->input_signals[var][n] = iys[var_offset];
            ++var_offset;
        }
    }
}

static void copy_to_initial(simulator_t* simulator, double* iys) {
    uint64_t var_offset = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        uint64_t start;
        if (simulator->reinit_signals[var]) {
            start = 0;
        } else {
            start = simulator->state_variables[var];
        }

        for (uint64_t n = start; n < simulator->variable_hod_array[var] + 1;
             ++n) {
            iys[var_offset] = simulator->input_signals[var][n];
            ++var_offset;
        }
    }
}

static void print_sundials_vector(const char* str, N_Vector vec) {
#ifdef DEBUG_MODE
    double* ys = NV_DATA_S(vec);
    for (uint64_t i = 0; i < NV_LENGTH_S(vec); ++i) {
        debugln("%s[%d] = %f", str, i, ys[i]);
    }
#else
    (void)str;
    (void)vec;
    return;
#endif
}

static void print_signals(simulator_t* simulator) {
#ifdef DEBUG_MODE
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        debug("Var %d (%s) = ", var, simulator->signal_names[var]);
        for (uint64_t n = 0; n < simulator->variable_hod_array[var] + 1; ++n) {
            debug("%f ", simulator->input_signals[var][n]);
        }
        debug("\n");
    }
#else
    (void)simulator;
    return;
#endif
}

static int init_residual(N_Vector u, N_Vector fu, void* raw_user_data) {
    user_data_t* user_data = raw_user_data;
    model_t*     model     = user_data->model;
    simulator_t* simulator = user_data->simulator;

    // We have to set the input signals to the right values. Some are going to
    // be provided by the iys vector, some must stay the same.
    copy_from_initial(simulator, NV_DATA_S(u));
    double* rs = NV_DATA_S(fu);

    uint64_t next_var       = 0;
    uint64_t equation_count = 0;
    print_sundials_vector("iys", u);
    print_signals(simulator);
    return_t ret = model->methods->residual(
        model->environment, simulator->model_state, simulator->first_activation,
        &next_var, true, simulator->equation_hod_array,
        simulator->input_signals, simulator->workspace, &rs, &equation_count);
    print_sundials_vector("rs", fu);
    return (int)ret;
}

uint64_t count_residual_relations(simulator_t* simulator) {
    uint64_t count = 0;
    for (uint64_t rel = 0; rel < simulator->relations_count; ++rel) {
        count += simulator->equation_hod_array[rel] + 1;
    }

    return count;
}

uint64_t count_known_state_variables(simulator_t* simulator) {
    uint64_t count = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        if (!simulator->reinit_signals[var]) {
            count += simulator->state_variables[var];
        }
    }

    return count;
}

uint64_t count_initial_unknowns(simulator_t* simulator) {
    uint64_t count = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        if (simulator->reinit_signals[var]) {
            count += simulator->variable_hod_array[var] + 1;
        } else {
            count += 1;
        }
    }

    return count;
}

return_t sundials_init(simulator_t* simulator, model_t* model) {

    debugln("Signals before sundials_init call");
    print_signals(simulator);

    return_t     code;
    int          ret;
    init_alloc_t alloc = {.kin_mem = NULL,
                          .iys     = NULL,
                          .us      = NULL,
                          .fs      = NULL,
                          .jac     = NULL,
                          .ls      = NULL};

    uint64_t residual_relations_count = count_residual_relations(simulator);

    uint64_t known_states = count_known_state_variables(simulator);

    uint64_t total_relations_count =
        simulator->init_relations_count + residual_relations_count;

    uint64_t freedom = compute_degrees_of_freedom(
        simulator->relations_count, simulator->equation_hod_array,
        simulator->variable_hod_array);

    debugln("Degrees of freedom: %d\n", freedom);

    if (simulator->init_relations_count + known_states != freedom) {
        debug("I know of the initial values of %d state variables and have %d "
              "initial relations, but have %d degrees "
              "of freedom.\n",
              known_states, simulator->init_relations_count, freedom);
        return CATASTROPHIC_ERROR;
    }

    void* kin_mem = KINCreate();
    CHECK_KIN_NON_NULL(alloc, kin_mem, code);
    alloc.kin_mem = kin_mem;

    N_Vector iys = N_VNew_Serial(total_relations_count);
    CHECK_KIN_NON_NULL(alloc, iys, code);
    alloc.iys = iys;
    copy_to_initial(simulator, NV_DATA_S(iys));

    ret = KINInit(kin_mem, init_residual, iys);
    CHECK_KIN_CODE(alloc, ret, code, SOLVER_ERROR);

    SUNMatrix jac =
        SUNDenseMatrix(total_relations_count, total_relations_count);
    CHECK_KIN_NON_NULL(alloc, jac, code);
    alloc.jac = jac;

    SUNLinearSolver ls = SUNLinSol_Dense(iys, jac);
    CHECK_KIN_NON_NULL(alloc, ls, code);
    alloc.ls = ls;

    ret = KINSetLinearSolver(kin_mem, ls, jac);
    CHECK_KIN_CODE(alloc, ret, code, SOLVER_ERROR);

    user_data_t user_data = {.model = model, .simulator = simulator};

    ret = KINSetUserData(kin_mem, &user_data);
    CHECK_KIN_CODE(alloc, ret, code, CATASTROPHIC_ERROR);

    // Scaling vectors
    N_Vector us = N_VNew_Serial(total_relations_count);
    CHECK_KIN_NON_NULL(alloc, us, code);
    alloc.us = us;
    N_VConst(1, us);
    N_Vector fs = N_VNew_Serial(total_relations_count);
    CHECK_KIN_NON_NULL(alloc, fs, code);
    alloc.fs = fs;
    N_VConst(1, fs);

    ret = KINSol(kin_mem, iys, KIN_NONE, us, fs);
    CHECK_KIN_CODE(alloc, ret, code, SOLVER_ERROR);

    copy_from_initial(simulator, NV_DATA_S(iys));

    print_signals(simulator);

    code = SUCCESS;
    cleanup_init_alloc(&alloc);
    return code;
}

static uint64_t sundials_input_vector_size(simulator_t* simulator) {
    uint64_t size = 0;

    for (uint64_t eqn = 0; eqn < simulator->relations_count; ++eqn) {
        size += simulator->equation_hod_array[eqn] + 1 +
                simulator->state_variables[eqn];
    }

    return size;
}

static int sundials_residual(realtype tt, N_Vector yy, N_Vector yp, N_Vector rr,
                             void* raw_user_data) {

    (void)tt;
    debugln("Residual evaluated at: %f", tt);

    user_data_t* user_data = (user_data_t*)raw_user_data;
    model_t*     model     = user_data->model;
    simulator_t* simulator = user_data->simulator;

    double* ys       = NV_DATA_S(yy);
    double* yps      = NV_DATA_S(yp);
    double* rs       = NV_DATA_S(rr);
    double* first_rs = rs;

    print_sundials_vector("ys", yy);
    print_sundials_vector("yps", yp);
    copy_from_flat_vec(simulator, ys);
    print_signals(simulator);

    uint64_t next_var       = 0;
    uint64_t equation_count = 0;
    return_t ret            = model->methods->residual(
        model->environment, simulator->model_state, simulator->first_activation,
        &next_var, false, simulator->equation_hod_array,
        simulator->input_signals, simulator->workspace, &rs, &equation_count);

    if (ret != SUCCESS) {
        return ret;
    }

    // The residual function doesn't touch the yps vector. All the derivatives
    // appear in the ys vector next to their source variable. So that Sundials
    // isn't lost, we must make sure that we add equations of the form yps[i] =
    // ys[i - 1], so that it can connect the dots.
    //
    // For that we need to maintain the offset in the input vector
    uint64_t var_offset = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        for (uint64_t n = 0; n < simulator->state_variables[var] + 1; ++n) {
            if (n != simulator->state_variables[var]) {
                debugln("Var: %d, var_offset: %d", var, var_offset);
                *rs = yps[var_offset] - simulator->input_signals[var][n + 1];
                ++rs;
            }
            ++var_offset;
        }
        var_offset += simulator->variable_hod_array[var] -
                      simulator->state_variables[var];
    }
    // print_sundials_vector("rs", rr);

    uint64_t expected = sundials_input_vector_size(simulator);
    if (expected != (uint64_t)(rs - first_rs)) {
        debugln("Expected %lu size but wrote %lu\n", expected,
                (uint64_t)(rs - first_rs));
        return -40000;
    }
    rs = first_rs;
    print_sundials_vector("rs", rr);
    // for (int i = 0; i < expected; ++i) {
    //     debugln("rs[%d] = %f", i, rs[i]);
    // }

    return 0;
}

// According to the Sundials documentation. id[k] should be set to 1.0 if k is
// a differentiable variable, 0.0 otherwise.
//
// In our case, Sundials should consider all variables differential upto
// simulator->state_variables[var] *excluded*
static void adjust_id_vector(simulator_t* simulator, double* id) {
    uint64_t var_offset = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        for (uint64_t n = 0; n < simulator->variable_hod_array[var]; ++n) {
            id[var_offset] = (n < simulator->state_variables[var]) ? 1.0 : 0.0;
            ++var_offset;
        }
    }
}

static int sundials_root(realtype tt, N_Vector yy, N_Vector yp, double* gout,
                         void* raw_user_data) {
    (void)tt;
    (void)yp;

    user_data_t* user_data = (user_data_t*)raw_user_data;
    model_t*     model     = user_data->model;
    simulator_t* simulator = user_data->simulator;
    double*      ys        = NV_DATA_S(yy);
    copy_from_flat_vec(simulator, ys);
    uint64_t next_var = 0;
    return_t ret      = model->methods->compute_roots(
        model->environment, simulator->model_state, simulator->first_activation,
        &next_var, simulator->input_signals, &gout);
    return (int)ret;
}

typedef struct sundials_solve_alloc {
    N_Vector           yy;
    N_Vector           yp;
    N_Vector           id;
    void*              ida_mem;
    SUNMatrix          jac;
    SUNLinearSolver    ls;
    SUNNonlinearSolver nls;
} solve_alloc_t;

static void cleanup_solve_alloc(solve_alloc_t* alloc) {
    if (alloc->yy != NULL) {
        N_VDestroy(alloc->yy);
    }

    if (alloc->yp != NULL) {
        N_VDestroy(alloc->yp);
    }

    if (alloc->id != NULL) {
        N_VDestroy(alloc->id);
    }

    if (alloc->ida_mem != NULL) {
        IDAFree(&alloc->ida_mem);
    }

    if (alloc->jac != NULL) {
        SUNMatDestroy(alloc->jac);
    }

    if (alloc->ls != NULL) {
        SUNLinSolFree(alloc->ls);
    }

    if (alloc->nls != NULL) {
        SUNNonlinSolFree(alloc->nls);
    }
}

#define CHECK_IDA_CODE(alloc, ret, code, err_code)                             \
    if (ret != IDA_SUCCESS) {                                                  \
        code = err_code;                                                       \
        cleanup_solve_alloc(&alloc);                                           \
        return code;                                                           \
    }

#define CHECK_IDA_NON_NULL(alloc, ret, code)                                   \
    if (ret == NULL) {                                                         \
        code = CATASTROPHIC_ERROR;                                             \
        cleanup_solve_alloc(&alloc);                                           \
        return code;                                                           \
    }

return_t sundials_solve(simulator_t* simulator, model_t* model, double t_start,
                        double t_end, double t_step, double* t_reached) {

    int           ret;
    return_t      code;
    solve_alloc_t alloc = {.yy      = NULL,
                           .yp      = NULL,
                           .id      = NULL,
                           .ida_mem = NULL,
                           .jac     = NULL,
                           .ls      = NULL,
                           .nls     = NULL};

    uint64_t input_vector_size = sundials_input_vector_size(simulator);
    debugln("======== Sundials input vector size: %d\n", input_vector_size);

    N_Vector yy = N_VNew_Serial(input_vector_size);
    CHECK_IDA_NON_NULL(alloc, yy, code);
    alloc.yy   = yy;
    double* ys = NV_DATA_S(yy);

    N_Vector yp = N_VNew_Serial(input_vector_size);
    CHECK_IDA_NON_NULL(alloc, yp, code);
    alloc.yp    = yp;
    double* yps = NV_DATA_S(yp);

    // Initialise ys and yps with the data from simulator->input_signals
    uint64_t var_offset = 0;
    for (uint64_t var = 0; var < simulator->variable_count; ++var) {
        for (uint64_t n = 0; n < simulator->variable_hod_array[var] + 1; ++n) {
            ys[var_offset] = simulator->input_signals[var][n];
            // yps doesn't have an entry when n is the HOD of var. Initialise to
            // 0 so we don't get garbage values in yps
            if (n < simulator->variable_hod_array[var]) {
                yps[var_offset] = simulator->input_signals[var][n + 1];
            } else {
                yps[var_offset] = 0.0;
            }
            ++var_offset;
        }
    }
    print_sundials_vector("ys", yy);
    print_sundials_vector("yps", yp);

    void* ida_mem = IDACreate();
    CHECK_IDA_NON_NULL(alloc, ida_mem, code);
    alloc.ida_mem = ida_mem;

    // Initialize the IDA solver
    ret = IDAInit(ida_mem, sundials_residual, t_start, yy, yp);
    CHECK_IDA_CODE(alloc, ret, code, SOLVER_ERROR);

    N_Vector id = N_VNew_Serial(input_vector_size);
    CHECK_IDA_NON_NULL(alloc, id, code);
    alloc.id = id;

    adjust_id_vector(simulator, NV_DATA_S(id));
    print_sundials_vector("id", id);
    ret = IDASetId(ida_mem, id);
    CHECK_IDA_CODE(alloc, ret, code, SOLVER_ERROR);

    user_data_t user_data = {.model = model, .simulator = simulator};

    ret = IDASetUserData(ida_mem, &user_data);
    CHECK_IDA_CODE(alloc, ret, code, CATASTROPHIC_ERROR);

    // Set the solver tolerances
    ret = IDASStolerances(ida_mem, 10e-3, 10e-3);
    CHECK_IDA_CODE(alloc, ret, code, CATASTROPHIC_ERROR);

    // Handle root finding
    if (simulator->root_count > 0) {
        ret = IDARootInit(ida_mem, simulator->root_count, sundials_root);
        CHECK_IDA_CODE(alloc, ret, code, CATASTROPHIC_ERROR);

        // Set the directions of the roots
        uint64_t next_var  = 0;
        int*     next_root = simulator->root_dir;
        ret                = model->methods->root_setup(
            model->environment, simulator->model_state,
            simulator->first_activation, &next_var, &next_root);
        IDASetRootDirection(ida_mem, simulator->root_dir);
        CHECK_IDA_CODE(alloc, ret, code, CATASTROPHIC_ERROR);
    }

    // Set linear solver/Jacobian matrix

    SUNMatrix jac = SUNDenseMatrix(input_vector_size, input_vector_size);
    CHECK_IDA_NON_NULL(alloc, jac, code);
    alloc.jac = jac;

    SUNLinearSolver ls = SUNLinSol_Dense(yy, jac);
    CHECK_IDA_NON_NULL(alloc, ls, code);
    alloc.ls = ls;

    // IDASetJacFn(ida_mem, NULL);

    ret = IDASetLinearSolver(ida_mem, ls, jac);
    CHECK_IDA_CODE(alloc, ret, code, SOLVER_ERROR);

    // Set nonlin solver

    SUNNonlinearSolver nls = SUNNonlinSol_Newton(yy);
    CHECK_IDA_NON_NULL(alloc, nls, code);
    alloc.nls = nls;

    ret = IDASetNonlinearSolver(ida_mem, nls);
    CHECK_IDA_CODE(alloc, ret, code, CATASTROPHIC_ERROR);

    if (simulator->first_activation) {
        printf("tt ");
        for (uint64_t var = 0; var < simulator->variable_count; ++var) {
            printf("%s ", simulator->signal_names[var]);
        }
        printf("\n");
    }

    // Main simulation loop
    double t_at = t_start;

    while (t_at < t_end) {
        debugln("Current time: %f", t_at);
        print_signals(simulator);

        printf("%f ", t_at);
        for (uint64_t var = 0; var < simulator->variable_count; ++var) {
            printf("%f ", simulator->input_signals[var][0]);
        }
        printf("\n");

        // // for (uint32_t var = 0; var < data->output_var_ct; ++var) {
        // //     printf(", %f", ys[data->var_offset[var]]);
        // // }
        // printf("\n");

        double t_goal = fmin(t_end, t_at + t_step);
        ret           = IDASolve(ida_mem, t_goal, &t_at, yy, yp, IDA_NORMAL);
        debugln("Time reached: %f", t_at);

        copy_from_flat_vec(simulator, NV_DATA_S(yy));

        if (ret == IDA_ROOT_RETURN || ret != IDA_SUCCESS) {
            break;
        }
    }

    *t_reached = t_at;

    if (ret == IDA_ROOT_RETURN) {
        code = ROOT_RET;
        ret  = IDAGetRootInfo(ida_mem, simulator->root_info);
        CHECK_IDA_CODE(alloc, ret, code, CATASTROPHIC_ERROR);
    } else if (ret == IDA_SUCCESS) {
        code = SUCCESS;
    } else {
        code = SOLVER_ERROR;
    }

    cleanup_solve_alloc(&alloc);

    return code;
}
