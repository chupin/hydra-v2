#include <sundials/sundials_matrix.h>
#include <math.h>

int estimate_condition_number_dense(SUNMatrix A, double* result) {
    if (SUNMatGetID(A) != SUNMATRIX_DENSE) {
        return 1;
    }

    sunindextype M = SM_ROWS_D(A);
    sunindextype N = SM_COLUMNS_D(A);

    if (M != N) {
        return 2;
    }

    realtype* data = SM_DATA_D(A);

    realtype min_elt = 0.0;
    realtype max_elt = INFINITY;

    for (int i = 0; i < M; ++i) {
        realtype line_sum = 0.0;
        for (int j = 0; j < M; ++j) {
            line_sum += abs(data[j + M * i];
        }
        max_elm = max(max_elt, line_sum);
        min_elt = min(min_elt, 2 * data[i + M * i] - line_sum);
    }

    *result = max_elt / min_elt;
    return 0;

}

int estimate_condition_number_sparse(SUNMatrix A, double* result) {
    if (SUNMatGetID(A) != SUNMATRIX_SPARSE) {
        return 1;
    }



}