#include <stdarg.h>
#include <stdio.h>

#include "debug.h"

int debug_ident = 0;
#define DEBUG_IDENT_BUMP (4)

void incr_debug_ident(void) { debug_ident += DEBUG_IDENT_BUMP; }
void decr_debug_ident(void) { debug_ident -= DEBUG_IDENT_BUMP; }

#ifdef DEBUG_MODE
void debug(const char* format, ...) {
    va_list args;
    va_start(args, format);

    vfprintf(stderr, format, args);

    fflush(stderr);

    va_end(args);
}

void debugln(const char* format, ...) {
    va_list args;
    va_start(args, format);

    for (int i = 0; i < debug_ident; ++i) {
        fprintf(stderr, "*");
    }

    if (debug_ident > 0) {
        fprintf(stderr, " ");
    }

    vfprintf(stderr, format, args);

    fprintf(stderr, "\n");

    fflush(stderr);

    va_end(args);
}
#else
void debug(const char* format, ...) {
    (void)format;
    return;
}
void debugln(const char* format, ...) {
    (void)format;
    return;
}
#endif
