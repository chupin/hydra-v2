#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "debug.h"
#include "integer_matrix.h"

uint32_t get_matrix_size(matrix_t* matrix) { return matrix->size; }

bool get_row_col_info(const row_t* row, uint32_t var, uint8_t* entry,
                      double* double_entry, bool* appears_linearly) {
    int32_t min_search = 0;
    int32_t max_search = row->row_size - 1;
    while (max_search >= min_search) {

        int32_t search = min_search + (max_search - min_search) / 2;

        if (row->row_indices[search] == var) {
            if (entry != NULL) {
                *entry = row->row_entries[search];
            }
            if (double_entry != NULL) {
                *double_entry = row->double_row_entries[search];
            }
            if (appears_linearly != NULL) {
                *appears_linearly = row->linear_entry[search];
            }
            return true;
        } else if (row->row_indices[search] < var) {
            min_search = search + 1;
        } else {
            max_search = search - 1;
        }
    }
    return false;
}

bool get_row_index(matrix_t* matrix, uint32_t row_ct, uint32_t col_ct,
                   uint32_t* result_index) {
    row_t*   row      = &matrix->rows[row_ct];
    uint32_t row_size = row->row_size;

    int32_t min_search = 0;
    int32_t max_search = row_size - 1;
    while (max_search >= min_search) {

        int32_t search = min_search + (max_search - min_search) / 2;

        if (row->row_indices[search] == col_ct) {
            *result_index = (uint32_t)search;
            return true;
        } else if (row->row_indices[search] < col_ct) {
            min_search = search + 1;
        } else {
            max_search = search - 1;
        }
    }
    return false;
}

bool get_matrix_index(matrix_t* matrix, uint32_t row_ct, uint32_t col_ct,
                      uint8_t* result) {
    uint32_t result_index;
    bool     finite = get_row_index(matrix, row_ct, col_ct, &result_index);
    if (!finite) {
        return false;
    }

    *result = matrix->rows[row_ct].row_entries[result_index];
    return true;
}

double get_double_matrix_index(matrix_t* matrix, uint32_t row_ct,
                               uint32_t col_ct) {
    uint32_t result_index;
    bool     finite = get_row_index(matrix, row_ct, col_ct, &result_index);
    if (!finite) {
        return -INFINITY;
    }

    return matrix->rows[row_ct].double_row_entries[result_index];
}

struct best best_net_value(matrix_t* matrix, double* prices, uint32_t person) {
    struct best result = {.best_value        = -INFINITY,
                          .second_best_value = -INFINITY};
    row_t*      row    = &matrix->rows[person];
    for (uint32_t i = 0; i < row->row_size; ++i) {
        uint32_t object       = row->row_indices[i];
        double   affinity     = row->double_row_entries[i];
        double   object_value = affinity - prices[object];

        if (object_value > result.best_value) {
            result.second_best_value  = result.best_value;
            result.best_value         = object_value;
            result.second_best_object = result.best_object;
            result.best_object        = object;
        } else if (object_value > result.second_best_value) {
            result.second_best_value  = object_value;
            result.second_best_object = object;
        }
    }

    return result;
}

row_t gen_random_row(uint32_t size, double sparsity, uint8_t max_idx) {
    uint32_t  row_size       = floor(sparsity * (double)size);
    uint32_t* indices        = calloc(row_size, sizeof(uint32_t));
    uint8_t*  entries        = calloc(row_size, sizeof(uint8_t));
    double*   double_entries = calloc(row_size, sizeof(double));

    uint32_t chunk_size = size / row_size;
    if (chunk_size == 0) {
        chunk_size = 1;
    }
    uint32_t chunk_index = 0;
    for (uint32_t i = 0; i < row_size; ++i) {
        uint32_t mod;
        if (i == row_size - 1) {
            mod = size - chunk_index;
        } else {
            mod = chunk_size;
        }
        indices[i] = chunk_index + (rand() % mod);
        // Make sure the indices have been generated in order:
        assert(!(i > 0) || indices[i - 1] < indices[i]);
        entries[i] = ((uint8_t)rand()) % (max_idx + 1);
        assert(entries[i] <= max_idx);
        double_entries[i] = (double)entries[i];
        chunk_index += chunk_size;
    }

    row_t row = {.row_size           = row_size,
                 .row_indices        = indices,
                 .row_entries        = entries,
                 .double_row_entries = double_entries};

    return row;
}

matrix_t* gen_random_matrix(uint32_t size, double sparsity, int8_t max_idx) {
    matrix_t* matrix = calloc(1, sizeof(matrix_t));
    matrix->size     = size;
    matrix->rows     = calloc(size, sizeof(row_t));
    for (uint32_t i = 0; i < size; ++i) {
        matrix->rows[i] = gen_random_row(size, sparsity, max_idx);
    }
    return matrix;
}

void print_as_pairs(matrix_t* matrix) {
    for (uint32_t i = 0; i < matrix->size; ++i) {
        debug("%d: ", i);
        row_t* row = &matrix->rows[i];
        for (uint32_t j = 0; j < row->row_size; ++j) {
            debug("(%d,%d) ", row->row_indices[j], row->row_entries[j]);
        }
        debug("\n");
    }
}

void print_gap(uint32_t previous, uint32_t next) {
    uint32_t gap = next - previous;
    while (gap > 0) {
        debug(". ");
        --gap;
    }
}

void print_matrix(matrix_t* matrix) {
    for (uint32_t i = 0; i < matrix->size; ++i) {
        row_t*   row      = &matrix->rows[i];
        uint32_t previous = 0;
        for (uint32_t j = 0; j < row->row_size; ++j) {
            uint32_t idx = row->row_indices[j];
            assert(idx >= previous);
            print_gap(previous, idx);
            debug("%d ", row->row_entries[j]);
            previous = idx + 1;
        }
        print_gap(previous, matrix->size);

        debug("\n");
    }
    debug("\n");
}

void free_matrix(matrix_t* matrix) {
    for (uint32_t i = 0; i < matrix->size; ++i) {
        free(matrix->rows[i].row_entries);
        free(matrix->rows[i].row_indices);
        free(matrix->rows[i].double_row_entries);
    }
    free(matrix->rows);
    free(matrix);
}

// Checks that the matrix contains only variables with indices less than its
// size. Otherwise than means that there are less equations than variables. Also
// verifies that the indices of each row are sorted. Other malformations of the
// system can be caught when the index reduction fail.
return_t is_sane(matrix_t* sig_mat) {
    for (uint32_t i = 0; i < sig_mat->size; ++i) {
        row_t*   row         = &sig_mat->rows[i];
        uint32_t prev_index  = 0;
        bool     first_index = true;
        for (uint32_t j = 0; j < row->row_size; ++j) {
            uint32_t current_index = row->row_indices[j];
            if (current_index >= sig_mat->size) {
                debugln("The index of the variable %d is larger or equal the "
                        "size of the matrix %d\n",
                        current_index, sig_mat->size);
                return CATASTROPHIC_ERROR;
            }
            if (!first_index && prev_index > current_index) {
                debugln("Information on variable %d was stored before %d\n",
                        current_index, prev_index);
                return CATASTROPHIC_ERROR;
            }
            first_index = false;
            prev_index  = current_index;
        }
    }

    return SUCCESS;
}

/*
#define N1 (3)

uint32_t idx1[2] = { 0, 2 };
uint8_t entries1[2] = { 2, 0 };
double double_entries1[2] = { 2.0, 0.0 };

const row_t row1 = {
    .row_size = 2,
    .row_indices = idx1,
    .row_entries = entries1,
    .double_row_entries = double_entries1,
};

uint32_t idx2[2] = { 1, 2 };
uint8_t entries2[2] = { 2, 0 };
double double_entries2[2] = { 2.0, 0.0 };

const row_t row2 = {
    .row_size = 2,
    .row_indices = idx2,
    .row_entries = entries2,
    .double_row_entries = double_entries2,
};

uint32_t idx3[2] = { 0, 1 };
uint8_t entries3[2] = { 0, 0 };
double double_entries3[2] = { 0.0, 0.0 };

const row_t row3 = {
    .row_size = 2,
    .row_indices = idx3,
    .row_entries = entries3,
    .double_row_entries = double_entries3,
};

row_t rows[3] = { row1, row2, row3 };

matrix_t test_matrix1 = {
    .size = 3,
    .rows = rows,
};

matrix_t* test_matrix1_addr = &test_matrix1;
*/
/*
uint32_t idx1[3] = { 0, 1, 7 };
uint8_t entries1[3] = { 1, 0, 0 };
double double_entries1[3] = { 1.0, 0.0, 0.0 };

const row_t row1 = {
    .row_size = 3,
    .row_indices = idx1,
    .row_entries = entries1,
    .double_row_entries = double_entries1,
};

uint32_t idx2[5] = { 0, 1, 2, 3, 7 };
uint8_t entries2[5] = { 0, 1, 0, 0, 0, };
double double_entries2[5] = { 0.0, 1.0, 0.0, 0.0, 0.0 };

const row_t row2 = {
    .row_size = 5,
    .row_indices = idx2,
    .row_entries = entries2,
    .double_row_entries = double_entries2,
};

uint32_t idx3[5] = { 1, 2, 4, 6, 7 };
uint8_t entries3[5] = { 0, 1, 0, 0, 0 };
double double_entries3[5] = { 0.0, 1.0, 0.0, 0.0, 0.0 };

const row_t row3 = {
    .row_size = 5,
    .row_indices = idx3,
    .row_entries = entries3,
    .double_row_entries = double_entries3,
};

uint32_t idx4[4] = { 1, 3, 4, 7 };
uint8_t entries4[4] = { 0, 1, 0, 0 };
double double_entries4[4] = { 0.0, 1.0, 0.0, 0.0 };

const row_t row4 = {
    .row_size = 4,
    .row_indices = idx4,
    .row_entries = entries4,
    .double_row_entries = double_entries4,
};

uint32_t idx5[5] = { 2, 3, 4, 5, 7 };
uint8_t entries5[5] = { 0, 0, 1, 0, 0 };
double double_entries5[5] = { 0.0, 0.0, 1.0, 0.0, 0.0 };

const row_t row5 = {
    .row_size = 5,
    .row_indices = idx5,
    .row_entries = entries5,
    .double_row_entries = double_entries5,
};

uint32_t idx6[3] = { 4, 5, 6 };
uint8_t entries6[3] = { 0, 1, 0 };
double double_entries6[3] = { 0.0, 1.0, 0.0, };

const row_t row6 = {
    .row_size = 3,
    .row_indices = idx6,
    .row_entries = entries6,
    .double_row_entries = double_entries6,
};

uint32_t idx7[3] = { 2, 5, 6 };
uint8_t entries7[3] = { 0, 0, 1 };
double double_entries7[3] = { 0.0, 0.0, 1.0, };

const row_t row7 = {
    .row_size = 3,
    .row_indices = idx7,
    .row_entries = entries7,
    .double_row_entries = double_entries7,
};

uint32_t idx8[1] = { 6 };
uint8_t entries8[1] = { 0 };
double double_entries8[1] = { 0.0 };

const row_t row8 = {
    .row_size = 1,
    .row_indices = idx8,
    .row_entries = entries8,
    .double_row_entries = double_entries8,
};

row_t rows[8] = { row1, row2, row3, row4, row5, row6, row7, row8 };

matrix_t test_matrix1 = {
    .size = 8,
    .rows = rows,
};

matrix_t* test_matrix1_addr = &test_matrix1;
*/