#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "debug.h"
#include "dummy_selection.h"
#include "index_reduction.h"
#include "sundials_main.h"

static void free_simulator(simulator_t* simulator) {
    free(simulator->reinit_signals);
    free(simulator->signal_names);

    free(simulator->input_signals_slab);
    free(simulator->input_signals);
    free(simulator->workspace);

    free(simulator->rows);
    free(simulator->row_indices);
    free(simulator->row_entries);
    free(simulator->row_double_entries);
    free(simulator->row_linearity_info_entries);
    free(simulator->equation_hod_array);
    free(simulator->variable_hod_array);
    free(simulator->state_variables);

    free(simulator->old_variable_hod_array);
    free(simulator->old_input_signals_slab);
    free(simulator->old_input_signals);

    free(simulator->root_dir);
    free(simulator->root_info);
}

#define MUST_REALLOCATE(allocated, count)                                      \
    (allocated < count || count < 2 * allocated)

static void simulator_reallocate_variables(simulator_t* simulator) {
    if (MUST_REALLOCATE(simulator->allocated_variables,
                        simulator->variable_count)) {

        debugln("Reallocate variable collections");

        simulator->allocated_variables = simulator->variable_count;

        simulator->reinit_signals =
            realloc((void*)simulator->reinit_signals,
                    simulator->variable_count * sizeof(bool));
        simulator->signal_names =
            realloc((void*)simulator->signal_names,
                    simulator->variable_count * sizeof(char*));
        simulator->input_signals =
            realloc((void*)simulator->input_signals,
                    simulator->variable_count * sizeof(double*));
        simulator->equation_hod_array =
            realloc((void*)simulator->equation_hod_array,
                    simulator->relations_count * sizeof(uint64_t));
        simulator->variable_hod_array =
            realloc((void*)simulator->variable_hod_array,
                    simulator->variable_count * sizeof(uint64_t));
        simulator->state_variables =
            realloc((void*)simulator->state_variables,
                    simulator->variable_count * sizeof(uint64_t));
        simulator->rows = realloc((void*)simulator->rows,
                                  simulator->relations_count * sizeof(row_t));
    }

    if (MUST_REALLOCATE(simulator->allocated_entries,
                        simulator->entries_count)) {
        debugln("Reallocating entries collections");

        simulator->allocated_entries = simulator->entries_count;
        simulator->row_indices =
            realloc((void*)simulator->row_indices,
                    simulator->entries_count * sizeof(uint32_t));
        simulator->row_entries =
            realloc((void*)simulator->row_entries,
                    simulator->entries_count * sizeof(int8_t));
        simulator->row_double_entries =
            realloc((void*)simulator->row_double_entries,
                    simulator->entries_count * sizeof(double));
        simulator->row_linearity_info_entries =
            realloc((void*)simulator->row_linearity_info_entries,
                    simulator->entries_count * sizeof(bool));
    }

    if (MUST_REALLOCATE(simulator->allocated_roots, simulator->root_count)) {
        debugln("Reallocating roots collections");

        simulator->allocated_roots = simulator->root_count;
        simulator->root_dir        = realloc((void*)simulator->root_dir,
                                      simulator->root_count * sizeof(int));
        simulator->root_info       = realloc((void*)simulator->root_info,
                                       simulator->root_count * sizeof(int));
    }
}

return_t solve_main(simulator_t* simulator, model_t* model, double t_start,
                    double t_end, double t_step) {

    methods_t* methods = model->methods;
    return_t   ret;
    uint64_t   next_var;

    // Record the time reached by the simulation
    double t_at = t_start;

    // When the simulator starts, we must start by constructing the
    // model state by using the constructor method
    debugln("First activation");

    next_var = 0;
    ret      = methods->constructor(
        model->environment, &next_var, &simulator->model_state,
        &simulator->variable_count, &simulator->relations_count,
        &simulator->init_relations_count, &simulator->entries_count,
        &simulator->root_count);

    // We then enter the main solving loop. The solver will run until t_at
    // is at least t_end
    while (t_at < t_end) {

        // At this point, we have gathered some information about the model
        // state we are working with. In particular, we know the values of
        // the counts of variables, relations, entries and roots. They will
        // have either been set by the constructor method just above or by
        // the event handler in a previous round of the loop.

        debugln("Variable count        %d", simulator->variable_count);
        debugln("Init relations count  %d", simulator->init_relations_count);
        debugln("Relations count       %d", simulator->relations_count);
        debugln("Entries count         %d", simulator->entries_count);
        debugln("Root count            %d", simulator->root_count);

        // The variable count must be equal to the relation count
        if (simulator->variable_count != simulator->relations_count) {
            fprintf(
                stderr,
                "The system is not balanced. There are %lu equations for %lu "
                "variables\n",
                simulator->relations_count, simulator->variable_count);
            return CATASTROPHIC_ERROR;
        }

        // We can now allocate (or reallocate) the arrays whose length depends
        // on the values above
        simulator_reallocate_variables(simulator);

        // If there has been any structural change, we must perform
        // index-reduction and state selection. We must also readjust and copy
        // over the old inputs values into a new input vector
        if (simulator->structural_change) {

            debugln("Handling structural changes");

            // Start by setting the signals info (whether signals have been
            // reinitialised, and there names)
            debugln("Set signal informations");

            next_var = 0;
            ret      = methods->set_signal_info(
                model->environment, simulator->model_state,
                simulator->first_activation, &next_var,
                simulator->reinit_signals, simulator->signal_names);
            CHECK_RET(ret);

            debugln("Computing the signature matrix\n");

            next_var                    = 0;
            row_t*    next_row          = simulator->rows;
            uint32_t* next_row_index    = simulator->row_indices;
            int8_t*   next_row_entry    = simulator->row_entries;
            double*   next_double_entry = simulator->row_double_entries;
            bool*     next_linearity_info_entry =
                simulator->row_linearity_info_entries;
            ret = methods->signature(model->environment, simulator->model_state,
                                     simulator->first_activation, &next_var,
                                     &next_row, &next_row_index,
                                     &next_row_entry, &next_double_entry,
                                     &next_linearity_info_entry);
            CHECK_RET(ret);

            debugln("Perform index reduction");

            matrix_t sig_mat = {.size = simulator->relations_count,
                                .rows = simulator->rows};

            ret = is_sane(&sig_mat);
            CHECK_RET(ret);

            ret = index_reduction(&sig_mat, simulator->equation_hod_array,
                                  simulator->variable_hod_array);
            CHECK_RET(ret);

            // At this point, we copy over the inputs contained in
            // simulator->old_input_signals into simulator->input_signals.
            //
            // We do this in two passes: first we compute the number of slots
            // we'll need, then we allocate one big array able to contain all
            // the slots. Then we set the pointers for each variable to point to
            // the current offset in the big block
            simulator->input_signals_slot_count = 0;
            for (uint64_t var = 0; var < simulator->variable_count; ++var) {
                simulator->input_signals_slot_count +=
                    simulator->variable_hod_array[var] + 1;
            }

            // Allocates the big block of memory that contains all inputs
            if (simulator->allocated_input_slots <
                simulator->input_signals_slot_count) {
                simulator->allocated_input_slots =
                    simulator->input_signals_slot_count;
                // Exceptionally we don't use realloc in all cases. If it's the
                // activation, all the inputs must be set to the default value
                // of 0
                if (simulator->first_activation) {
                    simulator->input_signals_slab = calloc(
                        simulator->input_signals_slot_count, sizeof(double));
                } else {
                    simulator->input_signals_slab = realloc(
                        (void*)simulator->input_signals_slab,
                        simulator->input_signals_slot_count * sizeof(double));
                }
            }

            uint64_t var_offset = 0;
            // Dill simulator->input_signals with offsets into
            // simulator->input_signals_slab
            for (uint64_t var = 0; var < simulator->variable_count; ++var) {
                simulator->input_signals[var] =
                    simulator->input_signals_slab + var_offset;
                var_offset += simulator->variable_hod_array[var] + 1;
            }

            // Then we copy over the old inputs into the new inputs.
            next_var              = 0;
            uint64_t old_next_var = 0;
            ret                   = methods->copy_inputs(
                model->environment, simulator->model_state,
                simulator->first_activation, &next_var,
                !simulator
                     ->first_activation, // if first_activation is false, the
                                         // model didn't exist and so the method
                                         // shouldn't try to touch
                                         // simulator->old_input_signals or
                                         // simulator->variable_hod_array.
                                         //
                                         // TODO: this shouldn't be needed
                                         // anymore since now all methods pass a
                                         // boolean to indicate whether the
                                         // block is active or not.
                simulator->old_variable_hod_array,
                simulator->variable_hod_array, simulator->old_input_signals,
                simulator->input_signals, &old_next_var);

            // We then use the dummy-derivatives method to know which
            // variables are actually state variables
            uint64_t* active_equations =
                malloc(simulator->relations_count * sizeof(uint64_t));
            for (uint64_t eqn = 0; eqn < simulator->relations_count; ++eqn) {
                active_equations[eqn] = simulator->equation_hod_array[eqn];
                simulator->state_variables[eqn] =
                    simulator->variable_hod_array[eqn];
            }
            bool success = select_dummy(&sig_mat, active_equations,
                                        simulator->state_variables);
            free(active_equations);
            if (!success) {
                return INDEX_REDUCTION_FAILED;
            }
            debugln("State variables");
            for (uint64_t var = 0; var < simulator->variable_count; ++var) {
                debugln("Var %lu: %lu (%s)", var,
                        simulator->state_variables[var],
                        simulator->reinit_signals[var] ? "reinit" : "cont");
            }

            // We must compute the required workspace size for the residual
            //
            next_var                   = 0;
            uint64_t workspace_request = 0;
            uint64_t equation_count    = 0;

            debugln("Compute workspace size");
            ret = methods->workspace_size(
                model->environment, simulator->model_state,
                simulator->first_activation, &next_var,
                simulator->equation_hod_array, &equation_count,
                &workspace_request);

            debugln("Workspace request: %d", workspace_request);

            if (simulator->allocated_workspace < workspace_request) {

                debugln("Reallocating workspace (size was %d)",
                        simulator->allocated_workspace);

                simulator->allocated_workspace = workspace_request;
                simulator->workspace           = realloc(
                    simulator->workspace, workspace_request * sizeof(double));
            }
        }

        if (simulator->any_change) {
            debugln("Initialising the system");

            ret = sundials_init(simulator, model);
            CHECK_RET(ret);
        }

        debugln("Starting continuous simulation");

        // The system has been reduced, we can go about computing the initial
        // conditions
        ret = sundials_solve(simulator, model, t_at, t_end, t_step, &t_at);
        debugln("Time reached: %f", t_at);

        switch (ret) {
        case SUCCESS:
            return SUCCESS;
        case ROOT_RET: {
            // We've encountered a root. The first thing we must do is copy over
            // the variable_hod_array and input_signals over to
            // old_variable_hod_array and old_input_signals. Then we can call
            // the event handler routine to compute the new model state and loop
            // around
            if (simulator->allocated_old_variables <
                simulator->variable_count) {
                simulator->allocated_old_variables = simulator->variable_count;

                simulator->old_variable_hod_array = realloc(
                    (void*)simulator->old_variable_hod_array,
                    simulator->allocated_old_variables * sizeof(uint64_t));

                if (simulator->allocated_old_input_signals_slots <
                    simulator->input_signals_slot_count) {
                    simulator->allocated_old_input_signals_slots =
                        simulator->input_signals_slot_count;
                    simulator->old_input_signals_slab =
                        realloc((void*)simulator->old_input_signals_slab,
                                simulator->allocated_old_input_signals_slots *
                                    sizeof(double));
                }
                simulator->old_input_signals = realloc(
                    (void*)simulator->old_input_signals,
                    simulator->allocated_old_variables * sizeof(double*));
            }

            // Fill simulator->old_input_signals correctly. First set
            // simulator->old_input_signals[var] to the right offset in
            // simulator->old_input_signals_slab and then fill this part of the
            // array with the values from simulator->input_signals
            uint64_t var_offset = 0;
            for (uint64_t var = 0; var < simulator->variable_count; ++var) {
                simulator->old_input_signals[var] =
                    simulator->old_input_signals_slab + var_offset;
                var_offset += simulator->variable_hod_array[var] + 1;
                simulator->old_variable_hod_array[var] =
                    simulator->variable_hod_array[var];
                for (uint64_t n = 0; n < simulator->variable_hod_array[var];
                     ++n) {
                    simulator->old_input_signals[var][n] =
                        simulator->input_signals[var][n];
                }
            }

            // We can now call the event handling method
            next_var                        = 0;
            void*    model_state_storage    = NULL;
            int32_t* next_root              = simulator->root_info;
            simulator->variable_count       = 0;
            simulator->relations_count      = 0;
            simulator->init_relations_count = 0;
            simulator->entries_count        = 0;
            simulator->root_count           = 0;
            methods->event_handling(
                model->environment, simulator->model_state,
                simulator->first_activation, &next_var,
                simulator->input_signals, &model_state_storage, &next_root,
                &simulator->any_change, &simulator->structural_change,
                &simulator->variable_count, &simulator->relations_count,
                &simulator->init_relations_count, &simulator->entries_count,
                &simulator->root_count);
            // This is not our first activation anymore
            simulator->first_activation = false;
            break;
        }
        default:
            return SOLVER_ERROR;
        }
    }

    return SUCCESS;
}

model_t* HYDRA_MODEL();

int main(void) {
    simulator_t simulator = {
        .first_activation  = true,
        .any_change        = true,
        .structural_change = true,
        .model_state       = NULL,

        .allocated_variables = 0,
        .allocated_entries   = 0,
        .allocated_roots     = 0,

        .reinit_signals = NULL,
        .signal_names   = NULL,

        .variable_count       = 0,
        .init_relations_count = 0,
        .relations_count      = 0,
        .entries_count        = 0,
        .root_count           = 0,

        .allocated_input_slots    = 0,
        .input_signals_slot_count = 0,
        .input_signals_slab       = NULL,
        .input_signals            = NULL,

        .allocated_workspace = 0,
        .workspace           = NULL,

        .rows = NULL,

        .row_indices                = NULL,
        .row_entries                = NULL,
        .row_double_entries         = NULL,
        .row_linearity_info_entries = NULL,

        .equation_hod_array = NULL,
        .variable_hod_array = NULL,
        .state_variables    = NULL,

        .allocated_old_variables           = 0,
        .old_variable_hod_array            = NULL,
        .allocated_old_input_signals_slots = 0,
        .old_input_signals_slab            = NULL,
        .old_input_signals                 = NULL,

        .root_dir  = NULL,
        .root_info = NULL,
    };
    debug("Started simulator. Evaluating the Hydra model\n");
    model_t* model = HYDRA_MODEL();
    debug("Hydra model located at %x\n", model);
    return_t ret = solve_main(&simulator, model, 0.0, 2, 0.01);
    free_simulator(&simulator);
    switch (ret) {
    case SUCCESS: {
        fprintf(stderr, "Successful simulation\n");
        return 0;
    }
    default: {
        fprintf(stderr, "%s\n", error_code_to_string(ret));
        return (int)ret;
    }
    }
}