	.text
	.file	"<string>"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3               # -- Begin function __sdebl_intrinsic_sgn
.LCPI0_0:
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.text
	.p2align	4, 0x90
	.type	.L__sdebl_intrinsic_sgn,@function
.L__sdebl_intrinsic_sgn:                # @__sdebl_intrinsic_sgn
	.cfi_startproc
# %bb.0:
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB0_1
	jp	.LBB0_1
# %bb.2:                                # %if.exit_0
	movaps	%xmm1, %xmm0
	retq
.LBB0_1:                                # %if.else_0
	xorps	%xmm1, %xmm1
	xorl	%eax, %eax
	ucomisd	%xmm1, %xmm0
	seta	%al
	movsd	.LCPI0_0(,%rax,8), %xmm1 # xmm1 = mem[0],zero
	movaps	%xmm1, %xmm0
	retq
.Lfunc_end0:
	.size	.L__sdebl_intrinsic_sgn, .Lfunc_end0-.L__sdebl_intrinsic_sgn
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function pi_396_fun
.LCPI1_0:
	.quad	4614256656431372362     # double 3.1415926000000001
	.text
	.globl	pi_396_fun
	.p2align	4, 0x90
	.type	pi_396_fun,@function
pi_396_fun:                             # @pi_396_fun
	.cfi_startproc
# %bb.0:
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	retq
.Lfunc_end1:
	.size	pi_396_fun, .Lfunc_end1-pi_396_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_397 # -- Begin function __anon_obj_constructor_397
	.p2align	4, 0x90
	.type	__anon_obj_constructor_397,@function
__anon_obj_constructor_397:             # @__anon_obj_constructor_397
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rcx, %rdi
	movq	%rdx, %r10
	movq	%rsi, %r15
	movq	104(%rsp), %rcx
	movq	96(%rsp), %rdx
	movq	(%rsi), %r11
	movq	(%rdi), %rsi
	movq	(%r8), %rbp
	movq	(%r9), %r12
	movq	(%rdx), %r14
	movq	(%rcx), %rbx
	movb	$1, %al
	testb	%al, %al
	jne	.LBB2_1
# %bb.2:                                # %if.else_0
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rbx, (%rsp)            # 8-byte Spill
	movq	%r11, %rbx
	callq	malloc
	movq	%rbx, %r11
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	96(%rsp), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	104(%rsp), %rcx
	movq	24(%rsp), %r9           # 8-byte Reload
	jmp	.LBB2_3
.LBB2_1:
	xorl	%eax, %eax
.LBB2_3:                                # %if.exit_0
	incq	%r12
	incq	%rbp
	incq	%r14
	movq	%r11, (%r15)
	movq	%rax, (%r10)
	movq	%rsi, (%rdi)
	movq	%rbp, (%r13)
	movq	%r12, (%r9)
	movq	%r14, (%rdx)
	movq	%rbx, (%rcx)
	xorl	%eax, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end2:
	.size	__anon_obj_constructor_397, .Lfunc_end2-__anon_obj_constructor_397
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_397 # -- Begin function __anon_obj_signature_397
	.p2align	4, 0x90
	.type	__anon_obj_signature_397,@function
__anon_obj_signature_397:               # @__anon_obj_signature_397
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movl	40(%rsp), %r10d
	movq	32(%rsp), %r11
	movq	(%rdx), %r14
	movq	(%rcx), %rax
	movq	(%r8), %r15
	movq	(%r9), %rbx
	movq	(%r11), %rdi
	movq	%r15, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rdi, 24(%rax)
	movl	%r10d, (%r15)
	movb	$1, (%rbx)
	movabsq	$4607182418800017408, %rsi # imm = 0x3FF0000000000000
	movq	%rsi, (%rdi)
	addq	$4, %r15
	incq	%rbx
	addq	$8, %rdi
	movl	$1, (%rax)
	addq	$32, %rax
	movq	%rax, (%rcx)
	movq	%r15, (%r8)
	movq	%rbx, (%r9)
	movq	%rdi, (%r11)
	movq	%r14, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end3:
	.size	__anon_obj_signature_397, .Lfunc_end3-__anon_obj_signature_397
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function __anon_obj_residual_397
.LCPI4_0:
	.quad	-4616189618054758400    # double -1
.LCPI4_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	__anon_obj_residual_397
	.p2align	4, 0x90
	.type	__anon_obj_residual_397,@function
__anon_obj_residual_397:                # @__anon_obj_residual_397
	.cfi_startproc
# %bb.0:
	pushq	%r14
	.cfi_def_cfa_offset 16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset %rbx, -24
	.cfi_offset %r14, -16
	movq	56(%rsp), %rsi
	movq	48(%rsp), %r10
	movq	40(%rsp), %r11
	movq	24(%rsp), %rbx
	movq	(%rdx), %r14
	movq	(%r11), %rdi
	movq	(%r10), %rax
	testb	$1, %cl
	je	.LBB4_3
# %bb.1:
	testb	$1, %r8b
	je	.LBB4_3
# %bb.2:                                # %if.then_0
	movq	(%rbx,%rsi,8), %rcx
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
.LBB4_3:                                # %if.exit_0
	movq	(%r9,%rax,8), %rcx
	cmpq	$11, %rcx
	jae	.LBB4_4
# %bb.6:                                # %assertion.success_0
	movq	(%rbx,%rsi,8), %rsi
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	addsd	.LCPI4_0(%rip), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	testq	%rcx, %rcx
	je	.LBB4_12
# %bb.7:                                # %if.else_1
	incq	%rcx
	movl	$1, %ebx
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	cmpq	%rcx, %rbx
	jb	.LBB4_9
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_11:                               # %for.step_0
                                        #   in Loop: Header=BB4_9 Depth=1
	subsd	%xmm2, %xmm1
	movsd	%xmm1, (%rdi)
	addq	$8, %rdi
	incq	%rbx
	cmpq	%rcx, %rbx
	jae	.LBB4_12
.LBB4_9:                                # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rsi,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	testq	%rbx, %rbx
	movapd	%xmm0, %xmm2
	je	.LBB4_11
# %bb.10:                               # %for.step_0
                                        #   in Loop: Header=BB4_9 Depth=1
	xorpd	%xmm2, %xmm2
	jmp	.LBB4_11
.LBB4_4:                                # %assertion.failed_0
	movl	$-2, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	retq
.LBB4_12:                               # %if.exit_1
	.cfi_def_cfa_offset 24
	incq	%rax
	movq	%rdi, (%r11)
	movq	%rax, (%r10)
	movq	%r14, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end4:
	.size	__anon_obj_residual_397, .Lfunc_end4-__anon_obj_residual_397
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_397 # -- Begin function __anon_obj_root_setup_397
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_397,@function
__anon_obj_root_setup_397:              # @__anon_obj_root_setup_397
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	__anon_obj_root_setup_397, .Lfunc_end5-__anon_obj_root_setup_397
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_397 # -- Begin function __anon_obj_compute_roots_397
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_397,@function
__anon_obj_compute_roots_397:           # @__anon_obj_compute_roots_397
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	__anon_obj_compute_roots_397, .Lfunc_end6-__anon_obj_compute_roots_397
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_397 # -- Begin function __anon_obj_event_handler_397
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_397,@function
__anon_obj_event_handler_397:           # @__anon_obj_event_handler_397
	.cfi_startproc
# %bb.0:
	movq	32(%rsp), %r8
	movq	56(%rsp), %r9
	movq	48(%rsp), %r10
	movq	40(%rsp), %r11
	movq	(%rdx), %rax
	movq	(%r11), %rcx
	movq	(%r10), %rsi
	movq	(%r9), %rdi
	incq	%rsi
	incq	(%r8)
	movq	%rcx, (%r11)
	movq	%rsi, (%r10)
	movq	%rdi, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	__anon_obj_event_handler_397, .Lfunc_end7-__anon_obj_event_handler_397
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_397 # -- Begin function __anon_obj_set_signal_info_397
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_397,@function
__anon_obj_set_signal_info_397:         # @__anon_obj_set_signal_info_397
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	__anon_obj_set_signal_info_397, .Lfunc_end8-__anon_obj_set_signal_info_397
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_397 # -- Begin function __anon_obj_work_space_397
	.p2align	4, 0x90
	.type	__anon_obj_work_space_397,@function
__anon_obj_work_space_397:              # @__anon_obj_work_space_397
	.cfi_startproc
# %bb.0:
	movq	(%rdx), %rax
	movq	(%r9), %rcx
	incq	(%r8)
	movq	%rcx, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	__anon_obj_work_space_397, .Lfunc_end9-__anon_obj_work_space_397
	.cfi_endproc
                                        # -- End function
	.globl	time_398_fun            # -- Begin function time_398_fun
	.p2align	4, 0x90
	.type	time_398_fun,@function
time_398_fun:                           # @time_398_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB10_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
.LBB10_2:                               # %if.exit_0
	movq	$.L__anon_obj_function_record_397, (%rax)
	movq	$0, 8(%rax)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end10:
	.size	time_398_fun, .Lfunc_end10-time_398_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_399 # -- Begin function __anon_obj_constructor_399
	.p2align	4, 0x90
	.type	__anon_obj_constructor_399,@function
__anon_obj_constructor_399:             # @__anon_obj_constructor_399
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	128(%rsp), %r11
	movq	104(%rsp), %rbp
	movq	96(%rsp), %rdi
	movq	(%rsi), %r13
	movq	(%rcx), %rbx
	movq	(%r8), %r12
	movq	(%r9), %r14
	movq	(%rdi), %r15
	movb	$1, %al
	testb	%al, %al
	movq	(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jne	.LBB11_4
# %bb.1:                                # %if.else_0
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%r9, %r15
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%r8, %r13
	movq	%rcx, %r14
	movq	%rsi, %rbp
	callq	malloc
	movq	128(%rsp), %r11
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movq	%r13, %r8
	movq	(%rsp), %r13            # 8-byte Reload
	movq	%r15, %r9
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	96(%rsp), %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	144(%rsp), %r10
	movq	112(%rsp), %rbp
	cmpq	%rbp, %r11
	je	.LBB11_2
.LBB11_5:                               # %if.else_1
	cmpq	%r10, %r11
	je	.LBB11_2
# %bb.7:                                # %if.else_2
	movl	$1, %r11d
	cmpq	%r10, %rbp
	je	.LBB11_3
.LBB11_8:                               # %if.else_3
	movl	$1, %ebp
	jmp	.LBB11_9
.LBB11_4:
	xorl	%eax, %eax
	movq	144(%rsp), %r10
	movq	112(%rsp), %rbp
	cmpq	%rbp, %r11
	jne	.LBB11_5
.LBB11_2:
	xorl	%r11d, %r11d
	cmpq	%r10, %rbp
	jne	.LBB11_8
.LBB11_3:
	xorl	%ebp, %ebp
.LBB11_9:                               # %if.exit_3
	movq	%r13, (%rsi)
	movq	%rax, (%rdx)
	movq	%rbx, (%rcx)
	incq	%r12
	movq	%r12, (%r8)
	movq	%r14, (%r9)
	addq	%r11, %rbp
	leaq	1(%r15,%rbp), %rax
	movq	%rax, (%rdi)
	movq	104(%rsp), %rax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	xorl	%eax, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end11:
	.size	__anon_obj_constructor_399, .Lfunc_end11-__anon_obj_constructor_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_399 # -- Begin function __anon_obj_signature_399
	.p2align	4, 0x90
	.type	__anon_obj_signature_399,@function
__anon_obj_signature_399:               # @__anon_obj_signature_399
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	96(%rsp), %r10
	movq	80(%rsp), %rbx
	movq	64(%rsp), %rax
	movq	56(%rsp), %rbp
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	(%rdx), %rdx
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	movq	(%rcx), %rdi
	movq	(%r8), %r15
	movq	%r9, -8(%rsp)           # 8-byte Spill
	movq	(%r9), %rsi
	movq	(%rbp), %r14
	movq	%r15, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%r14, 24(%rdi)
	cmpq	%rax, %rbx
	jae	.LBB12_2
# %bb.1:
	movq	%rbx, %rbp
	movq	%rax, %rbx
	cmpq	%r10, %rbx
	jb	.LBB12_4
.LBB12_5:                               # %if.else_1
	movq	%r10, %rdx
	cmpq	%rdx, %rbp
	jb	.LBB12_7
.LBB12_8:                               # %if.else_2
	movq	%rdx, %rax
	jmp	.LBB12_9
.LBB12_2:                               # %if.else_0
	movq	%rax, %rbp
	cmpq	%r10, %rbx
	jae	.LBB12_5
.LBB12_4:
	movq	%rbx, %rdx
	movq	%r10, %rbx
	cmpq	%rdx, %rbp
	jae	.LBB12_8
.LBB12_7:
	movq	%rbp, %rax
	movq	%rdx, %rbp
.LBB12_9:                               # %if.exit_2
	movl	%eax, (%r15)
	movb	$0, (%rsi)
	movq	$0, (%r14)
	leaq	4(%r15), %r12
	leaq	1(%rsi), %r13
	leaq	8(%r14), %r10
	cmpq	%rbp, %rax
	jne	.LBB12_11
# %bb.10:                               # %if.then_3
	movzbl	(%rsi), %edx
	movl	%eax, (%r15)
	movb	%dl, (%rsi)
	cvtsi2sd	%edx, %xmm0
	movsd	%xmm0, (%r14)
	movl	$1, %eax
	movq	%r15, %rdx
	movq	%rsi, %r11
	movq	%r14, %r9
	cmpq	%rbx, %rbp
	je	.LBB12_13
.LBB12_14:                              # %if.else_4
	movl	%ebx, (%r12)
	movb	$0, (%r13)
	movq	$0, (%r10)
	incl	%eax
	jmp	.LBB12_15
.LBB12_11:                              # %if.else_3
	movl	%ebp, (%r12)
	movb	$0, (%r13)
	movq	$0, (%r10)
	movl	$2, %eax
	movq	%r12, %rdx
	leaq	4(%r12), %r12
	movq	%r13, %r11
	incq	%r13
	movq	%r10, %r9
	addq	$8, %r10
	cmpq	%rbx, %rbp
	jne	.LBB12_14
.LBB12_13:                              # %if.then_4
	movzbl	(%r11), %ebx
	movl	%ebp, (%rdx)
	movb	%bl, (%r11)
	xorps	%xmm0, %xmm0
	cvtsi2sd	%ebx, %xmm0
	movsd	%xmm0, (%r9)
.LBB12_15:                              # %if.exit_4
	movl	%eax, (%rdi)
	addq	$32, %rdi
	movl	%eax, %eax
	leaq	(%r15,%rax,4), %rdx
	addq	%rax, %rsi
	leaq	(%r14,%rax,8), %rax
	movq	%rdi, (%rcx)
	movq	%rdx, (%r8)
	movq	-8(%rsp), %rcx          # 8-byte Reload
	movq	%rsi, (%rcx)
	movq	56(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	-24(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, (%rax)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end12:
	.size	__anon_obj_signature_399, .Lfunc_end12-__anon_obj_signature_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_399 # -- Begin function __anon_obj_residual_399
	.p2align	4, 0x90
	.type	__anon_obj_residual_399,@function
__anon_obj_residual_399:                # @__anon_obj_residual_399
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r12
	.cfi_def_cfa_offset 32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	.cfi_offset %rbx, -40
	.cfi_offset %r12, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	64(%rsp), %r10
	movq	(%r10), %r11
	movq	(%r9,%r11,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB13_1
# %bb.3:                                # %assertion.success_0
	movq	104(%rsp), %r12
	movq	88(%rsp), %rcx
	movq	72(%rsp), %rbx
	movq	56(%rsp), %r8
	movq	48(%rsp), %r14
	movq	40(%rsp), %rax
	movq	(%rdx), %r9
	movq	(%r8), %rdi
	movq	(%rax,%rbx,8), %r15
	movq	(%rax,%rcx,8), %rbx
	movq	(%rax,%r12,8), %rax
	movsd	(%r15), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rbx), %xmm0
	movsd	%xmm0, (%r14)
	subsd	(%rax), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	testq	%rsi, %rsi
	je	.LBB13_7
# %bb.4:                                # %if.else_0
	incq	%rsi
	movl	$1, %ecx
	cmpq	%rsi, %rcx
	jae	.LBB13_7
	.p2align	4, 0x90
.LBB13_6:                               # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, (%r14,%rcx,8)
	subsd	(%rax,%rcx,8), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB13_6
.LBB13_7:                               # %if.exit_0
	incq	%r11
	movq	%rdi, (%r8)
	movq	%r11, (%r10)
	movq	%r9, (%rdx)
	xorl	%eax, %eax
	jmp	.LBB13_2
.LBB13_1:                               # %assertion.failed_0
	movl	$-2, %eax
.LBB13_2:                               # %assertion.failed_0
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end13:
	.size	__anon_obj_residual_399, .Lfunc_end13-__anon_obj_residual_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_399 # -- Begin function __anon_obj_root_setup_399
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_399,@function
__anon_obj_root_setup_399:              # @__anon_obj_root_setup_399
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end14:
	.size	__anon_obj_root_setup_399, .Lfunc_end14-__anon_obj_root_setup_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_399 # -- Begin function __anon_obj_compute_roots_399
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_399,@function
__anon_obj_compute_roots_399:           # @__anon_obj_compute_roots_399
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	__anon_obj_compute_roots_399, .Lfunc_end15-__anon_obj_compute_roots_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_399 # -- Begin function __anon_obj_event_handler_399
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_399,@function
__anon_obj_event_handler_399:           # @__anon_obj_event_handler_399
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	144(%rsp), %r14
	movq	128(%rsp), %r12
	movq	104(%rsp), %r15
	movq	96(%rsp), %r8
	movq	88(%rsp), %rbp
	movq	80(%rsp), %rbx
	movq	72(%rsp), %rdi
	movq	64(%rsp), %rax
	movq	56(%rsp), %rcx
	movq	(%rdx), %rsi
	movq	%rsi, -8(%rsp)          # 8-byte Spill
	movq	(%r9), %r13
	movb	(%rcx), %cl
	movb	(%rax), %al
	cmpq	112(%rsp), %r12
	movq	(%rdi), %r11
	movq	(%rbx), %r10
	movq	(%rbp), %rbp
	movq	(%r8), %r8
	movq	(%r15), %rsi
	je	.LBB16_1
# %bb.3:                                # %if.else_0
	cmpq	%r14, %r12
	jne	.LBB16_5
.LBB16_1:
	xorl	%r12d, %r12d
	cmpq	%r14, 112(%rsp)
	jne	.LBB16_6
.LBB16_2:
	xorl	%edi, %edi
	jmp	.LBB16_7
.LBB16_5:                               # %if.else_1
	movl	$1, %r12d
	cmpq	%r14, 112(%rsp)
	je	.LBB16_2
.LBB16_6:                               # %if.else_2
	movl	$1, %edi
.LBB16_7:                               # %if.exit_2
	movq	%r13, (%r9)
	andb	$1, %cl
	movq	56(%rsp), %rbx
	movb	%cl, (%rbx)
	andb	$1, %al
	movq	64(%rsp), %rcx
	movb	%al, (%rcx)
	movq	72(%rsp), %rax
	movq	%r11, (%rax)
	incq	%r10
	movq	80(%rsp), %rax
	movq	%r10, (%rax)
	movq	88(%rsp), %rax
	movq	%rbp, (%rax)
	addq	%r12, %rdi
	leaq	1(%r8,%rdi), %rax
	movq	96(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	104(%rsp), %rax
	movq	%rsi, (%rax)
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end16:
	.size	__anon_obj_event_handler_399, .Lfunc_end16-__anon_obj_event_handler_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_399 # -- Begin function __anon_obj_set_signal_info_399
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_399,@function
__anon_obj_set_signal_info_399:         # @__anon_obj_set_signal_info_399
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end17:
	.size	__anon_obj_set_signal_info_399, .Lfunc_end17-__anon_obj_set_signal_info_399
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_399 # -- Begin function __anon_obj_work_space_399
	.p2align	4, 0x90
	.type	__anon_obj_work_space_399,@function
__anon_obj_work_space_399:              # @__anon_obj_work_space_399
	.cfi_startproc
# %bb.0:
	movq	(%rdx), %rax
	movq	(%r8), %rsi
	movq	(%r9), %rdi
	movq	(%rcx,%rsi,8), %rcx
	incq	%rcx
	cmpq	%rcx, %rdi
	cmovaq	%rdi, %rcx
	incq	%rsi
	movq	%rsi, (%r8)
	movq	%rcx, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end18:
	.size	__anon_obj_work_space_399, .Lfunc_end18-__anon_obj_work_space_399
	.cfi_endproc
                                        # -- End function
	.globl	tension_400_fun         # -- Begin function tension_400_fun
	.p2align	4, 0x90
	.type	tension_400_fun,@function
tension_400_fun:                        # @tension_400_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB19_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
.LBB19_2:                               # %if.exit_0
	movq	$.L__anon_obj_function_record_399, (%rax)
	movq	$0, 8(%rax)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end19:
	.size	tension_400_fun, .Lfunc_end19-tension_400_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_401 # -- Begin function __anon_obj_constructor_401
	.p2align	4, 0x90
	.type	__anon_obj_constructor_401,@function
__anon_obj_constructor_401:             # @__anon_obj_constructor_401
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
	.cfi_def_cfa_offset 112
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	160(%rsp), %rdi
	movq	136(%rsp), %rbx
	movq	120(%rsp), %rcx
	movq	112(%rsp), %rax
	movq	(%rsi), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	(%r13), %rbp
	movq	(%r8), %r12
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	(%r9), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movb	$1, %al
	testb	%al, %al
	movq	(%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jne	.LBB20_1
# %bb.2:                                # %if.else_0
	xorl	%edi, %edi
	callq	malloc
	movq	160(%rsp), %rdi
	cmpq	%rbx, %rdi
	jne	.LBB20_5
.LBB20_4:
	xorl	%r11d, %r11d
	jmp	.LBB20_6
.LBB20_1:
	xorl	%eax, %eax
	cmpq	%rbx, %rdi
	je	.LBB20_4
.LBB20_5:                               # %if.else_1
	movl	$1, %r11d
.LBB20_6:                               # %if.exit_1
	movq	120(%rsp), %rdx
	movq	%rdx, %r8
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdi
	movq	%rdi, %r9
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	incq	%r12
	leaq	1(%r11,%rcx), %rcx
	movq	%rdi, (%r14)
	movq	%rax, (%rsi)
	movq	%rbp, (%r13)
	movq	%r12, (%r15)
	movq	%r10, (%rdx)
	movq	%rcx, (%r9)
	movq	%rbx, (%r8)
	xorl	%eax, %eax
	addq	$56, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end20:
	.size	__anon_obj_constructor_401, .Lfunc_end20-__anon_obj_constructor_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_401 # -- Begin function __anon_obj_signature_401
	.p2align	4, 0x90
	.type	__anon_obj_signature_401,@function
__anon_obj_signature_401:               # @__anon_obj_signature_401
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r13
	.cfi_def_cfa_offset 32
	pushq	%r12
	.cfi_def_cfa_offset 40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -48
	.cfi_offset %r12, -40
	.cfi_offset %r13, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	88(%rsp), %rbx
	movq	64(%rsp), %r12
	movq	48(%rsp), %r10
	movq	(%rdx), %r11
	movq	(%rcx), %rdi
	movq	(%r8), %r15
	movq	(%r9), %rsi
	movq	(%r10), %r14
	movq	%r15, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%r14, 24(%rdi)
	cmpq	%r12, %rbx
	jae	.LBB21_2
# %bb.1:
	movq	%rbx, %rax
	movq	%r12, %rbx
	jmp	.LBB21_3
.LBB21_2:                               # %if.else_0
	movq	%r12, %rax
.LBB21_3:                               # %if.exit_0
	movl	%eax, (%r15)
	movb	$0, (%rsi)
	movq	$0, (%r14)
	cmpq	%rbx, %rax
	jne	.LBB21_5
# %bb.4:                                # %if.then_1
	movzbl	(%rsi), %ebx
	movl	%eax, (%r15)
	movb	%bl, (%rsi)
	cvtsi2sd	%ebx, %xmm0
	movsd	%xmm0, (%r14)
	movl	$1, %eax
	jmp	.LBB21_6
.LBB21_5:                               # %if.else_1
	leaq	4(%r15), %rax
	leaq	1(%rsi), %r12
	leaq	8(%r14), %r13
	movl	%ebx, (%rax)
	movb	$0, (%r12)
	movq	$0, (%r13)
	movl	$2, %eax
.LBB21_6:                               # %if.exit_1
	movl	%eax, (%rdi)
	addq	$32, %rdi
	movl	%eax, %eax
	leaq	(%r15,%rax,4), %rbx
	addq	%rax, %rsi
	leaq	(%r14,%rax,8), %rax
	movq	%rdi, (%rcx)
	movq	%rbx, (%r8)
	movq	%rsi, (%r9)
	movq	%rax, (%r10)
	movq	%r11, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end21:
	.size	__anon_obj_signature_401, .Lfunc_end21-__anon_obj_signature_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_401 # -- Begin function __anon_obj_residual_401
	.p2align	4, 0x90
	.type	__anon_obj_residual_401,@function
__anon_obj_residual_401:                # @__anon_obj_residual_401
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset %rbx, -16
	movq	40(%rsp), %r10
	movq	(%r10), %r11
	movq	(%r9,%r11,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB22_6
# %bb.1:                                # %assertion.success_0
	movq	80(%rsp), %rax
	movq	56(%rsp), %rcx
	movq	32(%rsp), %r8
	movq	16(%rsp), %rbx
	movq	(%rdx), %r9
	movq	(%r8), %rdi
	movq	(%rbx,%rax,8), %rax
	movq	(%rbx,%rcx,8), %rcx
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rcx), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	testq	%rsi, %rsi
	je	.LBB22_5
# %bb.2:                                # %if.else_0
	incq	%rsi
	movl	$1, %ebx
	cmpq	%rsi, %rbx
	jae	.LBB22_5
	.p2align	4, 0x90
.LBB22_4:                               # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rcx,%rbx,8), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	incq	%rbx
	cmpq	%rsi, %rbx
	jb	.LBB22_4
.LBB22_5:                               # %if.exit_0
	incq	%r11
	movq	%rdi, (%r8)
	movq	%r11, (%r10)
	movq	%r9, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.LBB22_6:                               # %assertion.failed_0
	.cfi_def_cfa_offset 16
	movl	$-2, %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end22:
	.size	__anon_obj_residual_401, .Lfunc_end22-__anon_obj_residual_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_401 # -- Begin function __anon_obj_root_setup_401
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_401,@function
__anon_obj_root_setup_401:              # @__anon_obj_root_setup_401
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	__anon_obj_root_setup_401, .Lfunc_end23-__anon_obj_root_setup_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_401 # -- Begin function __anon_obj_compute_roots_401
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_401,@function
__anon_obj_compute_roots_401:           # @__anon_obj_compute_roots_401
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end24:
	.size	__anon_obj_compute_roots_401, .Lfunc_end24-__anon_obj_compute_roots_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_401 # -- Begin function __anon_obj_event_handler_401
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_401,@function
__anon_obj_event_handler_401:           # @__anon_obj_event_handler_401
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	104(%rsp), %r14
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rbp
	movq	80(%rsp), %rcx
	movq	72(%rsp), %rdi
	movq	64(%rsp), %r12
	movq	56(%rsp), %r13
	movq	144(%rsp), %r8
	movq	(%rdx), %r15
	movq	(%r9), %r11
	movb	(%r13), %al
	movb	(%r12), %bl
	movq	(%rdi), %rdi
	movq	(%rcx), %rcx
	cmpq	120(%rsp), %r8
	movq	(%rbp), %r8
	movq	(%rsi), %r10
	movq	(%r14), %rbp
	jne	.LBB25_2
# %bb.1:
	xorl	%r14d, %r14d
	jmp	.LBB25_3
.LBB25_2:                               # %if.else_0
	movl	$1, %r14d
.LBB25_3:                               # %if.exit_0
	movq	%r11, (%r9)
	andb	$1, %al
	movb	%al, (%r13)
	andb	$1, %bl
	movb	%bl, (%r12)
	movq	72(%rsp), %rax
	movq	%rdi, (%rax)
	incq	%rcx
	movq	80(%rsp), %rax
	movq	%rcx, (%rax)
	movq	88(%rsp), %rax
	movq	%r8, (%rax)
	leaq	1(%r14,%r10), %rax
	movq	96(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	104(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%r15, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end25:
	.size	__anon_obj_event_handler_401, .Lfunc_end25-__anon_obj_event_handler_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_401 # -- Begin function __anon_obj_set_signal_info_401
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_401,@function
__anon_obj_set_signal_info_401:         # @__anon_obj_set_signal_info_401
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end26:
	.size	__anon_obj_set_signal_info_401, .Lfunc_end26-__anon_obj_set_signal_info_401
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_401 # -- Begin function __anon_obj_work_space_401
	.p2align	4, 0x90
	.type	__anon_obj_work_space_401,@function
__anon_obj_work_space_401:              # @__anon_obj_work_space_401
	.cfi_startproc
# %bb.0:
	movq	(%rdx), %rax
	movq	(%r9), %rcx
	incq	(%r8)
	movq	%rcx, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end27:
	.size	__anon_obj_work_space_401, .Lfunc_end27-__anon_obj_work_space_401
	.cfi_endproc
                                        # -- End function
	.globl	intensity_402_fun       # -- Begin function intensity_402_fun
	.p2align	4, 0x90
	.type	intensity_402_fun,@function
intensity_402_fun:                      # @intensity_402_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB28_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
.LBB28_2:                               # %if.exit_0
	movq	$.L__anon_obj_function_record_401, (%rax)
	movq	$0, 8(%rax)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end28:
	.size	intensity_402_fun, .Lfunc_end28-intensity_402_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_403 # -- Begin function __anon_obj_constructor_403
	.p2align	4, 0x90
	.type	__anon_obj_constructor_403,@function
__anon_obj_constructor_403:             # @__anon_obj_constructor_403
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
	.cfi_def_cfa_offset 112
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	152(%rsp), %rdi
	movq	136(%rsp), %rbx
	movq	120(%rsp), %rcx
	movq	112(%rsp), %rax
	movq	(%rsi), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	(%r13), %rbp
	movq	(%r8), %r12
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	(%r9), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movb	$1, %al
	testb	%al, %al
	movq	(%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jne	.LBB29_1
# %bb.2:                                # %if.else_0
	xorl	%edi, %edi
	callq	malloc
	movq	152(%rsp), %rdi
	cmpq	%rbx, %rdi
	jne	.LBB29_5
.LBB29_4:
	xorl	%r11d, %r11d
	jmp	.LBB29_6
.LBB29_1:
	xorl	%eax, %eax
	cmpq	%rbx, %rdi
	je	.LBB29_4
.LBB29_5:                               # %if.else_1
	movl	$1, %r11d
.LBB29_6:                               # %if.exit_1
	movq	120(%rsp), %rdx
	movq	%rdx, %r8
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdi
	movq	%rdi, %r9
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	incq	%r12
	leaq	1(%r11,%rcx), %rcx
	movq	%rdi, (%r14)
	movq	%rax, (%rsi)
	movq	%rbp, (%r13)
	movq	%r12, (%r15)
	movq	%r10, (%rdx)
	movq	%rcx, (%r9)
	movq	%rbx, (%r8)
	xorl	%eax, %eax
	addq	$56, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end29:
	.size	__anon_obj_constructor_403, .Lfunc_end29-__anon_obj_constructor_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_403 # -- Begin function __anon_obj_signature_403
	.p2align	4, 0x90
	.type	__anon_obj_signature_403,@function
__anon_obj_signature_403:               # @__anon_obj_signature_403
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r13
	.cfi_def_cfa_offset 32
	pushq	%r12
	.cfi_def_cfa_offset 40
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset %rbx, -48
	.cfi_offset %r12, -40
	.cfi_offset %r13, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	80(%rsp), %rbx
	movq	64(%rsp), %r12
	movq	48(%rsp), %r10
	movq	(%rdx), %r11
	movq	(%rcx), %rdi
	movq	(%r8), %r15
	movq	(%r9), %rsi
	movq	(%r10), %r14
	movq	%r15, 8(%rdi)
	movq	%rsi, 16(%rdi)
	movq	%r14, 24(%rdi)
	cmpq	%r12, %rbx
	jae	.LBB30_2
# %bb.1:
	movq	%rbx, %rax
	movq	%r12, %rbx
	jmp	.LBB30_3
.LBB30_2:                               # %if.else_0
	movq	%r12, %rax
.LBB30_3:                               # %if.exit_0
	movl	%eax, (%r15)
	movb	$0, (%rsi)
	movq	$0, (%r14)
	cmpq	%rbx, %rax
	jne	.LBB30_5
# %bb.4:                                # %if.then_1
	movzbl	(%rsi), %ebx
	movl	%eax, (%r15)
	movb	%bl, (%rsi)
	cvtsi2sd	%ebx, %xmm0
	movsd	%xmm0, (%r14)
	movl	$1, %eax
	jmp	.LBB30_6
.LBB30_5:                               # %if.else_1
	leaq	4(%r15), %rax
	leaq	1(%rsi), %r12
	leaq	8(%r14), %r13
	movl	%ebx, (%rax)
	movb	$0, (%r12)
	movq	$0, (%r13)
	movl	$2, %eax
.LBB30_6:                               # %if.exit_1
	movl	%eax, (%rdi)
	addq	$32, %rdi
	movl	%eax, %eax
	leaq	(%r15,%rax,4), %rbx
	addq	%rax, %rsi
	leaq	(%r14,%rax,8), %rax
	movq	%rdi, (%rcx)
	movq	%rbx, (%r8)
	movq	%rsi, (%r9)
	movq	%rax, (%r10)
	movq	%r11, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end30:
	.size	__anon_obj_signature_403, .Lfunc_end30-__anon_obj_signature_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_403 # -- Begin function __anon_obj_residual_403
	.p2align	4, 0x90
	.type	__anon_obj_residual_403,@function
__anon_obj_residual_403:                # @__anon_obj_residual_403
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset %rbx, -16
	movq	40(%rsp), %r10
	movq	(%r10), %r11
	movq	(%r9,%r11,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB31_6
# %bb.1:                                # %assertion.success_0
	movq	72(%rsp), %rcx
	movq	56(%rsp), %rax
	movq	32(%rsp), %r8
	movq	16(%rsp), %rbx
	movq	(%rdx), %r9
	movq	(%r8), %rdi
	movq	(%rbx,%rax,8), %rax
	movq	(%rbx,%rcx,8), %rcx
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rcx), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	testq	%rsi, %rsi
	je	.LBB31_5
# %bb.2:                                # %if.else_0
	incq	%rsi
	movl	$1, %ebx
	cmpq	%rsi, %rbx
	jae	.LBB31_5
	.p2align	4, 0x90
.LBB31_4:                               # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rcx,%rbx,8), %xmm0
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	incq	%rbx
	cmpq	%rsi, %rbx
	jb	.LBB31_4
.LBB31_5:                               # %if.exit_0
	incq	%r11
	movq	%rdi, (%r8)
	movq	%r11, (%r10)
	movq	%r9, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.LBB31_6:                               # %assertion.failed_0
	.cfi_def_cfa_offset 16
	movl	$-2, %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end31:
	.size	__anon_obj_residual_403, .Lfunc_end31-__anon_obj_residual_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_403 # -- Begin function __anon_obj_root_setup_403
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_403,@function
__anon_obj_root_setup_403:              # @__anon_obj_root_setup_403
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end32:
	.size	__anon_obj_root_setup_403, .Lfunc_end32-__anon_obj_root_setup_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_403 # -- Begin function __anon_obj_compute_roots_403
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_403,@function
__anon_obj_compute_roots_403:           # @__anon_obj_compute_roots_403
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end33:
	.size	__anon_obj_compute_roots_403, .Lfunc_end33-__anon_obj_compute_roots_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_403 # -- Begin function __anon_obj_event_handler_403
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_403,@function
__anon_obj_event_handler_403:           # @__anon_obj_event_handler_403
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	104(%rsp), %r14
	movq	96(%rsp), %rsi
	movq	88(%rsp), %rbp
	movq	80(%rsp), %rcx
	movq	72(%rsp), %rdi
	movq	64(%rsp), %r12
	movq	56(%rsp), %r13
	movq	136(%rsp), %r8
	movq	(%rdx), %r15
	movq	(%r9), %r11
	movb	(%r13), %al
	movb	(%r12), %bl
	movq	(%rdi), %rdi
	movq	(%rcx), %rcx
	cmpq	120(%rsp), %r8
	movq	(%rbp), %r8
	movq	(%rsi), %r10
	movq	(%r14), %rbp
	jne	.LBB34_2
# %bb.1:
	xorl	%r14d, %r14d
	jmp	.LBB34_3
.LBB34_2:                               # %if.else_0
	movl	$1, %r14d
.LBB34_3:                               # %if.exit_0
	movq	%r11, (%r9)
	andb	$1, %al
	movb	%al, (%r13)
	andb	$1, %bl
	movb	%bl, (%r12)
	movq	72(%rsp), %rax
	movq	%rdi, (%rax)
	incq	%rcx
	movq	80(%rsp), %rax
	movq	%rcx, (%rax)
	movq	88(%rsp), %rax
	movq	%r8, (%rax)
	leaq	1(%r14,%r10), %rax
	movq	96(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	104(%rsp), %rax
	movq	%rbp, (%rax)
	movq	%r15, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end34:
	.size	__anon_obj_event_handler_403, .Lfunc_end34-__anon_obj_event_handler_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_403 # -- Begin function __anon_obj_set_signal_info_403
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_403,@function
__anon_obj_set_signal_info_403:         # @__anon_obj_set_signal_info_403
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end35:
	.size	__anon_obj_set_signal_info_403, .Lfunc_end35-__anon_obj_set_signal_info_403
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_403 # -- Begin function __anon_obj_work_space_403
	.p2align	4, 0x90
	.type	__anon_obj_work_space_403,@function
__anon_obj_work_space_403:              # @__anon_obj_work_space_403
	.cfi_startproc
# %bb.0:
	movq	(%rdx), %rax
	movq	(%r9), %rcx
	incq	(%r8)
	movq	%rcx, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end36:
	.size	__anon_obj_work_space_403, .Lfunc_end36-__anon_obj_work_space_403
	.cfi_endproc
                                        # -- End function
	.globl	two_pin_404_fun         # -- Begin function two_pin_404_fun
	.p2align	4, 0x90
	.type	two_pin_404_fun,@function
two_pin_404_fun:                        # @two_pin_404_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB37_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
.LBB37_2:                               # %if.exit_0
	movq	$.L__anon_obj_function_record_403, (%rax)
	movq	$0, 8(%rax)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end37:
	.size	two_pin_404_fun, .Lfunc_end37-two_pin_404_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_405 # -- Begin function __anon_obj_constructor_405
	.p2align	4, 0x90
	.type	__anon_obj_constructor_405,@function
__anon_obj_constructor_405:             # @__anon_obj_constructor_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	152(%rsp), %r15
	movq	144(%rsp), %rbx
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	(%rsi), %r13
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	(%rcx), %rbp
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	(%r8), %rcx
	xorl	%eax, %eax
	testb	%al, %al
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	(%r9), %rdi
	movq	(%rbx), %rsi
	movq	(%r15), %rax
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	jne	.LBB38_1
# %bb.2:                                # %if.else_0
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r14
	jmp	.LBB38_3
.LBB38_1:
	xorl	%r14d, %r14d
.LBB38_3:                               # %if.exit_0
	callq	intensity_402_fun
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, (%r14)
	callq	tension_400_fun
	movq	%rax, %r12
	movq	%rax, 8(%r14)
	callq	two_pin_404_fun
	movq	%rax, 16(%r14)
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	(%rcx), %rax
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r13, (%rsi)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, (%rdx)
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rbp)
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r13)
	movq	%r13, %r9
	movq	%rdx, %rcx
	movq	%rbx, %r13
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rbx)
	movq	%rbp, %r8
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%r15)
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	184(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB38_7
# %bb.4:                                # %assertion.success_0
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	(%rsi), %rbp
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rcx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	(%r8), %rbx
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	(%r13), %r11
	movq	(%r15), %rdi
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	(%rdx), %rdi
	movq	%rdi, 24(%r14)
	leaq	1(%rbp), %r10
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rdi
	movq	8(%r12), %r12
	movq	(%rdi), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%r10, (%rsi)
	incq	%rcx
	movq	%rcx, (%rax)
	movq	%rbx, (%r8)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r9)
	movq	%r11, (%r13)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdi
	movq	%rdx, %rbx
	movq	%rax, %rcx
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*72(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB38_7
# %bb.5:                                # %assertion.success_1
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rax
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	(%r11), %rcx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	(%r8), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %r10
	movq	(%r13), %r12
	movq	(%r15), %rbp
	movq	(%rbx), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rdx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %r14
	movq	8(%rdi), %rdi
	movq	(%r14), %r14
	movq	%rdx, (%rsi)
	incq	%rcx
	movq	%rcx, (%r11)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%r8)
	movq	%r10, (%r9)
	movq	%r12, (%r13)
	movq	%rbp, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%r11, %rcx
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%r14
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB38_7
# %bb.6:                                # %assertion.success_2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r11
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r10
	movq	(%r13), %rdi
	movq	(%r15), %r9
	movq	(%rbx), %r12
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r8, 32(%r14)
	movq	%r12, 40(%r14)
	movq	%r11, (%rax)
	movq	%r14, (%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rbp)
	incq	%rdx
	movq	%rdx, (%rsi)
	movq	%r10, (%rcx)
	addq	$2, %rdi
	movq	%rdi, (%r13)
	movq	%r9, (%r15)
	xorl	%eax, %eax
.LBB38_7:                               # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end38:
	.size	__anon_obj_constructor_405, .Lfunc_end38-__anon_obj_constructor_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_405 # -- Begin function __anon_obj_signature_405
	.p2align	4, 0x90
	.type	__anon_obj_signature_405,@function
__anon_obj_signature_405:               # @__anon_obj_signature_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%r8, %r12
	movq	%r9, 32(%rsp)           # 8-byte Spill
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*8(%rbp)
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB39_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	(%r12), %r11
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	96(%rsp), %r12
	movq	(%r12), %r10
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	32(%rbx), %rsi
	movq	(%r14), %rcx
	movq	8(%r14), %rdi
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%r11, (%r8)
	movq	%rdx, (%r9)
	movq	%r10, (%r12)
	movq	%r12, %rax
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r8, %r12
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	136(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*56(%rsp)               # 8-byte Folded Reload
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB39_4
# %bb.2:                                # %assertion.success_1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	(%r13), %r8
	movq	(%r12), %rcx
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	96(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %r11
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	8(%rbx), %rbx
	movq	%rax, (%r15)
	movq	%r8, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%r9)
	movq	%r11, (%r10)
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB39_4
# %bb.3:                                # %assertion.success_2
	movq	(%r13), %rax
	movq	(%r12), %rcx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	96(%rsp), %rsi
	movq	%rsi, %r8
	movq	(%rsi), %rsi
	movq	(%r15), %r9
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rsi, 24(%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, (%rcx)
	movb	$0, (%rdx)
	movq	$0, (%rsi)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rcx)
	movb	$0, 1(%rdx)
	movq	$0, 8(%rsi)
	movl	$2, (%rax)
	addq	$32, %rax
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$16, %rsi
	movq	%rax, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%rdi)
	movq	%rsi, (%r8)
	movq	%r9, (%r15)
	xorl	%eax, %eax
.LBB39_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end39:
	.size	__anon_obj_signature_405, .Lfunc_end39-__anon_obj_signature_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_405 # -- Begin function __anon_obj_residual_405
	.p2align	4, 0x90
	.type	__anon_obj_residual_405,@function
__anon_obj_residual_405:                # @__anon_obj_residual_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movq	16(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %r13
	movq	8(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%rsi), %rsi
	movq	%r9, 8(%rsp)            # 8-byte Spill
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*16(%rbp)
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB40_10
# %bb.1:                                # %assertion.success_0
	movq	112(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %r8
	movq	120(%rsp), %rax
	movq	(%rax), %rcx
	movq	%rax, %r9
	movq	(%r15), %rax
	leaq	1(%rax), %rdx
	movq	32(%rbx), %rsi
	movq	(%r13), %rbp
	movq	8(%r13), %rdi
	movq	%rax, %r13
	movq	16(%rbp), %rbp
	movq	%rdx, (%r15)
	movq	%r8, (%r10)
	movq	%rcx, (%r9)
	movq	%r9, %rax
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movzbl	%r14b, %r14d
	movzbl	%r12b, %r12d
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	16(%rsp), %r9           # 8-byte Reload
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	movq	120(%rsp), %r11
	jne	.LBB40_10
# %bb.2:                                # %assertion.success_1
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	112(%rsp), %r13
	movq	(%r13), %r8
	movq	(%r11), %rcx
	movq	(%r15), %rax
	leaq	1(%rax), %rdx
	movq	40(%rbx), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	16(%rbp), %r10
	movq	%rdx, (%r15)
	movq	%r8, (%r13)
	movq	%rcx, (%r11)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	%rax, %r12
	movq	16(%rsp), %r9           # 8-byte Reload
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	%r11, %r14
	callq	*%r10
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB40_10
# %bb.3:                                # %assertion.success_2
	movq	(%r14), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx,%rax,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB40_4
# %bb.5:                                # %assertion.success_3
	movq	%rbx, %rcx
	movq	%r14, %r10
	movq	(%r13), %rdx
	movq	(%r15), %r8
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rbp,%rdi,8), %rdi
	movq	(%rbp,%r12,8), %rbp
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rbx)
	subsd	(%rbp), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB40_9
# %bb.6:                                # %if.else_0
	incq	%rsi
	movl	$1, %ebx
	cmpq	%rsi, %rbx
	jae	.LBB40_9
	.p2align	4, 0x90
.LBB40_8:                               # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	subsd	(%rbp,%rbx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rbx
	cmpq	%rsi, %rbx
	jb	.LBB40_8
.LBB40_9:                               # %if.exit_0
	incq	%rax
	movq	%rdx, (%r13)
	movq	%rax, (%r10)
	movq	%r8, (%r15)
	xorl	%eax, %eax
	jmp	.LBB40_10
.LBB40_4:                               # %assertion.failed_3
	movl	$-2, %eax
.LBB40_10:                              # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end40:
	.size	__anon_obj_residual_405, .Lfunc_end40-__anon_obj_residual_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_405 # -- Begin function __anon_obj_root_setup_405
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_405,@function
__anon_obj_root_setup_405:              # @__anon_obj_root_setup_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %r15
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*24(%rbp)
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB41_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	24(%rdx), %r10
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB41_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB41_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB41_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end41:
	.size	__anon_obj_root_setup_405, .Lfunc_end41-__anon_obj_root_setup_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_405 # -- Begin function __anon_obj_compute_roots_405
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_405,@function
__anon_obj_compute_roots_405:           # @__anon_obj_compute_roots_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %r15
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*32(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB42_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	32(%rdx), %r10
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r12, %r8
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB42_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	32(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB42_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB42_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end42:
	.size	__anon_obj_compute_roots_405, .Lfunc_end42-__anon_obj_compute_roots_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_405 # -- Begin function __anon_obj_event_handler_405
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_405,@function
__anon_obj_event_handler_405:           # @__anon_obj_event_handler_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	16(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%rsi), %rsi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r9, %r13
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*40(%rbp)
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB43_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rcx
	movq	96(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movq	104(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movq	112(%rsp), %rax
	movq	(%rax), %rbp
	movq	120(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	128(%rsp), %rax
	movq	(%rax), %r9
	movq	136(%rsp), %rax
	movq	(%rax), %r11
	movq	144(%rsp), %rax
	movq	(%rax), %r10
	movq	%r14, %rdx
	movq	%r12, %r8
	movq	(%r14), %r12
	leaq	1(%r12), %r14
	movq	32(%rbx), %rsi
	movq	(%r15), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r15
	movq	%r14, (%rdx)
	movq	%rcx, (%r13)
	movq	96(%rsp), %rax
	movb	16(%rsp), %cl           # 1-byte Reload
	movb	%cl, (%rax)
	movq	104(%rsp), %rax
	movb	8(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rax)
	incq	%rbp
	movq	112(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	120(%rsp), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	128(%rsp), %r14
	movq	%r9, (%r14)
	movq	136(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	144(%rsp), %rax
	movq	%r10, (%rax)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r13, %r9
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%r15
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB43_4
# %bb.2:                                # %assertion.success_1
	movq	(%r13), %r15
	movq	96(%rsp), %rax
	movb	(%rax), %dl
	movq	104(%rsp), %rax
	movb	(%rax), %al
	movb	%al, (%rsp)             # 1-byte Spill
	movq	112(%rsp), %rax
	movq	(%rax), %rbp
	movq	120(%rsp), %rax
	movq	(%rax), %r8
	movq	128(%rsp), %rax
	movq	(%rax), %r9
	movq	136(%rsp), %r14
	movq	(%r14), %r10
	movq	144(%rsp), %rax
	movq	(%rax), %r11
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movq	40(%rbx), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	40(%rbx), %rbx
	movq	%rcx, (%r12)
	movq	%r15, (%r13)
	movq	96(%rsp), %rcx
	movb	%dl, (%rcx)
	movq	104(%rsp), %rcx
	movb	(%rsp), %dl             # 1-byte Reload
	movb	%dl, (%rcx)
	incq	%rbp
	movq	112(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	120(%rsp), %r15
	movq	%r8, (%r15)
	movq	128(%rsp), %rcx
	movq	%r9, (%rcx)
	movq	%r10, (%r14)
	movq	144(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	%r12, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB43_4
# %bb.3:                                # %assertion.success_2
	movq	128(%rsp), %rax
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	136(%rsp), %rcx
	movq	%rcx, %rbp
	movq	(%rcx), %rcx
	movq	144(%rsp), %rdx
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	movq	(%r12), %rsi
	addq	$2, %rcx
	movq	120(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%rbx)
	movq	%rcx, (%rbp)
	movq	%rdx, (%r8)
	movq	%rsi, (%r12)
	xorl	%eax, %eax
.LBB43_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end43:
	.size	__anon_obj_event_handler_405, .Lfunc_end43-__anon_obj_event_handler_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_405 # -- Begin function __anon_obj_set_signal_info_405
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_405,@function
__anon_obj_set_signal_info_405:         # @__anon_obj_set_signal_info_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	(%rsi), %r13
	movq	8(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%r9, 8(%rsp)            # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*48(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB44_4
# %bb.1:                                # %assertion.success_0
	movq	(%r12), %rax
	leaq	1(%rax), %rbp
	movzbl	%r14b, %ecx
	andb	$1, %r14b
	movb	%r14b, (%r15,%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	$i_40_name_406, (%r9,%rax,8)
	movq	32(%rbx), %rsi
	movq	(%r13), %rdx
	movq	8(%r13), %rdi
	movq	48(%rdx), %r10
	movq	%rbp, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%r15, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB44_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movb	%r14b, (%r15,%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	$u_42_name_407, (%r9,%rax,8)
	movq	40(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %rbp
	movq	%rcx, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	%r15, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB44_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB44_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end44:
	.size	__anon_obj_set_signal_info_405, .Lfunc_end44-__anon_obj_set_signal_info_405
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_405 # -- Begin function __anon_obj_work_space_405
	.p2align	4, 0x90
	.type	__anon_obj_work_space_405,@function
__anon_obj_work_space_405:              # @__anon_obj_work_space_405
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %r12
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%rdx, %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, %r15
	movq	%r9, %r14
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*56(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB45_4
# %bb.1:                                # %assertion.success_0
	movq	(%r15), %r8
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	32(%rbx), %rsi
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	movq	56(%rax), %rax
	movq	%rdx, (%r13)
	movq	%r8, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB45_4
# %bb.2:                                # %assertion.success_1
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rdx, (%r13)
	movq	%rax, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB45_4
# %bb.3:                                # %assertion.success_2
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rdx
	movq	(%r12,%rax,8), %rsi
	incq	%rsi
	cmpq	%rsi, %rcx
	cmovaq	%rcx, %rsi
	incq	%rax
	movq	%rax, (%r15)
	movq	%rsi, (%r14)
	movq	%rdx, (%r13)
	xorl	%eax, %eax
.LBB45_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end45:
	.size	__anon_obj_work_space_405, .Lfunc_end45-__anon_obj_work_space_405
	.cfi_endproc
                                        # -- End function
	.globl	resistor_408_fun        # -- Begin function resistor_408_fun
	.p2align	4, 0x90
	.type	resistor_408_fun,@function
resistor_408_fun:                       # @resistor_408_fun
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB46_2
# %bb.1:                                # %if.else_0
	movl	$8, %edi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	malloc
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	%rax, %rbx
.LBB46_2:                               # %if.exit_0
	movsd	%xmm0, (%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB46_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB46_4:                               # %if.exit_1
	movq	$.L__anon_obj_function_record_405, (%rax)
	movq	%rbx, 8(%rax)
	addq	$16, %rsp
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end46:
	.size	resistor_408_fun, .Lfunc_end46-resistor_408_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_409 # -- Begin function __anon_obj_constructor_409
	.p2align	4, 0x90
	.type	__anon_obj_constructor_409,@function
__anon_obj_constructor_409:             # @__anon_obj_constructor_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	152(%rsp), %r10
	movq	144(%rsp), %rax
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	(%rsi), %rbx
	movq	(%rcx), %r12
	movq	(%r8), %r11
	xorl	%ebp, %ebp
	testb	%bpl, %bpl
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	(%r9), %rsi
	movq	(%rax), %rdi
	movq	(%r10), %rax
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
	movq	%r8, %r13
	jne	.LBB47_2
# %bb.1:                                # %if.else_0
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbp
.LBB47_2:                               # %if.exit_0
	callq	tension_400_fun
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, (%rbp)
	callq	two_pin_404_fun
	movq	%rax, %r14
	movq	%rax, 8(%rbp)
	callq	intensity_402_fun
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rbp)
	movq	(%r14), %rax
	movq	8(%r14), %rdi
	movq	(%rax), %r10
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, (%rcx)
	movq	%r12, (%r15)
	movq	%r13, %r12
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rbp)
	movq	%rbp, %r9
	movq	144(%rsp), %rdx
	movq	%rdx, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rdx)
	movq	152(%rsp), %r14
	movq	%rcx, %rsi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rcx
	movq	%r13, %r8
	movq	184(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB47_6
# %bb.3:                                # %assertion.success_0
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r11
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	(%r10), %rcx
	movq	(%r12), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rsi
	movq	144(%rsp), %rbx
	movq	(%rbx), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%r14), %rbx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	(%r8), %rdi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rdi, 24(%rdx)
	leaq	1(%r11), %rdi
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdx
	movq	8(%rbp), %rbp
	movq	(%rdx), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, (%rax)
	incq	%rcx
	movq	%rcx, (%r10)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r12)
	incq	%rsi
	movq	%rsi, (%r9)
	movq	144(%rsp), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rbx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	movq	%r10, %rcx
	movq	%r12, %r8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rbx
	movq	%rbx, %rbp
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*88(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB47_6
# %bb.4:                                # %assertion.success_1
	movq	%rbx, %r13
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r10
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rcx
	movq	(%r12), %rbx
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rdx
	movq	(%rdx), %r11
	movq	(%r14), %r8
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r14, %rbp
	movq	%rdi, %r14
	movq	(%r15), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leaq	1(%r10), %rdx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdx, (%rax)
	incq	%rcx
	movq	%rcx, (%r14)
	movq	%rbx, (%r12)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r9)
	movq	144(%rsp), %rcx
	movq	%r11, (%rcx)
	movq	%r8, (%rbp)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rax, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%r12, %r8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*88(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB47_6
# %bb.5:                                # %assertion.success_2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	(%r14), %r8
	movq	(%r12), %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %r10
	movq	144(%rsp), %rcx
	movq	(%rcx), %rdi
	movq	(%rbp), %r9
	movq	(%r15), %rcx
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	%r11, 32(%rsi)
	movq	%rcx, 40(%rsi)
	movq	%r13, (%rax)
	movq	%rsi, (%r15)
	movq	%r8, (%r14)
	incq	%rdx
	movq	%rdx, (%r12)
	movq	%r10, (%rbx)
	addq	$2, %rdi
	movq	144(%rsp), %rax
	movq	%rdi, (%rax)
	movq	%r9, (%rbp)
	xorl	%eax, %eax
.LBB47_6:                               # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end47:
	.size	__anon_obj_constructor_409, .Lfunc_end47-__anon_obj_constructor_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_409 # -- Begin function __anon_obj_signature_409
	.p2align	4, 0x90
	.type	__anon_obj_signature_409,@function
__anon_obj_signature_409:               # @__anon_obj_signature_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rax
	movq	16(%rsi), %r14
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%r8, %r12
	movq	%r9, 32(%rsp)           # 8-byte Spill
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*8(%rbp)
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB48_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	(%r12), %r11
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	96(%rsp), %r12
	movq	(%r12), %r10
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	32(%rbx), %rsi
	movq	(%r14), %rcx
	movq	8(%r14), %rdi
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%r11, (%r8)
	movq	%rdx, (%r9)
	movq	%r10, (%r12)
	movq	%r12, %rax
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r8, %r12
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	136(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*56(%rsp)               # 8-byte Folded Reload
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB48_4
# %bb.2:                                # %assertion.success_1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	(%r13), %r8
	movq	(%r12), %rcx
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	96(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %r11
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	8(%rbx), %rbx
	movq	%rax, (%r15)
	movq	%r8, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%r9)
	movq	%r11, (%r10)
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB48_4
# %bb.3:                                # %assertion.success_2
	movq	(%r13), %rax
	movq	(%r12), %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdx
	movq	96(%rsp), %rsi
	movq	%rsi, %r8
	movq	(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rsi, 24(%rax)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, (%rcx)
	movb	$1, (%rdx)
	movabsq	$4607182418800017408, %rbp # imm = 0x3FF0000000000000
	movq	%rbp, (%rsi)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rcx)
	movb	$0, 1(%rdx)
	movq	$0, 8(%rsi)
	movl	$2, (%rax)
	addq	$32, %rax
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$16, %rsi
	movq	%rax, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%rbx)
	movq	%rsi, (%r8)
	movq	%rdi, (%r15)
	xorl	%eax, %eax
.LBB48_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end48:
	.size	__anon_obj_signature_409, .Lfunc_end48-__anon_obj_signature_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_409 # -- Begin function __anon_obj_residual_409
	.p2align	4, 0x90
	.type	__anon_obj_residual_409,@function
__anon_obj_residual_409:                # @__anon_obj_residual_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movl	%r8d, %r14d
	movl	%ecx, %r12d
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%rsi), %rsi
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*16(%rbp)
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB49_13
# %bb.1:                                # %assertion.success_0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	96(%rsp), %rsi
	movq	112(%rsp), %r11
	movq	(%r11), %rcx
	movq	120(%rsp), %r10
	movq	(%r10), %r8
	movq	(%r15), %rdi
	leaq	1(%rdi), %rdx
	testb	$1, %r12b
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %eax
	je	.LBB49_4
# %bb.2:                                # %assertion.success_0
	testb	$1, %al
	je	.LBB49_4
# %bb.3:                                # %if.then_0
	movq	(%rsi,%rdi,8), %rsi
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
.LBB49_4:                               # %if.exit_0
	movq	%rbp, %rdi
	movq	32(%rbx), %rsi
	movq	(%rbp), %rbp
	movq	8(%rdi), %rdi
	movq	16(%rbp), %rbp
	movq	%rdx, (%r15)
	movq	%rcx, (%r11)
	movq	%r8, (%r10)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movzbl	%r12b, %r14d
	movzbl	%al, %r12d
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%r13, %r15
	movq	%r13, %r9
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB49_13
# %bb.5:                                # %assertion.success_1
	movq	112(%rsp), %r10
	movq	(%r10), %r8
	movq	120(%rsp), %rax
	movq	%rax, %r11
	movq	(%rax), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx), %r13
	leaq	1(%r13), %rax
	movq	40(%rbx), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	16(%rbp), %rbp
	movq	%rax, (%rdx)
	movq	%r8, (%r10)
	movq	%rcx, (%r11)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	%r15, %r9
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB49_13
# %bb.6:                                # %assertion.success_2
	movq	%r15, %rcx
	movq	112(%rsp), %r10
	movq	(%rsp), %r15            # 8-byte Reload
	movq	120(%rsp), %r8
	movq	(%r8), %rax
	movq	(%rcx,%rax,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB49_7
# %bb.8:                                # %assertion.success_3
	movq	%rbx, %rbp
	movq	(%r10), %rdx
	movq	(%r15), %r9
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rbx,%rcx,8), %rdi
	movq	(%rbx,%r13,8), %rbp
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movq	104(%rsp), %rcx
	movsd	%xmm0, (%rcx)
	subsd	(%rbp), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB49_12
# %bb.9:                                # %if.else_1
	incq	%rsi
	movl	$1, %ebx
	cmpq	%rsi, %rbx
	jae	.LBB49_12
	.p2align	4, 0x90
.LBB49_11:                              # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rdi,%rbx,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	subsd	(%rbp,%rbx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rbx
	cmpq	%rsi, %rbx
	jb	.LBB49_11
.LBB49_12:                              # %if.exit_1
	incq	%rax
	movq	%rdx, (%r10)
	movq	%rax, (%r8)
	movq	%r9, (%r15)
	xorl	%eax, %eax
	jmp	.LBB49_13
.LBB49_7:                               # %assertion.failed_3
	movl	$-2, %eax
.LBB49_13:                              # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end49:
	.size	__anon_obj_residual_409, .Lfunc_end49-__anon_obj_residual_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_409 # -- Begin function __anon_obj_root_setup_409
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_409,@function
__anon_obj_root_setup_409:              # @__anon_obj_root_setup_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	16(%rsi), %r14
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*24(%rbp)
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB50_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	24(%rdx), %r10
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB50_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB50_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB50_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end50:
	.size	__anon_obj_root_setup_409, .Lfunc_end50-__anon_obj_root_setup_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_409 # -- Begin function __anon_obj_compute_roots_409
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_409,@function
__anon_obj_compute_roots_409:           # @__anon_obj_compute_roots_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	16(%rsi), %r14
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*32(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB51_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	32(%rdx), %r10
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r12, %r8
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB51_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	32(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB51_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB51_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end51:
	.size	__anon_obj_compute_roots_409, .Lfunc_end51-__anon_obj_compute_roots_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_409 # -- Begin function __anon_obj_event_handler_409
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_409,@function
__anon_obj_event_handler_409:           # @__anon_obj_event_handler_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	8(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	16(%rsi), %r15
	movq	24(%rsi), %rsi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r9, %r13
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*40(%rbp)
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB52_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rcx
	movq	96(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movq	104(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movq	112(%rsp), %rax
	movq	(%rax), %rbp
	movq	120(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	128(%rsp), %rax
	movq	(%rax), %r9
	movq	136(%rsp), %rax
	movq	(%rax), %r11
	movq	144(%rsp), %rax
	movq	(%rax), %r10
	movq	%r14, %rdx
	movq	%r12, %r8
	movq	(%r14), %r12
	leaq	1(%r12), %r14
	movq	32(%rbx), %rsi
	movq	(%r15), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r15
	movq	%r14, (%rdx)
	movq	%rcx, (%r13)
	movq	96(%rsp), %rax
	movb	16(%rsp), %cl           # 1-byte Reload
	movb	%cl, (%rax)
	movq	104(%rsp), %rax
	movb	8(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rax)
	incq	%rbp
	movq	112(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	120(%rsp), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	128(%rsp), %r14
	movq	%r9, (%r14)
	movq	136(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	144(%rsp), %rax
	movq	%r10, (%rax)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r13, %r9
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%r15
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB52_4
# %bb.2:                                # %assertion.success_1
	movq	(%r13), %r15
	movq	96(%rsp), %rax
	movb	(%rax), %dl
	movq	104(%rsp), %rax
	movb	(%rax), %al
	movb	%al, (%rsp)             # 1-byte Spill
	movq	112(%rsp), %rax
	movq	(%rax), %rbp
	movq	120(%rsp), %rax
	movq	(%rax), %r8
	movq	128(%rsp), %rax
	movq	(%rax), %r9
	movq	136(%rsp), %r14
	movq	(%r14), %r10
	movq	144(%rsp), %rax
	movq	(%rax), %r11
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movq	40(%rbx), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	40(%rbx), %rbx
	movq	%rcx, (%r12)
	movq	%r15, (%r13)
	movq	96(%rsp), %rcx
	movb	%dl, (%rcx)
	movq	104(%rsp), %rcx
	movb	(%rsp), %dl             # 1-byte Reload
	movb	%dl, (%rcx)
	incq	%rbp
	movq	112(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	120(%rsp), %r15
	movq	%r8, (%r15)
	movq	128(%rsp), %rcx
	movq	%r9, (%rcx)
	movq	%r10, (%r14)
	movq	144(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	%r12, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB52_4
# %bb.3:                                # %assertion.success_2
	movq	128(%rsp), %rax
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	136(%rsp), %rcx
	movq	%rcx, %rbp
	movq	(%rcx), %rcx
	movq	144(%rsp), %rdx
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	movq	(%r12), %rsi
	addq	$2, %rcx
	movq	120(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%rbx)
	movq	%rcx, (%rbp)
	movq	%rdx, (%r8)
	movq	%rsi, (%r12)
	xorl	%eax, %eax
.LBB52_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end52:
	.size	__anon_obj_event_handler_409, .Lfunc_end52-__anon_obj_event_handler_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_409 # -- Begin function __anon_obj_set_signal_info_409
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_409,@function
__anon_obj_set_signal_info_409:         # @__anon_obj_set_signal_info_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rax
	movq	16(%rsi), %r13
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%r9, 8(%rsp)            # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*48(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB53_4
# %bb.1:                                # %assertion.success_0
	movq	(%r12), %rax
	leaq	1(%rax), %rbp
	movzbl	%r14b, %ecx
	andb	$1, %r14b
	movb	%r14b, (%r15,%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	$i_68_name_410, (%r9,%rax,8)
	movq	32(%rbx), %rsi
	movq	(%r13), %rdx
	movq	8(%r13), %rdi
	movq	48(%rdx), %r10
	movq	%rbp, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%r15, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB53_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movb	%r14b, (%r15,%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	$u_70_name_411, (%r9,%rax,8)
	movq	40(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %rbp
	movq	%rcx, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	%r15, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB53_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB53_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end53:
	.size	__anon_obj_set_signal_info_409, .Lfunc_end53-__anon_obj_set_signal_info_409
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_409 # -- Begin function __anon_obj_work_space_409
	.p2align	4, 0x90
	.type	__anon_obj_work_space_409,@function
__anon_obj_work_space_409:              # @__anon_obj_work_space_409
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rsi), %rax
	movq	16(%rsi), %r12
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%rdx, %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, %r15
	movq	%r9, %r14
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*56(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB54_4
# %bb.1:                                # %assertion.success_0
	movq	(%r15), %r8
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	32(%rbx), %rsi
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	movq	56(%rax), %rax
	movq	%rdx, (%r13)
	movq	%r8, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB54_4
# %bb.2:                                # %assertion.success_1
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rdx, (%r13)
	movq	%rax, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB54_4
# %bb.3:                                # %assertion.success_2
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rdx
	movq	(%r12,%rax,8), %rsi
	incq	%rsi
	cmpq	%rsi, %rcx
	cmovaq	%rcx, %rsi
	incq	%rax
	movq	%rax, (%r15)
	movq	%rsi, (%r14)
	movq	%rdx, (%r13)
	xorl	%eax, %eax
.LBB54_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end54:
	.size	__anon_obj_work_space_409, .Lfunc_end54-__anon_obj_work_space_409
	.cfi_endproc
                                        # -- End function
	.globl	inductance_412_fun      # -- Begin function inductance_412_fun
	.p2align	4, 0x90
	.type	inductance_412_fun,@function
inductance_412_fun:                     # @inductance_412_fun
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB55_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	malloc
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, %rbx
.LBB55_2:                               # %if.exit_0
	movsd	%xmm0, (%rbx)
	movsd	%xmm1, 8(%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB55_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB55_4:                               # %if.exit_1
	movq	$.L__anon_obj_function_record_409, (%rax)
	movq	%rbx, 8(%rax)
	addq	$16, %rsp
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end55:
	.size	inductance_412_fun, .Lfunc_end55-inductance_412_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_413 # -- Begin function __anon_obj_constructor_413
	.p2align	4, 0x90
	.type	__anon_obj_constructor_413,@function
__anon_obj_constructor_413:             # @__anon_obj_constructor_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	152(%rsp), %r10
	movq	144(%rsp), %rax
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	(%rsi), %rbx
	movq	(%rcx), %r12
	movq	(%r8), %r11
	xorl	%ebp, %ebp
	testb	%bpl, %bpl
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	(%r9), %rsi
	movq	(%rax), %rdi
	movq	(%r10), %rax
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
	movq	%r8, %r13
	jne	.LBB56_2
# %bb.1:                                # %if.else_0
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbp
.LBB56_2:                               # %if.exit_0
	callq	intensity_402_fun
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, (%rbp)
	callq	two_pin_404_fun
	movq	%rax, %r14
	movq	%rax, 8(%rbp)
	callq	tension_400_fun
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rbp)
	movq	(%r14), %rax
	movq	8(%r14), %rdi
	movq	(%rax), %r10
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, (%rcx)
	movq	%r12, (%r15)
	movq	%r13, %r12
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rbp)
	movq	%rbp, %r9
	movq	144(%rsp), %rdx
	movq	%rdx, %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rdx)
	movq	152(%rsp), %r14
	movq	%rcx, %rsi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rcx
	movq	%r13, %r8
	movq	184(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	movq	184(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB56_6
# %bb.3:                                # %assertion.success_0
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r11
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	(%r10), %rcx
	movq	(%r12), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rsi
	movq	144(%rsp), %rbx
	movq	(%rbx), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	(%r14), %rbx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	(%r8), %rdi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rdi, 24(%rdx)
	leaq	1(%r11), %rdi
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdx
	movq	8(%rbp), %rbp
	movq	(%rdx), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, (%rax)
	incq	%rcx
	movq	%rcx, (%r10)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r12)
	incq	%rsi
	movq	%rsi, (%r9)
	movq	144(%rsp), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rbx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	movq	%r10, %rcx
	movq	%r12, %r8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rbx
	movq	%rbx, %rbp
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*88(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB56_6
# %bb.4:                                # %assertion.success_1
	movq	%rbx, %r13
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r10
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rcx
	movq	(%r12), %rbx
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rdx
	movq	(%rdx), %r11
	movq	(%r14), %r8
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	%r14, %rbp
	movq	%rdi, %r14
	movq	(%r15), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leaq	1(%r10), %rdx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdx, (%rax)
	incq	%rcx
	movq	%rcx, (%r14)
	movq	%rbx, (%r12)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r9)
	movq	144(%rsp), %rcx
	movq	%r11, (%rcx)
	movq	%r8, (%rbp)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rax, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%r12, %r8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*88(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB56_6
# %bb.5:                                # %assertion.success_2
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	(%r14), %r8
	movq	(%r12), %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %r10
	movq	144(%rsp), %rcx
	movq	(%rcx), %rdi
	movq	(%rbp), %r9
	movq	(%r15), %rcx
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %r11           # 8-byte Reload
	movq	%r11, 32(%rsi)
	movq	%rcx, 40(%rsi)
	movq	%r13, (%rax)
	movq	%rsi, (%r15)
	movq	%r8, (%r14)
	incq	%rdx
	movq	%rdx, (%r12)
	movq	%r10, (%rbx)
	addq	$2, %rdi
	movq	144(%rsp), %rax
	movq	%rdi, (%rax)
	movq	%r9, (%rbp)
	xorl	%eax, %eax
.LBB56_6:                               # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end56:
	.size	__anon_obj_constructor_413, .Lfunc_end56-__anon_obj_constructor_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_413 # -- Begin function __anon_obj_signature_413
	.p2align	4, 0x90
	.type	__anon_obj_signature_413,@function
__anon_obj_signature_413:               # @__anon_obj_signature_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rax
	movq	16(%rsi), %r14
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%r8, %r12
	movq	%r9, 32(%rsp)           # 8-byte Spill
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*8(%rbp)
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB57_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	(%r12), %r11
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	96(%rsp), %r12
	movq	(%r12), %r10
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	32(%rbx), %rsi
	movq	(%r14), %rcx
	movq	8(%r14), %rdi
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%r15)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%r11, (%r8)
	movq	%rdx, (%r9)
	movq	%r10, (%r12)
	movq	%r12, %rax
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r8, %r12
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	136(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*56(%rsp)               # 8-byte Folded Reload
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB57_4
# %bb.2:                                # %assertion.success_1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	(%r13), %r8
	movq	(%r12), %rcx
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdx
	movq	96(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %r11
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	40(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	8(%rbx), %rbx
	movq	%rax, (%r15)
	movq	%r8, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%r9)
	movq	%r11, (%r10)
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB57_4
# %bb.3:                                # %assertion.success_2
	movq	(%r13), %rax
	movq	(%r12), %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdx
	movq	96(%rsp), %rsi
	movq	%rsi, %r8
	movq	(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rsi, 24(%rax)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, (%rcx)
	movb	$1, (%rdx)
	movabsq	$4607182418800017408, %rbp # imm = 0x3FF0000000000000
	movq	%rbp, (%rsi)
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 4(%rcx)
	movb	$0, 1(%rdx)
	movq	$0, 8(%rsi)
	movl	$2, (%rax)
	addq	$32, %rax
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$16, %rsi
	movq	%rax, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%rbx)
	movq	%rsi, (%r8)
	movq	%rdi, (%r15)
	xorl	%eax, %eax
.LBB57_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end57:
	.size	__anon_obj_signature_413, .Lfunc_end57-__anon_obj_signature_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_413 # -- Begin function __anon_obj_residual_413
	.p2align	4, 0x90
	.type	__anon_obj_residual_413,@function
__anon_obj_residual_413:                # @__anon_obj_residual_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movl	%r8d, %r14d
	movl	%ecx, %r12d
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movq	8(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	24(%rsi), %rsi
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*16(%rbp)
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB58_13
# %bb.1:                                # %assertion.success_0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	96(%rsp), %rsi
	movq	112(%rsp), %r11
	movq	(%r11), %rcx
	movq	120(%rsp), %r10
	movq	(%r10), %r8
	movq	(%r15), %rdi
	leaq	1(%rdi), %rdx
	testb	$1, %r12b
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%r14d, %eax
	je	.LBB58_4
# %bb.2:                                # %assertion.success_0
	testb	$1, %al
	je	.LBB58_4
# %bb.3:                                # %if.then_0
	movq	(%rsi,%rdi,8), %rsi
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
.LBB58_4:                               # %if.exit_0
	movq	%rbp, %rdi
	movq	32(%rbx), %rsi
	movq	(%rbp), %rbp
	movq	8(%rdi), %rdi
	movq	16(%rbp), %rbp
	movq	%rdx, (%r15)
	movq	%rcx, (%r11)
	movq	%r8, (%r10)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movzbl	%r12b, %r14d
	movzbl	%al, %r12d
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	%r13, %r15
	movq	%r13, %r9
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB58_13
# %bb.5:                                # %assertion.success_1
	movq	112(%rsp), %r10
	movq	(%r10), %r8
	movq	120(%rsp), %rax
	movq	%rax, %r11
	movq	(%rax), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx), %r13
	leaq	1(%r13), %rax
	movq	40(%rbx), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	16(%rbp), %rbp
	movq	%rax, (%rdx)
	movq	%r8, (%r10)
	movq	%rcx, (%r11)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	%r15, %r9
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB58_13
# %bb.6:                                # %assertion.success_2
	movq	%r15, %rcx
	movq	112(%rsp), %r10
	movq	(%rsp), %r15            # 8-byte Reload
	movq	120(%rsp), %r8
	movq	(%r8), %rax
	movq	(%rcx,%rax,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB58_7
# %bb.8:                                # %assertion.success_3
	movq	%rbx, %rbp
	movq	(%r10), %rdx
	movq	(%r15), %r9
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rbx,%rcx,8), %rdi
	movq	(%rbx,%r13,8), %rbp
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movq	104(%rsp), %rcx
	movsd	%xmm0, (%rcx)
	subsd	(%rbp), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB58_12
# %bb.9:                                # %if.else_1
	incq	%rsi
	movl	$1, %ebx
	cmpq	%rsi, %rbx
	jae	.LBB58_12
	.p2align	4, 0x90
.LBB58_11:                              # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	8(%rdi,%rbx,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, (%rcx,%rbx,8)
	subsd	(%rbp,%rbx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rbx
	cmpq	%rsi, %rbx
	jb	.LBB58_11
.LBB58_12:                              # %if.exit_1
	incq	%rax
	movq	%rdx, (%r10)
	movq	%rax, (%r8)
	movq	%r9, (%r15)
	xorl	%eax, %eax
	jmp	.LBB58_13
.LBB58_7:                               # %assertion.failed_3
	movl	$-2, %eax
.LBB58_13:                              # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end58:
	.size	__anon_obj_residual_413, .Lfunc_end58-__anon_obj_residual_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_413 # -- Begin function __anon_obj_root_setup_413
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_413,@function
__anon_obj_root_setup_413:              # @__anon_obj_root_setup_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	16(%rsi), %r14
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%r8, (%rsp)             # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*24(%rbp)
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB59_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	24(%rdx), %r10
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB59_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB59_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB59_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end59:
	.size	__anon_obj_root_setup_413, .Lfunc_end59-__anon_obj_root_setup_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_413 # -- Begin function __anon_obj_compute_roots_413
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_413,@function
__anon_obj_compute_roots_413:           # @__anon_obj_compute_roots_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	16(%rsi), %r14
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*32(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB60_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, 8(%rsp)           # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	32(%rdx), %r10
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r12, %r8
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB60_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	32(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB60_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB60_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end60:
	.size	__anon_obj_compute_roots_413, .Lfunc_end60-__anon_obj_compute_roots_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_413 # -- Begin function __anon_obj_event_handler_413
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_413,@function
__anon_obj_event_handler_413:           # @__anon_obj_event_handler_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	8(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	16(%rsi), %r15
	movq	24(%rsi), %rsi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r9, %r13
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*40(%rbp)
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB61_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rcx
	movq	96(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movq	104(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movq	112(%rsp), %rax
	movq	(%rax), %rbp
	movq	120(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	128(%rsp), %rax
	movq	(%rax), %r9
	movq	136(%rsp), %rax
	movq	(%rax), %r11
	movq	144(%rsp), %rax
	movq	(%rax), %r10
	movq	%r14, %rdx
	movq	%r12, %r8
	movq	(%r14), %r12
	leaq	1(%r12), %r14
	movq	32(%rbx), %rsi
	movq	(%r15), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %r15
	movq	%r14, (%rdx)
	movq	%rcx, (%r13)
	movq	96(%rsp), %rax
	movb	16(%rsp), %cl           # 1-byte Reload
	movb	%cl, (%rax)
	movq	104(%rsp), %rax
	movb	8(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rax)
	incq	%rbp
	movq	112(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	120(%rsp), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	128(%rsp), %r14
	movq	%r9, (%r14)
	movq	136(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	144(%rsp), %rax
	movq	%r10, (%rax)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r13, %r9
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%r15
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB61_4
# %bb.2:                                # %assertion.success_1
	movq	(%r13), %r15
	movq	96(%rsp), %rax
	movb	(%rax), %dl
	movq	104(%rsp), %rax
	movb	(%rax), %al
	movb	%al, (%rsp)             # 1-byte Spill
	movq	112(%rsp), %rax
	movq	(%rax), %rbp
	movq	120(%rsp), %rax
	movq	(%rax), %r8
	movq	128(%rsp), %rax
	movq	(%rax), %r9
	movq	136(%rsp), %r14
	movq	(%r14), %r10
	movq	144(%rsp), %rax
	movq	(%rax), %r11
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movq	40(%rbx), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	40(%rbx), %rbx
	movq	%rcx, (%r12)
	movq	%r15, (%r13)
	movq	96(%rsp), %rcx
	movb	%dl, (%rcx)
	movq	104(%rsp), %rcx
	movb	(%rsp), %dl             # 1-byte Reload
	movb	%dl, (%rcx)
	incq	%rbp
	movq	112(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	120(%rsp), %r15
	movq	%r8, (%r15)
	movq	128(%rsp), %rcx
	movq	%r9, (%rcx)
	movq	%r10, (%r14)
	movq	144(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	%r12, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB61_4
# %bb.3:                                # %assertion.success_2
	movq	128(%rsp), %rax
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	136(%rsp), %rcx
	movq	%rcx, %rbp
	movq	(%rcx), %rcx
	movq	144(%rsp), %rdx
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	movq	(%r12), %rsi
	addq	$2, %rcx
	movq	120(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%rbx)
	movq	%rcx, (%rbp)
	movq	%rdx, (%r8)
	movq	%rsi, (%r12)
	xorl	%eax, %eax
.LBB61_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end61:
	.size	__anon_obj_event_handler_413, .Lfunc_end61-__anon_obj_event_handler_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_413 # -- Begin function __anon_obj_set_signal_info_413
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_413,@function
__anon_obj_set_signal_info_413:         # @__anon_obj_set_signal_info_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rax
	movq	16(%rsi), %r13
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%r9, 8(%rsp)            # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*48(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB62_4
# %bb.1:                                # %assertion.success_0
	movq	(%r12), %rax
	leaq	1(%rax), %rbp
	movzbl	%r14b, %ecx
	andb	$1, %r14b
	movb	%r14b, (%r15,%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	$u_100_name_414, (%r9,%rax,8)
	movq	32(%rbx), %rsi
	movq	(%r13), %rdx
	movq	8(%r13), %rdi
	movq	48(%rdx), %r10
	movq	%rbp, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%r15, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB62_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movb	%r14b, (%r15,%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	$i_98_name_415, (%r9,%rax,8)
	movq	40(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %rbp
	movq	%rcx, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	%r15, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB62_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB62_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end62:
	.size	__anon_obj_set_signal_info_413, .Lfunc_end62-__anon_obj_set_signal_info_413
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_413 # -- Begin function __anon_obj_work_space_413
	.p2align	4, 0x90
	.type	__anon_obj_work_space_413,@function
__anon_obj_work_space_413:              # @__anon_obj_work_space_413
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rsi), %rax
	movq	16(%rsi), %r12
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%rdx, %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, %r15
	movq	%r9, %r14
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*56(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB63_4
# %bb.1:                                # %assertion.success_0
	movq	(%r15), %r8
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	32(%rbx), %rsi
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	movq	56(%rax), %rax
	movq	%rdx, (%r13)
	movq	%r8, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB63_4
# %bb.2:                                # %assertion.success_1
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	40(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rdx, (%r13)
	movq	%rax, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB63_4
# %bb.3:                                # %assertion.success_2
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rdx
	movq	(%r12,%rax,8), %rsi
	incq	%rsi
	cmpq	%rsi, %rcx
	cmovaq	%rcx, %rsi
	incq	%rax
	movq	%rax, (%r15)
	movq	%rsi, (%r14)
	movq	%rdx, (%r13)
	xorl	%eax, %eax
.LBB63_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end63:
	.size	__anon_obj_work_space_413, .Lfunc_end63-__anon_obj_work_space_413
	.cfi_endproc
                                        # -- End function
	.globl	capacitor_416_fun       # -- Begin function capacitor_416_fun
	.p2align	4, 0x90
	.type	capacitor_416_fun,@function
capacitor_416_fun:                      # @capacitor_416_fun
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB64_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	malloc
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, %rbx
.LBB64_2:                               # %if.exit_0
	movsd	%xmm0, (%rbx)
	movsd	%xmm1, 8(%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB64_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB64_4:                               # %if.exit_1
	movq	$.L__anon_obj_function_record_413, (%rax)
	movq	%rbx, 8(%rax)
	addq	$16, %rsp
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end64:
	.size	capacitor_416_fun, .Lfunc_end64-capacitor_416_fun
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function __anon_obj_constructor_417
.LCPI65_0:
	.quad	4609047870845172685     # double 1.4142135623730951
	.text
	.globl	__anon_obj_constructor_417
	.p2align	4, 0x90
	.type	__anon_obj_constructor_417,@function
__anon_obj_constructor_417:             # @__anon_obj_constructor_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	152(%rsp), %r10
	movq	144(%rsp), %r11
	movq	(%rsi), %rbx
	movq	(%rcx), %r14
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	(%r8), %r8
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rdi), %xmm1          # xmm1 = mem[0],zero
	xorl	%r13d, %r13d
	testb	%r13b, %r13b
	movq	(%r9), %rbp
	movq	(%r11), %rax
	movq	(%r10), %rdi
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	movq	%rsi, %r15
	movq	%rcx, %rbp
	jne	.LBB65_2
# %bb.1:                                # %if.else_0
	movl	$64, %edi
	callq	malloc
	movq	%rax, %r13
.LBB65_2:                               # %if.exit_0
	callq	time_398_fun
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, (%r13)
	callq	tension_400_fun
	movq	%rax, %r12
	movq	%rax, 8(%r13)
	callq	two_pin_404_fun
	movq	%rax, %r14
	movq	%rax, 16(%r13)
	callq	pi_396_fun
	addsd	%xmm0, %xmm0
	mulsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 24(%r13)
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI65_0(%rip), %xmm0
	movsd	%xmm0, 32(%r13)
	movq	(%r14), %rax
	movq	8(%r14), %rdi
	movq	(%rax), %r10
	movq	%r15, %r14
	movq	%rbx, (%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbp)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rcx)
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	144(%rsp), %rdx
	movq	%rdx, %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%rax)
	movq	%rcx, %r8
	movq	152(%rsp), %r15
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%r15)
	movq	%r14, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movq	%rbx, %r9
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB65_6
# %bb.3:                                # %assertion.success_0
	movq	%r12, 40(%rsp)          # 8-byte Spill
	movq	(%r14), %r10
	movq	(%rbp), %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	(%r8), %r9
	movq	(%rbx), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	144(%rsp), %r12
	movq	(%r12), %r11
	movq	(%r15), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdi
	movq	%rdi, 40(%r13)
	leaq	1(%r10), %rax
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rax, (%r14)
	incq	%rcx
	movq	%rcx, (%rbp)
	movq	%r9, (%r8)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rbx)
	movq	%r11, (%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rsi
	movq	%rbp, %rcx
	movq	%rbx, %r9
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	callq	*48(%rsp)               # 8-byte Folded Reload
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB65_6
# %bb.4:                                # %assertion.success_1
	movq	(%r14), %r10
	movq	(%rbp), %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	(%r8), %r9
	movq	(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rdx
	movq	(%rdx), %r11
	movq	(%r15), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	1(%r10), %rax
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rsi
	movq	8(%r12), %rdi
	movq	%rbp, %r12
	movq	(%rsi), %rbp
	movq	%rax, (%r14)
	incq	%rcx
	movq	%rcx, (%r12)
	movq	%r9, (%r8)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	144(%rsp), %rax
	movq	%r11, (%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rsi
	movq	%r12, %rcx
	movq	%rbx, %r9
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB65_6
# %bb.5:                                # %assertion.success_2
	movq	(%r14), %rax
	movq	(%r12), %r8
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdx
	movq	(%rbx), %r10
	movq	144(%rsp), %rcx
	movq	(%rcx), %rdi
	movq	(%r15), %r9
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rcx
	movq	(%rsp), %r11            # 8-byte Reload
	movq	%r11, 48(%r13)
	movq	%rcx, 56(%r13)
	movq	%rax, (%r14)
	movq	%r13, (%rsi)
	movq	%r8, (%r12)
	incq	%rdx
	movq	%rdx, (%rbp)
	movq	%r10, (%rbx)
	addq	$2, %rdi
	movq	144(%rsp), %rax
	movq	%rdi, (%rax)
	movq	%r9, (%r15)
	xorl	%eax, %eax
.LBB65_6:                               # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end65:
	.size	__anon_obj_constructor_417, .Lfunc_end65-__anon_obj_constructor_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_417 # -- Begin function __anon_obj_signature_417
	.p2align	4, 0x90
	.type	__anon_obj_signature_417,@function
__anon_obj_signature_417:               # @__anon_obj_signature_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	16(%rsi), %rax
	movq	40(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %r15
	movq	%rcx, %r13
	movq	%r8, %r12
	movq	%r9, 16(%rsp)           # 8-byte Spill
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*8(%rbp)
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB66_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	(%r12), %r11
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%r9), %rdx
	movq	80(%rsp), %r12
	movq	(%r12), %rbp
	movq	(%r15), %rcx
	leaq	1(%rcx), %rax
	movq	48(%rbx), %rsi
	movq	(%r14), %r10
	movq	8(%r14), %rdi
	movq	%rcx, %r14
	movq	8(%r10), %r10
	movq	%rax, (%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%r11, (%r8)
	movq	%rdx, (%r9)
	movq	%rbp, (%r12)
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r8, %rbp
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB66_4
# %bb.2:                                # %assertion.success_1
	movq	(%r13), %r8
	movq	%rbp, %r12
	movq	(%rbp), %rcx
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%r9), %rdx
	movq	80(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %r11
	movq	(%r15), %rbp
	leaq	1(%rbp), %rax
	movq	56(%rbx), %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	8(%rbx), %rbx
	movq	%rax, (%r15)
	movq	%r8, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%r9)
	movq	%r11, (%r10)
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%rbp, (%rsp)            # 8-byte Spill
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB66_4
# %bb.3:                                # %assertion.success_2
	movq	(%r13), %rax
	movq	(%r12), %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rbx), %rdx
	movq	80(%rsp), %rsi
	movq	%rsi, %r8
	movq	(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rsi, 24(%rax)
	movl	%r14d, (%rcx)
	movb	$0, (%rdx)
	movq	$0, (%rsi)
	movq	(%rsp), %rbp            # 8-byte Reload
	movl	%ebp, 4(%rcx)
	movb	$0, 1(%rdx)
	movq	$0, 8(%rsi)
	movl	$2, (%rax)
	addq	$32, %rax
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$16, %rsi
	movq	%rax, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%rbx)
	movq	%rsi, (%r8)
	movq	%rdi, (%r15)
	xorl	%eax, %eax
.LBB66_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end66:
	.size	__anon_obj_signature_417, .Lfunc_end66-__anon_obj_signature_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_417 # -- Begin function __anon_obj_residual_417
	.p2align	4, 0x90
	.type	__anon_obj_residual_417,@function
__anon_obj_residual_417:                # @__anon_obj_residual_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	152(%rsp), %r13
	movq	16(%rsi), %rax
	movsd	24(%rsi), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	32(%rsi), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	40(%rsi), %rsi
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movq	%r9, 8(%rsp)            # 8-byte Spill
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*16(%rbp)
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB67_28
# %bb.1:                                # %assertion.success_0
	movq	160(%rsp), %rax
	movq	(%rax), %r8
	movq	168(%rsp), %rcx
	movq	%rcx, %r10
	movq	(%rcx), %rcx
	movq	(%r12), %r11
	leaq	1(%r11), %rdx
	movq	48(%rbx), %rsi
	movq	(%r14), %rbp
	movq	8(%r14), %rdi
	movq	16(%rbp), %rbp
	movq	%rdx, (%r12)
	movq	%r8, (%rax)
	movq	%rcx, (%r10)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movzbl	%r15b, %r14d
	movzbl	24(%rsp), %r15d         # 1-byte Folded Reload
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movl	%r15d, %r8d
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	%r11, 24(%rsp)          # 8-byte Spill
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB67_28
# %bb.2:                                # %assertion.success_1
	movq	160(%rsp), %rax
	movq	%rax, %r9
	movq	(%rax), %r8
	movq	168(%rsp), %rcx
	movq	(%rcx), %r10
	movq	(%r12), %rax
	leaq	1(%rax), %rdx
	movq	56(%rbx), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	16(%rbp), %r11
	movq	%rdx, (%r12)
	movq	%r8, (%r9)
	movq	%r9, %rbp
	movq	%r10, (%rcx)
	movq	%rcx, %rbx
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	%r14d, %ecx
	movq	%rax, %r14
	movl	%r15d, %r8d
	movq	16(%rsp), %r9           # 8-byte Reload
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %r15
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	movq	216(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	callq	*%r11
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB67_28
# %bb.3:                                # %assertion.success_2
	movq	%r15, %rcx
	movq	%rbp, %rdx
	movq	(%r15), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rcx,%rax,8), %rbp
	cmpq	$11, %rbp
	jae	.LBB67_4
# %bb.5:                                # %assertion.success_3
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r12, 80(%rsp)          # 8-byte Spill
	movq	(%r12), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	(%rbx,%r14,8), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rbx,%rax,8), %rax
	leaq	1(%rbp), %rcx
	leaq	8(%r13,%rbp,8), %r14
	leaq	8(%r14,%rbp,8), %r15
	leaq	2(%rbp), %r12
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	imulq	%rcx, %r12
	andq	$-2, %r12
	leaq	(%r15,%r12,4), %rbx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, (%r13)
	callq	sin
	movq	32(%rsp), %r9           # 8-byte Reload
	movsd	%xmm0, 8(%r13,%rbp,8)
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, 8(%r14,%rbp,8)
	movsd	8(%r13,%rbp,8), %xmm0   # xmm0 = mem[0],zero
	movsd	%xmm0, (%r15,%r12,4)
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm0
	movsd	%xmm0, 8(%rbx,%rbp,8)
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movsd	%xmm1, (%r9)
	addq	$8, %r9
	testq	%rbp, %rbp
	je	.LBB67_27
# %bb.6:                                # %if.else_0
	movq	%rbx, %r10
	leaq	8(%rbx,%rbp,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %r12d
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB67_8
	jmp	.LBB67_27
	.p2align	4, 0x90
.LBB67_25:                              # %iir.sum.exit_1
                                        #   in Loop: Header=BB67_8 Depth=1
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	mulsd	(%r13,%r12,8), %xmm1
	addsd	%xmm0, %xmm1
.LBB67_26:                              # %if.exit_4
                                        #   in Loop: Header=BB67_8 Depth=1
	movsd	%xmm1, (%r10,%r12,8)
	mulsd	%xmm4, %xmm1
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	%xmm1, (%rax,%r12,8)
	movq	8(%rsp), %rax           # 8-byte Reload
	movsd	(%rax,%r12,8), %xmm0    # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	%xmm0, (%r9)
	addq	$8, %r9
	incq	%r12
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jae	.LBB67_27
.LBB67_8:                               # %for.step_0
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB67_21 Depth 2
                                        #       Child Loop BB67_23 Depth 3
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	(%rax,%r12,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	movsd	%xmm0, (%r13,%r12,8)
	testq	%r12, %r12
	je	.LBB67_9
# %bb.10:                               # %if.else_1
                                        #   in Loop: Header=BB67_8 Depth=1
	cmpq	$1, %r12
	jne	.LBB67_12
# %bb.11:                               # %if.then_2
                                        #   in Loop: Header=BB67_8 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movq	%r9, %rbx
	callq	cos
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%rbx, %r9
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	%xmm0, 8(%r14)
	testq	%r12, %r12
	jne	.LBB67_19
.LBB67_18:                              # %if.then_5
                                        #   in Loop: Header=BB67_8 Depth=1
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r15)
	movsd	(%r14), %xmm1           # xmm1 = mem[0],zero
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	jmp	.LBB67_26
.LBB67_9:                               # %if.then_1
                                        #   in Loop: Header=BB67_8 Depth=1
	movsd	(%r13), %xmm0           # xmm0 = mem[0],zero
	movq	%r9, %rbx
	callq	sin
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%rbx, %r9
	movsd	48(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	%xmm0, (%r14)
	testq	%r12, %r12
	je	.LBB67_18
.LBB67_19:                              # %if.else_5
                                        #   in Loop: Header=BB67_8 Depth=1
	leaq	1(%r12), %rax
	movq	%r12, %rdx
	imulq	%rax, %rdx
	andq	$-2, %rdx
	leaq	(%r15,%rdx,4), %r8
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%r15,%rdx,4)
	xorpd	%xmm0, %xmm0
	movl	$2, %edx
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	cmpq	%rax, %rdx
	jb	.LBB67_21
	jmp	.LBB67_25
	.p2align	4, 0x90
.LBB67_24:                              # %iir.sum.exit_0
                                        #   in Loop: Header=BB67_21 Depth=2
	movsd	%xmm1, (%r8,%rdx,8)
	mulsd	(%r14,%rdx,8), %xmm1
	incq	%rdx
	addsd	%xmm1, %xmm0
	cmpq	%rax, %rdx
	jae	.LBB67_25
.LBB67_21:                              # %iir.sum.step_0
                                        #   Parent Loop BB67_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB67_23 Depth 3
	movq	%r12, %rsi
	subq	%rdx, %rsi
	addq	$2, %rsi
	xorpd	%xmm1, %xmm1
	movl	$1, %edi
	movq	%r12, %rbp
	cmpq	%rsi, %rdi
	jae	.LBB67_24
	.p2align	4, 0x90
.LBB67_23:                              # %iir.sum.step_1
                                        #   Parent Loop BB67_8 Depth=1
                                        #     Parent Loop BB67_21 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	-1(%rbp), %rbx
	imulq	%rbx, %rbp
	andq	$-2, %rbp
	leaq	(%r15,%rbp,4), %rbp
	movq	.L__choose_array-8(,%r12,8), %rcx
	movsd	-8(%rcx,%rdi,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	(%r13,%rdi,8), %xmm2
	mulsd	-8(%rbp,%rdx,8), %xmm2
	incq	%rdi
	addsd	%xmm2, %xmm1
	movq	%rbx, %rbp
	cmpq	%rsi, %rdi
	jb	.LBB67_23
	jmp	.LBB67_24
.LBB67_12:                              # %if.else_2
                                        #   in Loop: Header=BB67_8 Depth=1
	cmpq	$2, %r12
	jne	.LBB67_14
# %bb.13:                               # %if.then_3
                                        #   in Loop: Header=BB67_8 Depth=1
	xorpd	%xmm0, %xmm0
	subsd	(%r14), %xmm0
	movsd	%xmm0, 16(%r14)
	testq	%r12, %r12
	jne	.LBB67_19
	jmp	.LBB67_18
.LBB67_14:                              # %if.else_3
                                        #   in Loop: Header=BB67_8 Depth=1
	cmpq	$3, %r12
	jne	.LBB67_16
# %bb.15:                               # %if.then_4
                                        #   in Loop: Header=BB67_8 Depth=1
	xorpd	%xmm0, %xmm0
	subsd	8(%r14), %xmm0
	movsd	%xmm0, 24(%r14)
	testq	%r12, %r12
	jne	.LBB67_19
	jmp	.LBB67_18
.LBB67_16:                              # %if.else_4
                                        #   in Loop: Header=BB67_8 Depth=1
	movsd	-32(%r14,%r12,8), %xmm0 # xmm0 = mem[0],zero
	movsd	%xmm0, (%r14,%r12,8)
	testq	%r12, %r12
	jne	.LBB67_19
	jmp	.LBB67_18
.LBB67_4:                               # %assertion.failed_3
	movl	$-2, %eax
	jmp	.LBB67_28
.LBB67_27:                              # %if.exit_5
	movq	72(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	160(%rsp), %rax
	movq	%r9, (%rax)
	movq	168(%rsp), %rax
	movq	%rcx, (%rax)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	xorl	%eax, %eax
.LBB67_28:                              # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end67:
	.size	__anon_obj_residual_417, .Lfunc_end67-__anon_obj_residual_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_417 # -- Begin function __anon_obj_root_setup_417
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_417,@function
__anon_obj_root_setup_417:              # @__anon_obj_root_setup_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r14
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	40(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*24(%rbp)
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB68_4
# %bb.1:                                # %assertion.success_0
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %r8
	leaq	1(%r8), %rcx
	movq	48(%rbx), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %rbp
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	%r12, %rcx
	callq	*%rbp
	testl	%eax, %eax
	jne	.LBB68_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	56(%rbx), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB68_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB68_4:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end68:
	.size	__anon_obj_root_setup_417, .Lfunc_end68-__anon_obj_root_setup_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_417 # -- Begin function __anon_obj_compute_roots_417
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_417,@function
__anon_obj_compute_roots_417:           # @__anon_obj_compute_roots_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	40(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*32(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB69_4
# %bb.1:                                # %assertion.success_0
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	(%r12), %rax
	movq	(%r13), %r9
	leaq	1(%r9), %rcx
	movq	48(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	32(%rdx), %rbp
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	*%rbp
	testl	%eax, %eax
	jne	.LBB69_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	movq	(%r13), %rbp
	leaq	1(%rbp), %rcx
	movq	56(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	32(%rdx), %rbx
	movq	%rcx, (%r13)
	movq	%rax, (%r12)
	movq	%r13, %rdx
	movq	%r14, %rcx
	movq	%r12, %r8
	movq	(%rsp), %r9             # 8-byte Reload
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB69_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB69_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end69:
	.size	__anon_obj_compute_roots_417, .Lfunc_end69-__anon_obj_compute_roots_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_417 # -- Begin function __anon_obj_event_handler_417
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_417,@function
__anon_obj_event_handler_417:           # @__anon_obj_event_handler_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
	.cfi_def_cfa_offset 112
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	16(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	40(%rsi), %rsi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r9, %r13
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*40(%rbp)
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB70_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rax
	movb	(%rax), %cl
	movq	120(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movq	128(%rsp), %rax
	movq	(%rax), %rbp
	movq	136(%rsp), %rax
	movq	(%rax), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax
	movq	(%rax), %r9
	movq	152(%rsp), %rax
	movq	(%rax), %r10
	movq	160(%rsp), %rax
	movq	(%rax), %r11
	movq	%r14, %rdx
	movq	%r12, %r8
	movq	(%r14), %r12
	leaq	1(%r12), %r14
	movq	48(%rbx), %rsi
	movq	(%r15), %rax
	movq	8(%r15), %rdi
	movq	40(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r14, (%rdx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r13)
	movq	112(%rsp), %rax
	movb	%cl, (%rax)
	movq	120(%rsp), %rax
	movb	8(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rax)
	incq	%rbp
	movq	128(%rsp), %rax
	movq	%rbp, (%rax)
	movq	136(%rsp), %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	movq	144(%rsp), %rax
	movq	%r9, (%rax)
	movq	152(%rsp), %r14
	movq	%r10, (%r14)
	movq	160(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r13, %r9
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*88(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB70_4
# %bb.2:                                # %assertion.success_1
	movq	(%r13), %r15
	movq	112(%rsp), %rax
	movb	(%rax), %dl
	movq	120(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 24(%rsp)           # 1-byte Spill
	movq	128(%rsp), %rax
	movq	(%rax), %rbp
	movq	136(%rsp), %rax
	movq	(%rax), %r8
	movq	144(%rsp), %rax
	movq	(%rax), %r9
	movq	152(%rsp), %r14
	movq	(%r14), %r10
	movq	160(%rsp), %rax
	movq	(%rax), %r11
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movq	56(%rbx), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	40(%rbx), %rbx
	movq	%rcx, (%r12)
	movq	%r15, (%r13)
	movq	112(%rsp), %rcx
	movb	%dl, (%rcx)
	movq	120(%rsp), %rcx
	movb	24(%rsp), %dl           # 1-byte Reload
	movb	%dl, (%rcx)
	incq	%rbp
	movq	128(%rsp), %rcx
	movq	%rbp, (%rcx)
	movq	136(%rsp), %r15
	movq	%r8, (%r15)
	movq	144(%rsp), %rcx
	movq	%r9, (%rcx)
	movq	%r10, (%r14)
	movq	160(%rsp), %rbp
	movq	%r11, (%rbp)
	movq	%r12, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r13, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	200(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB70_4
# %bb.3:                                # %assertion.success_2
	movq	144(%rsp), %rax
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	152(%rsp), %rcx
	movq	%rcx, %rbp
	movq	(%rcx), %rcx
	movq	160(%rsp), %rdx
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	movq	(%r12), %rsi
	addq	$2, %rcx
	movq	136(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%rbx)
	movq	%rcx, (%rbp)
	movq	%rdx, (%r8)
	movq	%rsi, (%r12)
	xorl	%eax, %eax
.LBB70_4:                               # %assertion.failed_0
	addq	$56, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end70:
	.size	__anon_obj_event_handler_417, .Lfunc_end70-__anon_obj_event_handler_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_417 # -- Begin function __anon_obj_set_signal_info_417
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_417,@function
__anon_obj_set_signal_info_417:         # @__anon_obj_set_signal_info_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, %r13d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	40(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*48(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB71_4
# %bb.1:                                # %assertion.success_0
	movq	(%r12), %rax
	leaq	1(%rax), %rbp
	movzbl	%r13b, %ecx
	andb	$1, %r13b
	movb	%r13b, (%r15,%rax)
	movq	$t_128_name_418, (%r14,%rax,8)
	movq	48(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %r10
	movq	%rbp, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB71_4
# %bb.2:                                # %assertion.success_1
	movq	(%r12), %rax
	leaq	1(%rax), %rcx
	movb	%r13b, (%r15,%rax)
	movq	$u_130_name_419, (%r14,%rax,8)
	movq	56(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %rbp
	movq	%rcx, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	16(%rsp), %ecx          # 4-byte Reload
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB71_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB71_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end71:
	.size	__anon_obj_set_signal_info_417, .Lfunc_end71-__anon_obj_set_signal_info_417
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_417 # -- Begin function __anon_obj_work_space_417
	.p2align	4, 0x90
	.type	__anon_obj_work_space_417,@function
__anon_obj_work_space_417:              # @__anon_obj_work_space_417
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	(%rsi), %r12
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	40(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%rdx, %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, %r15
	movq	%r9, %r14
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*56(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB72_4
# %bb.1:                                # %assertion.success_0
	movq	(%r15), %r8
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	48(%rbx), %rsi
	movq	(%r12), %rax
	movq	8(%r12), %rdi
	movq	56(%rax), %rax
	movq	%rdx, (%r13)
	movq	%r8, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB72_4
# %bb.2:                                # %assertion.success_1
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	56(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rdx, (%r13)
	movq	%rax, (%r15)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB72_4
# %bb.3:                                # %assertion.success_2
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rdx
	movq	(%r12,%rax,8), %rsi
	leaq	1(%rsi), %rdi
	leaq	2(%rsi), %rbp
	imulq	%rbp, %rdi
	shrq	%rdi
	leaq	1(%rsi,%rdi), %rdi
	leaq	1(%rsi,%rdi), %rdi
	leaq	1(%rsi,%rdi), %rdi
	leaq	1(%rsi,%rdi), %rsi
	cmpq	%rsi, %rcx
	cmovaq	%rcx, %rsi
	incq	%rax
	movq	%rax, (%r15)
	movq	%rsi, (%r14)
	movq	%rdx, (%r13)
	xorl	%eax, %eax
.LBB72_4:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end72:
	.size	__anon_obj_work_space_417, .Lfunc_end72-__anon_obj_work_space_417
	.cfi_endproc
                                        # -- End function
	.globl	voltage_source_ac_420_fun # -- Begin function voltage_source_ac_420_fun
	.p2align	4, 0x90
	.type	voltage_source_ac_420_fun,@function
voltage_source_ac_420_fun:              # @voltage_source_ac_420_fun
	.cfi_startproc
# %bb.0:
	pushq	%rbx
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -16
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB73_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	malloc
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, %rbx
.LBB73_2:                               # %if.exit_0
	movsd	%xmm0, (%rbx)
	movsd	%xmm1, 8(%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB73_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB73_4:                               # %if.exit_1
	movq	$.L__anon_obj_function_record_417, (%rax)
	movq	%rbx, 8(%rax)
	addq	$16, %rsp
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end73:
	.size	voltage_source_ac_420_fun, .Lfunc_end73-voltage_source_ac_420_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_421 # -- Begin function __anon_obj_constructor_421
	.p2align	4, 0x90
	.type	__anon_obj_constructor_421,@function
__anon_obj_constructor_421:             # @__anon_obj_constructor_421
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, %rdi
	movq	%rsi, %r14
	movq	104(%rsp), %rcx
	movq	96(%rsp), %rdx
	movq	(%rsi), %r10
	movq	(%r13), %rsi
	movq	(%r8), %r12
	movq	(%r9), %rbx
	movq	(%rdx), %rbp
	movq	(%rcx), %r8
	movb	$1, %al
	testb	%al, %al
	jne	.LBB74_1
# %bb.2:                                # %if.else_0
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	xorl	%edi, %edi
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r10, (%rsp)            # 8-byte Spill
	callq	malloc
	movq	(%rsp), %r10            # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx
	movq	104(%rsp), %rcx
	jmp	.LBB74_3
.LBB74_1:
	xorl	%eax, %eax
.LBB74_3:                               # %if.exit_0
	incq	%r12
	incq	%rbp
	movq	%r10, (%r14)
	movq	%rax, (%rdi)
	movq	%rsi, (%r13)
	movq	%r12, (%r15)
	movq	%rbx, (%r9)
	movq	%rbp, (%rdx)
	movq	%r8, (%rcx)
	xorl	%eax, %eax
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end74:
	.size	__anon_obj_constructor_421, .Lfunc_end74-__anon_obj_constructor_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_421 # -- Begin function __anon_obj_signature_421
	.p2align	4, 0x90
	.type	__anon_obj_signature_421,@function
__anon_obj_signature_421:               # @__anon_obj_signature_421
	.cfi_startproc
# %bb.0:
	pushq	%r14
	.cfi_def_cfa_offset 16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset %rbx, -24
	.cfi_offset %r14, -16
	movl	32(%rsp), %r10d
	movq	24(%rsp), %r11
	movq	(%rdx), %r14
	movq	(%rcx), %rax
	movq	(%r8), %rsi
	movq	(%r9), %rbx
	movq	(%r11), %rdi
	movq	%rsi, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rdi, 24(%rax)
	movl	%r10d, (%rsi)
	movb	$0, (%rbx)
	movq	$0, (%rdi)
	addq	$4, %rsi
	incq	%rbx
	addq	$8, %rdi
	movl	$1, (%rax)
	addq	$32, %rax
	movq	%rax, (%rcx)
	movq	%rsi, (%r8)
	movq	%rbx, (%r9)
	movq	%rdi, (%r11)
	movq	%r14, (%rdx)
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end75:
	.size	__anon_obj_signature_421, .Lfunc_end75-__anon_obj_signature_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_421 # -- Begin function __anon_obj_residual_421
	.p2align	4, 0x90
	.type	__anon_obj_residual_421,@function
__anon_obj_residual_421:                # @__anon_obj_residual_421
	.cfi_startproc
# %bb.0:
	movq	32(%rsp), %r10
	movq	(%r10), %r11
	movq	(%r9,%r11,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB76_6
# %bb.1:                                # %assertion.success_0
	movq	40(%rsp), %rax
	movq	24(%rsp), %r8
	movq	8(%rsp), %rcx
	movq	(%rdx), %r9
	movq	(%r8), %rdi
	movq	(%rcx,%rax,8), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	testq	%rsi, %rsi
	je	.LBB76_5
# %bb.2:                                # %if.else_0
	incq	%rsi
	movl	$1, %ecx
	cmpq	%rsi, %rcx
	jae	.LBB76_5
	.p2align	4, 0x90
.LBB76_4:                               # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB76_4
.LBB76_5:                               # %if.exit_0
	incq	%r11
	movq	%rdi, (%r8)
	movq	%r11, (%r10)
	movq	%r9, (%rdx)
	xorl	%eax, %eax
	retq
.LBB76_6:                               # %assertion.failed_0
	movl	$-2, %eax
	retq
.Lfunc_end76:
	.size	__anon_obj_residual_421, .Lfunc_end76-__anon_obj_residual_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_421 # -- Begin function __anon_obj_root_setup_421
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_421,@function
__anon_obj_root_setup_421:              # @__anon_obj_root_setup_421
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end77:
	.size	__anon_obj_root_setup_421, .Lfunc_end77-__anon_obj_root_setup_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_421 # -- Begin function __anon_obj_compute_roots_421
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_421,@function
__anon_obj_compute_roots_421:           # @__anon_obj_compute_roots_421
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end78:
	.size	__anon_obj_compute_roots_421, .Lfunc_end78-__anon_obj_compute_roots_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_421 # -- Begin function __anon_obj_event_handler_421
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_421,@function
__anon_obj_event_handler_421:           # @__anon_obj_event_handler_421
	.cfi_startproc
# %bb.0:
	movq	32(%rsp), %r8
	movq	56(%rsp), %r9
	movq	48(%rsp), %r10
	movq	40(%rsp), %r11
	movq	(%rdx), %rax
	movq	(%r11), %rcx
	movq	(%r10), %rsi
	movq	(%r9), %rdi
	incq	%rsi
	incq	(%r8)
	movq	%rcx, (%r11)
	movq	%rsi, (%r10)
	movq	%rdi, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end79:
	.size	__anon_obj_event_handler_421, .Lfunc_end79-__anon_obj_event_handler_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_421 # -- Begin function __anon_obj_set_signal_info_421
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_421,@function
__anon_obj_set_signal_info_421:         # @__anon_obj_set_signal_info_421
	.cfi_startproc
# %bb.0:
	xorl	%eax, %eax
	retq
.Lfunc_end80:
	.size	__anon_obj_set_signal_info_421, .Lfunc_end80-__anon_obj_set_signal_info_421
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_421 # -- Begin function __anon_obj_work_space_421
	.p2align	4, 0x90
	.type	__anon_obj_work_space_421,@function
__anon_obj_work_space_421:              # @__anon_obj_work_space_421
	.cfi_startproc
# %bb.0:
	movq	(%rdx), %rax
	movq	(%r9), %rcx
	incq	(%r8)
	movq	%rcx, (%r9)
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end81:
	.size	__anon_obj_work_space_421, .Lfunc_end81-__anon_obj_work_space_421
	.cfi_endproc
                                        # -- End function
	.globl	ground_422_fun          # -- Begin function ground_422_fun
	.p2align	4, 0x90
	.type	ground_422_fun,@function
ground_422_fun:                         # @ground_422_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB82_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
.LBB82_2:                               # %if.exit_0
	movq	$.L__anon_obj_function_record_421, (%rax)
	movq	$0, 8(%rax)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end82:
	.size	ground_422_fun, .Lfunc_end82-ground_422_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_423 # -- Begin function __anon_obj_constructor_423
	.p2align	4, 0x90
	.type	__anon_obj_constructor_423,@function
__anon_obj_constructor_423:             # @__anon_obj_constructor_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
	.cfi_def_cfa_offset 128
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	144(%rsp), %r11
	movq	136(%rsp), %r10
	movq	128(%rsp), %rbx
	movq	(%rsi), %r12
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	(%rcx), %r14
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	(%r8), %r8
	movq	(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %r15
	xorl	%eax, %eax
	testb	%al, %al
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	(%r9), %r13
	movq	(%rbx), %rbp
	movq	(%r10), %r9
	jne	.LBB83_1
# %bb.2:                                # %if.else_0
	movl	$16, %edi
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %rbp
	movq	%r8, %rbx
	movq	%r9, (%rsp)             # 8-byte Spill
	callq	malloc
	movq	(%rsp), %r9             # 8-byte Reload
	movq	%rbx, %r8
	movq	144(%rsp), %r11
	movq	128(%rsp), %rbx
	movq	%rbp, %rsi
	movq	136(%rsp), %r10
	movq	%r13, %rdx
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB83_3
.LBB83_1:
	xorl	%eax, %eax
.LBB83_3:                               # %if.exit_0
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	leaq	2(%r12), %rax
	addq	$2, %r14
	movq	(%r15), %rcx
	movq	8(%r15), %rdi
	movq	(%rcx), %r15
	movq	%rax, (%rsi)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%r14, (%rcx)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%r8, (%rax)
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	%rax, %r8
	movq	%r13, (%r14)
	movq	%rbp, (%rbx)
	movq	%rcx, %r13
	movq	%r9, (%r10)
	movq	%rax, %rbp
	movq	%r14, %r9
	pushq	%r12
	leaq	1(%r12), %r12
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	%rdx, %rbx
	callq	*%r15
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB83_6
# %bb.4:                                # %assertion.success_0
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %r11
	movq	(%r13), %rcx
	movq	%rbp, %r15
	movq	(%rbp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	(%r14), %rbp
	movq	128(%rsp), %rax
	movq	(%rax), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	136(%rsp), %r10
	movq	%rbx, %rdx
	movq	(%r10), %rbx
	movq	(%rdx), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	1(%r11), %r8
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%r8, (%r9)
	incq	%rcx
	movq	%rcx, (%r13)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%r15)
	movq	%rbp, (%r14)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	168(%rsp), %rbp
	movq	%rbx, (%r10)
	movq	160(%rsp), %rbx
	movq	%r9, %rsi
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%r13, %rcx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*56(%rsp)               # 8-byte Folded Reload
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB83_6
# %bb.5:                                # %assertion.success_1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %r11
	movq	(%r13), %r9
	movq	(%r15), %rdx
	movq	(%r14), %r10
	movq	128(%rsp), %rcx
	movq	%rcx, %rax
	movq	(%rcx), %rdi
	movq	136(%rsp), %rcx
	movq	%rcx, %rsi
	movq	(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	(%rbx), %r12
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%r8, (%rcx)
	movq	%r12, 8(%rcx)
	movq	%r11, (%rbp)
	movq	%rcx, (%rbx)
	movq	%r9, (%r13)
	incq	%rdx
	movq	%rdx, (%r15)
	movq	%r10, (%r14)
	addq	$2, %rdi
	movq	%rdi, (%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, (%rsi)
	xorl	%eax, %eax
.LBB83_6:                               # %assertion.failed_0
	addq	$72, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end83:
	.size	__anon_obj_constructor_423, .Lfunc_end83-__anon_obj_constructor_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_423 # -- Begin function __anon_obj_signature_423
	.p2align	4, 0x90
	.type	__anon_obj_signature_423,@function
__anon_obj_signature_423:               # @__anon_obj_signature_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	96(%rsp), %r11
	movq	(%rdx), %rbp
	movq	(%rcx), %r8
	movq	(%r12), %rcx
	movq	(%r9), %rdx
	movq	(%r11), %r9
	movq	(%rdi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rdi
	leaq	2(%rbp), %rbx
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	8(%rax), %rax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	(%rsi), %rsi
	movq	%rbx, (%r15)
	movq	%r8, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%r14)
	movq	112(%rsp), %rbx
	movq	%r9, (%r11)
	leaq	1(%rbp), %r10
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	%r10, 48(%rsp)          # 8-byte Spill
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB84_3
# %bb.1:                                # %assertion.success_0
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	(%r13), %r8
	movq	(%r12), %r9
	movq	(%r14), %rdx
	movq	96(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %rbp
	movq	(%r15), %rbx
	leaq	1(%rbx), %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	8(%rax), %rax
	movq	%rcx, (%r15)
	movq	%r8, (%r13)
	movq	%r9, (%r12)
	movq	%rdx, (%r14)
	movq	128(%rsp), %r11
	movq	%rbp, (%r10)
	movq	120(%rsp), %rbp
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rbp
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB84_3
# %bb.2:                                # %assertion.success_1
	movq	(%r13), %rax
	movq	(%r12), %rcx
	movq	(%r14), %rdx
	movq	96(%rsp), %rsi
	movq	%rsi, %r8
	movq	(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rsi, 24(%rax)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, (%rcx)
	movb	$0, (%rdx)
	movq	$0, (%rsi)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	%ebp, 4(%rcx)
	movb	$0, 1(%rdx)
	movq	$0, 8(%rsi)
	movl	$2, (%rax)
	addq	$32, %rax
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$16, %rsi
	movq	%rax, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%r14)
	movq	%rsi, (%r8)
	movq	%rdi, (%r15)
	xorl	%eax, %eax
.LBB84_3:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end84:
	.size	__anon_obj_signature_423, .Lfunc_end84-__anon_obj_signature_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_423 # -- Begin function __anon_obj_residual_423
	.p2align	4, 0x90
	.type	__anon_obj_residual_423,@function
__anon_obj_residual_423:                # @__anon_obj_residual_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	104(%rsp), %r10
	movq	96(%rsp), %r11
	movq	(%rdx), %r13
	movq	(%r11), %rax
	movq	(%r10), %r14
	movq	(%rdi), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %rdx
	leaq	2(%r13), %rbp
	movq	(%rsi), %rsi
	movq	(%rdx), %rbx
	movq	8(%rdx), %rdi
	movq	16(%rbx), %rbx
	movq	%rbp, (%r12)
	movq	%rax, (%r11)
	movq	%r14, (%r10)
	movq	112(%rsp), %rax
	movq	120(%rsp), %rbp
	leaq	1(%r13), %r14
	movq	%r12, %rdx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	movq	%r11, %rbp
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB85_9
# %bb.1:                                # %assertion.success_0
	movq	136(%rsp), %r11
	movq	(%rbp), %rcx
	movq	%rbp, %r8
	movq	104(%rsp), %rdx
	movq	(%rdx), %r9
	movq	(%r12), %rax
	leaq	1(%rax), %rbx
	movq	8(%r15), %rsi
	movq	%rax, %r15
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	16(%rbp), %rax
	movq	%rbx, (%r12)
	movq	%rcx, (%r8)
	movq	%r8, %r10
	movq	%r9, (%rdx)
	movq	%rdx, %rbp
	movzbl	(%rsp), %ecx            # 1-byte Folded Reload
	movzbl	4(%rsp), %r8d           # 1-byte Folded Reload
	movq	%r12, %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	%r10, %r14
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	136(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	movq	104(%rsp), %r10
	jne	.LBB85_9
# %bb.2:                                # %assertion.success_1
	movq	(%r10), %rax
	movq	(%rbx,%rax,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB85_3
# %bb.4:                                # %assertion.success_2
	movq	%r14, %r9
	movq	%rbp, %rcx
	movq	(%r14), %rdx
	movq	(%r12), %r8
	movq	(%rbp,%r13,8), %rdi
	movq	(%rbp,%r15,8), %rbp
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rbp), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB85_8
# %bb.5:                                # %if.else_0
	incq	%rsi
	movl	$1, %ecx
	cmpq	%rsi, %rcx
	jae	.LBB85_8
	.p2align	4, 0x90
.LBB85_7:                               # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rbp,%rcx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB85_7
.LBB85_8:                               # %if.exit_0
	incq	%rax
	movq	%rdx, (%r9)
	movq	%rax, (%r10)
	movq	%r8, (%r12)
	xorl	%eax, %eax
	jmp	.LBB85_9
.LBB85_3:                               # %assertion.failed_2
	movl	$-2, %eax
.LBB85_9:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end85:
	.size	__anon_obj_residual_423, .Lfunc_end85-__anon_obj_residual_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_423 # -- Begin function __anon_obj_root_setup_423
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_423,@function
__anon_obj_root_setup_423:              # @__anon_obj_root_setup_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	(%rdx), %rax
	movq	(%rcx), %r10
	movq	(%rdi), %r13
	movq	8(%rdi), %rdx
	leaq	2(%rax), %r11
	movq	(%rsi), %rsi
	movq	(%rdx), %rcx
	movq	8(%rdx), %rdi
	movq	24(%rcx), %rbp
	movq	%r11, (%rbx)
	movq	%r10, (%r14)
	movq	%rbx, %rdx
	movq	%r14, %rcx
	pushq	%rax
	leaq	1(%rax), %r12
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB86_3
# %bb.1:                                # %assertion.success_0
	movq	72(%rsp), %rax
	movq	64(%rsp), %r10
	movq	(%r14), %rcx
	movq	(%rbx), %r9
	leaq	1(%r9), %rdx
	movq	8(%r15), %rsi
	movq	(%r13), %rbp
	movq	8(%r13), %rdi
	movq	24(%rbp), %rbp
	movq	%rdx, (%rbx)
	movq	%rcx, (%r14)
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r12, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB86_3
# %bb.2:                                # %assertion.success_1
	xorl	%eax, %eax
.LBB86_3:                               # %assertion.failed_0
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end86:
	.size	__anon_obj_root_setup_423, .Lfunc_end86-__anon_obj_root_setup_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_423 # -- Begin function __anon_obj_compute_roots_423
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_423,@function
__anon_obj_compute_roots_423:           # @__anon_obj_compute_roots_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	64(%rsp), %r10
	movq	(%rdx), %rax
	movq	(%r8), %r8
	movq	(%rdi), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	8(%rdi), %rdx
	leaq	2(%rax), %rbp
	movq	(%rsi), %rsi
	movq	(%rdx), %rcx
	movq	8(%rdx), %rdi
	movq	32(%rcx), %r11
	movq	%rbp, (%rbx)
	movq	%r8, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r15, %r8
	pushq	%rax
	leaq	1(%rax), %r13
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%r11
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB87_3
# %bb.1:                                # %assertion.success_0
	movq	80(%rsp), %r11
	movq	72(%rsp), %r10
	movq	(%r15), %rcx
	movq	(%rbx), %rbp
	leaq	1(%rbp), %rdx
	movq	8(%r12), %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	32(%rax), %rax
	movq	%rdx, (%rbx)
	movq	%rcx, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r15, %r8
	movq	%r13, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB87_3
# %bb.2:                                # %assertion.success_1
	xorl	%eax, %eax
.LBB87_3:                               # %assertion.failed_0
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end87:
	.size	__anon_obj_compute_roots_423, .Lfunc_end87-__anon_obj_compute_roots_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_423 # -- Begin function __anon_obj_event_handler_423
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_423,@function
__anon_obj_event_handler_423:           # @__anon_obj_event_handler_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
	.cfi_def_cfa_offset 128
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	176(%rsp), %r10
	movq	168(%rsp), %r8
	movq	160(%rsp), %r15
	movq	152(%rsp), %r14
	movq	136(%rsp), %rcx
	movq	128(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movb	(%rcx), %al
	movb	%al, 7(%rsp)            # 1-byte Spill
	movq	(%rdi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	40(%rsi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rcx
	movq	(%rdx), %rbp
	movq	(%r9), %rax
	movq	(%rcx), %rbx
	movq	(%r14), %rsi
	movq	%r15, %rdi
	movq	(%r15), %r13
	movq	(%r8), %r11
	movq	(%r10), %r12
	leaq	2(%rbp), %r15
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r15, (%rdx)
	movq	%rax, (%r9)
	movq	128(%rsp), %rax
	movb	8(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rax)
	movq	136(%rsp), %rax
	movb	7(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rax)
	addq	$2, %rbx
	movq	144(%rsp), %r15
	movq	%rbx, (%r15)
	movq	%rsi, (%r14)
	movq	%r14, %r15
	movq	%r13, (%rdi)
	movq	%r11, (%r8)
	movq	%r8, %r11
	movq	%r12, (%r10)
	movq	184(%rsp), %r13
	movq	192(%rsp), %rbx
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r9, %r14
	pushq	%rbp
	leaq	1(%rbp), %r12
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%rax, %rbp
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	216(%rsp), %rax
	movq	%rax, %rbx
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*120(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB88_3
# %bb.1:                                # %assertion.success_0
	movq	%r14, %r9
	movq	(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movb	(%rbx), %r13b
	movb	(%rbp), %bl
	movq	144(%rsp), %rax
	movq	(%rax), %rbp
	movq	152(%rsp), %rax
	movq	(%rax), %r15
	movq	160(%rsp), %rax
	movq	(%rax), %r8
	movq	168(%rsp), %r14
	movq	(%r14), %r14
	movq	176(%rsp), %rax
	movq	(%rax), %r11
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %r10
	leaq	1(%r10), %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	40(%rax), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rcx, (%rdx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r9)
	movq	128(%rsp), %rax
	movb	%r13b, (%rax)
	movq	136(%rsp), %rax
	movb	%bl, (%rax)
	incq	%rbp
	movq	144(%rsp), %rax
	movq	%rbp, (%rax)
	movq	152(%rsp), %r13
	movq	%r15, (%r13)
	movq	160(%rsp), %rbp
	movq	%r8, (%rbp)
	movq	168(%rsp), %rax
	movq	%r14, (%rax)
	movq	208(%rsp), %r14
	movq	176(%rsp), %rbx
	movq	%r11, (%rbx)
	movq	200(%rsp), %r11
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*112(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB88_3
# %bb.2:                                # %assertion.success_1
	movq	160(%rsp), %rax
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	168(%rsp), %rcx
	movq	%rcx, %rbp
	movq	(%rcx), %rcx
	movq	176(%rsp), %rdx
	movq	%rdx, %r9
	movq	(%rdx), %rdx
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rbx), %rsi
	addq	$2, %rcx
	movq	152(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%r8)
	movq	%rcx, (%rbp)
	movq	%rdx, (%r9)
	movq	%rsi, (%rbx)
	xorl	%eax, %eax
.LBB88_3:                               # %assertion.failed_0
	addq	$72, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end88:
	.size	__anon_obj_event_handler_423, .Lfunc_end88-__anon_obj_event_handler_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_423 # -- Begin function __anon_obj_set_signal_info_423
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_423,@function
__anon_obj_set_signal_info_423:         # @__anon_obj_set_signal_info_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movl	%ecx, %r13d
	movq	%rdx, %rbx
	movq	88(%rsp), %r11
	movq	(%rdx), %rax
	movq	(%rdi), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %rcx
	leaq	2(%rax), %rdx
	movl	%r13d, %r12d
	andb	$1, %r12b
	movb	%r12b, (%r8,%rax)
	movq	$n1_i_168_name_424, (%r9,%rax,8)
	movb	%r12b, 1(%r8,%rax)
	movq	$n1_v_170_name_425, 8(%r9,%rax,8)
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rsi
	movq	(%rcx), %rbp
	movq	8(%rcx), %rdi
	movq	48(%rbp), %r10
	movq	%rdx, (%rbx)
	movq	%rbx, %rdx
	movl	%r13d, %ecx
	pushq	%rax
	leaq	1(%rax), %rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB89_3
# %bb.1:                                # %assertion.success_0
	movq	104(%rsp), %r11
	movq	96(%rsp), %r10
	movq	(%rbx), %rax
	leaq	1(%rax), %rcx
	movb	%r12b, (%r15,%rax)
	movq	$p2_i_180_name_426, (%r14,%rax,8)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	8(%rdx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %r12
	movq	%rcx, (%rbx)
	movzbl	%r13b, %ecx
	movq	%rbx, %rdx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%r12
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB89_3
# %bb.2:                                # %assertion.success_1
	xorl	%eax, %eax
.LBB89_3:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end89:
	.size	__anon_obj_set_signal_info_423, .Lfunc_end89-__anon_obj_set_signal_info_423
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_423 # -- Begin function __anon_obj_work_space_423
	.p2align	4, 0x90
	.type	__anon_obj_work_space_423,@function
__anon_obj_work_space_423:              # @__anon_obj_work_space_423
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	80(%rsp), %r10
	movq	88(%rsp), %r11
	movq	(%rdx), %rbp
	movq	(%r8), %r8
	movq	(%r9), %rdx
	movq	(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %rdi
	leaq	2(%rbp), %rax
	movq	(%rsi), %rsi
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rax, (%r12)
	movq	%r8, (%r15)
	movq	%rdx, (%r9)
	movq	%r12, %rdx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r15, %r8
	pushq	%rbp
	leaq	1(%rbp), %rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB90_3
# %bb.1:                                # %assertion.success_0
	movq	104(%rsp), %r11
	movq	96(%rsp), %r10
	movq	(%r15), %r8
	movq	(%r14), %rdx
	movq	(%r12), %rbx
	leaq	1(%rbx), %rax
	movq	8(%r13), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	56(%rcx), %r13
	movq	%rax, (%r12)
	movq	%r8, (%r15)
	movq	%rdx, (%r14)
	movq	%r12, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%r13
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB90_3
# %bb.2:                                # %assertion.success_1
	movq	(%r14), %rax
	movq	(%r12), %rcx
	incq	(%r15)
	movq	%rax, (%r14)
	movq	%rcx, (%r12)
	xorl	%eax, %eax
.LBB90_3:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end90:
	.size	__anon_obj_work_space_423, .Lfunc_end90-__anon_obj_work_space_423
	.cfi_endproc
                                        # -- End function
	.globl	serial_427_fun          # -- Begin function serial_427_fun
	.p2align	4, 0x90
	.type	serial_427_fun,@function
serial_427_fun:                         # @serial_427_fun
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB91_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
.LBB91_2:                               # %if.exit_0
	movq	%r15, (%rbx)
	movq	%r14, 8(%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB91_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB91_4:                               # %if.exit_1
	movq	$.L__anon_obj_function_record_423, (%rax)
	movq	%rbx, 8(%rax)
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end91:
	.size	serial_427_fun, .Lfunc_end91-serial_427_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_428 # -- Begin function __anon_obj_constructor_428
	.p2align	4, 0x90
	.type	__anon_obj_constructor_428,@function
__anon_obj_constructor_428:             # @__anon_obj_constructor_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
	.cfi_def_cfa_offset 128
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	144(%rsp), %r11
	movq	136(%rsp), %rbx
	movq	128(%rsp), %r12
	movq	(%rsi), %rbp
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	(%rcx), %r14
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	(%r8), %r15
	movq	(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rdi), %rdi
	xorl	%eax, %eax
	testb	%al, %al
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	(%r9), %rdx
	movq	(%r12), %r13
	movq	(%rbx), %r8
	jne	.LBB92_1
# %bb.2:                                # %if.else_0
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	$16, %edi
	movq	%rsi, %r12
	movq	%rbp, %rbx
	movq	%r8, %rbp
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	malloc
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %r8
	movq	%rbx, %rbp
	movq	144(%rsp), %r11
	movq	%r12, %rsi
	movq	128(%rsp), %r12
	movq	136(%rsp), %rbx
	jmp	.LBB92_3
.LBB92_1:
	xorl	%eax, %eax
.LBB92_3:                               # %if.exit_0
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	3(%rbp), %rax
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	(%rcx), %r10
	movq	%rax, (%rsi)
	addq	$3, %r14
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r14, (%rcx)
	incq	%r15
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	%r15, (%r14)
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	%rdx, (%r9)
	addq	$3, %r13
	movq	%r13, (%r12)
	movq	%r8, (%rbx)
	leaq	2(%rbp), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %r8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	movq	%rsi, %r15
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB92_6
# %bb.4:                                # %assertion.success_0
	movq	(%r15), %r10
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rcx
	movq	(%r14), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	48(%rsp), %r13          # 8-byte Reload
	movq	(%r13), %r9
	movq	(%r12), %r11
	movq	(%rbx), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rsi
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	leaq	1(%r10), %r8
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%r8, (%r15)
	incq	%rcx
	movq	%rcx, (%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r14)
	movq	%r9, (%r13)
	movq	%r11, (%r12)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rbx)
	incq	%rbp
	movq	%r15, %rsi
	movq	%rax, %rcx
	movq	%r14, %r8
	movq	%r13, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rbp
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	callq	*48(%rsp)               # 8-byte Folded Reload
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB92_6
# %bb.5:                                # %assertion.success_1
	movq	(%r15), %r11
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r9
	movq	(%r14), %rdx
	movq	(%r13), %r10
	movq	(%r12), %rdi
	movq	%rbp, %rbx
	movq	(%rbp), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	%r8, (%rsi)
	movq	%rcx, 8(%rsi)
	movq	%r11, (%r15)
	movq	%rsi, (%rbp)
	movq	%r9, (%rax)
	incq	%rdx
	movq	%rdx, (%r14)
	movq	%r10, (%r13)
	addq	$3, %rdi
	movq	%rdi, (%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.LBB92_6:                               # %assertion.failed_0
	addq	$72, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end92:
	.size	__anon_obj_constructor_428, .Lfunc_end92-__anon_obj_constructor_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_428 # -- Begin function __anon_obj_signature_428
	.p2align	4, 0x90
	.type	__anon_obj_signature_428,@function
__anon_obj_signature_428:               # @__anon_obj_signature_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	96(%rsp), %r11
	movl	128(%rsp), %r8d
	movq	(%rdx), %r14
	movq	(%rcx), %rcx
	movq	(%r12), %rdx
	movq	(%r9), %rbp
	movq	(%r11), %rbx
	movq	(%rdi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	leaq	1(%r14), %rdi
	movq	%rdx, 8(%rcx)
	movq	%rbp, 16(%rcx)
	movq	%rbx, 24(%rcx)
	movl	%r8d, (%rdx)
	movb	$0, (%rbp)
	movq	$0, (%rbx)
	movl	%r14d, 4(%rdx)
	movb	$0, 1(%rbp)
	movq	$0, 8(%rbx)
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	%edi, 8(%rdx)
	movb	$0, 2(%rbp)
	movq	$0, 16(%rbx)
	movl	$3, (%rcx)
	movq	(%rax), %r8
	movq	8(%rax), %rdi
	movq	8(%r8), %r10
	leaq	3(%r14), %r8
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	(%rsi), %rsi
	movq	%r8, (%r15)
	addq	$32, %rcx
	movq	%rcx, (%r13)
	addq	$12, %rdx
	movq	%rdx, (%r12)
	addq	$3, %rbp
	movq	%rbp, (%r9)
	addq	$24, %rbx
	movq	%rbx, (%r11)
	leaq	2(%r14), %rbx
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r9, %rbp
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB93_3
# %bb.1:                                # %assertion.success_0
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%r13), %r8
	movq	(%r12), %r9
	movq	%rbp, %r14
	movq	(%rbp), %rdx
	movq	96(%rsp), %rax
	movq	%rax, %r10
	movq	(%rax), %rbp
	movq	(%r15), %rbx
	leaq	1(%rbx), %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	8(%rax), %rax
	movq	%rcx, (%r15)
	movq	%r8, (%r13)
	movq	%r9, (%r12)
	movq	%rdx, (%r14)
	movq	%rbp, (%r10)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	40(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rbp
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	136(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB93_3
# %bb.2:                                # %assertion.success_1
	movq	112(%rsp), %rax
	movq	(%r13), %rcx
	movq	(%r12), %rdx
	movq	(%r14), %rsi
	movq	96(%rsp), %rdi
	movq	%rdi, %r8
	movq	(%rdi), %rdi
	movq	(%r15), %rbx
	movq	%rdx, 8(%rcx)
	movq	%rsi, 16(%rcx)
	movq	%rdi, 24(%rcx)
	movl	%eax, (%rdx)
	movb	$0, (%rsi)
	movq	$0, (%rdi)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 4(%rdx)
	movb	$0, 1(%rsi)
	movq	$0, 8(%rdi)
	movl	%ebp, 8(%rdx)
	movb	$0, 2(%rsi)
	movq	$0, 16(%rdi)
	movl	$3, (%rcx)
	addq	$32, %rcx
	addq	$12, %rdx
	addq	$3, %rsi
	addq	$24, %rdi
	movq	%rcx, (%r13)
	movq	%rdx, (%r12)
	movq	%rsi, (%r14)
	movq	%rdi, (%r8)
	movq	%rbx, (%r15)
	xorl	%eax, %eax
.LBB93_3:                               # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end93:
	.size	__anon_obj_signature_428, .Lfunc_end93-__anon_obj_signature_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_428 # -- Begin function __anon_obj_residual_428
	.p2align	4, 0x90
	.type	__anon_obj_residual_428,@function
__anon_obj_residual_428:                # @__anon_obj_residual_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	120(%rsp), %r14
	movq	(%r14), %r11
	movq	(%r9,%r11,8), %r12
	cmpq	$11, %r12
	jae	.LBB94_1
# %bb.2:                                # %assertion.success_0
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	152(%rsp), %r15
	movq	112(%rsp), %rbp
	movq	104(%rsp), %r13
	movq	96(%rsp), %rax
	movq	(%rdx), %rbx
	movq	(%rbp), %r10
	movq	(%rdi), %rbp
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %r9
	leaq	2(%rbx), %rsi
	movq	(%rax,%rbx,8), %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rax,%rbx,8), %rbx
	movq	(%rax,%r15,8), %rdi
	movsd	(%rbp), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rdi), %xmm0
	movsd	%xmm0, (%r13)
	addsd	(%rbx), %xmm0
	movsd	%xmm0, (%r10)
	addq	$8, %r10
	testq	%r12, %r12
	je	.LBB94_3
# %bb.10:                               # %if.else_0
	incq	%r12
	movl	$1, %eax
	cmpq	%r12, %rax
	jae	.LBB94_3
	.p2align	4, 0x90
.LBB94_12:                              # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rbp,%rax,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rdi,%rax,8), %xmm0
	movsd	%xmm0, (%r13,%rax,8)
	addsd	(%rbx,%rax,8), %xmm0
	movsd	%xmm0, (%r10)
	addq	$8, %r10
	incq	%rax
	cmpq	%r12, %rax
	jb	.LBB94_12
.LBB94_3:
	movq	%r14, %r15
	movq	%rdx, %r12
	movq	(%rsp), %rdx            # 8-byte Reload
	incq	%r11
	movq	%rsi, %r14
	leaq	1(%rsi), %rax
	movq	(%rdx), %rsi
	movq	(%r9), %rbx
	movq	8(%r9), %rdi
	movq	16(%rbx), %rbx
	movq	%rax, (%r12)
	movq	112(%rsp), %rbp
	movq	%r10, (%rbp)
	movq	%r11, (%r15)
	movzbl	%cl, %ecx
	movzbl	%r8b, %r8d
	movq	%r12, %rdx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%r8d, 24(%rsp)          # 4-byte Spill
	movq	32(%rsp), %r9           # 8-byte Reload
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	movq	152(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	movq	152(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	movq	152(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB94_14
# %bb.4:                                # %assertion.success_1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	1(%rax), %r10
	movq	(%rbp), %r8
	movq	(%r15), %rdx
	movq	(%r12), %r11
	leaq	1(%r11), %rax
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rcx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	16(%rcx), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rax, (%r12)
	movq	%r8, (%rbp)
	movq	%rdx, (%r15)
	movq	%r12, %rdx
	movl	28(%rsp), %ecx          # 4-byte Reload
	movl	24(%rsp), %r8d          # 4-byte Reload
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%r11, 24(%rsp)          # 8-byte Spill
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	movq	152(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	callq	*64(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	jne	.LBB94_14
# %bb.5:                                # %assertion.success_2
	movq	%rbx, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%r15), %r10
	movq	(%rbx,%r10,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB94_1
# %bb.6:                                # %assertion.success_3
	movq	%rbp, %r9
	movq	%r14, %rbx
	movq	136(%rsp), %rcx
	movq	(%rbp), %rdx
	movq	(%r12), %r8
	movq	16(%r14,%rdi,8), %rdi
	movq	(%r14,%rcx,8), %rbp
	movq	(%r14,%rax,8), %rbx
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rbp), %xmm0
	movsd	%xmm0, (%r13)
	addsd	(%rbx), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB94_13
# %bb.7:                                # %if.else_1
	incq	%rsi
	movl	$1, %ecx
	cmpq	%rsi, %rcx
	jae	.LBB94_13
	.p2align	4, 0x90
.LBB94_9:                               # %for.step_1
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rbp,%rcx,8), %xmm0
	movsd	%xmm0, (%r13,%rcx,8)
	addsd	(%rbx,%rcx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB94_9
.LBB94_13:                              # %if.exit_1
	incq	%r10
	movq	%rdx, (%r9)
	movq	%r10, (%r15)
	movq	%r8, (%r12)
	xorl	%eax, %eax
	jmp	.LBB94_14
.LBB94_1:                               # %assertion.failed_0
	movl	$-2, %eax
.LBB94_14:                              # %assertion.failed_1
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end94:
	.size	__anon_obj_residual_428, .Lfunc_end94-__anon_obj_residual_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_428 # -- Begin function __anon_obj_root_setup_428
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_428,@function
__anon_obj_root_setup_428:              # @__anon_obj_root_setup_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	(%rdx), %rbp
	movq	(%rcx), %r8
	movq	(%rdi), %r13
	movq	8(%rdi), %rcx
	leaq	2(%rbp), %r9
	leaq	3(%rbp), %rdx
	movq	(%rsi), %rsi
	movq	(%rcx), %rax
	movq	8(%rcx), %rdi
	movq	24(%rax), %rax
	movq	%rdx, (%rbx)
	movq	%r8, (%r15)
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB95_3
# %bb.1:                                # %assertion.success_0
	incq	%rbp
	movq	(%r15), %rax
	movq	(%rbx), %r9
	leaq	1(%r9), %rcx
	movq	8(%r12), %rsi
	movq	(%r13), %rdx
	movq	8(%r13), %rdi
	movq	24(%rdx), %r10
	movq	%rcx, (%rbx)
	movq	%rax, (%r15)
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB95_3
# %bb.2:                                # %assertion.success_1
	xorl	%eax, %eax
.LBB95_3:                               # %assertion.failed_0
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end95:
	.size	__anon_obj_root_setup_428, .Lfunc_end95-__anon_obj_root_setup_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_428 # -- Begin function __anon_obj_compute_roots_428
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_428,@function
__anon_obj_compute_roots_428:           # @__anon_obj_compute_roots_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rcx, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	(%rdx), %rbp
	movq	(%r8), %r8
	movq	(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rdi), %rcx
	leaq	2(%rbp), %r10
	leaq	3(%rbp), %rdx
	movq	(%rsi), %rsi
	movq	(%rcx), %rax
	movq	8(%rcx), %rdi
	movq	32(%rax), %rax
	movq	%rdx, (%rbx)
	movq	%r8, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r12, %r8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB96_3
# %bb.1:                                # %assertion.success_0
	incq	%rbp
	movq	(%r12), %r8
	movq	(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	8(%r13), %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	32(%rcx), %r10
	movq	%rdx, (%rbx)
	movq	%r8, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB96_3
# %bb.2:                                # %assertion.success_1
	xorl	%eax, %eax
.LBB96_3:                               # %assertion.failed_0
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end96:
	.size	__anon_obj_compute_roots_428, .Lfunc_end96-__anon_obj_compute_roots_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_428 # -- Begin function __anon_obj_event_handler_428
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_428,@function
__anon_obj_event_handler_428:           # @__anon_obj_event_handler_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
	.cfi_def_cfa_offset 128
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	176(%rsp), %r11
	movq	168(%rsp), %rdx
	movq	160(%rsp), %r14
	movq	152(%rsp), %r15
	movq	136(%rsp), %rcx
	movq	128(%rsp), %rax
	movb	(%rax), %al
	movb	%al, (%rsp)             # 1-byte Spill
	movb	(%rcx), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movq	(%rdi), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	40(%rsi), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax
	movq	(%r13), %r10
	movq	%r9, %rcx
	movq	(%r9), %rbp
	movq	(%rax), %rax
	movq	(%r15), %rbx
	movq	(%r14), %r8
	movq	(%rdx), %rsi
	movq	%r11, %r9
	movq	(%r11), %r11
	leaq	3(%r10), %r12
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%r12, (%r13)
	movq	%rbp, (%rcx)
	movq	%rcx, %r12
	movq	128(%rsp), %rbp
	movb	(%rsp), %cl             # 1-byte Reload
	movb	%cl, (%rbp)
	movq	136(%rsp), %rcx
	movb	16(%rsp), %dl           # 1-byte Reload
	movb	%dl, (%rcx)
	addq	$3, %rax
	movq	144(%rsp), %rbp
	movq	%rax, (%rbp)
	incq	%rbx
	movq	%rbx, (%r15)
	movq	%r15, %rbp
	movq	%r8, (%r14)
	movq	%r14, %r15
	addq	$3, %rsi
	movq	168(%rsp), %r14
	movq	%rsi, (%r14)
	movq	%r11, (%r9)
	movq	%r9, %r11
	leaq	2(%r10), %rax
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%r13, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %rbx
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	movq	%r10, 24(%rsp)          # 8-byte Spill
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	216(%rsp), %rax
	movq	%rax, %r14
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	216(%rsp), %rax
	movq	%rax, %r15
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	216(%rsp), %rax
	movq	%rax, %rbp
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*120(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB97_3
# %bb.1:                                # %assertion.success_0
	movq	(%rbx), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movb	(%rbp), %r13b
	movb	(%r15), %r15b
	movq	(%r14), %rbp
	movq	152(%rsp), %rax
	movq	(%rax), %r12
	movq	160(%rsp), %rax
	movq	(%rax), %r8
	movq	168(%rsp), %rax
	movq	(%rax), %r9
	movq	176(%rsp), %r14
	movq	(%r14), %r11
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx), %r10
	leaq	1(%r10), %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	8(%rax), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	40(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rcx, (%rdx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	128(%rsp), %rax
	movb	%r13b, (%rax)
	movq	136(%rsp), %rax
	movb	%r15b, (%rax)
	incq	%rbp
	movq	144(%rsp), %rax
	movq	%rbp, (%rax)
	movq	152(%rsp), %r15
	movq	%r12, (%r15)
	movq	160(%rsp), %rax
	movq	%r8, (%rax)
	movq	168(%rsp), %rbp
	movq	%r9, (%rbp)
	movq	%r11, (%r14)
	movq	16(%rsp), %rax          # 8-byte Reload
	incq	%rax
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	%rbx, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*104(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB97_3
# %bb.2:                                # %assertion.success_1
	movq	160(%rsp), %rax
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	168(%rsp), %rcx
	movq	%rcx, %rbx
	movq	(%rcx), %rcx
	movq	176(%rsp), %rdx
	movq	%rdx, %r9
	movq	(%rdx), %rdx
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	(%rbp), %rsi
	addq	$3, %rcx
	movq	152(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%r8)
	movq	%rcx, (%rbx)
	movq	%rdx, (%r9)
	movq	%rsi, (%rbp)
	xorl	%eax, %eax
.LBB97_3:                               # %assertion.failed_0
	addq	$72, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end97:
	.size	__anon_obj_event_handler_428, .Lfunc_end97-__anon_obj_event_handler_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_428 # -- Begin function __anon_obj_set_signal_info_428
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_428,@function
__anon_obj_set_signal_info_428:         # @__anon_obj_set_signal_info_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %rbx
	movl	%ecx, %ebp
	movq	%rdx, %r15
	movq	(%rdx), %r13
	movq	(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %rax
	leaq	2(%r13), %r10
	movl	%ebp, %r12d
	andb	$1, %r12b
	movb	%r12b, (%r8,%r13)
	movq	$n1_i_198_name_429, (%r9,%r13,8)
	movb	%r12b, 1(%r8,%r13)
	movq	$n2_i_202_name_430, 8(%r9,%r13,8)
	leaq	3(%r13), %rcx
	movb	%r12b, 2(%r8,%r13)
	movq	$p1_i_206_name_431, 16(%r9,%r13,8)
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	movq	48(%rdx), %rax
	movq	%rcx, (%r15)
	movq	%r15, %rdx
	movl	%ebp, %ecx
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB98_3
# %bb.1:                                # %assertion.success_0
	incq	%r13
	movq	(%r15), %rax
	leaq	1(%rax), %rcx
	movb	%r12b, (%rbx,%rax)
	movq	$p2_i_210_name_432, (%r14,%rax,8)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	8(%rdx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %r10
	movq	%rcx, (%r15)
	movzbl	%bpl, %ecx
	movq	%r15, %rdx
	movq	%rbx, %r8
	movq	%r14, %r9
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB98_3
# %bb.2:                                # %assertion.success_1
	xorl	%eax, %eax
.LBB98_3:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end98:
	.size	__anon_obj_set_signal_info_428, .Lfunc_end98-__anon_obj_set_signal_info_428
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_428 # -- Begin function __anon_obj_work_space_428
	.p2align	4, 0x90
	.type	__anon_obj_work_space_428,@function
__anon_obj_work_space_428:              # @__anon_obj_work_space_428
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %r13
	movq	(%rdx), %r12
	movq	(%r8), %rax
	movq	(%r9), %rbp
	movq	(%rdi), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %rdx
	movq	(%rcx,%rax,8), %rbx
	incq	%rbx
	cmpq	%rbx, %rbp
	cmovaq	%rbp, %rbx
	leaq	3(%r12), %r8
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	(%rsi), %rsi
	movq	(%rdx), %rbp
	movq	8(%rdx), %rdi
	movq	56(%rbp), %rbp
	movq	%r8, (%r13)
	incq	%rax
	movq	%rax, (%r15)
	movq	%rbx, (%r9)
	leaq	2(%r12), %rax
	movq	%r13, %rdx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%r15, %r8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB99_3
# %bb.1:                                # %assertion.success_0
	incq	%r12
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsi), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rdx, (%r13)
	movq	%rax, (%r15)
	movq	%rcx, (%r14)
	movq	%r13, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB99_3
# %bb.2:                                # %assertion.success_1
	movq	(%r15), %rax
	movq	(%r14), %rcx
	movq	(%r13), %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	(%rsi,%rax,8), %rsi
	incq	%rsi
	cmpq	%rsi, %rcx
	cmovaq	%rcx, %rsi
	incq	%rax
	movq	%rax, (%r15)
	movq	%rsi, (%r14)
	movq	%rdx, (%r13)
	xorl	%eax, %eax
.LBB99_3:                               # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end99:
	.size	__anon_obj_work_space_428, .Lfunc_end99-__anon_obj_work_space_428
	.cfi_endproc
                                        # -- End function
	.globl	parallel_433_fun        # -- Begin function parallel_433_fun
	.p2align	4, 0x90
	.type	parallel_433_fun,@function
parallel_433_fun:                       # @parallel_433_fun
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB100_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
.LBB100_2:                              # %if.exit_0
	movq	%r15, (%rbx)
	movq	%r14, 8(%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB100_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB100_4:                              # %if.exit_1
	movq	$.L__anon_obj_function_record_428, (%rax)
	movq	%rbx, 8(%rax)
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end100:
	.size	parallel_433_fun, .Lfunc_end100-parallel_433_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_434 # -- Begin function __anon_obj_constructor_434
	.p2align	4, 0x90
	.type	__anon_obj_constructor_434,@function
__anon_obj_constructor_434:             # @__anon_obj_constructor_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rax
	movq	144(%rsp), %rdx
	movq	(%rsi), %rbp
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	(%rcx), %r13
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	(%r8), %rbx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	(%r9), %r14
	movq	(%rdx), %r12
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	xorl	%ecx, %ecx
	testb	%cl, %cl
	movq	(%rax), %rax
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r15
	jne	.LBB101_1
# %bb.2:                                # %if.else_0
	movl	$32, %edi
	callq	malloc
	jmp	.LBB101_3
.LBB101_1:
	xorl	%eax, %eax
.LBB101_3:                              # %if.exit_0
	movq	%rax, 32(%rsp)          # 8-byte Spill
	callq	ground_422_fun
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	leaq	2(%rbp), %rcx
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	movq	(%rdx), %r10
	movq	%r15, %rsi
	movq	%rcx, (%r15)
	addq	$2, %r13
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	%r13, (%r15)
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	%rbx, (%r13)
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%r14, (%rbx)
	movq	%rbx, %r9
	movq	144(%rsp), %rcx
	movq	%rcx, %rax
	movq	%r12, (%rcx)
	movq	152(%rsp), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rcx, %r12
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r15, %rcx
	movq	%r13, %r8
	pushq	%rbp
	leaq	1(%rbp), %rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB101_7
# %bb.4:                                # %assertion.success_0
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r15, %rcx
	movq	(%rsi), %r14
	movq	(%r15), %rax
	movq	(%r13), %r15
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %r10
	movq	%r12, %rdi
	movq	144(%rsp), %r12
	movq	(%r12), %rdx
	movq	(%rdi), %r11
	movq	(%rbx), %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	leaq	5(%r14), %rdi
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %r12
	movq	8(%rbp), %r8
	movq	(%r12), %r12
	movq	%rdi, (%rsi)
	addq	$5, %rax
	movq	%rax, (%rcx)
	addq	$2, %r15
	movq	%r15, (%r13)
	movq	%r10, (%r9)
	leaq	1(%r14), %rax
	addq	$5, %rdx
	movq	144(%rsp), %r10
	movq	%rdx, (%r10)
	leaq	3(%r14), %rbp
	movq	152(%rsp), %r15
	movq	%r11, (%r15)
	leaq	4(%r14), %r11
	movq	%r8, %rdi
	movq	%rbx, %rdx
	movq	%r13, %r8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%r12
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB101_7
# %bb.5:                                # %assertion.success_1
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi), %rax
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %rcx
	movq	(%r13), %rbx
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %r11
	movq	144(%rsp), %rdi
	movq	(%rdi), %rbp
	movq	152(%rsp), %rdx
	movq	(%rdx), %r8
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leaq	2(%rax), %r10
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %r15
	movq	8(%rdi), %rdi
	movq	(%r15), %r15
	movq	%r10, (%rsi)
	addq	$2, %rcx
	movq	%rcx, (%r12)
	incq	%rbx
	movq	%rbx, (%r13)
	movq	%r11, (%r9)
	addq	$2, %rbp
	movq	144(%rsp), %rbx
	movq	%rbp, (%rbx)
	movq	152(%rsp), %rbp
	movq	%r8, (%rbp)
	addq	$2, %r14
	movq	%r12, %rcx
	movq	%r13, %r8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	leaq	1(%rax), %rax
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	callq	*%r15
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB101_7
# %bb.6:                                # %assertion.success_2
	movq	%rbx, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	(%rsi), %r15
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r9
	movq	(%r13), %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r10
	movq	(%rbx), %rdi
	movq	%rbp, %r8
	movq	(%rbp), %r11
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %r12
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, 8(%r14)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, 16(%r14)
	movq	%r12, 24(%r14)
	movq	%r15, (%rsi)
	movq	%r14, (%rbp)
	movq	%r9, (%rax)
	incq	%rdx
	movq	%rdx, (%r13)
	movq	%r10, (%rcx)
	addq	$2, %rdi
	movq	144(%rsp), %rax
	movq	%rdi, (%rax)
	movq	%r11, (%r8)
	xorl	%eax, %eax
.LBB101_7:                              # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end101:
	.size	__anon_obj_constructor_434, .Lfunc_end101-__anon_obj_constructor_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_434 # -- Begin function __anon_obj_signature_434
	.p2align	4, 0x90
	.type	__anon_obj_signature_434,@function
__anon_obj_signature_434:               # @__anon_obj_signature_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
	.cfi_def_cfa_offset 112
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	112(%rsp), %r11
	movq	(%rdx), %r14
	movq	(%rcx), %rax
	movq	(%r8), %rcx
	movq	(%r9), %rdx
	movq	(%r11), %rbx
	movq	(%rdi), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	(%rsi), %rdi
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	8(%rbp), %r10
	leaq	2(%r14), %rbp
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rsi
	movq	%rbp, (%r15)
	movq	%rax, (%r13)
	movq	%rcx, (%r8)
	movq	%rdx, (%r9)
	movq	%rbx, (%r11)
	leaq	1(%r14), %rbp
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r9, 24(%rsp)           # 8-byte Spill
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB102_4
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rcx
	movq	(%r12), %rdx
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rbx
	movq	112(%rsp), %r10
	movq	(%r10), %rax
	movq	(%r15), %r11
	leaq	1(%r11), %r8
	movq	%rdx, 8(%rcx)
	movq	%rbx, 16(%rcx)
	movq	%rax, 24(%rcx)
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	%ebp, (%rdx)
	movb	$0, (%rbx)
	movq	$0, (%rax)
	movl	%r11d, 4(%rdx)
	movb	$0, 1(%rbx)
	movq	$0, 8(%rax)
	movl	$2, (%rcx)
	leaq	8(%rdx), %rsi
	leaq	2(%rbx), %rdi
	leaq	16(%rax), %rbp
	movq	%rsi, 40(%rcx)
	leaq	2(%r11), %rsi
	movq	%rdi, 48(%rcx)
	movq	%rbp, 56(%rcx)
	movl	%r14d, 8(%rdx)
	movb	$0, 2(%rbx)
	movq	$0, 16(%rax)
	movl	%r8d, 12(%rdx)
	movq	%r8, %r14
	movb	$0, 3(%rbx)
	movq	$0, 24(%rax)
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movl	%esi, 16(%rdx)
	movb	$0, 4(%rbx)
	movq	$0, 32(%rax)
	movl	$3, 32(%rcx)
	leaq	5(%r11), %r8
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	8(%rbp), %rbp
	movq	%r8, (%r15)
	addq	$64, %rcx
	movq	%rcx, (%r13)
	addq	$20, %rdx
	movq	%rdx, (%r12)
	addq	$5, %rbx
	movq	%rbx, (%r9)
	addq	$40, %rax
	movq	%rax, (%r10)
	leaq	3(%r11), %rbx
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	leaq	4(%r11), %r14
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB102_4
# %bb.2:                                # %assertion.success_1
	movq	%r14, 8(%rsp)           # 8-byte Spill
	movq	(%r13), %rcx
	movq	(%r12), %rdx
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rbp
	movq	112(%rsp), %r11
	movq	(%r11), %rax
	movq	(%r15), %r10
	leaq	1(%r10), %r14
	movq	%rdx, 8(%rcx)
	movq	%rbp, 16(%rcx)
	movq	%rax, 24(%rcx)
	movl	%ebx, (%rdx)
	movb	$0, (%rbp)
	movq	$0, (%rax)
	movl	%r10d, 4(%rdx)
	movb	$0, 1(%rbp)
	movq	$0, 8(%rax)
	movl	$2, (%rcx)
	addq	$32, %rcx
	addq	$8, %rdx
	addq	$2, %rbp
	addq	$16, %rax
	leaq	2(%r10), %r8
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsi), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	8(%rbx), %rbx
	movq	%r8, (%r15)
	movq	%rcx, (%r13)
	movq	%rdx, (%r12)
	movq	%rbp, (%r9)
	movq	%rax, (%r11)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	pushq	40(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB102_4
# %bb.3:                                # %assertion.success_2
	movq	(%r13), %rax
	movq	(%r12), %rcx
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rdx
	movq	112(%rsp), %rsi
	movq	%rsi, %r8
	movq	(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rsi, 24(%rax)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%ebx, (%rcx)
	movb	$0, (%rdx)
	movq	$0, (%rsi)
	movl	%r14d, 4(%rcx)
	movb	$0, 1(%rdx)
	movq	$0, 8(%rsi)
	movl	$2, (%rax)
	addq	$32, %rax
	addq	$8, %rcx
	addq	$2, %rdx
	addq	$16, %rsi
	movq	%rax, (%r13)
	movq	%rcx, (%r12)
	movq	%rdx, (%rbp)
	movq	%rsi, (%r8)
	movq	%rdi, (%r15)
	xorl	%eax, %eax
.LBB102_4:                              # %assertion.failed_0
	addq	$56, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end102:
	.size	__anon_obj_signature_434, .Lfunc_end102-__anon_obj_signature_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_434 # -- Begin function __anon_obj_residual_434
	.p2align	4, 0x90
	.type	__anon_obj_residual_434,@function
__anon_obj_residual_434:                # @__anon_obj_residual_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
	.cfi_def_cfa_offset 128
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	152(%rsp), %r11
	movq	144(%rsp), %r10
	movq	(%rdx), %r13
	movq	(%r10), %rax
	movq	(%r11), %r9
	movq	(%rdi), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	2(%r13), %rdx
	movq	(%rsi), %rsi
	movq	(%rsi), %rbp
	movq	8(%rsi), %rdi
	movq	16(%rbp), %rbp
	movq	8(%r14), %rsi
	movq	%rdx, (%rbx)
	movq	%rax, (%r10)
	movq	%r9, (%r11)
	movq	128(%rsp), %rax
	leaq	1(%r13), %r12
	movq	%rbx, %rdx
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movq	%r15, %r9
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB103_27
# %bb.1:                                # %assertion.success_0
	movq	128(%rsp), %rbp
	movl	44(%rsp), %r10d         # 4-byte Reload
	movl	4(%rsp), %r11d          # 4-byte Reload
	movq	%r12, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	152(%rsp), %rax
	movq	(%rax), %r8
	movq	(%r15,%r8,8), %rdx
	cmpq	$11, %rdx
	jae	.LBB103_2
# %bb.3:                                # %assertion.success_1
	movq	144(%rsp), %r9
	movq	(%r9), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rbx
	movq	(%rbp,%rbx,8), %rax
	movq	8(%rbp,%r13,8), %rsi
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	subsd	(%rsi), %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	testq	%rdx, %rdx
	je	.LBB103_7
# %bb.4:                                # %if.else_0
	incq	%rdx
	movl	$1, %edi
	cmpq	%rdx, %rdi
	jae	.LBB103_7
	.p2align	4, 0x90
.LBB103_6:                              # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rax,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	subsd	(%rsi,%rdi,8), %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	incq	%rdi
	cmpq	%rdx, %rdi
	jb	.LBB103_6
.LBB103_7:                              # %if.exit_0
	movq	8(%r15,%r8,8), %rdx
	cmpq	$11, %rdx
	jae	.LBB103_2
# %bb.8:                                # %assertion.success_2
	leaq	1(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	incq	%r8
	leaq	2(%rax), %r14
	movq	8(%rbp,%rbx,8), %rsi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	16(%rbp,%rbx,8), %rdi
	movq	(%rbp,%r13,8), %rax
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rdi), %xmm0
	movq	136(%rsp), %rbx
	movsd	%xmm0, (%rbx)
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	testq	%rdx, %rdx
	je	.LBB103_9
# %bb.10:                               # %if.else_1
	incq	%rdx
	movl	$1, %ebp
	movl	%r11d, %r9d
	movq	64(%rsp), %r13          # 8-byte Reload
	cmpq	%rdx, %rbp
	jae	.LBB103_12
	.p2align	4, 0x90
.LBB103_28:                             # %for.step_1
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rdi,%rbp,8), %xmm0
	movsd	%xmm0, (%rbx,%rbp,8)
	addsd	(%rax,%rbp,8), %xmm0
	movsd	%xmm0, (%rcx)
	addq	$8, %rcx
	incq	%rbp
	cmpq	%rdx, %rbp
	jb	.LBB103_28
.LBB103_12:
	movq	%r15, %r12
	jmp	.LBB103_13
.LBB103_9:
	movq	%r15, %r12
	movl	%r11d, %r9d
	movq	64(%rsp), %r13          # 8-byte Reload
.LBB103_13:                             # %if.exit_1
	incq	%r8
	leaq	2(%r14), %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rsi
	movq	(%r13), %rdx
	movq	8(%r13), %rdi
	movq	16(%rdx), %r11
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rdx)
	movq	144(%rsp), %rax
	movq	%rcx, (%rax)
	movq	152(%rsp), %r13
	movq	%r8, (%r13)
	movzbl	%r10b, %r15d
	movzbl	%r9b, %ebp
	movl	%r15d, %ecx
	movl	%ebp, %r8d
	movq	%rax, %r10
	movq	%r12, %r9
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	leaq	1(%r14), %rax
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rbx
	callq	*%r11
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB103_27
# %bb.14:                               # %assertion.success_3
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	movq	(%rbx), %r8
	movq	(%r12,%r8,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB103_2
# %bb.15:                               # %assertion.success_4
	movl	%r15d, %r11d
	movq	%r12, %rbx
	incq	16(%rsp)                # 8-byte Folded Spill
	movq	144(%rsp), %r9
	movq	(%r9), %rdx
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	(%r12), %r13
	leaq	1(%r13), %r10
	movq	128(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	24(%rax,%rcx,8), %rdi
	movq	(%rax,%r13,8), %rax
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rax), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB103_16
# %bb.23:                               # %if.else_2
	incq	%rsi
	movl	$1, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	cmpq	%rsi, %rcx
	jae	.LBB103_17
	.p2align	4, 0x90
.LBB103_25:                             # %for.step_2
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rax,%rcx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB103_25
	jmp	.LBB103_17
.LBB103_16:
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB103_17:                             # %if.exit_2
	incq	%r8
	leaq	1(%r10), %rax
	movq	24(%r14), %rsi
	movq	(%rbp), %rcx
	movq	8(%rbp), %rdi
	movq	16(%rcx), %rbp
	movq	%rax, (%r12)
	movq	%rdx, (%r9)
	movq	152(%rsp), %rax
	movq	%r8, (%rax)
	movq	%r12, %rdx
	movl	%r11d, %ecx
	movl	4(%rsp), %r8d           # 4-byte Reload
	movq	%r9, %r11
	movq	%rbx, %r9
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	184(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%rax, %r15
	callq	*%rbp
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB103_27
# %bb.18:                               # %assertion.success_5
	movq	(%r15), %rax
	movq	(%rbx,%rax,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB103_2
# %bb.19:                               # %assertion.success_6
	movq	%r15, %r9
	movq	144(%rsp), %rcx
	movq	(%rcx), %rdx
	movq	(%r12), %r8
	movq	128(%rsp), %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	32(%rcx,%rdi,8), %rdi
	movq	8(%rcx,%r13,8), %rbp
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rbp), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	testq	%rsi, %rsi
	je	.LBB103_26
# %bb.20:                               # %if.else_3
	incq	%rsi
	movl	$1, %ecx
	cmpq	%rsi, %rcx
	jae	.LBB103_26
.LBB103_22:                             # %for.step_3
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rbp,%rcx,8), %xmm0
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	incq	%rcx
	cmpq	%rsi, %rcx
	jb	.LBB103_22
.LBB103_26:                             # %if.exit_3
	incq	%rax
	movq	144(%rsp), %rcx
	movq	%rdx, (%rcx)
	movq	%rax, (%r9)
	movq	%r8, (%r12)
	xorl	%eax, %eax
	jmp	.LBB103_27
.LBB103_2:                              # %assertion.failed_1
	movl	$-2, %eax
.LBB103_27:                             # %assertion.failed_0
	addq	$72, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end103:
	.size	__anon_obj_residual_434, .Lfunc_end103-__anon_obj_residual_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_434 # -- Begin function __anon_obj_root_setup_434
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_434,@function
__anon_obj_root_setup_434:              # @__anon_obj_root_setup_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	pushq	%rax
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	(%rdx), %r9
	movq	(%rcx), %rax
	movq	(%rdi), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	8(%rdi), %r13
	leaq	1(%r9), %r15
	leaq	2(%r9), %rcx
	movq	(%rsi), %rdx
	movq	8(%rsi), %rsi
	movq	(%rdx), %rbp
	movq	8(%rdx), %rdi
	movq	24(%rbp), %rbp
	movq	%rcx, (%rbx)
	movq	%rax, (%r14)
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	*%rbp
	testl	%eax, %eax
	jne	.LBB104_4
# %bb.1:                                # %assertion.success_0
	movq	(%r14), %rax
	movq	(%rbx), %rbp
	leaq	1(%rbp), %r10
	leaq	3(%rbp), %r9
	leaq	4(%rbp), %r8
	leaq	5(%rbp), %rcx
	movq	16(%r12), %rsi
	movq	(%r13), %rdx
	movq	8(%r13), %rdi
	movq	24(%rdx), %r11
	movq	%rcx, (%rbx)
	movq	%rax, (%r14)
	movq	%rbx, %rdx
	movq	%r14, %rcx
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%r11
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB104_4
# %bb.2:                                # %assertion.success_1
	addq	$2, %rbp
	movq	(%r14), %rax
	movq	(%rbx), %r9
	leaq	1(%r9), %r8
	leaq	2(%r9), %rcx
	movq	24(%r12), %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %r10
	movq	%rcx, (%rbx)
	movq	%rax, (%r14)
	movq	%rbx, %rdx
	movq	%r14, %rcx
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB104_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB104_4:                              # %assertion.failed_0
	addq	$8, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end104:
	.size	__anon_obj_root_setup_434, .Lfunc_end104-__anon_obj_root_setup_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_434 # -- Begin function __anon_obj_compute_roots_434
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_434,@function
__anon_obj_compute_roots_434:           # @__anon_obj_compute_roots_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	(%rdx), %rax
	movq	(%r8), %rbx
	movq	(%rdi), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	8(%rdi), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	1(%rax), %r14
	leaq	2(%rax), %rdx
	movq	(%rsi), %rdi
	movq	8(%rsi), %rsi
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	32(%rbp), %rbp
	movq	%rdx, (%r12)
	movq	%rbx, (%r8)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r14, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB105_4
# %bb.1:                                # %assertion.success_0
	movq	(%r15), %rax
	movq	(%r12), %rbp
	leaq	1(%rbp), %r10
	leaq	3(%rbp), %r11
	leaq	4(%rbp), %r9
	leaq	5(%rbp), %rcx
	movq	16(%r13), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	32(%rdx), %rbx
	movq	%rcx, (%r12)
	movq	%rax, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r15, %r8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB105_4
# %bb.2:                                # %assertion.success_1
	addq	$2, %rbp
	movq	(%r15), %r8
	movq	(%r12), %rax
	leaq	1(%rax), %r9
	leaq	2(%rax), %rdx
	movq	24(%r13), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	32(%rcx), %r10
	movq	%rdx, (%r12)
	movq	%r8, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r15, %r8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB105_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB105_4:                              # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end105:
	.size	__anon_obj_compute_roots_434, .Lfunc_end105-__anon_obj_compute_roots_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_434 # -- Begin function __anon_obj_event_handler_434
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_434,@function
__anon_obj_event_handler_434:           # @__anon_obj_event_handler_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	movq	192(%rsp), %r8
	movq	184(%rsp), %r11
	movq	176(%rsp), %r10
	movq	152(%rsp), %rsi
	movq	144(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 8(%rsp)            # 1-byte Spill
	movb	(%rsi), %al
	movb	%al, 24(%rsp)           # 1-byte Spill
	movq	(%rdi), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	(%rcx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %r14
	movq	40(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	168(%rsp), %rsi
	movq	160(%rsp), %rax
	movq	(%rdx), %rbp
	movq	(%r9), %rbx
	movq	(%rax), %rax
	movq	(%rsi), %rsi
	movq	(%r10), %r15
	movq	%r11, %rdi
	movq	(%r11), %r11
	movq	(%r8), %r12
	leaq	2(%rbp), %r13
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	8(%rcx), %r10
	movq	%r13, (%rdx)
	movq	%rbx, (%r9)
	movq	144(%rsp), %rbx
	movb	8(%rsp), %cl            # 1-byte Reload
	movb	%cl, (%rbx)
	movq	152(%rsp), %rcx
	movb	24(%rsp), %bl           # 1-byte Reload
	movb	%bl, (%rcx)
	addq	$2, %rax
	movq	160(%rsp), %rbx
	movq	%rax, (%rbx)
	movq	168(%rsp), %r13
	movq	%rsi, (%r13)
	movq	176(%rsp), %rax
	movq	%r15, (%rax)
	movq	%r11, (%rdi)
	movq	%rdi, %r11
	movq	%r12, (%r8)
	movq	%r8, %r15
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdi
	movq	%r10, %rsi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	%r9, 32(%rsp)           # 8-byte Spill
	pushq	%rbp
	leaq	1(%rbp), %rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*96(%rsp)               # 8-byte Folded Reload
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB106_4
# %bb.1:                                # %assertion.success_0
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax
	movb	(%rax), %r13b
	movq	152(%rsp), %rax
	movb	(%rax), %r15b
	movq	160(%rsp), %rax
	movq	(%rax), %rbp
	movq	168(%rsp), %rax
	movq	(%rax), %rax
	movq	176(%rsp), %rcx
	movq	(%rcx), %r8
	movq	184(%rsp), %rcx
	movq	(%rcx), %rcx
	movq	192(%rsp), %r14
	movq	(%r14), %r11
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	(%rdx), %r12
	leaq	5(%r12), %r14
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %r10
	movq	8(%rdi), %rdi
	movq	40(%r10), %rbx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r14, (%rdx)
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, (%r9)
	movq	144(%rsp), %rbx
	movb	%r13b, (%rbx)
	movq	152(%rsp), %rbx
	movb	%r15b, (%rbx)
	addq	$5, %rbp
	movq	160(%rsp), %rbx
	movq	%rbp, (%rbx)
	addq	$2, %rax
	movq	168(%rsp), %r13
	movq	%rax, (%r13)
	movq	176(%rsp), %r15
	movq	%r8, (%r15)
	leaq	1(%r12), %rax
	addq	$5, %rcx
	movq	184(%rsp), %r14
	movq	%rcx, (%r14)
	leaq	3(%r12), %rbx
	movq	192(%rsp), %r10
	movq	%r11, (%r10)
	leaq	4(%r12), %rbp
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*112(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB106_4
# %bb.2:                                # %assertion.success_1
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	144(%rsp), %rax
	movb	(%rax), %r11b
	movq	152(%rsp), %rax
	movb	(%rax), %r15b
	movq	160(%rsp), %rax
	movq	(%rax), %rbp
	movq	168(%rsp), %rax
	movq	(%rax), %rcx
	movq	176(%rsp), %rax
	movq	(%rax), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rax
	movq	(%rax), %rdx
	movq	192(%rsp), %r14
	movq	(%r14), %r13
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r10
	leaq	2(%r10), %rsi
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	24(%rdi), %r8
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	40(%rbx), %rbx
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, (%rax)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, (%r9)
	movq	144(%rsp), %rsi
	movb	%r11b, (%rsi)
	movq	152(%rsp), %rsi
	movb	%r15b, (%rsi)
	addq	$2, %rbp
	movq	160(%rsp), %r15
	movq	%rbp, (%r15)
	incq	%rcx
	movq	168(%rsp), %rbp
	movq	%rcx, (%rbp)
	movq	176(%rsp), %rbx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rbx)
	addq	$2, %rdx
	movq	184(%rsp), %r11
	movq	%rdx, (%r11)
	movq	%r13, (%r14)
	addq	$2, %r12
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r8, %rsi
	movq	%rax, %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	leaq	1(%r10), %rax
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*112(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB106_4
# %bb.3:                                # %assertion.success_2
	movq	176(%rsp), %rax
	movq	%rax, %r8
	movq	(%rax), %rax
	movq	184(%rsp), %rcx
	movq	%rcx, %rbx
	movq	(%rcx), %rcx
	movq	192(%rsp), %rdx
	movq	%rdx, %r9
	movq	(%rdx), %rdx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rbp), %rsi
	addq	$2, %rcx
	movq	168(%rsp), %rdi
	incq	(%rdi)
	movq	%rax, (%r8)
	movq	%rcx, (%rbx)
	movq	%rdx, (%r9)
	movq	%rsi, (%rbp)
	xorl	%eax, %eax
.LBB106_4:                              # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end106:
	.size	__anon_obj_event_handler_434, .Lfunc_end106-__anon_obj_event_handler_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_434 # -- Begin function __anon_obj_set_signal_info_434
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_434,@function
__anon_obj_set_signal_info_434:         # @__anon_obj_set_signal_info_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	(%rdx), %rax
	movq	(%rdi), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rsi), %rdi
	leaq	2(%rax), %rdx
	movl	%ecx, %r13d
	andb	$1, %r13b
	movb	%r13b, (%r8,%rax)
	movq	$gp_i_226_name_435, (%r9,%rax,8)
	movb	%r13b, 1(%r8,%rax)
	movq	$gp_v_228_name_436, 8(%r9,%rax,8)
	movq	8(%rsi), %rsi
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	48(%rbp), %r10
	movq	%rdx, (%rbx)
	movq	%rbx, %rdx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	pushq	%rax
	leaq	1(%rax), %rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB107_4
# %bb.1:                                # %assertion.success_0
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r12, %rax
	movq	%rbx, %rdx
	movq	(%rbx), %r12
	leaq	1(%r12), %r11
	movb	%r13b, (%r15,%r12)
	movq	$n1_v_232_name_437, (%r14,%r12,8)
	leaq	3(%r12), %r10
	movb	%r13b, 1(%r15,%r12)
	movq	$n1_i_230_name_438, 8(%r14,%r12,8)
	movb	%r13b, 2(%r15,%r12)
	movq	$n2_i_234_name_439, 16(%r14,%r12,8)
	leaq	4(%r12), %rbp
	leaq	5(%r12), %rcx
	movb	%r13b, 3(%r15,%r12)
	movq	$p1_i_238_name_440, 24(%r14,%r12,8)
	movb	%r13b, 4(%r15,%r12)
	movq	$p1_v_240_name_441, 32(%r14,%r12,8)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rax), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	48(%rax), %rax
	movq	%rcx, (%rbx)
	movzbl	4(%rsp), %ecx           # 1-byte Folded Reload
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB107_4
# %bb.2:                                # %assertion.success_1
	addq	$2, %r12
	movq	(%rbx), %rax
	movb	%r13b, (%r15,%rax)
	movq	$p2_i_242_name_442, (%r14,%rax,8)
	leaq	2(%rax), %rcx
	movb	%r13b, 1(%r15,%rax)
	movq	$p2_v_244_name_443, 8(%r14,%rax,8)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsi), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	48(%rbp), %rbp
	movq	%rcx, (%rbx)
	movq	%rbx, %rdx
	movl	4(%rsp), %ecx           # 4-byte Reload
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	leaq	1(%rax), %rax
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB107_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB107_4:                              # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end107:
	.size	__anon_obj_set_signal_info_434, .Lfunc_end107-__anon_obj_set_signal_info_434
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_434 # -- Begin function __anon_obj_work_space_434
	.p2align	4, 0x90
	.type	__anon_obj_work_space_434,@function
__anon_obj_work_space_434:              # @__anon_obj_work_space_434
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	(%rdx), %rax
	movq	(%r8), %r8
	movq	(%r9), %rdx
	movq	(%rdi), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	leaq	2(%rax), %rbp
	movq	(%r13), %rdi
	movq	8(%r13), %rsi
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rbp, (%r12)
	movq	%r8, (%r15)
	movq	%rdx, (%r9)
	movq	%r12, %rdx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%r15, %r8
	pushq	%rax
	leaq	1(%rax), %rbp
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB108_4
# %bb.1:                                # %assertion.success_0
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	(%r15), %rax
	movq	(%r14), %rsi
	movq	(%r12), %rbp
	leaq	1(%rbp), %r11
	leaq	3(%rbp), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	8(%rcx,%rax,8), %rdx
	incq	%rdx
	cmpq	%rdx, %rsi
	cmovaq	%rsi, %rdx
	addq	$2, %rax
	leaq	4(%rbp), %r10
	leaq	5(%rbp), %r8
	movq	16(%r13), %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%r8, (%r12)
	movq	%rax, (%r15)
	movq	%rdx, (%r14)
	movq	%r12, %rdx
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	32(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB108_4
# %bb.2:                                # %assertion.success_1
	addq	$2, %rbp
	movq	(%r15), %rax
	movq	(%r14), %r8
	movq	(%r12), %rbx
	incq	%rax
	leaq	2(%rbx), %rdx
	movq	24(%r13), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	56(%rcx), %r10
	movq	%rdx, (%r12)
	movq	%rax, (%r15)
	movq	%r8, (%r14)
	movq	%r12, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%r15, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	16(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	leaq	1(%rbx), %rax
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB108_4
# %bb.3:                                # %assertion.success_2
	movq	(%r14), %rax
	movq	(%r12), %rcx
	incq	(%r15)
	movq	%rax, (%r14)
	movq	%rcx, (%r12)
	xorl	%eax, %eax
.LBB108_4:                              # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end108:
	.size	__anon_obj_work_space_434, .Lfunc_end108-__anon_obj_work_space_434
	.cfi_endproc
                                        # -- End function
	.globl	grounded_444_fun        # -- Begin function grounded_444_fun
	.p2align	4, 0x90
	.type	grounded_444_fun,@function
grounded_444_fun:                       # @grounded_444_fun
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%rbx
	.cfi_def_cfa_offset 32
	.cfi_offset %rbx, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	testb	%bl, %bl
	jne	.LBB109_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
.LBB109_2:                              # %if.exit_0
	movq	%r15, (%rbx)
	movq	%r14, 8(%rbx)
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB109_4
# %bb.3:                                # %if.else_1
	movl	$16, %edi
	callq	malloc
.LBB109_4:                              # %if.exit_1
	movq	$.L__anon_obj_function_record_434, (%rax)
	movq	%rbx, 8(%rax)
	popq	%rbx
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end109:
	.size	grounded_444_fun, .Lfunc_end109-grounded_444_fun
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_constructor_445 # -- Begin function __anon_obj_constructor_445
	.p2align	4, 0x90
	.type	__anon_obj_constructor_445,@function
__anon_obj_constructor_445:             # @__anon_obj_constructor_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
	.cfi_def_cfa_offset 160
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	168(%rsp), %rax
	movq	160(%rsp), %rdx
	movq	(%rsi), %r13
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	(%rcx), %rbp
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	(%r8), %r15
	xorl	%ecx, %ecx
	testb	%cl, %cl
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	(%r9), %rdi
	movq	(%rdx), %rcx
	movq	(%rax), %rax
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	jne	.LBB110_1
# %bb.2:                                # %if.else_0
	movl	$80, %edi
	callq	malloc
	movq	%rax, %r14
	jmp	.LBB110_3
.LBB110_1:
	xorl	%r14d, %r14d
.LBB110_3:                              # %if.exit_0
	callq	intensity_402_fun
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rax, (%r14)
	callq	tension_400_fun
	movq	%rax, %r12
	movq	%rax, 8(%r14)
	callq	two_pin_404_fun
	movq	%rax, 16(%r14)
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	(%rcx), %rax
	movq	%r13, (%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, (%rcx)
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%r15, (%r13)
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%r15)
	movq	%rcx, %rbp
	movq	160(%rsp), %rcx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rcx, %r10
	movq	168(%rsp), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rdx, (%rcx)
	movq	%rcx, %r11
	movq	%rbx, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movq	%r13, %r8
	movq	%r15, %r9
	movq	200(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	200(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB110_13
# %bb.4:                                # %assertion.success_0
	movq	%r12, (%rsp)            # 8-byte Spill
	movq	(%rbx), %rax
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	(%r10), %rcx
	movq	(%r13), %r8
	movq	(%r15), %r9
	movq	160(%rsp), %r12
	movq	(%r12), %rbp
	movq	168(%rsp), %rdx
	movq	(%rdx), %r11
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdi
	movq	%rdi, 24(%r14)
	leaq	1(%rax), %rdx
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rdx, (%rbx)
	incq	%rcx
	movq	%rcx, (%r10)
	movq	%r8, (%r13)
	movq	%r9, (%r15)
	movq	%rbp, (%r12)
	movq	%r12, %rbp
	movq	168(%rsp), %rcx
	movq	%r11, (%rcx)
	movq	24(%rsp), %r12          # 8-byte Reload
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r10, %rcx
	movq	%r13, %r8
	movq	%r15, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*72(%rsp)               # 8-byte Folded Reload
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB110_13
# %bb.5:                                # %assertion.success_1
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%rbx), %rbx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %r14
	movq	%r15, %rax
	movq	(%r13), %r15
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	168(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	(%r12), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movb	$1, %r13b
	testb	%r13b, %r13b
	jne	.LBB110_6
# %bb.7:                                # %if.else_1
	xorl	%edi, %edi
	callq	malloc
	jmp	.LBB110_8
.LBB110_6:
	xorl	%eax, %eax
.LBB110_8:                              # %if.exit_1
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbx, (%rsp)            # 8-byte Spill
	leaq	1(%rbx), %r12
	incq	%r14
	testb	%r13b, %r13b
	jne	.LBB110_9
# %bb.10:                               # %if.else_2
	xorl	%edi, %edi
	callq	malloc
	jmp	.LBB110_11
.LBB110_9:
	xorl	%eax, %eax
.LBB110_11:                             # %if.exit_2
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	160(%rsp), %rcx
	incq	%r15
	movq	32(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movq	96(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movq	(%rbp), %rax
	movq	8(%rbp), %rdi
	movq	(%rax), %r10
	movq	8(%rsp), %r13           # 8-byte Reload
	movq	%r12, (%r13)
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	%r14, (%r12)
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	%r15, (%r14)
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, (%r15)
	movq	%rsi, (%rcx)
	movq	%rcx, %rax
	movq	168(%rsp), %rbp
	movq	%rbx, (%rbp)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rsi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%r15, %r9
	pushq	8(%rsp)                 # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	216(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	%rax, %rbp
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB110_13
# %bb.12:                               # %assertion.success_2
	movq	(%r13), %rax
	movq	(%r12), %r8
	movq	(%r14), %r9
	movq	(%r15), %r11
	movq	(%rbp), %rdi
	movq	168(%rsp), %rcx
	movq	(%rcx), %r10
	movq	(%rbx), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 32(%rsi)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 56(%rsi)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 64(%rsi)
	movb	$1, %cl
	movb	%cl, 48(%rsi)
	movq	$0, 40(%rsi)
	movq	%rdx, 72(%rsi)
	movq	%rax, (%r13)
	movq	%rsi, (%rbx)
	movq	%r8, (%r12)
	movq	%r9, (%r14)
	movq	%r11, (%r15)
	movq	%rdi, (%rbp)
	movq	168(%rsp), %rax
	movq	%r10, (%rax)
	xorl	%eax, %eax
.LBB110_13:                             # %assertion.failed_0
	addq	$104, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end110:
	.size	__anon_obj_constructor_445, .Lfunc_end110-__anon_obj_constructor_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_signature_445 # -- Begin function __anon_obj_signature_445
	.p2align	4, 0x90
	.type	__anon_obj_signature_445,@function
__anon_obj_signature_445:               # @__anon_obj_signature_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*8(%rbp)
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB111_7
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %r8
	movq	(%r12), %r9
	movq	(%r15), %rdx
	movq	80(%rsp), %r10
	movq	(%r10), %r11
	movq	(%r14), %rbp
	leaq	1(%rbp), %rax
	movq	32(%rbx), %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	8(%rcx), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rax, (%r14)
	movq	%r8, (%r13)
	movq	%r9, (%r12)
	movq	%rdx, (%r15)
	movq	%r11, (%r10)
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r15, %r9
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%r10, %rbp
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*48(%rsp)               # 8-byte Folded Reload
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB111_7
# %bb.2:                                # %assertion.success_1
	movq	%rbp, %r11
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	(%r13), %rax
	movq	(%r12), %r14
	movq	(%r15), %r13
	movq	(%rbp), %r12
	movq	(%rdx), %r10
	leaq	1(%r10), %r9
	cmpq	$1, 40(%rbx)
	je	.LBB111_4
# %bb.3:                                # %switch.branch_0
	movq	%r14, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%r12, 24(%rax)
	movl	%r10d, (%r14)
	jmp	.LBB111_5
.LBB111_4:                              # %switch.branch_1
	movq	%r14, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%r12, 24(%rax)
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%esi, (%r14)
.LBB111_5:                              # %switch.exit_0
	movb	$0, (%r13)
	movq	$0, (%r12)
	addq	$4, %r14
	incq	%r13
	addq	$8, %r12
	movl	$1, (%rax)
	addq	$32, %rax
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	72(%rbx), %rsi
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	8(%rbp), %rbp
	movq	%r9, (%rdx)
	movq	%rax, (%rcx)
	movq	%r14, (%r8)
	movq	%r13, (%r15)
	movq	%r12, (%r11)
	movq	%r15, %r9
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB111_7
# %bb.6:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB111_7:                              # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end111:
	.size	__anon_obj_signature_445, .Lfunc_end111-__anon_obj_signature_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_residual_445 # -- Begin function __anon_obj_residual_445
	.p2align	4, 0x90
	.type	__anon_obj_residual_445,@function
__anon_obj_residual_445:                # @__anon_obj_residual_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %ebx
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	16(%rsi), %rax
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rsi), %rsi
	movq	%r9, 32(%rsp)           # 8-byte Spill
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	152(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*16(%rbp)
	addq	$64, %rsp
	.cfi_adjust_cfa_offset -64
	testl	%eax, %eax
	jne	.LBB112_11
# %bb.1:                                # %assertion.success_0
	movq	112(%rsp), %rax
	movq	%rax, %rcx
	movq	(%rax), %rax
	movq	%rcx, %r8
	movq	120(%rsp), %rcx
	movq	%rcx, %r10
	movq	(%rcx), %rcx
	movq	(%r15), %r11
	leaq	1(%r11), %rdx
	movq	32(%r12), %rsi
	movq	(%r14), %rbp
	movq	8(%r14), %rdi
	movq	16(%rbp), %rbp
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rdx, (%r15)
	movq	%rax, (%r8)
	movq	%r8, %rax
	movq	%rcx, (%r10)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movzbl	%bl, %r14d
	movzbl	%r13b, %ebp
	movq	%r15, %rdx
	movl	%r14d, %ecx
	movl	%ebp, %r8d
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	movq	%r11, 24(%rsp)          # 8-byte Spill
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	168(%rsp), %r13
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	callq	*104(%rsp)              # 8-byte Folded Reload
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	movq	112(%rsp), %r11
	jne	.LBB112_11
# %bb.2:                                # %assertion.success_1
	movq	%r13, %rdi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	movq	%rbx, %r9
	movq	(%r11), %rbx
	movq	120(%rsp), %rax
	movq	(%rax), %r13
	movq	(%r15), %r10
	cmpq	$1, 40(%r12)
	movq	%r15, %rdx
	je	.LBB112_12
# %bb.3:                                # %switch.branch_0
	movq	(%r9,%r13,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB112_4
# %bb.5:                                # %assertion.success_2
	movq	(%rdi,%r10,8), %rdi
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rbx)
	addq	$8, %rbx
	testq	%rsi, %rsi
	je	.LBB112_8
# %bb.6:                                # %if.else_0
	incq	%rsi
	movl	$1, %ebp
	cmpq	%rsi, %rbp
	jae	.LBB112_8
	.p2align	4, 0x90
.LBB112_18:                             # %for.step_0
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, (%rbx)
	addq	$8, %rbx
	incq	%rbp
	cmpq	%rsi, %rbp
	jb	.LBB112_18
.LBB112_8:
	movq	%rax, %r15
	incq	%r13
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB112_9
.LBB112_12:                             # %switch.branch_1
	movq	(%r9,%r13,8), %rsi
	cmpq	$11, %rsi
	jae	.LBB112_4
# %bb.13:                               # %assertion.success_3
	movq	(%rdi,%rcx,8), %rdi
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rbx)
	addq	$8, %rbx
	testq	%rsi, %rsi
	je	.LBB112_14
# %bb.15:                               # %if.else_1
	incq	%rsi
	movl	$1, %ebp
	cmpq	%rsi, %rbp
	jae	.LBB112_14
	.p2align	4, 0x90
.LBB112_17:                             # %for.step_1
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rdi,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	%xmm0, (%rbx)
	addq	$8, %rbx
	incq	%rbp
	cmpq	%rsi, %rbp
	jb	.LBB112_17
.LBB112_14:
	movq	%rax, %r15
	movq	8(%rsp), %rdi           # 8-byte Reload
	incq	%r13
.LBB112_9:                              # %switch.exit_0
	leaq	1(%r10), %rbp
	movq	72(%r12), %rsi
	movq	(%rdi), %rax
	movq	8(%rdi), %rdi
	movq	16(%rax), %rax
	movq	%rbp, (%rdx)
	movq	%rbx, (%r11)
	movq	%r13, (%r15)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %ecx
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$80, %rsp
	.cfi_adjust_cfa_offset -80
	testl	%eax, %eax
	jne	.LBB112_11
# %bb.10:                               # %assertion.success_4
	xorl	%eax, %eax
	jmp	.LBB112_11
.LBB112_4:                              # %assertion.failed_2
	movl	$-2, %eax
.LBB112_11:                             # %assertion.failed_0
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end112:
	.size	__anon_obj_residual_445, .Lfunc_end112-__anon_obj_residual_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_root_setup_445 # -- Begin function __anon_obj_root_setup_445
	.p2align	4, 0x90
	.type	__anon_obj_root_setup_445,@function
__anon_obj_root_setup_445:              # @__anon_obj_root_setup_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r12
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	88(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*24(%rbp)
	addq	$16, %rsp
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB113_7
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	(%r14), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	24(%rdx), %r10
	movq	%rcx, (%r14)
	movq	%rax, (%r13)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r15, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	movq	88(%rsp), %r11
	jne	.LBB113_7
# %bb.2:                                # %assertion.success_1
	movq	%rbp, %r10
	movq	(%r13), %rcx
	movq	(%r14), %rax
	leaq	1(%rax), %rdx
	cmpq	$1, 40(%rbx)
	je	.LBB113_4
# %bb.3:                                # %switch.branch_0
	movl	$-1, (%rcx)
	jmp	.LBB113_5
.LBB113_4:                              # %switch.branch_1
	movl	$1, (%rcx)
.LBB113_5:                              # %switch.exit_0
	addq	$4, %rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	72(%rbx), %rsi
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	24(%rbp), %rbp
	movq	%rdx, (%r14)
	movq	%rcx, (%r13)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	movq	%r15, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB113_7
# %bb.6:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB113_7:                              # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end113:
	.size	__anon_obj_root_setup_445, .Lfunc_end113-__anon_obj_root_setup_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_compute_roots_445 # -- Begin function __anon_obj_compute_roots_445
	.p2align	4, 0x90
	.type	__anon_obj_compute_roots_445,@function
__anon_obj_compute_roots_445:           # @__anon_obj_compute_roots_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r13
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	(%rsi), %r14
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*32(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB114_7
# %bb.1:                                # %assertion.success_0
	movq	(%r13), %rax
	movq	(%r12), %rbp
	leaq	1(%rbp), %rcx
	movq	32(%rbx), %rsi
	movq	(%r14), %rdx
	movq	8(%r14), %rdi
	movq	32(%rdx), %r10
	movq	%rcx, (%r12)
	movq	%rax, (%r13)
	movq	%r12, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r13, %r8
	movq	%r15, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	movq	104(%rsp), %r14
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	movq	%r14, %r10
	movq	88(%rsp), %r11
	movq	96(%rsp), %r14
	jne	.LBB114_7
# %bb.2:                                # %assertion.success_1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%r13), %rdx
	movq	(%r12), %rax
	leaq	1(%rax), %r8
	cmpq	$1, 40(%rbx)
	je	.LBB114_4
# %bb.3:                                # %switch.branch_0
	movq	(%rcx,%rbp,8), %rsi
	jmp	.LBB114_5
.LBB114_4:                              # %switch.branch_1
	movq	(%rcx,%rax,8), %rsi
.LBB114_5:                              # %switch.exit_0
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, (%rdx)
	addq	$8, %rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	72(%rbx), %rsi
	movq	(%rdi), %rbp
	movq	8(%rdi), %rdi
	movq	32(%rbp), %rbp
	movq	%r8, (%r12)
	movq	%rdx, (%r13)
	movq	%r12, %rdx
	movq	%r13, %r8
	movq	%r15, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbp
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB114_7
# %bb.6:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB114_7:                              # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end114:
	.size	__anon_obj_compute_roots_445, .Lfunc_end114-__anon_obj_compute_roots_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_event_handler_445 # -- Begin function __anon_obj_event_handler_445
	.p2align	4, 0x90
	.type	__anon_obj_event_handler_445,@function
__anon_obj_event_handler_445:           # @__anon_obj_event_handler_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
	.cfi_def_cfa_offset 144
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	168(%rsp), %r15
	movq	16(%rsi), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rdi
	movq	152(%rsp), %rax
	movq	(%rsi), %rbp
	movq	8(%rsi), %r13
	movq	24(%rsi), %rsi
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r8, 80(%rsp)           # 8-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	%rax, %r12
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*40(%rbx)
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB115_17
# %bb.1:                                # %assertion.success_0
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	80(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rcx
	movq	144(%rsp), %rax
	movb	(%rax), %al
	movb	%al, 40(%rsp)           # 1-byte Spill
	movb	(%r12), %al
	movb	%al, 24(%rsp)           # 1-byte Spill
	movq	160(%rsp), %rax
	movq	(%rax), %rax
	movq	(%r15), %r8
	movq	176(%rsp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rdx
	movq	(%rdx), %r10
	movq	192(%rsp), %rdx
	movq	(%rdx), %r11
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %r15
	leaq	1(%r15), %r12
	movq	32(%r14), %rsi
	movq	(%rbp), %r13
	movq	8(%rbp), %rdi
	movq	40(%r13), %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rbp
	movq	%r12, (%rdx)
	movq	176(%rsp), %r12
	movq	%rcx, (%r9)
	movq	144(%rsp), %rcx
	movb	40(%rsp), %bl           # 1-byte Reload
	movb	%bl, (%rcx)
	movq	152(%rsp), %rcx
	movb	24(%rsp), %bl           # 1-byte Reload
	movb	%bl, (%rcx)
	incq	%rax
	movq	160(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	168(%rsp), %rax
	movq	%r8, (%rax)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r12)
	movq	%r10, (%rbp)
	movq	192(%rsp), %r13
	movq	%r11, (%r13)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	pushq	%r15
	movq	176(%rsp), %rbx
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r12
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %r12
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	232(%rsp), %rbx
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	movq	232(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*144(%rsp)              # 8-byte Folded Reload
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB115_17
# %bb.2:                                # %assertion.success_1
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	(%r15), %rcx
	movq	144(%rsp), %rbp
	movb	(%rbp), %r11b
	movb	(%rdx), %r10b
	movq	(%rbx), %rbp
	movq	(%r12), %rbx
	movq	176(%rsp), %rdx
	movq	(%rdx), %r9
	movq	184(%rsp), %rax
	movq	(%rax), %rax
	movq	(%r13), %r8
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rsi
	incq	%rbp
	leaq	1(%rsi), %rdi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpq	$1, 40(%r14)
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r9, 40(%rsp)           # 8-byte Spill
	je	.LBB115_9
# %bb.3:                                # %switch.branch_0
	incq	%rbx
	incq	%rax
	leaq	4(%rcx), %rdi
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	cmpl	$0, (%rcx)
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB115_4
# %bb.5:                                # %if.else_0
	movb	$1, %r11b
	testb	%r11b, %r11b
	jne	.LBB115_6
# %bb.7:                                # %if.else_1
	xorl	%edi, %edi
	movq	%r8, %rbx
	callq	malloc
	movb	$1, %r11b
	movq	%rbx, %r8
	movq	32(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB115_8
.LBB115_9:                              # %switch.branch_1
	incq	%rbx
	incq	%rax
	cmpl	$0, (%rcx)
	leaq	4(%rcx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB115_4
# %bb.10:                               # %if.else_2
	movb	$1, %r11b
	testb	%r11b, %r11b
	jne	.LBB115_11
# %bb.12:                               # %if.else_3
	xorl	%edi, %edi
	movq	%r8, %rbx
	callq	malloc
	movb	$1, %r11b
	movq	%rbx, %r8
	movq	32(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB115_13
.LBB115_4:                              # %if.then_0
	movb	$0, 48(%r14)
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	176(%rsp), %rbx
	movq	184(%rsp), %r13
	jmp	.LBB115_15
.LBB115_6:
	xorl	%eax, %eax
.LBB115_8:                              # %if.exit_0
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	176(%rsp), %rbx
	movq	184(%rsp), %r13
	incq	%r8
	movq	$1, 40(%r14)
	jmp	.LBB115_14
.LBB115_11:
	xorl	%eax, %eax
.LBB115_13:                             # %if.exit_2
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	176(%rsp), %rbx
	movq	184(%rsp), %r13
	incq	%r8
	movq	$0, 40(%r14)
.LBB115_14:                             # %switch.exit_0
	movb	$1, 48(%r14)
	movq	%rax, 64(%r14)
	movb	$1, %r10b
.LBB115_15:                             # %switch.exit_0
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	72(%r14), %rsi
	movq	(%rdi), %rcx
	movq	8(%rdi), %rdi
	movq	40(%rcx), %r12
	movq	%rax, (%rdx)
	movq	%r15, %r9
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	andb	$1, %r11b
	movq	144(%rsp), %r15
	movb	%r11b, (%r15)
	andb	$1, %r10b
	movq	152(%rsp), %rcx
	movq	%rcx, %r11
	movb	%r10b, (%rcx)
	movq	160(%rsp), %rax
	movq	%rax, %r14
	movq	%rbp, (%rax)
	movq	168(%rsp), %rcx
	movq	%rcx, %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r13)
	movq	192(%rsp), %rcx
	movq	%rcx, %rbp
	movq	%r8, (%rcx)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	pushq	48(%rsp)                # 8-byte Folded Reload
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	232(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r13
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
	.cfi_adjust_cfa_offset 8
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%r14
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	callq	*%r12
	addq	$96, %rsp
	.cfi_adjust_cfa_offset -96
	testl	%eax, %eax
	jne	.LBB115_17
# %bb.16:                               # %assertion.success_2
	xorl	%eax, %eax
.LBB115_17:                             # %assertion.failed_0
	addq	$88, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end115:
	.size	__anon_obj_event_handler_445, .Lfunc_end115-__anon_obj_event_handler_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_set_signal_info_445 # -- Begin function __anon_obj_set_signal_info_445
	.p2align	4, 0x90
	.type	__anon_obj_set_signal_info_445,@function
__anon_obj_set_signal_info_445:         # @__anon_obj_set_signal_info_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %r13
	movl	%ecx, %r14d
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rsi), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*48(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB116_4
# %bb.1:                                # %assertion.success_0
	movq	(%r12), %rax
	leaq	1(%rax), %rbp
	movzbl	%r14b, %ecx
	andb	$1, %r14b
	movb	%r14b, (%r13,%rax)
	movq	$i_256_name_446, (%r15,%rax,8)
	movq	32(%rbx), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %r10
	movq	%rbp, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%r13, %r8
	movq	%r15, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %rax
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%r10
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	movq	%r15, %r9
	movq	88(%rsp), %r11
	movq	96(%rsp), %r15
	jne	.LBB116_4
# %bb.2:                                # %assertion.success_1
	movq	%rbp, %r10
	movq	%r13, %r8
	movl	8(%rsp), %ecx           # 4-byte Reload
	movq	104(%rsp), %rbp
	movq	(%r12), %rax
	leaq	1(%rax), %r13
	movb	%r14b, (%r8,%rax)
	movq	$u_258_name_447, (%r9,%rax,8)
	cmpq	$1, 40(%rbx)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	72(%rbx), %rsi
	movq	(%rdi), %rdx
	movq	8(%rdi), %rdi
	movq	48(%rdx), %rbx
	movq	%r13, (%r12)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r12, %rdx
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB116_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB116_4:                              # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end116:
	.size	__anon_obj_set_signal_info_445, .Lfunc_end116-__anon_obj_set_signal_info_445
	.cfi_endproc
                                        # -- End function
	.globl	__anon_obj_work_space_445 # -- Begin function __anon_obj_work_space_445
	.p2align	4, 0x90
	.type	__anon_obj_work_space_445,@function
__anon_obj_work_space_445:              # @__anon_obj_work_space_445
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	pushq	%r15
	.cfi_def_cfa_offset 24
	pushq	%r14
	.cfi_def_cfa_offset 32
	pushq	%r13
	.cfi_def_cfa_offset 40
	pushq	%r12
	.cfi_def_cfa_offset 48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
	.cfi_def_cfa_offset 80
	.cfi_offset %rbx, -56
	.cfi_offset %r12, -48
	.cfi_offset %r13, -40
	.cfi_offset %r14, -32
	.cfi_offset %r15, -24
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%r8, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	(%rsi), %r15
	movq	8(%rsi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rsi
	movq	(%rax), %rbp
	movq	8(%rax), %rdi
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
	.cfi_adjust_cfa_offset 8
	callq	*56(%rbp)
	addq	$32, %rsp
	.cfi_adjust_cfa_offset -32
	testl	%eax, %eax
	jne	.LBB117_4
# %bb.1:                                # %assertion.success_0
	movq	(%r12), %r8
	movq	(%r14), %rcx
	movq	(%r13), %rbp
	leaq	1(%rbp), %rdx
	movq	32(%rbx), %rsi
	movq	(%r15), %rax
	movq	8(%r15), %rdi
	movq	56(%rax), %rax
	movq	%rdx, (%r13)
	movq	%r8, (%r12)
	movq	%rcx, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %r15
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	movq	120(%rsp), %rbp
	pushq	%rbp
	.cfi_adjust_cfa_offset 8
	callq	*%rax
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	movq	%r15, %r11
	movq	96(%rsp), %r15
	jne	.LBB117_4
# %bb.2:                                # %assertion.success_1
	movq	%rbp, %r10
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%r12), %rdx
	movq	(%r14), %r8
	movq	(%r13), %rax
	leaq	1(%rax), %rbp
	cmpq	$1, 40(%rbx)
	incq	%rdx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	72(%rbx), %rsi
	movq	(%rdi), %rbx
	movq	8(%rdi), %rdi
	movq	56(%rbx), %rbx
	movq	%rbp, (%r13)
	movq	%rdx, (%r12)
	movq	%r8, (%r14)
	subq	$8, %rsp
	.cfi_adjust_cfa_offset 8
	movq	%r13, %rdx
	movq	%r12, %r8
	movq	%r14, %r9
	pushq	%rax
	.cfi_adjust_cfa_offset 8
	pushq	120(%rsp)
	.cfi_adjust_cfa_offset 8
	pushq	%r15
	.cfi_adjust_cfa_offset 8
	pushq	%r11
	.cfi_adjust_cfa_offset 8
	pushq	%r10
	.cfi_adjust_cfa_offset 8
	callq	*%rbx
	addq	$48, %rsp
	.cfi_adjust_cfa_offset -48
	testl	%eax, %eax
	jne	.LBB117_4
# %bb.3:                                # %assertion.success_2
	xorl	%eax, %eax
.LBB117_4:                              # %assertion.failed_0
	addq	$24, %rsp
	.cfi_def_cfa_offset 56
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%r12
	.cfi_def_cfa_offset 40
	popq	%r13
	.cfi_def_cfa_offset 32
	popq	%r14
	.cfi_def_cfa_offset 24
	popq	%r15
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end117:
	.size	__anon_obj_work_space_445, .Lfunc_end117-__anon_obj_work_space_445
	.cfi_endproc
                                        # -- End function
	.globl	ic_diode_448_fun        # -- Begin function ic_diode_448_fun
	.p2align	4, 0x90
	.type	ic_diode_448_fun,@function
ic_diode_448_fun:                       # @ic_diode_448_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB118_2
# %bb.1:                                # %if.else_0
	movl	$16, %edi
	callq	malloc
.LBB118_2:                              # %if.exit_0
	movq	$.L__anon_obj_function_record_445, (%rax)
	movq	$0, 8(%rax)
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end118:
	.size	ic_diode_448_fun, .Lfunc_end118-ic_diode_448_fun
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function half_wave_rectifier_449_fun
.LCPI119_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	half_wave_rectifier_449_fun
	.p2align	4, 0x90
	.type	half_wave_rectifier_449_fun,@function
half_wave_rectifier_449_fun:            # @half_wave_rectifier_449_fun
	.cfi_startproc
# %bb.0:
	pushq	%r15
	.cfi_def_cfa_offset 16
	pushq	%r14
	.cfi_def_cfa_offset 24
	pushq	%r12
	.cfi_def_cfa_offset 32
	pushq	%rbx
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
	.cfi_def_cfa_offset 64
	.cfi_offset %rbx, -40
	.cfi_offset %r12, -32
	.cfi_offset %r14, -24
	.cfi_offset %r15, -16
	movsd	%xmm4, 16(%rsp)         # 8-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	voltage_source_ac_420_fun
	movq	%rax, %r14
	movsd	.LCPI119_0(%rip), %xmm0 # xmm0 = mem[0],zero
	callq	resistor_408_fun
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	inductance_412_fun
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	serial_427_fun
	movq	%rax, %r15
	callq	ic_diode_448_fun
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	capacitor_416_fun
	movq	%rax, %rbx
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	resistor_408_fun
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	parallel_433_fun
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	serial_427_fun
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	serial_427_fun
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	grounded_444_fun
	addq	$24, %rsp
	.cfi_def_cfa_offset 40
	popq	%rbx
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end119:
	.size	half_wave_rectifier_449_fun, .Lfunc_end119-half_wave_rectifier_449_fun
	.cfi_endproc
                                        # -- End function
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3               # -- Begin function example_450_fun
.LCPI120_0:
	.quad	4576918229304087675     # double 0.01
.LCPI120_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	example_450_fun
	.p2align	4, 0x90
	.type	example_450_fun,@function
example_450_fun:                        # @example_450_fun
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	movsd	.LCPI120_0(%rip), %xmm3 # xmm3 = mem[0],zero
	movsd	.LCPI120_1(%rip), %xmm0 # xmm0 = mem[0],zero
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm2
	movaps	%xmm0, %xmm4
	callq	half_wave_rectifier_449_fun
	popq	%rcx
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end120:
	.size	example_450_fun, .Lfunc_end120-example_450_fun
	.cfi_endproc
                                        # -- End function
	.type	.L__int_factorial,@object # @__int_factorial
	.section	.rodata,"a",@progbits
	.p2align	4
.L__int_factorial:
	.quad	1                       # 0x1
	.quad	1                       # 0x1
	.quad	2                       # 0x2
	.quad	6                       # 0x6
	.quad	24                      # 0x18
	.quad	120                     # 0x78
	.quad	720                     # 0x2d0
	.quad	5040                    # 0x13b0
	.quad	40320                   # 0x9d80
	.quad	362880                  # 0x58980
	.quad	3628800                 # 0x375f00
	.size	.L__int_factorial, 88

	.type	.L__double_factorial,@object # @__double_factorial
	.p2align	4
.L__double_factorial:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4618441417868443648     # double 6
	.quad	4627448617123184640     # double 24
	.quad	4638144666238189568     # double 120
	.quad	4649544402794971136     # double 720
	.quad	4662263553305083904     # double 5040
	.quad	4675774352187195392     # double 40320
	.quad	4689977843394805760     # double 362880
	.quad	4705047200009289728     # double 3628800
	.size	.L__double_factorial, 88

	.type	.L__choose_array_0,@object # @__choose_array_0
	.p2align	3
.L__choose_array_0:
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_0, 8

	.type	.L__choose_array_1,@object # @__choose_array_1
	.p2align	3
.L__choose_array_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_1, 16

	.type	.L__choose_array_2,@object # @__choose_array_2
	.p2align	4
.L__choose_array_2:
	.quad	4607182418800017408     # double 1
	.quad	4611686018427387904     # double 2
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_2, 24

	.type	.L__choose_array_3,@object # @__choose_array_3
	.p2align	4
.L__choose_array_3:
	.quad	4607182418800017408     # double 1
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_3, 32

	.type	.L__choose_array_4,@object # @__choose_array_4
	.p2align	4
.L__choose_array_4:
	.quad	4607182418800017408     # double 1
	.quad	4616189618054758400     # double 4
	.quad	4618441417868443648     # double 6
	.quad	4616189618054758400     # double 4
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_4, 40

	.type	.L__choose_array_5,@object # @__choose_array_5
	.p2align	4
.L__choose_array_5:
	.quad	4607182418800017408     # double 1
	.quad	4617315517961601024     # double 5
	.quad	4621819117588971520     # double 10
	.quad	4621819117588971520     # double 10
	.quad	4617315517961601024     # double 5
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_5, 48

	.type	.L__choose_array_6,@object # @__choose_array_6
	.p2align	4
.L__choose_array_6:
	.quad	4607182418800017408     # double 1
	.quad	4618441417868443648     # double 6
	.quad	4624633867356078080     # double 15
	.quad	4626322717216342016     # double 20
	.quad	4624633867356078080     # double 15
	.quad	4618441417868443648     # double 6
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_6, 56

	.type	.L__choose_array_7,@object # @__choose_array_7
	.p2align	4
.L__choose_array_7:
	.quad	4607182418800017408     # double 1
	.quad	4619567317775286272     # double 7
	.quad	4626604192193052672     # double 21
	.quad	4630122629401935872     # double 35
	.quad	4630122629401935872     # double 35
	.quad	4626604192193052672     # double 21
	.quad	4619567317775286272     # double 7
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_7, 64

	.type	.L__choose_array_8,@object # @__choose_array_8
	.p2align	4
.L__choose_array_8:
	.quad	4607182418800017408     # double 1
	.quad	4620693217682128896     # double 8
	.quad	4628574517030027264     # double 28
	.quad	4633078116657397760     # double 56
	.quad	4634626229029306368     # double 70
	.quad	4633078116657397760     # double 56
	.quad	4628574517030027264     # double 28
	.quad	4620693217682128896     # double 8
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_8, 72

	.type	.L__choose_array_9,@object # @__choose_array_9
	.p2align	4
.L__choose_array_9:
	.quad	4607182418800017408     # double 1
	.quad	4621256167635550208     # double 9
	.quad	4630263366890291200     # double 36
	.quad	4635611391447793664     # double 84
	.quad	4638566878703255552     # double 126
	.quad	4638566878703255552     # double 126
	.quad	4635611391447793664     # double 84
	.quad	4630263366890291200     # double 36
	.quad	4621256167635550208     # double 9
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_9, 80

	.type	.L__choose_array_10,@object # @__choose_array_10
	.p2align	4
.L__choose_array_10:
	.quad	4607182418800017408     # double 1
	.quad	4621819117588971520     # double 10
	.quad	4631530004285489152     # double 45
	.quad	4638144666238189568     # double 120
	.quad	4641592734702895104     # double 210
	.quad	4643070478330626048     # double 252
	.quad	4641592734702895104     # double 210
	.quad	4638144666238189568     # double 120
	.quad	4631530004285489152     # double 45
	.quad	4621819117588971520     # double 10
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_10, 88

	.type	.L__choose_array_11,@object # @__choose_array_11
	.p2align	4
.L__choose_array_11:
	.quad	4607182418800017408     # double 1
	.quad	4622382067542392832     # double 11
	.quad	4632937379169042432     # double 55
	.quad	4640009437958897664     # double 165
	.quad	4644513037586268160     # double 330
	.quad	4646835206144131072     # double 462
	.quad	4646835206144131072     # double 462
	.quad	4644513037586268160     # double 330
	.quad	4640009437958897664     # double 165
	.quad	4632937379169042432     # double 55
	.quad	4622382067542392832     # double 11
	.quad	4607182418800017408     # double 1
	.size	.L__choose_array_11, 96

	.type	.L__choose_array,@object # @__choose_array
	.p2align	4
.L__choose_array:
	.quad	.L__choose_array_0
	.quad	.L__choose_array_1
	.quad	.L__choose_array_2
	.quad	.L__choose_array_3
	.quad	.L__choose_array_4
	.quad	.L__choose_array_5
	.quad	.L__choose_array_6
	.quad	.L__choose_array_7
	.quad	.L__choose_array_8
	.quad	.L__choose_array_9
	.quad	.L__choose_array_10
	.quad	.L__choose_array_11
	.size	.L__choose_array, 96

	.type	.L__tangent_numbers0,@object # @__tangent_numbers0
	.p2align	3
.L__tangent_numbers0:
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.size	.L__tangent_numbers0, 16

	.type	.L__tangent_numbers1,@object # @__tangent_numbers1
	.p2align	4
.L__tangent_numbers1:
	.quad	4607182418800017408     # double 1
	.quad	0                       # double 0
	.quad	4607182418800017408     # double 1
	.size	.L__tangent_numbers1, 24

	.type	.L__tangent_numbers2,@object # @__tangent_numbers2
	.p2align	4
.L__tangent_numbers2:
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4611686018427387904     # double 2
	.size	.L__tangent_numbers2, 32

	.type	.L__tangent_numbers3,@object # @__tangent_numbers3
	.p2align	4
.L__tangent_numbers3:
	.quad	4611686018427387904     # double 2
	.quad	0                       # double 0
	.quad	4620693217682128896     # double 8
	.quad	0                       # double 0
	.quad	4618441417868443648     # double 6
	.size	.L__tangent_numbers3, 40

	.type	.L__tangent_numbers4,@object # @__tangent_numbers4
	.p2align	4
.L__tangent_numbers4:
	.quad	0                       # double 0
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4630826316843712512     # double 40
	.quad	0                       # double 0
	.quad	4627448617123184640     # double 24
	.size	.L__tangent_numbers4, 48

	.type	.L__tangent_numbers5,@object # @__tangent_numbers5
	.p2align	4
.L__tangent_numbers5:
	.quad	4625196817309499392     # double 16
	.quad	0                       # double 0
	.quad	4638989091168321536     # double 136
	.quad	0                       # double 0
	.quad	4642648265865560064     # double 240
	.quad	0                       # double 0
	.quad	4638144666238189568     # double 120
	.size	.L__tangent_numbers5, 56

	.type	.L__tangent_numbers6,@object # @__tangent_numbers6
	.p2align	4
.L__tangent_numbers6:
	.quad	0                       # double 0
	.quad	4643492690795692032     # double 272
	.quad	0                       # double 0
	.quad	4653133208748032000     # double 1232
	.quad	0                       # double 0
	.quad	4655103533585006592     # double 1680
	.quad	0                       # double 0
	.quad	4649544402794971136     # double 720
	.size	.L__tangent_numbers6, 64

	.type	.L__tangent_numbers7,@object # @__tangent_numbers7
	.p2align	4
.L__tangent_numbers7:
	.quad	4643492690795692032     # double 272
	.quad	0                       # double 0
	.quad	4660944139351752704     # double 3968
	.quad	0                       # double 0
	.quad	4667875460653252608     # double 12096
	.quad	0                       # double 0
	.quad	4668614332467118080     # double 13440
	.quad	0                       # double 0
	.quad	4662263553305083904     # double 5040
	.size	.L__tangent_numbers7, 72

	.type	.L__tangent_numbers8,@object # @__tangent_numbers8
	.p2align	4
.L__tangent_numbers8:
	.quad	0                       # double 0
	.quad	4665447738979123200     # double 7936
	.quad	0                       # double 0
	.quad	4677973375442747392     # double 56320
	.quad	0                       # double 0
	.quad	4683602874976960512     # double 129024
	.quad	0                       # double 0
	.quad	4683048721116561408     # double 120960
	.quad	0                       # double 0
	.quad	4675774352187195392     # double 40320
	.size	.L__tangent_numbers8, 80

	.type	.L__tangent_numbers9,@object # @__tangent_numbers9
	.p2align	4
.L__tangent_numbers9:
	.quad	4665447738979123200     # double 7936
	.quad	0                       # double 0
	.quad	4685318113116291072     # double 176896
	.quad	0                       # double 0
	.quad	4695240106045341696     # double 814080
	.quad	0                       # double 0
	.quad	4699158215730921472     # double 1491840
	.quad	0                       # double 0
	.quad	4697946004161298432     # double 1209600
	.quad	0                       # double 0
	.quad	4689977843394805760     # double 362880
	.size	.L__tangent_numbers9, 88

	.type	.L__tangent_numbers10,@object # @__tangent_numbers10
	.p2align	4
.L__tangent_numbers10:
	.quad	0                       # double 0
	.quad	4689821712743661568     # double 353792
	.quad	0                       # double 0
	.quad	4705007067834875904     # double 3610112
	.quad	0                       # double 0
	.quad	4712815387098480640     # double 12207360
	.quad	0                       # double 0
	.quad	4715765582954233856     # double 18627840
	.quad	0                       # double 0
	.quad	4713405000208875520     # double 13305600
	.quad	0                       # double 0
	.quad	4705047200009289728     # double 3628800
	.size	.L__tangent_numbers10, 96

	.type	.L__tangent_numbers11,@object # @__tangent_numbers11
	.p2align	4
.L__tangent_numbers11:
	.quad	4689821712743661568     # double 353792
	.quad	0                       # double 0
	.quad	4712266043601453056     # double 11184128
	.quad	0                       # double 0
	.quad	4724595331340173312     # double 71867136
	.quad	0                       # double 0
	.quad	4730699390400856064     # double 191431680
	.quad	0                       # double 0
	.quad	4732669491899531264     # double 250145280
	.quad	0                       # double 0
	.quad	4729633551316680704     # double 159667200
	.quad	0                       # double 0
	.quad	4720626352061939712     # double 39916800
	.size	.L__tangent_numbers11, 104

	.type	.L__tangent_numbers,@object # @__tangent_numbers
	.p2align	4
.L__tangent_numbers:
	.quad	.L__tangent_numbers0
	.quad	.L__tangent_numbers1
	.quad	.L__tangent_numbers2
	.quad	.L__tangent_numbers3
	.quad	.L__tangent_numbers4
	.quad	.L__tangent_numbers5
	.quad	.L__tangent_numbers6
	.quad	.L__tangent_numbers7
	.quad	.L__tangent_numbers8
	.quad	.L__tangent_numbers9
	.quad	.L__tangent_numbers10
	.quad	.L__tangent_numbers11
	.size	.L__tangent_numbers, 96

	.type	.Lpi_396,@object        # @pi_396
	.p2align	3
.Lpi_396:
	.quad	0                       # 0x0
	.quad	pi_396_fun
	.size	.Lpi_396, 16

	.type	.L__anon_obj_function_record_397,@object # @__anon_obj_function_record_397
	.p2align	4
.L__anon_obj_function_record_397:
	.quad	__anon_obj_constructor_397
	.quad	__anon_obj_signature_397
	.quad	__anon_obj_residual_397
	.quad	__anon_obj_root_setup_397
	.quad	__anon_obj_compute_roots_397
	.quad	__anon_obj_event_handler_397
	.quad	__anon_obj_set_signal_info_397
	.quad	__anon_obj_work_space_397
	.size	.L__anon_obj_function_record_397, 64

	.type	.Ltime_398,@object      # @time_398
	.p2align	3
.Ltime_398:
	.quad	0                       # 0x0
	.quad	time_398_fun
	.size	.Ltime_398, 16

	.type	.L__anon_obj_function_record_399,@object # @__anon_obj_function_record_399
	.p2align	4
.L__anon_obj_function_record_399:
	.quad	__anon_obj_constructor_399
	.quad	__anon_obj_signature_399
	.quad	__anon_obj_residual_399
	.quad	__anon_obj_root_setup_399
	.quad	__anon_obj_compute_roots_399
	.quad	__anon_obj_event_handler_399
	.quad	__anon_obj_set_signal_info_399
	.quad	__anon_obj_work_space_399
	.size	.L__anon_obj_function_record_399, 64

	.type	.Ltension_400,@object   # @tension_400
	.p2align	3
.Ltension_400:
	.quad	0                       # 0x0
	.quad	tension_400_fun
	.size	.Ltension_400, 16

	.type	.L__anon_obj_function_record_401,@object # @__anon_obj_function_record_401
	.p2align	4
.L__anon_obj_function_record_401:
	.quad	__anon_obj_constructor_401
	.quad	__anon_obj_signature_401
	.quad	__anon_obj_residual_401
	.quad	__anon_obj_root_setup_401
	.quad	__anon_obj_compute_roots_401
	.quad	__anon_obj_event_handler_401
	.quad	__anon_obj_set_signal_info_401
	.quad	__anon_obj_work_space_401
	.size	.L__anon_obj_function_record_401, 64

	.type	.Lintensity_402,@object # @intensity_402
	.p2align	3
.Lintensity_402:
	.quad	0                       # 0x0
	.quad	intensity_402_fun
	.size	.Lintensity_402, 16

	.type	.L__anon_obj_function_record_403,@object # @__anon_obj_function_record_403
	.p2align	4
.L__anon_obj_function_record_403:
	.quad	__anon_obj_constructor_403
	.quad	__anon_obj_signature_403
	.quad	__anon_obj_residual_403
	.quad	__anon_obj_root_setup_403
	.quad	__anon_obj_compute_roots_403
	.quad	__anon_obj_event_handler_403
	.quad	__anon_obj_set_signal_info_403
	.quad	__anon_obj_work_space_403
	.size	.L__anon_obj_function_record_403, 64

	.type	.Ltwo_pin_404,@object   # @two_pin_404
	.p2align	3
.Ltwo_pin_404:
	.quad	0                       # 0x0
	.quad	two_pin_404_fun
	.size	.Ltwo_pin_404, 16

	.type	i_40_name_406,@object   # @i_40_name_406
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	i_40_name_406
i_40_name_406:
	.asciz	"i"
	.size	i_40_name_406, 2

	.type	u_42_name_407,@object   # @u_42_name_407
	.globl	u_42_name_407
u_42_name_407:
	.asciz	"u"
	.size	u_42_name_407, 2

	.type	.L__anon_obj_function_record_405,@object # @__anon_obj_function_record_405
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_405:
	.quad	__anon_obj_constructor_405
	.quad	__anon_obj_signature_405
	.quad	__anon_obj_residual_405
	.quad	__anon_obj_root_setup_405
	.quad	__anon_obj_compute_roots_405
	.quad	__anon_obj_event_handler_405
	.quad	__anon_obj_set_signal_info_405
	.quad	__anon_obj_work_space_405
	.size	.L__anon_obj_function_record_405, 64

	.type	.Lresistor_408,@object  # @resistor_408
	.p2align	3
.Lresistor_408:
	.quad	0                       # 0x0
	.quad	resistor_408_fun
	.size	.Lresistor_408, 16

	.type	i_68_name_410,@object   # @i_68_name_410
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	i_68_name_410
i_68_name_410:
	.asciz	"i"
	.size	i_68_name_410, 2

	.type	u_70_name_411,@object   # @u_70_name_411
	.globl	u_70_name_411
u_70_name_411:
	.asciz	"u"
	.size	u_70_name_411, 2

	.type	.L__anon_obj_function_record_409,@object # @__anon_obj_function_record_409
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_409:
	.quad	__anon_obj_constructor_409
	.quad	__anon_obj_signature_409
	.quad	__anon_obj_residual_409
	.quad	__anon_obj_root_setup_409
	.quad	__anon_obj_compute_roots_409
	.quad	__anon_obj_event_handler_409
	.quad	__anon_obj_set_signal_info_409
	.quad	__anon_obj_work_space_409
	.size	.L__anon_obj_function_record_409, 64

	.type	.Linductance_412,@object # @inductance_412
	.p2align	3
.Linductance_412:
	.quad	0                       # 0x0
	.quad	inductance_412_fun
	.size	.Linductance_412, 16

	.type	u_100_name_414,@object  # @u_100_name_414
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	u_100_name_414
u_100_name_414:
	.asciz	"u"
	.size	u_100_name_414, 2

	.type	i_98_name_415,@object   # @i_98_name_415
	.globl	i_98_name_415
i_98_name_415:
	.asciz	"i"
	.size	i_98_name_415, 2

	.type	.L__anon_obj_function_record_413,@object # @__anon_obj_function_record_413
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_413:
	.quad	__anon_obj_constructor_413
	.quad	__anon_obj_signature_413
	.quad	__anon_obj_residual_413
	.quad	__anon_obj_root_setup_413
	.quad	__anon_obj_compute_roots_413
	.quad	__anon_obj_event_handler_413
	.quad	__anon_obj_set_signal_info_413
	.quad	__anon_obj_work_space_413
	.size	.L__anon_obj_function_record_413, 64

	.type	.Lcapacitor_416,@object # @capacitor_416
	.p2align	3
.Lcapacitor_416:
	.quad	0                       # 0x0
	.quad	capacitor_416_fun
	.size	.Lcapacitor_416, 16

	.type	t_128_name_418,@object  # @t_128_name_418
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	t_128_name_418
t_128_name_418:
	.asciz	"t"
	.size	t_128_name_418, 2

	.type	u_130_name_419,@object  # @u_130_name_419
	.globl	u_130_name_419
u_130_name_419:
	.asciz	"u"
	.size	u_130_name_419, 2

	.type	.L__anon_obj_function_record_417,@object # @__anon_obj_function_record_417
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_417:
	.quad	__anon_obj_constructor_417
	.quad	__anon_obj_signature_417
	.quad	__anon_obj_residual_417
	.quad	__anon_obj_root_setup_417
	.quad	__anon_obj_compute_roots_417
	.quad	__anon_obj_event_handler_417
	.quad	__anon_obj_set_signal_info_417
	.quad	__anon_obj_work_space_417
	.size	.L__anon_obj_function_record_417, 64

	.type	.Lvoltage_source_ac_420,@object # @voltage_source_ac_420
	.p2align	3
.Lvoltage_source_ac_420:
	.quad	0                       # 0x0
	.quad	voltage_source_ac_420_fun
	.size	.Lvoltage_source_ac_420, 16

	.type	.L__anon_obj_function_record_421,@object # @__anon_obj_function_record_421
	.p2align	4
.L__anon_obj_function_record_421:
	.quad	__anon_obj_constructor_421
	.quad	__anon_obj_signature_421
	.quad	__anon_obj_residual_421
	.quad	__anon_obj_root_setup_421
	.quad	__anon_obj_compute_roots_421
	.quad	__anon_obj_event_handler_421
	.quad	__anon_obj_set_signal_info_421
	.quad	__anon_obj_work_space_421
	.size	.L__anon_obj_function_record_421, 64

	.type	.Lground_422,@object    # @ground_422
	.p2align	3
.Lground_422:
	.quad	0                       # 0x0
	.quad	ground_422_fun
	.size	.Lground_422, 16

	.type	n1_i_168_name_424,@object # @n1_i_168_name_424
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	n1_i_168_name_424
n1_i_168_name_424:
	.asciz	"n1_i"
	.size	n1_i_168_name_424, 5

	.type	n1_v_170_name_425,@object # @n1_v_170_name_425
	.globl	n1_v_170_name_425
n1_v_170_name_425:
	.asciz	"n1_v"
	.size	n1_v_170_name_425, 5

	.type	p2_i_180_name_426,@object # @p2_i_180_name_426
	.globl	p2_i_180_name_426
p2_i_180_name_426:
	.asciz	"p2_i"
	.size	p2_i_180_name_426, 5

	.type	.L__anon_obj_function_record_423,@object # @__anon_obj_function_record_423
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_423:
	.quad	__anon_obj_constructor_423
	.quad	__anon_obj_signature_423
	.quad	__anon_obj_residual_423
	.quad	__anon_obj_root_setup_423
	.quad	__anon_obj_compute_roots_423
	.quad	__anon_obj_event_handler_423
	.quad	__anon_obj_set_signal_info_423
	.quad	__anon_obj_work_space_423
	.size	.L__anon_obj_function_record_423, 64

	.type	.Lserial_427,@object    # @serial_427
	.p2align	3
.Lserial_427:
	.quad	0                       # 0x0
	.quad	serial_427_fun
	.size	.Lserial_427, 16

	.type	n1_i_198_name_429,@object # @n1_i_198_name_429
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	n1_i_198_name_429
n1_i_198_name_429:
	.asciz	"n1_i"
	.size	n1_i_198_name_429, 5

	.type	n2_i_202_name_430,@object # @n2_i_202_name_430
	.globl	n2_i_202_name_430
n2_i_202_name_430:
	.asciz	"n2_i"
	.size	n2_i_202_name_430, 5

	.type	p1_i_206_name_431,@object # @p1_i_206_name_431
	.globl	p1_i_206_name_431
p1_i_206_name_431:
	.asciz	"p1_i"
	.size	p1_i_206_name_431, 5

	.type	p2_i_210_name_432,@object # @p2_i_210_name_432
	.globl	p2_i_210_name_432
p2_i_210_name_432:
	.asciz	"p2_i"
	.size	p2_i_210_name_432, 5

	.type	.L__anon_obj_function_record_428,@object # @__anon_obj_function_record_428
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_428:
	.quad	__anon_obj_constructor_428
	.quad	__anon_obj_signature_428
	.quad	__anon_obj_residual_428
	.quad	__anon_obj_root_setup_428
	.quad	__anon_obj_compute_roots_428
	.quad	__anon_obj_event_handler_428
	.quad	__anon_obj_set_signal_info_428
	.quad	__anon_obj_work_space_428
	.size	.L__anon_obj_function_record_428, 64

	.type	.Lparallel_433,@object  # @parallel_433
	.p2align	3
.Lparallel_433:
	.quad	0                       # 0x0
	.quad	parallel_433_fun
	.size	.Lparallel_433, 16

	.type	gp_i_226_name_435,@object # @gp_i_226_name_435
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	gp_i_226_name_435
gp_i_226_name_435:
	.asciz	"gp_i"
	.size	gp_i_226_name_435, 5

	.type	gp_v_228_name_436,@object # @gp_v_228_name_436
	.globl	gp_v_228_name_436
gp_v_228_name_436:
	.asciz	"gp_v"
	.size	gp_v_228_name_436, 5

	.type	n1_v_232_name_437,@object # @n1_v_232_name_437
	.globl	n1_v_232_name_437
n1_v_232_name_437:
	.asciz	"n1_v"
	.size	n1_v_232_name_437, 5

	.type	n1_i_230_name_438,@object # @n1_i_230_name_438
	.globl	n1_i_230_name_438
n1_i_230_name_438:
	.asciz	"n1_i"
	.size	n1_i_230_name_438, 5

	.type	n2_i_234_name_439,@object # @n2_i_234_name_439
	.globl	n2_i_234_name_439
n2_i_234_name_439:
	.asciz	"n2_i"
	.size	n2_i_234_name_439, 5

	.type	p1_i_238_name_440,@object # @p1_i_238_name_440
	.globl	p1_i_238_name_440
p1_i_238_name_440:
	.asciz	"p1_i"
	.size	p1_i_238_name_440, 5

	.type	p1_v_240_name_441,@object # @p1_v_240_name_441
	.globl	p1_v_240_name_441
p1_v_240_name_441:
	.asciz	"p1_v"
	.size	p1_v_240_name_441, 5

	.type	p2_i_242_name_442,@object # @p2_i_242_name_442
	.globl	p2_i_242_name_442
p2_i_242_name_442:
	.asciz	"p2_i"
	.size	p2_i_242_name_442, 5

	.type	p2_v_244_name_443,@object # @p2_v_244_name_443
	.globl	p2_v_244_name_443
p2_v_244_name_443:
	.asciz	"p2_v"
	.size	p2_v_244_name_443, 5

	.type	.L__anon_obj_function_record_434,@object # @__anon_obj_function_record_434
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_434:
	.quad	__anon_obj_constructor_434
	.quad	__anon_obj_signature_434
	.quad	__anon_obj_residual_434
	.quad	__anon_obj_root_setup_434
	.quad	__anon_obj_compute_roots_434
	.quad	__anon_obj_event_handler_434
	.quad	__anon_obj_set_signal_info_434
	.quad	__anon_obj_work_space_434
	.size	.L__anon_obj_function_record_434, 64

	.type	.Lgrounded_444,@object  # @grounded_444
	.p2align	3
.Lgrounded_444:
	.quad	0                       # 0x0
	.quad	grounded_444_fun
	.size	.Lgrounded_444, 16

	.type	i_256_name_446,@object  # @i_256_name_446
	.section	.rodata.str1.1,"aMS",@progbits,1
	.globl	i_256_name_446
i_256_name_446:
	.asciz	"i"
	.size	i_256_name_446, 2

	.type	u_258_name_447,@object  # @u_258_name_447
	.globl	u_258_name_447
u_258_name_447:
	.asciz	"u"
	.size	u_258_name_447, 2

	.type	.L__anon_obj_function_record_445,@object # @__anon_obj_function_record_445
	.section	.rodata,"a",@progbits
	.p2align	4
.L__anon_obj_function_record_445:
	.quad	__anon_obj_constructor_445
	.quad	__anon_obj_signature_445
	.quad	__anon_obj_residual_445
	.quad	__anon_obj_root_setup_445
	.quad	__anon_obj_compute_roots_445
	.quad	__anon_obj_event_handler_445
	.quad	__anon_obj_set_signal_info_445
	.quad	__anon_obj_work_space_445
	.size	.L__anon_obj_function_record_445, 64

	.type	.Lic_diode_448,@object  # @ic_diode_448
	.p2align	3
.Lic_diode_448:
	.quad	0                       # 0x0
	.quad	ic_diode_448_fun
	.size	.Lic_diode_448, 16

	.type	.Lhalf_wave_rectifier_449,@object # @half_wave_rectifier_449
	.p2align	3
.Lhalf_wave_rectifier_449:
	.quad	0                       # 0x0
	.quad	half_wave_rectifier_449_fun
	.size	.Lhalf_wave_rectifier_449, 16

	.type	.Lexample_450,@object   # @example_450
	.p2align	3
.Lexample_450:
	.quad	0                       # 0x0
	.quad	example_450_fun
	.size	.Lexample_450, 16


	.section	".note.GNU-stack","",@progbits
