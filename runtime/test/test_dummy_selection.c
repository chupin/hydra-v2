#include "../dummy_selection.h"
#include "../integer_matrix.h"
#include "../sigma.h"

#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

#define N1 (3)

uint32_t idx1[2] = { 0, 2 };
uint8_t entries1[2] = { 2, 0 };
double double_entries1[2] = { 2.0, 0.0 };

const row_t row1 = {
    .row_size = 2,
    .row_indices = idx1,
    .row_entries = entries1,
    .double_row_entries = double_entries1,
};

uint32_t idx2[2] = { 1, 2 };
uint8_t entries2[2] = { 2, 0 };
double double_entries2[2] = { 2.0, 0.0 };

const row_t row2 = {
    .row_size = 2,
    .row_indices = idx2,
    .row_entries = entries2,
    .double_row_entries = double_entries2,
};

uint32_t idx3[2] = { 0, 1 };
uint8_t entries3[2] = { 0, 0 };
double double_entries3[2] = { 0.0, 0.0 };

const row_t row3 = {
    .row_size = 2,
    .row_indices = idx3,
    .row_entries = entries3,
    .double_row_entries = double_entries3,
};

row_t rows[3] = { row1, row2, row3 };

matrix_t test_matrix1 = {
    .size = 3,
    .rows = rows,
};

matrix_t* test_matrix1_addr = &test_matrix1;

int main(void) {

    time_t t;
    srand((unsigned) time(&t));

    /*
    uint32_t size = atoi(argv[1]);
    double sparsity = atof(argv[2]);
    int8_t max_idx = atoi(argv[3]);

    matrix_t* matrix = gen_random_matrix(size, sparsity, max_idx);

    printf("Test matrix generated\n");
    //print_matrix(matrix);
    */

    struct assig* assig = init_assig(test_matrix1_addr);

    struct timeval start, stop;
    gettimeofday(&start, NULL);

    bool solved = solve_assignment(assig);

    gettimeofday(&stop, NULL);
    printf(solved ? "Solved " : "No solution ");
    printf("in %lu microseconds\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    for (uint32_t person = 0; person < assig->a_size; ++person) {
        printf("%d ", assig->assig[person]);
    }
    printf("\n");

    if (!solved) {
        return -2;
    }

    int8_t* cs = calloc(assig->a_size, sizeof(int8_t));
    int8_t* ds = calloc(assig->a_size, sizeof(int8_t));

    gettimeofday(&start, NULL);

    smallest_dual_variable(assig, cs, ds);

    gettimeofday(&stop, NULL);

    printf("Dual problem solved in %lu microseconds\n",
           (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    for (uint32_t i = 0; i < assig->a_size; ++i) {
        printf("%d ", cs[i]);
    }
    printf("\n");

    for (uint32_t i = 0; i < assig->a_size; ++i) {
        printf("%d ", ds[i]);
    }
    printf("\n");

    // Making entries array
    entry_t* cs_entries = calloc(assig->a_size, sizeof(entry_t));
    entry_t* ds_entries = calloc(assig->a_size, sizeof(entry_t));
    for (uint32_t i = 0; i < assig->a_size; ++i) {
        cs_entries[i].entry_idx = i;
        cs_entries[i].entry_val = cs[i];

        ds_entries[i].entry_idx = i;
        ds_entries[i].entry_val = ds[i];
    }

    entries_t ncs = {
        .entry_ct = assig->a_size,
        .entries = cs_entries
    };
    entries_t nds = {
        .entry_ct = assig->a_size,
        .entries = ds_entries,
    };

    bool** results = calloc(nds.entry_ct, sizeof(bool*));
    for (uint32_t var = 0; var < nds.entry_ct; ++var) {
        results[var] = calloc(nds.entries[var].entry_val + 1, sizeof(bool));
    }

    gettimeofday(&start, NULL);
    solved = select_dummy(&assig->affinities, &ncs, &nds, results);
    gettimeofday(&stop, NULL);

    printf("Selecting dummy-derivatives in %lu microseconds\n",
           (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    // Print and free the result vector
    for (uint32_t var = 0; var < assig->a_size; ++var) {
        for (uint32_t n = 0; n < ds[var] + 1; ++n) {
            if (results[var][n]) {
                printf("D ");
            } else {
                printf("S ");
            }
        }
        //free(results[var]);
        printf("\n");
    }
    //free(results);

    free(cs);
    free(ds);
    free_assig(assig);
    return 0;
}