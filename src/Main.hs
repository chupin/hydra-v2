{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}

module Main
  ( main
  ) where

import           Data.Foldable                  ( traverse_ )
import           Data.Stream.Supply             ( newNumSupply )
import           Hydra.CodeGen                  ( codegenStep
                                                , emitModuleStep
                                                )
import           Hydra.Compiler          hiding ( info )
import           Hydra.Flatten                  ( flattenStep )
import           Hydra.Flatten.Opt              ( optStep )
import           Hydra.Lexer                    ( lexStep )
import           Hydra.OOIR                     ( generateIR )
import           Hydra.OOIR.MinimizeBand        ( minimizeBandStep )
import           Hydra.Parser                   ( parseStep )
import           Hydra.Steps
import           Hydra.Typing                   ( typingStep )
import           Options.Applicative
import           System.Exit                    ( ExitCode(ExitFailure)
                                                , exitSuccess
                                                , exitWith
                                                )

steps :: StepSpec CompilerM String _
steps =
  lexStep
    `Then` parseStep
    `Then` typingStep
    `Then` flattenStep
    `Then` optStep
    `Then` generateIR
    `Then` minimizeBandStep
    `Then` codegenStep
    `Then` emitModuleStep

getConfig :: IO (Config, Step Bool CompilerM String _)
getConfig = execParser $ info
  (helper <*> ((,) <$> configParser <*> parseSpec steps))
  (fullDesc <> progDesc "The Hydra modelling language compiler")

main :: IO ()
main = do
  (conf, steps) <- getConfig
  supply        <- newNumSupply
  file          <- readFile (sourceFile conf)
  res           <- runCompilerT (runStep steps file) conf supply
  let (exitMsg, exitPrgm) = case res of
        Left  StopMsg      -> (Just (Msg StoppingMsg), exitSuccess)
        Left  (ErrMsg msg) -> (Just (Msg msg), exitWith (ExitFailure 1))
        Right _            -> (Nothing, exitSuccess)
  traverse_ (\msg -> putMessages [msg]) exitMsg
  exitPrgm
