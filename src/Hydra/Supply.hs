{-# LANGUAGE OverloadedLists #-}

module Hydra.Supply where

import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Sequence                  ( Seq(..) )
import           Data.Stream.Infinite
import           Data.Stream.Supply

class Splitter f where
  splitAlong :: Supply s -> f a -> f (Supply s, a)

instance Splitter [] where
  splitAlong supply = go (split supply)
   where
    go (supply :> supplies) (x : xs) = (supply, x) : go supplies xs
    go _                    []       = []

instance Splitter Seq where
  splitAlong supply = go (split supply)
   where
    go (supply :> supplies) (x :<| xs) = (supply, x) :<| go supplies xs
    go _                    Empty      = Empty

instance Ord k => Splitter (Map k) where
  splitAlong supply ms = ms'
   where
    (_, ms') = M.foldrWithKey
      (\k v (supply :> supplies, ms) -> (supplies, M.insert k (supply, v) ms))
      (split supply, [])
      ms

mapWithSupply
  :: (Functor f, Splitter f) => Supply s -> (Supply s -> a -> b) -> f a -> f b
mapWithSupply s f = fmap (uncurry f) . splitAlong s

traverseWithSupply
  :: (Traversable f, Splitter f, Applicative m)
  => Supply s
  -> (Supply s -> a -> m b)
  -> f a
  -> m (f b)
traverseWithSupply s f = traverse (uncurry f) . splitAlong s
