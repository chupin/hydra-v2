{-# LANGUAGE DeriveFunctor #-}

module Hydra.Loc
  ( module Data.Text.Prettyprint.Location
  , Loc(..)
  , Localized(..)
  , Located(..)
  , locate
  , stripLoc
  ) where

import           Data.Text.Prettyprint.Location

import           Hydra.Pretty                   ( Pretty(..) )

data Loc = NoLoc | Loc Span

instance Semigroup Loc where
  NoLoc  <> _      = NoLoc
  _      <> NoLoc  = NoLoc
  Loc s1 <> Loc s2 = Loc (s1 <> s2)

instance Monoid Loc where
  mempty = NoLoc

class Localized loc where
  localize :: loc -> Loc

instance Localized Loc where
  localize = id

data Located i = Located !Loc i
  deriving Functor

instance Eq i => Eq (Located i) where
  Located _ i == Located _ j = i == j

instance Ord i => Ord (Located i) where
  Located _ i `compare` Located _ j = i `compare` j

instance Pretty i => Pretty (Located i) where
  pretty (Located _ i) = pretty i

instance Localized (Located i) where
  localize (Located loc _) = loc

locate :: Loc -> i -> Located i
locate = Located

stripLoc :: Located i -> i
stripLoc (Located _ i) = i
