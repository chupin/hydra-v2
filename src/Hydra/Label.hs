module Hydra.Label where

import           Hydra.Pretty

import           Data.Text                      ( Text )

newtype Label = Label { labelText :: Text }
              deriving(Eq, Ord, Show)

instance Pretty Label where
  pretty (Label s) = pretty s
