{-# LANGUAGE OverloadedStrings #-}

module Hydra.Const where

import           Hydra.Pretty
import           Hydra.Type

newtype Const = RealConst Rational
  deriving ( Eq, Ord, Show )

instance Pretty Const where
  pretty (RealConst ratio) = pretty (PrettyRational ratio)

typeConst :: Const -> TExpr ev sv
typeConst (RealConst _) = TSig TReal
