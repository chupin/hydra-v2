{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DefaultSignatures #-}

{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExplicitForAll #-}

{-# LANGUAGE FunctionalDependencies #-}

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE TupleSections #-}

module Hydra.LLVM.Utils
  ( module Hydra.LLVM.Utils
  , module LLVM.IRBuilder.Instruction
  , module LLVM.IRBuilder.Constant
  , module LLVM.IRBuilder.Module
  , module LLVM.IRBuilder.Monad
  ) where

import           Control.Applicative
import           Control.Arrow
import           Control.Lens            hiding ( from
                                                , indices
                                                , to
                                                )
import           Control.Monad.Fix
import           Control.Monad.State            ( gets )
import           LLVM.Pretty                    ( )

import qualified Data.ByteString.Short         as BS
import qualified Data.Map                      as M
import           Data.Maybe
import           Data.String                    ( fromString )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.Word

import           GHC.Generics

import           LLVM.AST                hiding ( function )
import qualified LLVM.AST.Constant             as LLC
import           LLVM.AST.Constant              ( Constant )
import qualified LLVM.AST.Float                as LLC
import           LLVM.AST.Global         hiding ( metadata )
import           LLVM.AST.IntegerPredicate
import           LLVM.AST.Linkage
import           LLVM.AST.ParameterAttribute    ( ParameterAttribute )
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.AST.Typed                as LLT
import           LLVM.IRBuilder.Constant
import           LLVM.IRBuilder.Instruction
                                         hiding ( add
                                                , icmp
                                                , mul
                                                , select
                                                , sub
                                                )
import qualified LLVM.IRBuilder.Instruction    as LL
import           LLVM.IRBuilder.Module
import           LLVM.IRBuilder.Monad

import           Prelude                 hiding ( EQ
                                                , max
                                                , min
                                                , not
                                                )

import           Control.Monad
import           Hydra.Error                    ( internalError )
import           Hydra.Ident
import           Hydra.Pretty                   ( pretty )

fromText :: Text -> BS.ShortByteString
fromText = fromString . T.unpack

fromShow :: Show i => i -> BS.ShortByteString
fromShow = fromString . show

fromIdent :: Ident -> BS.ShortByteString
fromIdent = fromShow . pretty

-- LLVM utils
getOpName :: Operand -> BS.ShortByteString
getOpName (LocalReference _ name) = case name of
  Name   bs -> bs
  UnName w  -> fromString (show w)
getOpName (ConstantOperand cc) = case cc of
  LLC.Int _ val -> fromString (show val)
  _             -> "constant"
getOpName (MetadataOperand _) = "meta"

(@@) :: Operand -> LLT.Type -> Operand
op @@ ty
  | LLT.typeOf op == ty = op
  | otherwise = internalError
    (  "The operand "
    ++ show op
    ++ " was expected to have type "
    ++ show ty
    ++ " but has type "
    ++ show (LLT.typeOf op)
    ++ "."
    )

-- Constants
nullConstant :: Type -> Constant
nullConstant ty = LLC.Null (LLT.ptr ty)

nullPtr :: Type -> Operand
nullPtr ty = constantOp (nullConstant ty)

undef :: Type -> Operand
undef ty = constantOp (LLC.Undef ty)

intConstant :: Integral i => Word32 -> i -> Constant
intConstant bit = LLC.Int bit . fromIntegral

bitConstant :: Bool -> Constant
bitConstant bit | bit       = intConstant 1 1
                | otherwise = intConstant 1 0

falseConstant, trueConstant :: Constant
falseConstant = bitConstant False

trueConstant = bitConstant True

bool :: Bool -> Operand
bool = constantOp . bitConstant

false, true :: Operand
false = bool False

true = bool True

int8Constant :: Integral i => i -> Constant
int8Constant = intConstant 8

int32Constant :: Integral i => i -> Constant
int32Constant = intConstant 32

int64Constant :: Integral i => i -> Constant
int64Constant = intConstant 64

doubleConstant :: Double -> Constant
doubleConstant = LLC.Float . LLC.Double

structConstant :: [Constant] -> Constant
structConstant vs = LLC.Struct { LLC.structName   = Nothing
                               , LLC.isPacked     = False
                               , LLC.memberValues = vs
                               }

arrayConstant :: Type -> [Constant] -> Constant
arrayConstant = LLC.Array

constantIbGep :: Constant -> [Constant] -> Constant
constantIbGep ptr idxs = LLC.GetElementPtr { LLC.inBounds = True
                                           , LLC.address  = ptr
                                           , LLC.indices  = idxs
                                           }

constantArrayToPtr :: Constant -> Constant
constantArrayToPtr array =
  constantIbGep array [int32Constant 0, int32Constant 0]

constantOp :: Constant -> Operand
constantOp = ConstantOperand

unsafeFromConstantOp :: Operand -> Constant
unsafeFromConstantOp (ConstantOperand const) = const
unsafeFromConstantOp op =
  internalError ("The operand " ++ show (pretty op) ++ " is not a constant.")

class Merge k where
  phiMerge :: MonadIRBuilder m => [(k, Name)] -> m k
  default phiMerge :: (MonadIRBuilder m, Generic k, GMerge (Rep k)) => [(k, Name)] -> m k
  phiMerge labels =
    to <$> gPhiMerge (fmap (first from) labels)

instance Merge Operand where
  phiMerge = phi

instance Merge ()
instance (Merge a, Merge b) => Merge (a, b)
instance (Merge a, Merge b, Merge c) => Merge (a, b, c)

class GMerge k where
  gPhiMerge :: MonadIRBuilder m => [(k a, Name)] -> m (k a)

instance GMerge U1 where
  gPhiMerge _ = pure U1

instance (GMerge p, GMerge q) => GMerge (p :*: q) where
  gPhiMerge labels = do
    let (ps, qs) =
          unzip [ ((p, label), (q, label)) | (p :*: q, label) <- labels ]
    ps <- gPhiMerge ps
    qs <- gPhiMerge qs
    pure (ps :*: qs)

instance Merge c => GMerge (K1 i c) where
  gPhiMerge labels =
    K1 <$> phiMerge (fmap (\(K1 c, label) -> (c, label)) labels)

instance GMerge f => GMerge (M1 i t f) where
  gPhiMerge labels =
    M1 <$> gPhiMerge (fmap (\(M1 c, label) -> (c, label)) labels)

class Argument arg where
  toArg :: [Operand] -> Maybe (arg, [Operand])
  default toArg :: (Generic arg, GArgument (Rep arg)) => [Operand] -> Maybe (arg, [Operand])
  toArg ops =
    case gToArg ops of
      Just (gen, ops) -> Just (to gen, ops)
      Nothing -> Nothing

  fromArg :: arg -> [Operand]

  default fromArg :: (Generic arg, GArgument (Rep arg)) => arg -> [Operand]
  fromArg = gFromArg . from

instance Argument () where
  toArg ops = Just ((), ops)
  fromArg () = []

instance Argument Operand where
  toArg (op : ops) = Just (op, ops)
  toArg []         = Nothing

  fromArg op = [op]

instance Argument a => Argument (StoreOf a)

class GArgument f where
  gToArg :: [Operand] -> Maybe (f a, [Operand])
  gFromArg :: f a -> [Operand]

instance GArgument U1 where
  gToArg ops = Just (U1, ops)
  gFromArg U1 = []

instance (GArgument p, GArgument q) => GArgument (p :*: q) where
  gToArg ops = do
    (p, ops) <- gToArg ops
    (q, ops) <- gToArg ops
    pure (p :*: q, ops)

  gFromArg (p :*: q) = gFromArg p ++ gFromArg q

instance Argument c => GArgument (K1 i c) where
  gToArg ops = do
    (c, ops) <- toArg ops
    pure (K1 c, ops)

  gFromArg (K1 c) = fromArg c

instance GArgument f => GArgument (M1 i t f) where
  gToArg ops = do
    (c, ops) <- gToArg ops
    pure (M1 c, ops)

  gFromArg (M1 c) = gFromArg c

data FunArgument = FunArgument
  { funArgTy    :: LLT.Type
  , funArgName  :: ParameterName
  , funArgAttrs :: [ParameterAttribute]
  }

data FunType = FunType [FunArgument] LLT.Type

toFunPtrTy :: FunType -> LLT.Type
toFunPtrTy (FunType args ret) = LLT.ptr $ LLT.FunctionType
  { LLT.resultType    = ret
  , LLT.argumentTypes = fmap funArgTy args
  , LLT.isVarArg      = False
  }

class Argument arg => Function fun arg m | fun -> arg m where
  functionType :: fun -> m FunType

newtype LLVMFun fun arg = LLVMFun
  { getLLVMFunPtr :: Operand
  }

unsafeMkLLVMFun :: Operand -> LLVMFun fun arg
unsafeMkLLVMFun = LLVMFun

callLLVMFun
  :: (Function fun arg m', MonadIRBuilder m)
  => LLVMFun fun arg
  -> arg
  -> m Operand
callLLVMFun (LLVMFun fun) input = call fun (fmap (, []) (fromArg input))

newtype StoreOf s = StoreOf s
                  deriving(Show, Generic)

class Storable s where
  storeIn :: MonadIRBuilder m => StoreOf s -> s -> m ()
  default storeIn :: (Generic s, GStorable (Rep s), MonadIRBuilder m) => StoreOf s -> s -> m ()
  storeIn (StoreOf ptr) val =
    gStoreIn (StoreOf (from ptr)) (from val)

  loadOut :: MonadIRBuilder m => StoreOf s -> m s
  default loadOut :: (Generic s, GStorable (Rep s), MonadIRBuilder m) => StoreOf s -> m s
  loadOut (StoreOf ptr) =
    to <$> gLoadOut (StoreOf (from ptr))

instance Storable ()

instance Storable Operand where
  storeIn (StoreOf ptr) val = LL.store ptr 0 val
  loadOut (StoreOf ptr) = LL.load ptr 0

class GStorable f where
  gStoreIn :: MonadIRBuilder m => StoreOf (f k) -> f k -> m ()
  gLoadOut :: MonadIRBuilder m => StoreOf (f k) -> m (f k)

instance GStorable U1 where
  gStoreIn (StoreOf U1) U1 = pure ()
  gLoadOut (StoreOf U1) = pure U1

instance (GStorable p, GStorable q) => GStorable (p :*: q) where
  gStoreIn (StoreOf (p :*: q)) (p' :*: q') = do
    gStoreIn (StoreOf p) p'
    gStoreIn (StoreOf q) q'

  gLoadOut (StoreOf (p :*: q)) = do
    p <- gLoadOut (StoreOf p)
    q <- gLoadOut (StoreOf q)
    pure (p :*: q)

instance Storable c => GStorable (K1 i c) where
  gStoreIn (StoreOf (K1 c)) (K1 c') = storeIn (StoreOf c) c'
  gLoadOut (StoreOf (K1 c)) = K1 <$> loadOut (StoreOf c)

instance GStorable f => GStorable (M1 i t f) where
  gStoreIn (StoreOf (M1 c)) (M1 c') = gStoreIn (StoreOf c) c'

  gLoadOut (StoreOf (M1 c)) = M1 <$> gLoadOut (StoreOf c)

ibgep
  :: (MonadIRBuilder m, MonadModuleBuilder m)
  => Operand
  -> [Operand]
  -> m Operand
ibgep addr idcs = do
  ty <- gepType (LLT.typeOf addr) idcs
  emitInstr
    ty
    GetElementPtr { inBounds = True
                  , address  = addr
                  , indices  = idcs
                  , metadata = []
                  }
 where
  gepType ty                     []        = pure (LLT.ptr ty)
  gepType (LLT.PointerType ty _) (_ : is') = gepType ty is'
  gepType (LLT.StructureType _ elTys) (ConstantOperand (LLC.Int 32 val) : is')
    = gepType (elTys !! fromIntegral val) is'
  gepType (LLT.StructureType _ _) (i : _) =
    error
      $  "ibgep: Indices into structures should be 32-bit constants. "
      ++ show i
  gepType (LLT.VectorType _ elTy    ) (_ : is') = gepType elTy is'
  gepType (LLT.ArrayType  _ elTy    ) (_ : is') = gepType elTy is'
  gepType (LLT.NamedTypeReference nm) is'       = do
    mayTy <- liftModuleState (gets (M.lookup nm . builderTypeDefs))
    case mayTy of
      Nothing -> error $ "ibgep: Couldn’t resolve typedef for: " ++ show nm
      Just ty -> gepType ty is'
  gepType t (_ : _) = error $ "ibgep: Can't index into a " ++ show t

nextPtr :: MonadIRBuilder m => MonadModuleBuilder m => Operand -> m Operand
nextPtr ptr = ibgep ptr [int32 1]

-- Store a value in the pointer and returns the address just after it
storeAndStep
  :: MonadIRBuilder m => MonadModuleBuilder m => Operand -> Operand -> m Operand
storeAndStep ptr val = do
  store ptr 0 val
  nextPtr ptr

-- Store in an array. First argument is the pointer to the head of the array,
-- second argument is the offset (an integer) and the last argument is the value
-- to store)
storeAt
  :: (MonadIRBuilder m, MonadModuleBuilder m)
  => Operand
  -> Operand
  -> Operand
  -> m ()
storeAt ptr offset val = do
  offPtr <- ibgep ptr [offset]
  store offPtr 0 val

loadFrom
  :: MonadIRBuilder m => MonadModuleBuilder m => Operand -> Operand -> m Operand
loadFrom ptr offset = do
  offPtr <- ibgep ptr [offset]
  load offPtr 0

makeAggregate
  :: MonadIRBuilder m
  => MonadModuleBuilder m => LLT.Type -> [Operand] -> m Operand
makeAggregate aggrTy ops = foldM
  (\struct (idx, op) -> insertValue struct op [idx])
  (undef aggrTy)
  (zip [0 ..] ops)

makeStruct, makeArray
  :: MonadIRBuilder m => MonadModuleBuilder m => [Operand] -> m Operand
makeStruct ops = makeAggregate (structType (fmap LLT.typeOf ops)) ops

makeArray ops =
  makeAggregate (arrayType (length ops) (LLT.typeOf (head ops))) ops

constantSizeOf :: Type -> Constant
constantSizeOf ty = LLC.ZExt (LLC.sizeof ty) LLT.i64

sizeOf :: (MonadIRBuilder m, MonadModuleBuilder m) => Type -> m Operand
sizeOf ty = do
  size <- gep (nullPtr ty) [int32 1]
  ptrtoint size LLT.i64

-- Rewrite of common operation supporting constant folding
withConstantFold
  :: MonadIRBuilder m
  => (Integer -> Integer -> Integer)
  -> (Operand -> Operand -> m Operand)
  -> Operand
  -> Operand
  -> m Operand
withConstantFold f _ (ConstantOperand (LLC.Int sp p)) (ConstantOperand (LLC.Int sq q))
  | sp == sq
  = pure (ConstantOperand (LLC.Int sp (f p q)))
withConstantFold _ op p q = op p q

add, sub, mul :: MonadIRBuilder m => Operand -> Operand -> m Operand
add = withConstantFold (+) LL.add

sub = withConstantFold (-) LL.sub

mul = withConstantFold (*) LL.mul

icmp :: MonadIRBuilder m => IntegerPredicate -> Operand -> Operand -> m Operand
icmp pred op@(ConstantOperand (LLC.Int sp p)) oq@(ConstantOperand (LLC.Int sq q))
  | sp == sq
  = case pred of
    EQ   -> pure $ if p == q then true else false
    NE   -> pure $ if p /= q then true else false
    pred -> LL.icmp pred op oq
icmp pred p q = LL.icmp pred p q

select :: MonadIRBuilder m => Operand -> Operand -> Operand -> m Operand
select (ConstantOperand (LLC.Int 1 p)) th el | p == 1    = pure th
                                             | otherwise = pure el
select pred th el = LL.select pred th el

min, max :: MonadIRBuilder m => Operand -> Operand -> m Operand
min p q = do
  cmp <- icmp ULT p q
  select cmp p q

max p q = do
  cmp <- icmp UGT p q
  select cmp p q

not :: MonadIRBuilder m => Operand -> m Operand
not = icmp EQ (bit 0)

fneg :: MonadIRBuilder m => Operand -> m Operand
fneg = fsub (double 0)

isNullPtr :: MonadIRBuilder m => Operand -> m Operand
isNullPtr ptr = icmp EQ ptr (nullPtr ty)
  where LLT.PointerType ty _ = LLT.typeOf ptr

isNotNullPtr :: MonadIRBuilder m => Operand -> m Operand
isNotNullPtr = not <=< isNullPtr

incr, decr :: MonadIRBuilder m => Operand -> m Operand
incr val = add val (constantOp (intConstant n 1))
  where LLT.IntegerType n = LLT.typeOf val

decr val = sub val (constantOp (intConstant n 1))
  where LLT.IntegerType n = LLT.typeOf val

intIs :: MonadIRBuilder m => Integral i => i -> Operand -> m Operand
intIs i val = icmp EQ val (constantOp (intConstant n i))
  where LLT.IntegerType n = LLT.typeOf val

-- Control flow
ite :: (MonadIRBuilder m, MonadFix m, Merge k) => Operand -> m k -> m k -> m k
ite (ConstantOperand (LLC.Int 1 n)) th el | n == 1    = th
                                          | otherwise = el
ite cmp th el = mdo
  condBr cmp thLabel elLabel
  thLabel     <- named block "if.then"
  thRes       <- th
  exitThLabel <- currentBlock
  br exitLabel
  elLabel     <- named block "if.else"
  elRes       <- el
  exitElLabel <- currentBlock
  br exitLabel
  exitLabel <- named block "if.exit"
  phiMerge [(thRes, exitThLabel), (elRes, exitElLabel)]

switchWithDefault
  :: MonadIRBuilder m
  => MonadFix m
  => Merge k => Operand -> Maybe (m k) -> [(Constant, m k)] -> m k
switchWithDefault (ConstantOperand cst) def branches
  | Just branch <- lookup cst branches <|> def = branch
switchWithDefault cond def branches = mdo
  br switchEntryPoint
  switchEntryPoint <- named block "switch.entry"
  switch cond defaultBranch entryLabels
  let go exitBlock (constant, generate) = do
        branchEntry <- named block "switch.branch"
        branchValue <- generate
        branchExit  <- currentBlock
        br exitBlock
        pure (constant, branchEntry, branchValue, branchExit)
  allBranches <- traverse (go exitBlock) branches
  let entryLabels =
        fmap (\(constant, entry, _, _) -> (constant, entry)) allBranches
      exitPhi = fmap (\(_, _, value, exit) -> (value, exit)) allBranches
  defaultBranch <- named block "switch.default"
  defaultValue  <- case def of
    Nothing -> do
      unreachable
      pure Nothing
    Just def -> do
      defaultValue <- def
      br exitBlock
      pure (Just defaultValue)

  exitBlock <- named block "switch.exit"
  case defaultValue of
    Nothing           -> phiMerge exitPhi
    Just defaultValue -> phiMerge ((defaultValue, defaultBranch) : exitPhi)

-- switch that assumes the list of alternatives is exhaustive. It automatically
-- generates a default unreachable block
switchExhaustive
  :: MonadIRBuilder m => MonadFix m => Operand -> [(Constant, Name)] -> m ()
switchExhaustive cond branches = mdo
  switch cond unreachableBlock branches
  unreachableBlock <- named block "unreachable"
  unreachable

-- One must be very careful wrt to the lazyness of the step function. In
-- particular, the accumulator argument of testFromStep must *not* be evaluated
-- by the test function. This can happen unvoluntarily when the accumulator is
-- some kind of constructed type, like a tuple. Use lazy patterns when that is
-- the case, see the for function for an example
while
  :: MonadIRBuilder m
  => MonadFix m
  => Merge a
  => Maybe BS.ShortByteString
  -> a
  -> (a -> m (Operand, a))
  -> (a -> m a)
  -> m a
while mnm initAcc test step = mdo
  let nm = fromMaybe "while" mnm
  entryBlock <- currentBlock
  br testBlock

  testBlock <- named block (nm <> ".test")
  mergedTestAcc <- phiMerge [(initAcc, entryBlock), (loopAcc, stepExitBlock)]
  (continue, testAcc) <- test mergedTestAcc
  condBr continue stepLabel exitLabel

  stepLabel     <- named block (nm <> ".step")
  loopAcc       <- step testAcc
  stepExitBlock <- currentBlock
  br testBlock

  exitLabel <- named block (nm <> ".exit")
  pure testAcc

data ForLoop k = ForLoop
  { forStepCt      :: Operand
  , forAccumulator :: k
  }

instance Merge k => Merge (ForLoop k) where
  phiMerge ps = do
    stepCt <- phi (fmap (first forStepCt) ps)
    acc    <- phiMerge (fmap (first forAccumulator) ps)
    pure (ForLoop stepCt acc)

for
  :: MonadIRBuilder m
  => MonadFix m
  => Merge k
  => Maybe BS.ShortByteString
  -> Operand
  -> Operand
  -> k
  -> (Operand -> k -> m k)
  -> m (Operand, k)
for nm start end initAcc step = case (LLT.typeOf start, LLT.typeOf end) of
  (LLT.IntegerType n, LLT.IntegerType m)
    | n == m -> do
      ForLoop ct k <- while (nm <|> Just "for")
                            (ForLoop start initAcc)
                            loopTest
                            (loopStep n)
      pure (ct, k)
    | otherwise -> internalError (msg ++ expl1 (n, m))
  (ty1, ty2) -> internalError (msg ++ expl2 (ty1, ty2))
 where
  msg
    = "A for loop can only be used with integer bounds of the \
              \same size. "

  expl1 (n, m) =
    "The start operand has size "
      ++ show n
      ++ " bits \
                       \while the end operand has size "
      ++ show m
      ++ " bits."

  expl2 (ty1, ty2) =
    "The start operand has type "
      ++ show ty1
      ++ " and the end operand \
          \has type "
      ++ show ty2
      ++ "."

  loopIdxNm = "loop_idx"

  loopTest loop@(ForLoop acc _) = do
    continue <- icmp ULT acc end
    pure (continue, loop)

  loopStep n (ForLoop acc k) = do
    k   <- step acc k
    acc <- named (add (constantOp (LLC.Int n 1)) acc) loopIdxNm
    pure (ForLoop acc k)

accumulate
  :: MonadIRBuilder m
  => MonadFix m
  => Merge k
  => Maybe BS.ShortByteString
  -> Operand
  -> Operand
  -> k
  -> (k -> k -> m k)
  -> (Operand -> m k)
  -> m k
accumulate nm start end init combine step = snd <$> for
  (nm <|> Just "accumulate")
  start
  end
  init
  (\i ok -> step i >>= combine ok)

iter
  :: MonadIRBuilder m
  => MonadFix m
  => Maybe BS.ShortByteString
  -> Operand
  -> Operand
  -> (Operand -> m ())
  -> m ()
iter nm start end step =
  void (for (nm <|> Just "iter") start end () (\acc _ -> step acc))

fsum
  :: MonadIRBuilder m
  => MonadFix m => Operand -> Operand -> (Operand -> m Operand) -> m Operand
fsum start end = accumulate (Just nm)
                            start
                            end
                            (double 0)
                            (\x y -> named (fadd x y) nm)
  where nm = "sum"

fprod
  :: MonadIRBuilder m
  => MonadFix m => Operand -> Operand -> (Operand -> m Operand) -> m Operand
fprod start end = accumulate (Just nm)
                             start
                             end
                             (double 1)
                             (\x y -> named (fmul x y) nm)
  where nm = "prod"

assertion :: MonadIRBuilder m => MonadFix m => m Operand -> Operand -> m ()
assertion cond err = do
  cond <- cond
  rec condBr cond successBlock failedBlock

      failedBlock <- named block "assertion.failed"
      ret err

      successBlock <- named block "assertion.success"
  pure ()

ret0 :: MonadIRBuilder m => m ()
ret0 = ret (int32 0)

-- Types
voidPtr :: LLT.Type
voidPtr = LLT.ptr LLT.i8

structType :: [LLT.Type] -> LLT.Type
structType tys =
  LLT.StructureType { LLT.isPacked = False, LLT.elementTypes = tys }

-- Definitions
globalMut :: MonadModuleBuilder m => Name -> LLT.Type -> Constant -> m Constant
globalMut nm ty initVal = do
  emitDefn $ GlobalDefinition globalVariableDefaults { isConstant = False }
    { name                  = nm
    , LLVM.AST.Global.type' = ty
    , linkage               = Private
    , initializer           = Just initVal
    }
  pure $ LLC.GlobalReference (LLT.ptr ty) nm

globalConstant
  :: MonadModuleBuilder m => Name -> LLT.Type -> Constant -> m Constant
globalConstant nm ty initVal = do
  emitDefn $ GlobalDefinition globalVariableDefaults { isConstant = True }
    { name                  = nm
    , LLVM.AST.Global.type' = ty
    , linkage               = Private
    , initializer           = Just initVal
    }
  pure $ LLC.GlobalReference (LLT.ptr ty) nm

privateFunction
  :: MonadModuleBuilder m
  => Name
  -> [(LLT.Type, ParameterName)]
  -> LLT.Type
  -> ([Operand] -> IRBuilderT m ())
  -> m Operand
privateFunction label argtys retty body = do
  let (tys, _) = unzip argtys
  (paramNames, blocks) <- runIRBuilderT emptyIRBuilder $ do
    paramNames <- forM argtys $ \(_, paramName) -> case paramName of
      NoParameterName -> fresh
      ParameterName p -> fresh `named` p
    body $ zipWith LocalReference tys paramNames
    return paramNames
  let def = GlobalDefinition functionDefaults
        { name        = label
        , parameters  = ( zipWith (\ty nm -> Parameter ty nm []) tys paramNames
                        , False
                        )
        , returnType  = retty
        , basicBlocks = blocks
        , linkage     = Private
        }
      funty = LLT.ptr $ FunctionType retty (fst <$> argtys) False
  emitDefn def
  pure $ ConstantOperand $ LLC.GlobalReference funty label

-- Types
arrayType :: Integral i => i -> LLT.Type -> LLT.Type
arrayType = LLT.ArrayType . fromIntegral

