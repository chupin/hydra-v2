{
{-# OPTIONS_GHC -Wno-all #-}
{-# LANGUAGE DataKinds, MultiParamTypeClasses, OverloadedStrings, FlexibleContexts #-}

module Hydra.Lexer (lexStep) where

import Hydra.Op
import Hydra.Tokens hiding (mkTok, mkConstTok)
import qualified Hydra.Tokens as Tokens
import Hydra.Steps
import Hydra.Loc
import Hydra.Error
import Hydra.Pretty
import Hydra.Compiler
import qualified Data.Text.Read as T
import Data.Either (fromRight)

}

%wrapper "monad"

$digits = 0-9
$lower = a-z
$upper = A-Z
$alpha = [$lower$upper]
$ascsymb = [\=\<\>\@\^\|\&\~\+\-\*\/\$\%\!\?\.]
$decdigit = [0-9]
$binit = [0-1]
$octit = [0-7]
$hexit = [0-9A-Fa-f]
$white = [\ \t\n\r\f\v]
$charesc = [abfnrtv\\\"\']

@lident = $lower($alpha|_|'|$decdigit)*
@uident = $upper($alpha|_|'|$decdigit)*

@op1 = [\|]     $ascsymb*
@op2 = [\&]     $ascsymb*
@op3 = [\=\<\>] $ascsymb*
@op4 = [\@\^]   $ascsymb*
@op5 = [\+\-]   $ascsymb*
@op6 = [\*\/\%] $ascsymb*
@mop = [\!\?]   $ascsymb*

@decimal     = $decdigit+
@binary      = $binit+
@octal       = $octit+
@hexadecimal = $hexit+
@exponent    = [eE] [\-\+]? @decimal

@floating_point = @decimal \. @decimal @exponent? | @decimal @exponent

@escape = \\ ($charesc | @decimal | o @octal | x @hexadecimal)
@char = \' ($printable # [\\\'] | $white | @escape) \'
@gap = \\ $white+ \\
@str = \" ($printable # [\\\"] | $white | @escape | @gap)* \"

tokens :-
       $white+              { skip }
       "--".*               { skip }
       "sigrel"             { mkConstTok TokModel }
       "let"                { mkConstTok TokLet }
       "init"               { mkConstTok TokInit }
       "reinit"             { mkConstTok TokReinit }
       "switch"             { mkConstTok TokSwitch }
       "when"               { mkConstTok TokWhen }
       "mode"               { mkConstTok TokMode }
       "der"                { mkConstTok TokDer }
       "sqrt"               { mkConstTok (TokOp (Unop (unop Sqrt))) }
       "exp"                { mkConstTok (TokOp (Unop (unop Exp))) }
       "log"                { mkConstTok (TokOp (Unop (unop Log))) }
       "sin"                { mkConstTok (TokOp (Unop (unop Sin))) }
       "tan"                { mkConstTok (TokOp (Unop (unop Tan))) }
       "cos"                { mkConstTok (TokOp (Unop (unop Cos))) }
       "asin"               { mkConstTok (TokOp (Unop (unop Asin))) }
       "atan"               { mkConstTok (TokOp (Unop (unop Atan))) }
       "acos"               { mkConstTok (TokOp (Unop (unop Acos))) }
       "sinh"               { mkConstTok (TokOp (Unop (unop Sinh))) }
       "tanh"               { mkConstTok (TokOp (Unop (unop Tanh))) }
       "cosh"               { mkConstTok (TokOp (Unop (unop Cosh))) }
       "asinh"              { mkConstTok (TokOp (Unop (unop Asinh))) }
       "atanh"              { mkConstTok (TokOp (Unop (unop Atanh))) }
       "acosh"              { mkConstTok (TokOp (Unop (unop Acosh))) }
       "abs"                { mkConstTok (TokOp (Unop (unop Abs))) }
       "up"                 { mkConstTok TokUp }
       "down"               { mkConstTok TokDown }
       "updown"             { mkConstTok TokUpDown }
       ","                  { mkConstTok TokComma }
       "="                  { mkConstTok TokEq }
       "<>"                 { mkConstTok TokConn }
       "+"                  { mkConstTok (TokOp (Binop PriorityAdd (binop Add))) }
       "*"                  { mkConstTok (TokOp (Binop PriorityMul (binop Mul))) }
       "-"                  { mkConstTok (TokOp (Binop PrioritySub (binop Sub))) }
       "/"                  { mkConstTok (TokOp (Binop PriorityDiv (binop Div))) }
       "^"                  { mkConstTok (TokOp (Binop PriorityPow (binop Pow))) }
       "("                  { mkConstTok TokLParen }
       ")"                  { mkConstTok TokRParen }
       "{"                  { mkConstTok TokLBrack }
       "}"                  { mkConstTok TokRBrack }
       ";"                  { mkConstTok TokSemiCol }
       "->"                 { mkConstTok TokArr }
       @lident              { mkTok (\s -> TokLIdent s) }
       @uident              { mkTok (\s -> TokUIdent s) }
       @decimal |
         0[bB] @binary |
         0[oO] @octal |
         0[xX] @hexadecimal { mkTok (\s -> TokInt (fst (fromRight undefined (T.decimal s)))) }
       @floating_point      { mkTok (\s -> TokDouble (fst (fromRight undefined (T.rational s)))) }

{

posn :: AlexPosn -> Int -> FilePath -> Loc
posn (AlexPn o r c) len fp = Loc Span { spanStart = Pos { posFile = fp
                                                        , posLine = r
                                                        , posCol = c
                                                        , posOffset = o
                                                        }
                                      , spanLen = len
                                      }

alexEOF = pure TokEOF

scanner s =
  runAlex s loop
  where loop = do
         tok <- alexMonadScan
         case tok of
           TokEOF -> pure []
           _ -> do toks <- loop
                   pure (tok : toks)


data LexingError = LexingError String

instance Message Error LexingError where
  msgTitle _ = "lexing error"
  msgBody _ (LexingError body) = [pretty body]

lexStep :: StepSpec CompilerM String (VSep (Token Loc))
lexStep = Step { stepConf = fromStepName "lexing"
               , stepExec = SingleStep $ \i -> case scanner i of
                                     Left err -> throw (LexingError err)
                                     Right toks -> do
                                       conf <- conf
                                       let fp = sourceFile conf
                                       pure (VSep (fmap (fmap ($ fp)) toks))
                }

mkTok = Tokens.mkTok posn
mkConstTok = Tokens.mkConstTok posn

}
