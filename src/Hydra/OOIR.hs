{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}

{-# LANGUAGE DeriveGeneric #-}
module Hydra.OOIR
  ( module Hydra.OOIR
  , module Hydra.OOIR.AST
  ) where

import           Control.Lens            hiding ( Const
                                                , Empty
                                                )
import           Control.Monad.RWS.Strict

import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Set                       ( Set )
import qualified Data.Set                      as S

import           Control.Comonad
import           Data.Text                      ( Text )
import           GHC.Generics                   ( Generic )
import           Hydra.Compiler
import qualified Hydra.Flatten.AST             as F
import           Hydra.OOIR.AST
import           Hydra.Pretty
import           Hydra.Steps

-- This module is in charge of the conversion from the core representation to
-- the OOIR form. The translation is fairly straight-forward, but we have to be
-- careful with captures
--
-- When encountering an identifier, depending on the context we are in, this
-- might lead to different choices. In particular, if we are compiling a closure
-- or a model, a local variable might have to be replaced and captured by the
-- object being compiled if it comes from outside the model.

-- The context for the translator consists in two scopes: the direct scope,
-- which contains local variables directly accessible by an expression. These
-- are the direct arguments of a function, or the arguments of a mode in a
-- switch. The captured scope contains all other variables: free-variables of
-- closures and models. The translation functions are in charge of replacing the
-- identifiers that are member of the captured scope and recording their
-- capture.
data TranslCtxt = TranslCtxt
  { directScope   :: Set Ident
  , capturedScope :: Set Ident
  }
  deriving Generic

-- The state of the translator contains substitution for captured identifiers
-- (mapping the original name to the new name after capture)
data TranslSt k = TranslSt
  { capturedIdents  :: Map Ident Capture
  , blockAttributes :: k
  }
  deriving Generic

type TranslM k a = RWST TranslCtxt () (TranslSt k) CompilerM a

runTranslM :: TranslM () a -> TranslCtxt -> CompilerM (a, Map Ident Capture)
runTranslM run ctxt = do
  (a, TranslSt { capturedIdents = idts }, _) <- runRWST
    run
    ctxt
    TranslSt { capturedIdents = [], blockAttributes = () }
  pure (a, idts)

runTopLevelTranslM :: TranslM () a -> CompilerM a
runTopLevelTranslM run = do
  (a, capt) <- runTranslM run (TranslCtxt [] [])
  if not (M.null capt)
    then internalError "A top level declaration cannot capture local variables"
    else pure a

isolateTransl :: TranslM (Map Ident Expr) a -> TranslM () (a, Map Ident Expr)
isolateTransl run = do
  r <- ask
  TranslSt { capturedIdents = subst, blockAttributes = () } <- get
  (a, TranslSt { capturedIdents = subst, blockAttributes = k }, ()) <-
    lift
      $ runRWST run r TranslSt { capturedIdents = subst, blockAttributes = [] }
  put TranslSt { capturedIdents = subst, blockAttributes = () }
  pure (a, k)

liftTransl :: TranslM () a -> TranslM k a
liftTransl run = do
  r           <- ask
  subst       <- use #capturedIdents
  (a, st, ()) <- lift
    $ runRWST run r TranslSt { capturedIdents = subst, blockAttributes = () }
  #capturedIdents .= st ^. #capturedIdents
  pure a

fresh :: Text -> TranslM k Ident
fresh src = lift $ do
  supply <- getSupplyWith (ident src)
  pure $ extract supply

generateExpr :: F.Expr -> TranslM k Expr
generateExpr F.Model { F.modelRelations = rels, F.modelOutput = output } = do
  TranslCtxt { capturedScope = outer, directScope = direct } <- ask
  (block, captures) <- lift $ runTranslM
    go
    TranslCtxt { capturedScope = outer <> direct, directScope = [] }
  pure $ ModelObj Obj { modelCaptures = captures
                      , modelOutput   = output
                      , modelBlock    = block
                      }
 where
  go = do
    (rels, attrs) <- isolateTransl $ traverse generateRel rels
    pure Block { blockRelations = rels, blockAttributes = attrs }
generateExpr F.OpExpr { F.operExpr = op } = OpExpr <$> traverse generateExpr op
generateExpr expr                         = NormExpr <$> generateNormExpr expr

generateNormExpr :: F.Expr -> TranslM k NormExpr
generateNormExpr fun@F.GlobalExpr{} =
  Call <$> generateCall (F.functionCall fun [])
generateNormExpr F.Call { F.callExpr = call } = Call <$> generateCall call
generateNormExpr F.Const { F.constVal = F.RealConst c } =
  pure (AtomExpr (Const c))
generateNormExpr F.LocalExpr { F.localExprName = nm, F.localExprTyp = ty } = do
  -- Upon encountering a local identifier, we must first check whether or not
  -- we've seen it before. If so, we replace. Then, we must check whether or not
  -- it is captured, by looking-up into the captured variable scope.
  subst <- use #capturedIdents
  case M.lookup nm subst of
    Just Capture { captureRepl = repl } -> pure (AtomExpr (LocalExpr repl ty))
    Nothing                             -> do
      outer <- view #capturedScope
      if nm `S.member` outer
        then do
          repl <- fresh ("captured_" <> source nm)
          #capturedIdents
            %= M.insert nm Capture { captureRepl = repl, captureTy = ty }
          pure (AtomExpr (LocalExpr repl ty))
        else pure (AtomExpr (LocalExpr nm ty))
generateNormExpr expr = internalError
  ("Cannot generate a normalized expression from " ++ show (pretty expr))

generateCall :: F.Call -> TranslM k Call
generateCall (F.KnownCall sat global args) = do
  args <- traverse generateExpr args
  pure (KnownCall sat global args)
generateCall (F.UnknownCall fun args) = do
  fun  <- generateNormExpr fun
  args <- traverse generateExpr args
  pure (UnknownCall fun args)

generateAttrExpr :: F.Expr -> TranslM (Map Ident Expr) AtomExpr
generateAttrExpr expr = do
  -- Compile the expression. If it's already atomic, we just use it, if it's
  -- not, record it into the block attributes
  expr <- generateExpr expr
  case expr of
    NormExpr (AtomExpr expr) -> pure expr
    expr                     -> do
      repl <- fresh "model"
      #blockAttributes %= M.insert repl expr
      pure (LocalExpr repl (F.typeOf expr))

generateSig :: F.Sig -> TranslM (Map Ident Expr) Sig
generateSig F.Expr { F.sigExpr = e }       = SigExpr <$> generateAttrExpr e
generateSig F.Local { F.localSig = local } = pure (Local local)
generateSig F.Op { F.operSig = op }        = Op <$> traverse generateSig op

generateRel :: F.Rel -> TranslM (Map Ident Expr) Rel
generateRel (F.Rel       holds eqn  ) = Eqn holds <$> generateSig (F.eqnSig eqn)
generateRel (F.Let       ids   rels ) = Let ids <$> traverse generateRel rels
generateRel (F.ModelCall sigs  model) = do
  model <- generateAttrExpr model
  case model of
    LocalExpr model _ -> pure (ModelCall sigs model)
    Const _           -> internalError "A model cannot be a constant"
generateRel (F.Switch initMode branches) = do
  initMode <- traverse generateExpr initMode
  let
    insertArgs args scope =
      foldr (\(F.Arg ident _) scope -> S.insert ident scope) scope args

    genBranch F.Branch { F.branchArgs = args, F.branchRelations = rels, F.branchReinits = reinits, F.branchConditions = conds }
      = do
        ((conds, rels), attrs) <-
          isolateTransl $ locally #directScope (insertArgs args) $ do
            conds <- traverse generateCond conds
            rels  <- traverse generateRel rels
            pure (conds, rels)
        pure Branch
          { branchArguments  = args
          , branchBlock      = Block { blockAttributes = attrs
                                     , blockRelations  = rels
                                     }
          , branchConditions = conds
          , branchReinits    = reinits
          }
     where
      generateCond (F.Root { F.rootKind = kd, F.watchedSig = sig } F.:-> targetMode)
        = do
          sig        <- generateSig sig
          targetMode <- traverse generateSig targetMode
          pure (Root { rootKind = kd, watchedSig = sig } :-> targetMode)

  branches <- liftTransl $ traverse genBranch branches
  pure (Switch initMode branches)

generateInst :: F.Inst -> CompilerM Inst
generateInst F.Inst { F.instArgs = args, F.instBody = body } = do
  runTopLevelTranslM
    $ locally #directScope
              (const (foldr (\(F.Arg ident _) -> S.insert ident) [] args))
    $ do
        body <- generateExpr body
        pure Inst { instArgs = args, instBody = body }

generateDecl :: F.Decl -> CompilerM Decl
generateDecl F.MonoDecl { F.declName = nm, F.declInst = inst } = do
  inst <- generateInst inst
  pure MonoDecl { declName = nm, declInst = inst }
generateDecl F.Template { F.declName = nm, F.templateInsts = insts } = do
  insts <- traverse generateInst insts
  pure Template { declName = nm, templateInsts = insts }

generateModule :: F.Module -> CompilerM Module
generateModule F.Module { F.moduleDecls = decls } = do
  decls <- traverse generateDecl decls
  pure Module { moduleDecls = decls }

generateIR :: StepSpec CompilerM F.Module Module
generateIR = Step
  { stepExec = SingleStep generateModule
  , stepConf = StepConf { stepName       = "object-oriented IR generation"
                        , stepStopBefore = ["stop-before-ooir-generation"]
                        , stepStopAfter  = ["stop-after-ooir-generation"]
                        , stepSkip       = DontSkip
                        }
  }
