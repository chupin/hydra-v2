{-# LANGUAGE DataKinds #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.CodeGen
  ( codegenStep
  , emitModuleStep
  , CompiledModule
  ) where

import qualified Control.Exception             as Exc
import           Control.Monad
import           Control.Monad.Trans

import qualified LLVM.Context                  as LL
import qualified LLVM.Module                   as LL
                                         hiding ( Module )

import           Hydra.CodeGen.Expr
import           Hydra.Compiler
import           Hydra.OOIR
import           Hydra.Pretty
import           Hydra.Steps

import           System.Directory
import           System.FilePath                ( addExtension
                                                , dropExtension
                                                , takeExtension
                                                )
import           System.IO.Error                ( isPermissionError )

codegenStep :: StepSpec CompilerM Module CompiledModule
codegenStep = Step
  { stepConf = StepConf { stepStopBefore = ["stop-before-llvm-code-generation"]
                        , stepStopAfter  = ["stop-after-llvm-code-generation"]
                        , stepSkip       = DontSkip
                        , stepName       = "LLVM code generation"
                        }
  , stepExec = SingleStep compileModule
  }

data LLVMOutputKind = Unknown FilePath | TextFormat | BitcodeFormat

newtype EmitError = PermissionDenied FilePath

instance Message 'Error EmitError where
  msgTitle _ = "emission error"

  msgBody _ (PermissionDenied fp) =
    ["The file" <+> pretty fp <+> "is not writable."]

newtype EmitWarning = UnknownOutputExtension FilePath

instance Message 'Warning EmitWarning where
  msgTitle _ = "emission warning"

  msgBody _ (UnknownOutputExtension fp) =
    [preamb <+> "By default, the code will be emitted in LLVM textual format."]
   where
    preamb = case fp of
      []  -> "The output file hasn't been given an extension."
      ext -> "The extension" <+> pretty ext <+> "is unknown."

emitModule :: CompiledModule -> CompilerM ()
emitModule CompiledModule { llvmBlob = mod } = do
  conf <- conf
  let (file, kd) = case outputFile conf of
        Nothing ->
          (addExtension (dropExtension (sourceFile conf)) "ll", TextFormat)
        Just file -> (file, kd)
         where
          kd = case takeExtension file of
            ".ll" -> TextFormat
            ".bc" -> BitcodeFormat
            ext   -> Unknown ext

      writeLLVM = case kd of
        BitcodeFormat -> LL.writeBitcodeToFile
        _             -> LL.writeLLVMAssemblyToFile

  case kd of
    Unknown ext -> warn (UnknownOutputExtension ext)
    _           -> pure ()

  fileExists <- liftIO $ doesFileExist file
  when fileExists $ do
    permissions <- liftIO $ getPermissions file
    if writable permissions
      then liftIO $ Exc.catch
        (removeFile file)
        (\e -> unless (isPermissionError e) (Exc.throw e))
      else throw (PermissionDenied file)
  liftIO $ LL.withContext $ \ctxt -> LL.withModuleFromAST ctxt mod $ \mod -> do
    -- verify mod
    writeLLVM (LL.File file) mod

emitModuleStep :: StepSpec CompilerM CompiledModule ()
emitModuleStep = Step
  { stepConf = StepConf { stepStopBefore = ["stop-before-llvm-code-emission"]
                        , stepStopAfter  = ["stop-after-llvm-code-emission"]
                        , stepSkip       = DontSkip
                        , stepName       = "LLVM code emission"
                        }
  , stepExec = SingleStep emitModule
  }
