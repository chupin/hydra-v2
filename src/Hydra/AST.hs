{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}

{-# LANGUAGE DeriveGeneric #-}
module Hydra.AST
  ( module Hydra.AST
  , module Hydra.Ident
  , module Hydra.Op
  , module Hydra.Name
  , module Hydra.Const
  , module Hydra.Label
  ) where

import           Data.Foldable
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Semigroup                 ( Max )
import           Data.Sequence                  ( Seq(..) )
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           GHC.Generics                   ( Generic )
import           Hydra.Const
import           Hydra.Ident
import           Hydra.Label
import           Hydra.Loc
import           Hydra.Name
import           Hydra.Op
import           Hydra.Pretty
import           Hydra.Type

---------------------------------------------------------------------------
-- Signals
---------------------------------------------------------------------------
data RootKind = UpRoot | DownRoot | AnyRoot

data Sig g ety sty i =
    Expr { sigLoc     :: !Loc
         , sigExprTyp :: sty
         , sigExpr    :: Expr g ety sty i
         }
  | Local { sigLoc    :: !Loc
          , localTyp  :: sty
          , localName :: i
          }
  | Tuple { sigLoc       :: !Loc
          , tuplePayload :: Seq (Sig g ety sty i)
          }
  | Der { sigLoc     :: !Loc
        , derivedSig :: Sig g ety sty i
        }
  | Op { sigLoc  :: !Loc
       , operSig :: Op (Sig g ety sty i)
       }

data Event g ety sty i = Root
  { eventLoc   :: !Loc
  , rootKind   :: RootKind
  , watchedSig :: Sig g ety sty i
  }

data Expr g ety sty i =
    Const { exprLoc  :: !Loc
          , constVal :: Const
          }
  | LocalExpr { exprLoc       :: !Loc
              , localExprTyp  :: ety
              , localExprName :: i
              }
  | Global { exprLoc    :: !Loc
           , globalTyp  :: ety
           , globalInfo :: !g
           }
  | OpExpr { exprLoc  :: !Loc
           , operExpr :: Op (Expr g ety sty i)
           }
  | App { exprLoc  :: !Loc
        , appRetTy :: ety
        , exprFun  :: Expr g ety sty i
        , exprArg  :: Expr g ety sty i
        }
  | Model { exprLoc        :: !Loc
          , modelOutput    :: Pat sty i
          , modelRelations :: Seq (Rel g ety sty i)
          }

instance Typed (Sig g ety (TSig stv) i) (TSig stv) where
  typeOf Expr { sigExprTyp = ty }    = ty
  typeOf Local { localTyp = ty }     = ty
  typeOf Tuple { tuplePayload = es } = TSigTuple (fmap typeOf es)
  typeOf Der{}                       = trealSig
  typeOf Op{}                        = trealSig

instance Typed (Expr g (TExpr etv stv) (TSig stv) i) (TExpr etv stv) where
  typeOf Const { constVal = c }          = typeConst c
  typeOf LocalExpr { localExprTyp = ty } = ty
  typeOf Global { globalTyp = ty }       = ty
  typeOf Model { modelOutput = output }  = TModel (typeOf output)
  typeOf OpExpr{}                        = trealExpr
  typeOf App { appRetTy = ty }           = ty

instance Localized (Sig g ety sty i) where
  localize = sigLoc

instance Localized (Expr g ety sty i) where
  localize = exprLoc

exprSig :: sty -> Expr g ety sty i -> Sig g ety sty i
exprSig ty expr =
  Expr { sigLoc = localize expr, sigExpr = expr, sigExprTyp = ty }

realConstExpr :: Loc -> Rational -> Expr g ety sty i
realConstExpr loc = constExpr loc . RealConst

realConst :: Loc -> Rational -> Sig g ety (TSig sv) i
realConst loc c = exprSig TReal (constExpr loc (RealConst c))

localSig :: Loc -> sty -> i -> Sig g ety sty i
localSig loc ty i = Local { sigLoc = loc, localName = i, localTyp = ty }

derSig :: Loc -> Sig g ety sty i -> Sig g ety sty i
derSig loc s = Der { sigLoc = loc, derivedSig = s }

event :: Loc -> RootKind -> Sig g ety sty i -> Event g ety sty i
event loc root sig = Root { eventLoc = loc, rootKind = root, watchedSig = sig }

up, down, upDown :: Loc -> Sig g ety sty i -> Event g ety sty i
up loc = event loc UpRoot

down loc = event loc DownRoot

upDown loc = event loc AnyRoot

tupleSig :: Loc -> Seq (Sig g ety sty i) -> Sig g ety sty i
tupleSig loc ss = Tuple { sigLoc = loc, tuplePayload = ss }

opSig :: Loc -> Op (Sig g ety sty i) -> Sig g ety sty i
opSig loc op = Op { sigLoc = loc, operSig = op }

constExpr :: Loc -> Const -> Expr g ety sty i
constExpr loc c = Const { constVal = c, exprLoc = loc }

localExpr :: Loc -> ety -> i -> Expr g ety sty i
localExpr loc ty i =
  LocalExpr { exprLoc = loc, localExprTyp = ty, localExprName = i }

globalExpr :: Loc -> ety -> g -> Expr g ety sty i
globalExpr loc ty g = Global { exprLoc = loc, globalTyp = ty, globalInfo = g }

app :: Loc -> ety -> Expr g ety sty i -> Expr g ety sty i -> Expr g ety sty i
app loc ety fun arg =
  App { exprLoc = loc, exprFun = fun, exprArg = arg, appRetTy = ety }

opExpr :: Loc -> Op (Expr g ety sty i) -> Expr g ety sty i
opExpr loc op = OpExpr { operExpr = op, exprLoc = loc }

data SigVarOcc sty = SigVarOcc
  { diffCount    :: Max Word
  , variableType :: sty
  }
  deriving Generic

instance Semigroup (SigVarOcc sty) where
  SigVarOcc ct1 vty <> SigVarOcc ct2 _ = SigVarOcc (ct1 <> ct2) vty

freeSigVar :: Ord i => Sig g ety sty i -> Map i (SigVarOcc sty)
freeSigVar Expr{} = M.empty
freeSigVar Local { localTyp = ty, localName = i } =
  M.singleton i SigVarOcc { diffCount = 0, variableType = ty }
freeSigVar Tuple { tuplePayload = ts } =
  foldr (M.unionWith (<>) . freeSigVar) [] ts
freeSigVar Der { derivedSig = sig } = fmap succ (freeSigVar sig)
 where
  succ occ@SigVarOcc { diffCount = count } = occ { diffCount = count + 1 }
freeSigVar Op { operSig = op } = foldr (M.unionWith (<>) . freeSigVar) [] op

---------------------------------------------------------------------------
-- Patterns
---------------------------------------------------------------------------
data Pat ty i = PLocal { patLoc       :: !Loc
                       , localPatTyp  :: ty
                       , localPatName :: i
                       }
              | PTuple { patLoc          :: !Loc
                       , tuplePatPayload :: Seq (Pat ty i)
                       }

instance Typed (Pat (TExpr etv stv) i) (TExpr etv stv) where
  typeOf PLocal { localPatTyp = ty }     = ty
  typeOf PTuple { tuplePatPayload = ts } = TExprTuple (fmap typeOf ts)

instance Typed (Pat (TSig stv) i) (TSig stv) where
  typeOf PLocal { localPatTyp = ty }     = ty
  typeOf PTuple { tuplePatPayload = ts } = TSigTuple (fmap typeOf ts)

localPat :: Loc -> ty -> i -> Pat ty i
localPat loc ty i = PLocal { patLoc = loc, localPatTyp = ty, localPatName = i }

tuplePat :: Loc -> Seq (Pat ty i) -> Pat ty i
tuplePat loc ps = PTuple { patLoc = loc, tuplePatPayload = ps }

patBinds :: Ord i => Pat ty i -> Map i ty
patBinds PLocal { localPatTyp = ty, localPatName = i } = [(i, ty)]
patBinds PTuple { tuplePatPayload = is }               = foldMap patBinds is

patToSig :: Pat sty i -> Sig g ety sty i
patToSig PLocal { patLoc = loc, localPatTyp = typ, localPatName = i } =
  localSig loc typ i
patToSig PTuple { patLoc = loc, tuplePatPayload = ps } =
  tupleSig loc (fmap patToSig ps)

instance Localized (Pat ty i) where
  localize = patLoc

---------------------------------------------------------------------------
-- Relations
---------------------------------------------------------------------------
data Eqn g ety sty i = Sig g ety sty i :=: Sig g ety sty i

data Rel g ety sty i =
    Eqn (Eqn g ety sty i)
  | Init (Eqn g ety sty i)
  | Sig g ety sty i :<>: Expr g ety sty i
  | Let (Map i (Located sty)) (Seq (Rel g ety sty i))
  | Switch { initialMode    :: Located (Mode (Expr g ety sty i))
           , modeSet        :: Set Label
           , switchBranches :: Seq (Branch g ety sty i)
           }

data Branch g ety sty i = Branch
  { branchMode       :: Located (Mode (Pat ety i))
  , branchReinits    :: Set (Located i)
  , branchConditions :: Seq (Condition g ety sty i)
  , branchRelations  :: Seq (Rel g ety sty i)
  }

data Mode r = Mode
  { modeName :: Label
  , modeArgs :: Seq r
  }
  deriving (Functor, Foldable, Traversable)

data Condition g ety sty i = Event g ety sty i :-> Located
  (Mode (Sig g ety sty i))

---------------------------------------------------------------------------
-- Declarations
---------------------------------------------------------------------------
data Decl g ety sty i = Decl
  { declName   :: Name
  , declParams :: Seq (Pat ety i)
  , declBody   :: Expr g ety sty i
  , declType   :: Scheme ety
  }

instance Typed (Decl g ety sty i) (Scheme ety) where
  typeOf = declType

---------------------------------------------------------------------------
-- Modules
---------------------------------------------------------------------------
newtype Module g ety sty i = Module { moduleDecls :: Seq (Decl g ety sty i)
                                    }

---------------------------------------------------------------------------
-- Pretty printing
---------------------------------------------------------------------------
instance Pretty RootKind where
  pretty UpRoot   = "up"
  pretty DownRoot = "down"
  pretty AnyRoot  = "updown"

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Sig g ety sty i) where
  pretty Expr { sigExpr = expr } = pretty expr
  pretty Local { localName = i, localTyp = ty } =
    parens (pretty i <+> ":" <+> pretty ty)
  pretty Tuple { tuplePayload = ps } = parens (commaList (fmap pretty ps))
  pretty Der { derivedSig = sig }    = "der" <+> parens (pretty sig)
  pretty Op { operSig = op }         = prettyOp (fmap pretty op)

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Event g ety sty i) where
  pretty Root { watchedSig = sig, rootKind = rt } =
    pretty rt <> parens (pretty sig)

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Expr g ety sty i) where
  pretty Const { constVal = c } = pretty c
  pretty LocalExpr { localExprName = i, localExprTyp = ty } =
    parens (pretty i <+> ":" <+> pretty ty)
  pretty Global { globalInfo = g } = pretty g
  pretty OpExpr { operExpr = op }  = prettyOp (fmap pretty op)
  pretty App { exprFun = fun, exprArg = arg } =
    parens (pretty fun <+> pretty arg)
  pretty Model { modelOutput = output, modelRelations = rels } =
    nest ilvl ("model" <+> pretty output <+> "with" $+$ block rels)

instance (Pretty i, Pretty ty) => Pretty (Pat ty i) where
  pretty PLocal { localPatName = i, localPatTyp = ty } =
    parens (pretty i <+> ":" <+> pretty ty)
  pretty PTuple { tuplePatPayload = ps } = parens (commaList (fmap pretty ps))

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Eqn g ety sty i) where
  pretty (e1 :=: e2) = pretty e1 <+> "=" <+> pretty e2

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Rel g ety sty i) where
  pretty (Eqn  e        ) = pretty e
  pretty (Init e        ) = "init" <+> parens (pretty e)
  pretty (sig :<>: model) = pretty sig <+> "<>" <+> pretty model
  pretty (Let is rs) =
    nest
        ilvl
        (   "let"
        <+> commaList (M.mapWithKey (\i _ -> pretty i) is)
        <+> "in"
        $+$ block rs
        )
      $+$ "end"
  pretty (Switch ilab modeSet branches) = nest
    ilvl
    (   "switch"
    <+> "init"
    <+> pretty ilab
    <+> "modes"
    <+> commaList (fmap pretty (toList modeSet))
    $+$ lineBlock (fmap pretty branches)
    )

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Branch g ety sty i) where
  pretty (Branch mode reinits conds rels) = nest
    ilvl
    (   "mode"
    <+> pretty mode
    <>  (if S.null reinits
          then ""
          else
            space
            <>  "reinit"
            <+> commaList (fmap pretty (toList reinits))
            <>  space
        )
    <>  nest ilvl ("->" $+$ block rels $+$ lineBlock (fmap pretty conds))
    )

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Condition g ety sty i) where
  pretty (sig :-> mode) = "when" <+> pretty sig <+> "->" <+> pretty mode

instance Pretty r => Pretty (Mode r) where
  pretty (Mode name args) =
    pretty name <> parens (commaList (fmap pretty args))

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Decl g ety sty i) where
  pretty Decl { declName = declName, declParams = declParams, declBody = body }
    = nest
      ilvl
      (   "let"
      <+> pretty declName
      <+> spaceList (fmap pretty declParams)
      <+> "="
      $+$ pretty body
      )

instance (Pretty g, Pretty i, Pretty ety, Pretty sty) => Pretty (Module g ety sty i) where
  pretty Module { moduleDecls = moduleDecls } =
    lineBlock (fmap pretty moduleDecls)
