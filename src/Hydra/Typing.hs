{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ViewPatterns #-}

module Hydra.Typing
  ( typingStep
  , Global(..)
  , TypedSig
  , TypedExpr
  , TypedEqn
  , TypedRel
  ) where

import           Control.Comonad
import           Control.Lens            hiding ( (:>)
                                                , Const
                                                , Empty
                                                , op
                                                )
import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State

import           Data.Foldable
import           Data.Generics.Labels           ( )
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           Data.Stream.Supply
import           Data.Text                      ( Text )
import           Data.Void

import           GHC.Generics

import           Hydra.AST
import           Hydra.Compiler
import           Hydra.Loc
import           Hydra.Pretty
import           Hydra.Steps
import           Hydra.Type

type TypedDecl = Decl Global PTExpr PTSig Ident
type TypedExpr = Expr Global PTExpr PTSig Ident
type TypedSig = Sig Global PTExpr PTSig Ident
type TypedEqn = Eqn Global PTExpr PTSig Ident
type TypedRel = Rel Global PTExpr PTSig Ident

data Global = ModuleBound
  { globalName     :: Name
  , globalDecl     :: Decl Global PTExpr PTSig Ident
  , globalTypSubst :: TySubst EVar SVar
  }

instance Eq Global where
  ModuleBound { globalName = nm1 } == ModuleBound { globalName = nm2 } =
    nm1 == nm2

instance Ord Global where
  ModuleBound { globalName = nm1 } `compare` ModuleBound { globalName = nm2 } =
    nm1 `compare` nm2

instance Typed Global (Scheme PTExpr) where
  typeOf ModuleBound { globalDecl = decl } = typeOf decl

instance Pretty Global where
  pretty ModuleBound { globalName = nm } = pretty nm

-- Impure types
newtype MSVar s = MSVar Word
  deriving ( Eq, Ord )

newtype MEVar s = MEVar Word
  deriving ( Eq, Ord )

instance Pretty (MSVar s) where
  pretty (MSVar mv) = pretty (SVar mv)

instance Pretty (MEVar s) where
  pretty (MEVar mv) = pretty (EVar mv)

type MTSig s = TSig (MSVar s)

type MTExpr s = TExpr (MEVar s) (MSVar s)

data Entry ty = Entry
  { identRepl :: !Ident
  , identTyp  :: !ty
  }
  deriving Functor

data Scope s = GlobalScope (Map Name TypedDecl)
             | ExprScope (Map Name (Entry (MTExpr s))) (Scope s)
             | SigScope (Map Name (Entry (MTSig s))) (Scope s)

data FoundExpr s = FoundGlobal TypedDecl | FoundLocalExpr (Entry (MTExpr s))

data LkpResult s =
  FoundExpr (FoundExpr s) | FoundSig (Entry (MTSig s)) | NotFound

scopeLookup :: Name -> Scope s -> LkpResult s
scopeLookup name scope = case scope of
  GlobalScope scope -> case M.lookup name scope of
    Just decl -> FoundExpr (FoundGlobal decl)
    Nothing   -> NotFound
  SigScope scope previous -> case M.lookup name scope of
    Just entry -> FoundSig entry
    Nothing    -> scopeLookup name previous
  ExprScope scope previous -> case M.lookup name scope of
    Just entry -> FoundExpr (FoundLocalExpr entry)
    Nothing    -> scopeLookup name previous

data LocalTypeEnv s = LocalTypeEnv
  { scope        :: Scope s
  , tsigFreezer  :: Loc -> MTSig s -> TypChkM s PTSig
  , texprFreezer :: Loc -> MTExpr s -> TypChkM s PTExpr
  }
  deriving Generic

emptyTypeEnv
  :: Map Name TypedDecl
  -> (Loc -> MTExpr s -> TypChkM s PTExpr)
  -> (Loc -> MTSig s -> TypChkM s PTSig)
  -> LocalTypeEnv s
emptyTypeEnv globalTypeEnv freezeTExpr freezeTSig = LocalTypeEnv
  { scope        = GlobalScope globalTypeEnv
  , tsigFreezer  = freezeTSig
  , texprFreezer = freezeTExpr
  }

data TypeHeap s = TypeHeap
  { tsigVarHeap          :: Map (MSVar s) (MTSig s)
  , texprVarHeap         :: Map (MEVar s) (MTExpr s)
  , nonAmbiguousTSigVar  :: Map (MSVar s) Loc
  , nonAmbiguousTExprVar :: Map (MEVar s) Loc
  }
  deriving Generic

emptyHeap :: TypeHeap s
emptyHeap = TypeHeap { tsigVarHeap          = []
                     , texprVarHeap         = []
                     , nonAmbiguousTSigVar  = []
                     , nonAmbiguousTExprVar = []
                     }

newtype TypChkM s a =
  TypChkM { runTypChkM :: ReaderT (LocalTypeEnv s) (StateT (TypeHeap s) CompilerM) a }
  deriving ( Functor, Applicative, Monad, MonadFix, MonadReader (LocalTypeEnv s)
           , MonadState (TypeHeap s), MonadError ErrMsg, MonadConfig, MonadLog )

supplyEntry :: (Word -> ty) -> TypChkM s (Supply (Name -> Entry ty))
supplyEntry mty = do
  supply <- TypChkM $ lift $ lift getSupply
  pure $ extend
    (\s name ->
      let (s1, s2) = split2 s
      in  Entry { identRepl = ident name (extract s1)
                , identTyp  = mty (extract s2)
                }
    )
    supply

freshEntry :: (Word -> ty) -> Name -> TypChkM s (Entry ty)
freshEntry ty nm = do
  supply <- supplyEntry ty
  pure (extract supply nm)

mkExprEntry :: Word -> MTExpr s
mkExprEntry = TExprVar . MEVar

mkSigEntry :: Word -> MTSig s
mkSigEntry = TSigVar . MSVar

freshTExprEntry :: Name -> TypChkM s (Entry (MTExpr s))
freshTExprEntry = freshEntry mkExprEntry

freshTSigEntry :: Name -> TypChkM s (Entry (MTSig s))
freshTSigEntry = freshEntry mkSigEntry

freshTExpr :: TypChkM s (MTExpr s)
freshTExpr = identTyp <$> freshTExprEntry ""

freshTSig :: TypChkM s (MTSig s)
freshTSig = identTyp <$> freshTSigEntry ""

freezeTSig :: MonadState (TypeHeap s) m => MTSig s -> m PTSig
freezeTSig typ = do
  typ <- pruneTSig typ
  case typ of
    TSigVar (MSVar tv) -> pure (TSigVar (SVar tv))
    TReal              -> pure TReal
    TSigTuple ts       -> TSigTuple <$> traverse freezeTSig ts

freezeTExpr :: MonadState (TypeHeap s) m => MTExpr s -> m PTExpr
freezeTExpr typ = do
  typ <- pruneTExpr typ
  case typ of
    TExprVar   (MEVar tv) -> pure (TExprVar (EVar tv))
    TSig       typ        -> TSig <$> freezeTSig typ
    TExprTuple ts         -> TExprTuple <$> traverse freezeTExpr ts
    TModel     typ        -> TModel <$> freezeTSig typ
    TArr it ot            -> TArr <$> freezeTExpr it <*> freezeTExpr ot

instantiate :: Scheme PTExpr -> TypChkM s (TySubst EVar SVar, MTExpr s)
instantiate (Forall evs svs typ) = do
  let go fresh subst tv = do
        ty <- fresh
        pure (M.insert tv ty subst)
  ses  <- foldM (go freshTExpr) [] evs
  sss  <- foldM (go freshTSig) [] svs
  ses' <- traverse (freezing #texprFreezer NoLoc) ses
  sss' <- traverse (freezing #tsigFreezer NoLoc) sss
  let
    tySubst = TySubst ses' sss'

    substTExpr ses _   (TExprVar tv) = ses M.! tv
    substTExpr _   svs (TSig     ty) = TSig (substTSig svs ty)
    substTExpr _   svs (TModel   ty) = TModel (substTSig svs ty)
    substTExpr ses svs (TArr it ot) =
      TArr (substTExpr ses svs it) (substTExpr ses svs ot)
    substTExpr ses svs (TExprTuple ts) =
      TExprTuple (fmap (substTExpr ses svs) ts)

    substTSig _   TReal          = TReal
    substTSig svs (TSigVar   tv) = svs M.! tv
    substTSig svs (TSigTuple ts) = TSigTuple (fmap (substTSig svs) ts)
  pure (tySubst, substTExpr ses sss typ)

generalize :: MTExpr s -> TypChkM s (Scheme PTExpr)
generalize typ = do
  typ <- freezeTExpr typ
  let (evs, svs) = freeTExprVar typ
  pure (Forall evs svs typ)

pruneTExpr :: MonadState (TypeHeap s) m => MTExpr s -> m (MTExpr s)
pruneTExpr typ@(TExprVar tv) = do
  heap <- use #texprVarHeap
  case M.lookup tv heap of
    Nothing  -> pure typ
    Just typ -> do
      typ <- pruneTExpr typ
      modifying #texprVarHeap (M.insert tv typ)
      pure typ
pruneTExpr typ = pure typ

pruneTSig :: MonadState (TypeHeap s) m => MTSig s -> m (MTSig s)
pruneTSig typ@(TSigVar tv) = do
  heap <- use #tsigVarHeap
  case M.lookup tv heap of
    Nothing  -> pure typ
    Just typ -> do
      typ <- pruneTSig typ
      modifying #tsigVarHeap (M.insert tv typ)
      pure typ
pruneTSig typ = pure typ

--- Type errors and warnings
data TypChkInfo = DeclInferredType Name (Scheme PTExpr)

instance Message 'Info TypChkInfo where
  msgTitle _ = "type checking"

  msgBody _ (DeclInferredType name sch) = [pretty name <+> ":" <+> pretty sch]

newtype TypChkWarn = ExprInSigContext Loc

instance Message 'Warning TypChkWarn where
  msgTitle ExprInSigContext{} = "typing warning"

  msgBody rdrSpan (ExprInSigContext loc) =
    ["The derivative of a constant has been taken.", renderLoc rdrSpan loc]

  msgLoc (ExprInSigContext loc) = loc

data TypeError s =
    NotInScope Loc Text
  | NotInScopeGlobal Loc Global
  | CantUnifyTExpr Loc (MTExpr s) (MTExpr s)
  | CantUnifyTSig Loc (MTSig s) (MTSig s)
  | OccursCheckTExpr Loc (MEVar s) (MTExpr s)
  | OccursCheckTSig Loc (MSVar s) (MTSig s)
  | IsNotAFunction Loc (MTExpr s)
  | ThisLabelDoesNotExist Loc Label (Set Label)
  | SignalInAnExprContext Loc Name
  | NonSignalReinitialized Loc Name
  | AmbiguousType Loc

cantUnifyMsg
  :: (Pretty tv, Pretty typ)
  => (Span -> Doc ann)
  -> Loc
  -> tv
  -> typ
  -> [Doc ann]
cantUnifyMsg rdrSpan loc typ1 typ2 =
  [ "Cannot unify type" <+> pretty typ1 <+> "with type" <+> pretty typ2
  , renderLoc rdrSpan loc
  ]

occursCheckMsg
  :: (Pretty tv, Pretty typ)
  => (Span -> Doc ann)
  -> Loc
  -> tv
  -> typ
  -> [Doc ann]
occursCheckMsg rdrSpan loc tv typ =
  [ "Occurs check failed. Cannot construct infinite type "
    <+> pretty tv
    <+> "="
    <+> pretty typ
  , renderLoc rdrSpan loc
  ]

instance Message 'Error (TypeError s) where
  msgTitle NotInScope{}             = "scoping error"
  msgTitle NotInScopeGlobal{}       = "scoping error"
  msgTitle ThisLabelDoesNotExist{}  = "scoping error"
  msgTitle CantUnifyTExpr{}         = "type error"
  msgTitle CantUnifyTSig{}          = "type error"
  msgTitle OccursCheckTExpr{}       = "type error"
  msgTitle OccursCheckTSig{}        = "type error"
  msgTitle IsNotAFunction{}         = "type error"
  msgTitle SignalInAnExprContext{}  = "staging error"
  msgTitle NonSignalReinitialized{} = "staging error"
  msgTitle AmbiguousType{}          = "type error"

  msgBody rdrSpan (NotInScope loc s) =
    ["The variable" <+> pretty s <+> "is not in scope", renderLoc rdrSpan loc]
  msgBody rdrSpan (NotInScopeGlobal loc g) =
    [ "The global variable" <+> pretty g <+> "is not in scope"
    , renderLoc rdrSpan loc
    ]
  msgBody rdrSpan (CantUnifyTExpr loc t1 t2) = cantUnifyMsg rdrSpan loc t1 t2
  msgBody rdrSpan (CantUnifyTSig  loc t1 t2) = cantUnifyMsg rdrSpan loc t1 t2
  msgBody rdrSpan (ThisLabelDoesNotExist loc lab allLabs) =
    [ "The label" <+> pretty lab <+> "is not a valid mode for the switch:"
      , renderLoc rdrSpan loc
      , "The list of labels for this mode is:"
      ]
      ++ fmap pretty (toList allLabs)
  msgBody rdrSpan (OccursCheckTExpr loc tv typ) =
    occursCheckMsg rdrSpan loc tv typ
  msgBody rdrSpan (OccursCheckTSig loc tv typ) =
    occursCheckMsg rdrSpan loc tv typ
  msgBody rdrSpan (IsNotAFunction loc tfun) =
    [ "The type" <+> pretty tfun <+> "is not a function type"
    , renderLoc rdrSpan loc
    ]
  msgBody rdrSpan (SignalInAnExprContext loc name) =
    [ "The identifier"
      <+> pretty name
      <+> "is a signal but it was used in a context"
      <+> "where an expression was expected."
    , renderLoc rdrSpan loc
    ]
  msgBody rdrSpan (NonSignalReinitialized loc name) =
    [ "The identifier"
      <+> pretty name
      <+> "is an expression but it was marked for reinitialization."
      <+> "This is non-sensical."
    , renderLoc rdrSpan loc
    ]
  msgBody rdrSpan (AmbiguousType loc) =
    [ "The type of"
    , renderLoc rdrSpan loc
    , "is ambiguous."
    , "It cannot be fully-determined from the type \
      \of the expression in which it appears."
    ]

  msgLoc (NotInScope       loc _       ) = loc
  msgLoc (NotInScopeGlobal loc _       ) = loc
  msgLoc (CantUnifyTExpr        loc _ _) = loc
  msgLoc (CantUnifyTSig         loc _ _) = loc
  msgLoc (ThisLabelDoesNotExist loc _ _) = loc
  msgLoc (OccursCheckTExpr      loc _ _) = loc
  msgLoc (OccursCheckTSig       loc _ _) = loc
  msgLoc (IsNotAFunction         loc _ ) = loc
  msgLoc (SignalInAnExprContext  loc _ ) = loc
  msgLoc (NonSignalReinitialized loc _ ) = loc
  msgLoc (AmbiguousType loc            ) = loc

occursCheck :: Ord tv => (typ -> Set tv) -> tv -> typ -> Bool
occursCheck freeVars tv typ = tv `S.member` freeVars typ

unifyTSig :: Loc -> MTSig s -> MTSig s -> TypChkM s ()
unifyTSig loc typ1 typ2 = do
  typ1 <- pruneTSig typ1
  typ2 <- pruneTSig typ2
  case (typ1, typ2) of
    (TReal, TReal)                      -> pure ()
    (TSigVar tv, TSigVar qv) | tv == qv -> pure ()
    (TSigVar tv, ty) | occursCheck freeTSigVar tv ty ->
      throw (OccursCheckTSig loc tv ty)
    (TSigVar tv, ty       ) -> modifying #tsigVarHeap (M.insert tv ty)
    (_         , TSigVar _) -> unifyTSig loc typ2 typ1
    (TSigTuple ps, TSigTuple qs) | length ps == length qs ->
      sequence_ $ Seq.zipWith (unifyTSig loc) ps qs
    (ty1, ty2) -> throw (CantUnifyTSig loc ty1 ty2)

unifyTExpr :: Loc -> MTExpr s -> MTExpr s -> TypChkM s ()
unifyTExpr loc typ1 typ2 = do
  typ1 <- pruneTExpr typ1
  typ2 <- pruneTExpr typ2
  case (typ1, typ2) of
    (TSig   ty1  , TSig ty2    ) -> unifyTSig loc ty1 ty2
    (TModel ty1  , TModel ty2  ) -> unifyTSig loc ty1 ty2
    (TArr it1 ot1, TArr it2 ot2) -> do
      unifyTExpr loc it1 it2
      unifyTExpr loc ot1 ot2
    (TExprTuple ts1, TExprTuple ts2) | length ts1 == length ts2 ->
      sequence_ $ Seq.zipWith (unifyTExpr loc) ts1 ts2
    (TExprVar tv, ty) | occursCheck (fst . freeTExprVar) tv ty ->
      throw (OccursCheckTExpr loc tv ty)
    (TExprVar tv, ty        ) -> modifying #texprVarHeap (M.insert tv ty)
    (_          , TExprVar _) -> unifyTExpr loc typ2 typ1
    (typ1       , typ2      ) -> throw (CantUnifyTExpr loc typ1 typ2)

nonAmbiguousTSig :: Loc -> MTSig s -> TypChkM s ()
nonAmbiguousTSig loc (TSigVar ms) = do
  modifying #nonAmbiguousTSigVar (M.insertWith (const id) ms loc)
nonAmbiguousTSig _   TReal            = pure ()
nonAmbiguousTSig loc (TSigTuple stms) = traverse_ (nonAmbiguousTSig loc) stms

nonAmbiguous :: Loc -> MTExpr s -> TypChkM s ()
nonAmbiguous loc (TExprVar ms) =
  modifying #nonAmbiguousTExprVar (M.insertWith (const id) ms loc)
nonAmbiguous loc (TSig       tms) = nonAmbiguousTSig loc tms
nonAmbiguous loc (TExprTuple ts ) = traverse_ (nonAmbiguous loc) ts
nonAmbiguous loc (TModel     tms) = nonAmbiguousTSig loc tms
nonAmbiguous loc (TArr arg ret  ) = nonAmbiguous loc arg >> nonAmbiguous loc ret

freezing
  :: Getting
       (Loc -> ty -> TypChkM s ty')
       (LocalTypeEnv s)
       (Loc -> ty -> TypChkM s ty')
  -> Loc
  -> ty
  -> TypChkM s ty' -- -> t1 -> t2 -> m b
freezing freezer loc typ = do
  freezer <- view freezer
  freezer loc typ

typeSig
  :: Sig Void ety sty Name -> TypChkM s (MTSig s, Sig Global PTExpr PTSig Ident)
typeSig Expr { sigLoc = loc, sigExpr = expr } = do
  (typ, expr) <- typeExpr expr
  texpr       <- freshTSig
  unifyTExpr loc typ (TSig texpr)
  pureTExpr <- freezing #tsigFreezer loc texpr
  pure (texpr, exprSig pureTExpr expr)
typeSig Local { sigLoc = loc, localName = i } = do
  scope <- view #scope
  case scopeLookup i scope of
    NotFound    -> throw (NotInScope loc i)
    FoundExpr{} -> typeSig (exprSig () (localExpr loc () i))
    FoundSig Entry { identRepl = idt, identTyp = typ } -> do
      typ' <- freezing #tsigFreezer loc typ
      pure (typ, localSig loc typ' idt)
typeSig Tuple { sigLoc = loc, tuplePayload = ss } = do
  (ts, ss) <- Seq.unzip <$> traverse typeSig ss
  pure (TSigTuple ts, tupleSig loc ss)
typeSig Der { sigLoc = loc, derivedSig = sig } = do
  (typ, sig) <- typeSig sig
  unifyTSig loc typ TReal
  case sig of
    Expr{} -> warn (ExprInSigContext loc)
    _      -> pure ()
  pure (typ, derSig loc sig)
typeSig Op { sigLoc = loc, operSig = op } = case op of
  ArithBinOp op p q -> do
    (tp, p) <- typeSig p
    (tq, q) <- typeSig q
    unifyTSig (localize p) tp TReal
    unifyTSig (localize q) tq TReal
    pure (TReal, opSig loc (ArithBinOp op p q))
  ArithUnOp op p -> do
    (tp, p) <- typeSig p
    unifyTSig (localize p) tp TReal
    pure (TReal, opSig loc (ArithUnOp op p))

typeEvent
  :: Event Void ety sty Name -> TypChkM s (Event Global PTExpr PTSig Ident)
typeEvent Root { eventLoc = loc, rootKind = root, watchedSig = sig } = do
  (tsig, sig) <- typeSig sig
  unifyTSig loc tsig TReal
  pure (event loc root sig)

typeExpr
  :: Expr Void ety sty Name
  -> TypChkM s (MTExpr s, Expr Global PTExpr PTSig Ident)
typeExpr Const { constVal = c, exprLoc = loc } =
  pure (typeConst c, constExpr loc c)
typeExpr LocalExpr { exprLoc = loc, localExprName = name } = do
  scope <- view #scope
  case scopeLookup name scope of
    FoundExpr (FoundGlobal decl) -> do
      (tySubst, typ) <- instantiate (typeOf decl)
      typ'           <- freezing #texprFreezer loc typ
      pure
        ( typ
        , globalExpr
          loc
          typ'
          ModuleBound { globalName     = name
                      , globalDecl     = decl
                      , globalTypSubst = tySubst
                      }
        )
    FoundExpr (FoundLocalExpr Entry { identRepl = idt, identTyp = typ }) -> do
      typ' <- freezing #texprFreezer loc typ
      pure (typ, localExpr loc typ' idt)
    FoundSig{} -> throw (SignalInAnExprContext loc name)
    NotFound   -> throw (NotInScope loc name)
typeExpr App { exprLoc = loc, exprFun = fun, exprArg = arg } = do
  (tfun, fun) <- typeExpr fun
  (targ, arg) <- typeExpr arg
  case tfun of
    TArr targ' tret -> do
      unifyTExpr (localize fun) targ targ'
      tret' <- freezing #texprFreezer loc tret
      pure (tret, app loc tret' fun arg)
    typ -> throw (IsNotAFunction (localize fun) typ)
typeExpr OpExpr { exprLoc = loc, operExpr = op } = do
  op <- traverse
    (\expr -> do
      (texpr, expr) <- typeExpr expr
      unifyTExpr (localize expr) texpr (TSig TReal)
      pure expr
    )
    op
  pure (TSig TReal, opExpr loc op)
typeExpr Model { exprLoc = loc, modelOutput = outputNamed, modelRelations = rels }
  = do
    freezeTSig                <- view #tsigFreezer
    (toutput, output, outEnv) <- patEnv freezeTSig
                                        mkSigEntry
                                        TSigTuple
                                        outputNamed
    locally #scope (SigScope outEnv) $ do
      rels <- traverse typeRel rels
      pure
        ( TModel toutput
        , Model { exprLoc = loc, modelOutput = output, modelRelations = rels }
        )

typeEqn :: Eqn Void ety sty Name -> TypChkM s (Eqn Global PTExpr PTSig Ident)
typeEqn (lhs :=: rhs) = do
  (tleft , lhs) <- typeSig lhs
  (tright, rhs) <- typeSig rhs
  unifyTSig (localize lhs <> localize rhs) tleft tright
  pure (lhs :=: rhs)

typeRel :: Rel Void ety sty Name -> TypChkM s (Rel Global PTExpr PTSig Ident)
typeRel (Eqn  eqn        ) = Eqn <$> typeEqn eqn
typeRel (Init eqn        ) = Init <$> typeEqn eqn
typeRel (sleft :<>: model) = do
  (tmodel, model) <- typeExpr model
  (tleft , sleft) <- typeSig sleft
  unifyTExpr (localize sleft) (TModel tleft) tmodel
  pure (sleft :<>: model)
typeRel (Let ids rels) = do
  (nids, nenv) <- foldM
    (\(nameSet, env) (name, Located loc _) -> do
      entry@Entry { identTyp = typ, identRepl = ident } <- freshEntry
        mkSigEntry
        name
      typ' <- freezing #tsigFreezer loc typ
      pure (M.insert ident (Located loc typ') nameSet, M.insert name entry env)
    )
    ([], [])
    (M.toList ids)
  rels <- locally #scope (SigScope nenv) (traverse typeRel rels)
  pure (Let nids rels)
typeRel (Switch (Located initLoc (Mode initLabel args)) _ blocks) = do
  let modeSet = foldr
        (\Branch { branchMode = Located _ branchMode } modeSet ->
          S.insert (modeName branchMode) modeSet
        )
        []
        blocks
  when (initLabel `S.notMember` modeSet)
    $ throw (ThisLabelDoesNotExist initLoc initLabel modeSet)
  (targsinit, args) <- foldM
    (\(targs, args) arg -> do
      (targ, arg) <- typeExpr arg
      pure (targs :|> targ, args :|> arg)
    )
    ([], [])
    args
  (_, blocks) <- foldM
    (\(modeMap, blocks) block -> do
      (block, modeMap) <- typeBranch initLabel targsinit modeSet modeMap block
      pure (modeMap, blocks :|> block)
    )
    ([], [])
    blocks
  pure (Switch (Located initLoc (Mode initLabel args)) modeSet blocks)
 where
  typeBranch initLabel targsinit modeSet modeMap Branch { branchMode = Located branchModeLoc (Mode name argsNamed), branchReinits = reinits, branchConditions = conds, branchRelations = rels }
    = do
      freezeTExpr            <- view #texprFreezer
      (targs, args, argsEnv) <- patsEnv freezeTExpr
                                        mkExprEntry
                                        TExprTuple
                                        argsNamed
      when (name == initLabel) $ traverse_
        (uncurry (unifyTExpr branchModeLoc))
        (Seq.zip targs targsinit)
      let renameReinit (Located loc idt) = do
            scope <- view #scope
            case scopeLookup idt scope of
              FoundSig Entry { identRepl = idt } -> pure (Located loc idt)
              FoundExpr{} -> throw (NonSignalReinitialized loc idt)
              NotFound -> throw (NotInScope loc idt)
      reinits <- foldM
        (\reinits idt -> do
          idt <- renameReinit idt
          pure (S.insert idt reinits)
        )
        []
        reinits
      modeMap <- case M.lookup name modeMap of
        Nothing -> pure (M.insert name targs modeMap)
        Just targs'
          | length targs' == length targs' -> do
            sequence_
              $ Seq.zipWith3 unifyTExpr (fmap localize args) targs targs'
            pure modeMap
          | otherwise -> throw
            (CantUnifyTExpr branchModeLoc (TExprTuple targs) (TExprTuple targs')
            )
      (rels, conds, modeMap) <- locally #scope (ExprScope argsEnv) $ do
        rels             <- traverse typeRel rels
        (conds, modeMap) <- foldM
          (\(conds, modeMap) cond -> do
            (cond, modeMap) <- typeCond modeSet modeMap cond
            pure (conds :|> cond, modeMap)
          )
          ([], modeMap)
          conds
        pure (rels, conds, modeMap)
      pure
        ( Branch { branchMode       = Located branchModeLoc (Mode name args)
                 , branchReinits    = reinits
                 , branchConditions = conds
                 , branchRelations  = rels
                 }
        , modeMap
        )

  typeCond modeSet modeMap (evt :-> Located loc (Mode name args)) = do
    evt              <- typeEvent evt
    (locTargs, args) <- foldM
      (\(locTargs, args) arg -> do
        (targ, arg) <- typeSig arg
        pure (locTargs :|> (localize arg, targ), args :|> arg)
      )
      ([], [])
      args
    modeMap <- case M.lookup name modeMap of
      Just targs
        | length targs == length locTargs -> do
          let zper (loc, targ) targ' = unifyTExpr loc (TSig targ) targ'
          sequence_ $ Seq.zipWith zper locTargs targs
          pure modeMap
        | otherwise -> throw
          (CantUnifyTExpr loc
                          (TExprTuple (fmap (TSig . snd) locTargs))
                          (TExprTuple targs)
          )
      Nothing
        | name `S.member` modeSet -> pure
          (M.insert name (fmap (TSig . snd) locTargs) modeMap)
        | otherwise -> throw (ThisLabelDoesNotExist loc name modeSet)

    pure (evt :-> Located loc (Mode name args), modeMap)

patEnv
  :: (Loc -> mty -> TypChkM s pty)
  -> (Word -> mty)
  -> (Seq mty -> mty)
  -> Pat ty Name
  -> TypChkM s (mty, Pat pty Ident, Map Name (Entry mty))
patEnv freeze var _ PLocal { localPatName = name, patLoc = loc } = do
  entry@Entry { identRepl = idt, identTyp = typ } <- freshEntry var name
  typ' <- freeze loc typ
  pure (typ, localPat loc typ' idt, [(name, entry)])
patEnv freeze var tuple PTuple { tuplePatPayload = ps, patLoc = loc } = do
  (ts, ps', env) <- patsEnv freeze var tuple ps
  pure (tuple ts, tuplePat loc ps', env)

patsEnv
  :: (Loc -> mty -> TypChkM s pty)
  -> (Word -> mty)
  -> (Seq mty -> mty)
  -> Seq (Pat ty Name)
  -> TypChkM s (Seq mty, Seq (Pat pty Ident), Map Name (Entry mty))
patsEnv freeze var tuple pats = do
  let go (tpats, ps, env) pat = do
        (tpat, pat, nenv) <- patEnv freeze var tuple pat
        pure (tpats :|> tpat, ps :|> pat, nenv `M.union` env)
  foldM go ([], [], []) pats

checkAmbiguous
  :: (MTExpr s -> State (TypeHeap s) PTExpr)
  -> (MTSig s -> State (TypeHeap s) PTSig)
  -> Scheme PTExpr
  -> TypeHeap s
  -> CompilerM ()
checkAmbiguous freezeTExpr freezeTSig (Forall evs svs _) heap@TypeHeap { nonAmbiguousTSigVar = nonAmbTs, nonAmbiguousTExprVar = nonAmbTe }
  = M.traverseWithKey goe nonAmbTe >> M.traverseWithKey gos nonAmbTs >> pure ()
 where
  goe ev loc | fes `S.isSubsetOf` evs && fss `S.isSubsetOf` svs = pure ()
             | otherwise = throw (AmbiguousType loc)
   where
    (fes, fss) = freeTExprVar (evalState (freezeTExpr (TExprVar ev)) heap)
  gos sv loc | fss `S.isSubsetOf` svs = pure ()
             | otherwise              = throw (AmbiguousType loc)
    where fss = freeTSigVar (evalState (freezeTSig (TSigVar sv)) heap)

typeDecl :: Map Name TypedDecl -> Decl Void ety sty Name -> CompilerM TypedDecl
typeDecl globalEnv decl = do
  rec (typedDecl@Decl { declName = name, declType = typ }, heap) <- runStateT
        (runReaderT
          (runTypChkM (tdecl decl))
          (emptyTypeEnv globalEnv
                        (freezeWithHeap nonAmbiguous freezeTExpr heap)
                        (freezeWithHeap nonAmbiguousTSig freezeTSig heap)
          )
        )
        emptyHeap
  checkAmbiguous freezeTExpr freezeTSig typ heap
  info (DeclInferredType name typ)
  pure typedDecl

 where
  tdecl Decl { declName = name, declParams = nameArgs, declBody = expr } = do
    freezeTExpr           <- view #texprFreezer
    (targs, args, argEnv) <- patsEnv freezeTExpr mkExprEntry TExprTuple nameArgs
    locally #scope (ExprScope argEnv) $ do
      (texpr, expr) <- typeExpr expr
      scheme        <- generalize (tarr targs texpr)
      pure Decl { declName   = name
                , declParams = args
                , declBody   = expr
                , declType   = scheme
                }

  freezeWithHeap
    :: (Loc -> ty -> TypChkM s ())
    -> (ty -> State (TypeHeap s) ty')
    -> TypeHeap s
    -> Loc
    -> ty
    -> TypChkM s ty'
  freezeWithHeap registerNonAmbiguous freezer heap loc typ = do
    registerNonAmbiguous loc typ
    pure $ evalState (freezer typ) heap

typeModule
  :: Module Void ety sty Name -> CompilerM (Module Global PTExpr PTSig Ident)
typeModule Module { moduleDecls = decls } = do
  (_, decls) <- foldM
    (\(global, decls) decl -> do
      decl <- typeDecl global decl
      pure (M.insert (declName decl) decl global, decls :|> decl)
    )
    ([], [])
    decls
  pure Module { moduleDecls = decls }

typingStep
  :: StepSpec
       CompilerM
       (Module Void ety sty Name)
       (Module Global PTExpr PTSig Ident)
typingStep =
  Step { stepConf = fromStepName "typing", stepExec = SingleStep typeModule }
