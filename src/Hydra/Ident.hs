{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}

module Hydra.Ident
  ( Ident
  , source
  , ident
  ) where

import           Hydra.Pretty

import           Data.Text                      ( Text )

data Ident = Ident !Word !Text
  deriving (Eq, Ord, Show)

instance Pretty Ident where
  pretty (Ident n s) = pretty s <> "_" <> pretty n

source :: Ident -> Text
source (Ident _ s) = s

ident :: Text -> Word -> Ident
ident s n = Ident n s

