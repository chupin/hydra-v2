{-# LANGUAGE FlexibleContexts,
             FlexibleInstances, FunctionalDependencies, LambdaCase,
             OverloadedLists, OverloadedStrings, TypeFamilies,
             UndecidableInstances, MultiParamTypeClasses, DeriveGeneric #-}

module Hydra.Type where

import           Hydra.Pretty

import           Data.Foldable
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Maybe
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Set                      as S
import           Data.Set                       ( Set )
import qualified Data.Stream.Infinite          as Stream
import           GHC.Generics                   ( Generic )

-- Pure types
varName :: Word -> String
varName idx = strings Stream.!! fromIntegral idx
 where
  alpha :: [Char]
  alpha   = ['a' .. 'z']

  strings = Stream.prepend
    [ [c] | c <- alpha ]
    (Stream.concat (fmap (\xs -> fmap (\x -> xs ++ [x]) alpha) strings))

newtype SVar = SVar Word
  deriving ( Eq, Ord )

newtype EVar = EVar Word
  deriving ( Eq, Ord )

instance Pretty SVar where
  pretty (SVar pv) = "sig" <+> "'" <> pretty (varName pv)

instance Pretty EVar where
  pretty (EVar ev) = "'" <> pretty (varName ev)

type PTSig = TSig SVar

type PTExpr = TExpr EVar SVar

-- Type of signals. They can only be real or collections of reals. Variables are
-- also possible
data TSig sv = TSigVar !sv
             | TReal
             | TSigTuple (Seq (TSig sv))
             deriving(Eq, Ord)

-- Type of expressions. An expression can have all types that signals have and
-- then some more
data TExpr ev sv = TExprVar !ev
                 | TSig (TSig sv)
                 | TExprTuple (Seq (TExpr ev sv))
                 | TModel (TSig sv)
                 | TArr (TExpr ev sv) (TExpr ev sv)
                 deriving(Eq, Ord)

trealSig :: TSig tv
trealSig = TReal

trealExpr :: TExpr ev sv
trealExpr = TSig trealSig

tarr :: Foldable f => f (TExpr ev sv) -> TExpr ev sv -> TExpr ev sv
tarr targs tret = foldr TArr tret targs

freeTExprVar :: (Ord ev, Ord sv) => TExpr ev sv -> (Set ev, Set sv)
freeTExprVar (TExprVar   ev) = ([ev], [])
freeTExprVar (TSig       ty) = ([], freeTSigVar ty)
freeTExprVar (TExprTuple ts) = foldMap freeTExprVar ts
freeTExprVar (TModel     ty) = ([], freeTSigVar ty)
freeTExprVar (TArr it ot   ) = freeTExprVar it <> freeTExprVar ot

freeTSigVar :: Ord sv => TSig sv -> Set sv
freeTSigVar TReal          = []
freeTSigVar (TSigVar   sv) = [sv]
freeTSigVar (TSigTuple ts) = foldMap freeTSigVar ts

instance Pretty tv => Pretty (TSig tv) where
  pretty (TSigVar tv)   = pretty tv
  pretty TReal          = "real"
  pretty (TSigTuple ts) = parens (commaList (fmap pretty ts))

instance (Pretty tv, Pretty ev) => Pretty (TExpr ev tv) where
  pretty (TExprVar   ev    ) = pretty ev
  pretty (TSig       ts    ) = pretty ts
  pretty (TExprTuple ts    ) = parens (commaList (fmap pretty ts))
  pretty (TModel     ty    ) = "model" <+> pretty ty
  pretty (TArr input output) = prettyArr [input] output
   where
    prettyArr inputs (TArr input output) = prettyArr (inputs :|> input) output
    prettyArr inputs output              = parens
      (hcat (punctuate " -> " (toList (fmap pretty (inputs :|> output)))))

data family Scheme ty
data instance Scheme () = UnitScheme
data instance Scheme (TExpr ev sv) = Forall (Set ev) (Set sv) (TExpr ev sv)

instance (Pretty ev, Pretty sv) => Pretty (Scheme (TExpr ev sv)) where
  pretty (Forall evs svs typ)
    | S.null evs && S.null svs = pretty typ
    | S.null evs               = single psvs typ
    | S.null svs               = single pevs typ
    | otherwise = "forall" <+> pevs <+> "and" <+> psvs <+> "," <+> pretty typ
   where
    single fvs typ = "forall" <+> fvs <+> "," <+> pretty typ

    pevs = commaList (fmap pretty (toList evs))
    psvs = commaList (fmap pretty (toList svs))

data TySubst ev sv = TySubst
  { evarSubst :: Map ev (TExpr ev sv)
  , svarSubst :: Map sv (TSig sv)
  }
  deriving (Eq, Ord, Generic)

applyTySubst :: (Ord sv, Ord ev) => TySubst ev sv -> TExpr ev sv -> TExpr ev sv
applyTySubst (TySubst sev _) typ@(TExprVar ev) =
  fromMaybe typ (M.lookup ev sev)
applyTySubst (TySubst _ ssv) (TSig tsv) = TSig (applyTySubstTSig ssv tsv)
applyTySubst tySubst (TExprTuple ts) =
  TExprTuple (fmap (applyTySubst tySubst) ts)
applyTySubst (TySubst _ ssv) (TModel tsv) = TModel (applyTySubstTSig ssv tsv)
applyTySubst tySubst (TArr targ tret) =
  TArr (applyTySubst tySubst targ) (applyTySubst tySubst tret)

applyTySubstTSig :: Ord sv => Map sv (TSig sv) -> TSig sv -> TSig sv
applyTySubstTSig ssv typ@(TSigVar sv) = fromMaybe typ (M.lookup sv ssv)
applyTySubstTSig _   TReal            = TReal
applyTySubstTSig ssv (TSigTuple ts) =
  TSigTuple (fmap (applyTySubstTSig ssv) ts)

-- Apply the first substitution to the types contained in the second
-- substitution
composeTySubst
  :: (Ord ev, Ord sv) => TySubst ev sv -> TySubst ev sv -> TySubst ev sv
composeTySubst tySubst@(TySubst _ ssv1) (TySubst sev ssv) =
  TySubst (fmap (applyTySubst tySubst) sev) (fmap (applyTySubstTSig ssv1) ssv)

-- composeTySubst :: (Ord ev, Ord sv) => TySubst ev sv -> TySubst ev sv -> TySubst ev sv
-- composeTySubst

instance (Ord ev, Ord sv) => Semigroup (TySubst ev sv) where
  TySubst ev1 sv1 <> TySubst ev2 sv2 = TySubst (ev1 <> ev2) (sv1 <> sv2)

instance (Ord ev, Ord sv) => Monoid (TySubst ev sv) where
  mempty = TySubst [] []

instance (Ord ev, Pretty ev, Ord sv, Pretty sv) => Pretty (TySubst ev sv) where
  pretty TySubst { evarSubst = evTySubst, svarSubst = svTySubst } =
    case (M.null evTySubst, M.null svTySubst) of
      (True , True ) -> ""
      (False, True ) -> pevTySubst
      (True , False) -> psvTySubst
      (False, False) -> pevTySubst $+$ psvTySubst
   where
    pevTySubst = psubst "[E-var]" evTySubst

    psvTySubst = psubst "[S-var]" svTySubst

    psubst _ subst | M.null subst = ""
    psubst nm subst               = nest
      ilvl
      (nm $+$ lineBlock
        (M.mapWithKey (\ev ty -> pretty ev <+> "->" <+> pretty ty) subst)
      )

instantiateScheme
  :: (Ord ev, Ord sv) => TySubst ev sv -> Scheme (TExpr ev sv) -> TExpr ev sv
instantiateScheme tySubst (Forall _ _ texpr) = goE tySubst texpr
 where
  goE tySubst@TySubst { evarSubst = evs, svarSubst = svs } typ = case typ of
    TExprVar   ev     -> fromMaybe typ (M.lookup ev evs)
    TSig       ts     -> TSig (goS svs ts)
    TExprTuple ts     -> TExprTuple (fmap (goE tySubst) ts)
    TModel     ts     -> TModel (goS svs ts)
    TArr input output -> TArr (goE tySubst input) (goE tySubst output)

  goS svs typ@(TSigVar   tv) = fromMaybe typ (M.lookup tv svs)
  goS svs (    TSigTuple ts) = TSigTuple (fmap (goS svs) ts)
  goS _   TReal              = TReal

class Typed m ty | m -> ty where
  typeOf :: m -> ty
