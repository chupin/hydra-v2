{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}

module Hydra.Op where

import           Data.Ratio
import           Data.Stream.Supply

import           Prelude                 hiding ( exp
                                                , log
                                                )
import qualified Prelude

import           Hydra.Pretty
import           Hydra.Supply
import           Language.Haskell.TH.Syntax     ( Lift )


data ArithUnOp =
    Sqrt
  | Exp
  | Log
  | Sin
  | Tan
  | Cos
  | Asin
  | Atan
  | Acos
  | Sinh
  | Tanh
  | Cosh
  | Asinh
  | Atanh
  | Acosh
  | Abs
  | Sgn
  deriving ( Eq, Ord, Show, Lift )

instance Pretty ArithUnOp where
  pretty op = case op of
    Sqrt  -> "sqrt"
    Exp   -> "exp"
    Log   -> "log"
    Sin   -> "sin"
    Tan   -> "tan"
    Cos   -> "cos"
    Asin  -> "asin"
    Atan  -> "atan"
    Acos  -> "acos"
    Sinh  -> "sinh"
    Tanh  -> "tanh"
    Cosh  -> "cosh"
    Asinh -> "asinh"
    Atanh -> "atanh"
    Acosh -> "acosh"
    Abs   -> "abs"
    Sgn   -> "sgn"

data ArithBinOp = Add | Sub | Mul | Div | Pow
  deriving ( Eq, Ord, Show, Lift )

instance Pretty ArithBinOp where
  pretty op = case op of
    Add -> "+"
    Sub -> "-"
    Mul -> "*"
    Div -> "/"
    Pow -> "^"

data Op r = ArithBinOp ArithBinOp r r | ArithUnOp ArithUnOp r
  deriving ( Eq, Ord, Show, Functor, Foldable, Traversable )

instance Splitter Op where
  splitAlong s (ArithUnOp op p   ) = ArithUnOp op (s, p)
  splitAlong s (ArithBinOp op p q) = ArithBinOp op (s1, p) (s2, q)
    where (s1, s2) = split2 s

-- evalOp evaluates with Rational. Applications of functions that work on
-- fractionals works by casting to a double, applying the function and
-- converting back to a rational
evalOp :: Op Rational -> Rational
evalOp (ArithBinOp Add r1 r2) = r1 + r2
evalOp (ArithBinOp Sub r1 r2) = r1 - r2
evalOp (ArithBinOp Mul r1 r2) = r1 * r2
evalOp (ArithBinOp Div r1 r2) = r1 / r2
evalOp (ArithBinOp Pow r1 r2)
  | q == 1    = r1 ^ p
  | otherwise = toRational (fromRational r1 ** fromRational r2)
 where
  p = numerator r2
  q = denominator r2
evalOp (ArithUnOp op r) = case op of
  Sqrt  -> lift sqrt r
  Exp   -> lift Prelude.exp r
  Log   -> lift Prelude.log r
  Sin   -> lift sin r
  Tan   -> lift tan r
  Cos   -> lift cos r
  Asin  -> lift asin r
  Atan  -> lift atan r
  Acos  -> lift acos r
  Sinh  -> lift sinh r
  Tanh  -> lift tanh r
  Cosh  -> lift cosh r
  Asinh -> lift asinh r
  Atanh -> lift atanh r
  Acosh -> lift acosh r
  Abs   -> abs r
  Sgn   -> signum r
 where
  lift :: (Double -> Double) -> Rational -> Rational
  lift f r = toRational (f (fromRational r))
  {-# INLINE lift #-}

prettyOp :: Op (Doc ann) -> Doc ann
prettyOp (ArithBinOp bop r1 r2) = parens (r1 <+> pretty bop <+> r2)
prettyOp (ArithUnOp uop r     ) = pretty uop <> parens r

instance Pretty r => Pretty (Op r) where
  pretty = prettyOp . fmap pretty

binop :: ArithBinOp -> r -> r -> Op r
binop = ArithBinOp

binopM :: Monad m => ArithBinOp -> m r -> m r -> m (Op r)
binopM op p q = sequence (binop op p q)

unop :: ArithUnOp -> r -> Op r
unop = ArithUnOp

unopM :: Monad m => ArithUnOp -> m r -> m (Op r)
unopM op p = sequence (unop op p)

add :: r -> r -> Op r
add = binop Add

addM :: Monad m => m r -> m r -> m (Op r)
addM = binopM Add

sub :: r -> r -> Op r
sub = binop Sub

subM :: Monad m => m r -> m r -> m (Op r)
subM = binopM Sub

mul :: r -> r -> Op r
mul = binop Mul

mulM :: Monad m => m r -> m r -> m (Op r)
mulM = binopM Mul

div :: r -> r -> Op r
div = binop Div

divM :: Monad m => m r -> m r -> m (Op r)
divM = binopM Div

pow :: r -> r -> Op r
pow = binop Pow

powM :: Monad m => m r -> m r -> m (Op r)
powM = binopM Pow

exp :: r -> Op r
exp = unop Exp

expM :: Monad m => m r -> m (Op r)
expM = unopM Exp

log :: r -> Op r
log = unop Log

data OpTree r = OpLeaf r
              | OpConst Rational
              | OpNode (Op (OpTree r))

derOp :: ArithUnOp -> r -> OpTree r
derOp op x = case op of
  Sqrt  -> inv (square p)
  Exp   -> unop Exp p
  Log   -> inv p
  Sin   -> unop Cos p
  Cos   -> neg (unop Sin p)
  Tan   -> inv (square (unop Cos p))
  Asin  -> inv (sqrt (const 1 `sub` square p))
  Acos  -> neg (inv (sqrt (const 1 `sub` square p)))
  Atan  -> inv (sqrt (const 1 `add` square p))
  Sinh  -> unop Cosh p
  Cosh  -> unop Sinh p
  Tanh  -> inv (square (unop Cosh p))
  Asinh -> inv (sqrt (square p `add` const 1))
  Acosh -> inv (sqrt (square p `sub` const 1))
  Atanh -> inv (const 1 `sub` square p)
  Abs   -> unop Sgn p
  Sgn   -> const 0
 where
  p = OpLeaf x

  binop op p q = OpNode (ArithBinOp op p q)

  unop op p = OpNode (ArithUnOp op p)

  const = OpConst

  add   = binop Add

  sub   = binop Sub

  mul   = binop Mul

  div   = binop Div

  square p = p `mul` p

  sqrt = unop Sqrt

  neg p = const 0 `sub` p

  inv p = const 1 `div` p
