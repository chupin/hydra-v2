{-# LANGUAGE DeriveFunctor, FlexibleInstances, OverloadedStrings, RankNTypes #-}

module Hydra.Tokens
  ( Priority(..)
  , Token(..)
  , OpApp(..)
  , unopOfTok
  , binopOfTok
  , lidentOfTok
  , uidentOfTok
  , doubleOfTok
  , intOfTok
  , mkTok
  , mkConstTok
  ) where

import           Hydra.Loc
import           Hydra.Op
import           Hydra.Pretty

import           Data.Text                      ( Text )
import qualified Data.Text                     as T

data Priority = PriorityAdd
              | PrioritySub
              | PriorityMul
              | PriorityDiv
              | PriorityPow

data OpApp = Unop (forall r. r -> Op r)
           | Binop Priority (forall r. r -> r -> Op r)

instance Show OpApp where
  show (Unop op   ) = show (op ("arg" :: String))
  show (Binop _ op) = show (op ("arg1" :: String) ("arg2" :: String))

data Token loc = TokModel loc
               | TokLet loc
               | TokInit loc
               | TokReinit loc
               | TokSwitch loc
               | TokMode loc
               | TokWhen loc
               | TokDer loc
               | TokArr loc
               | TokLParen loc
               | TokRParen loc
               | TokLBrack loc
               | TokRBrack loc
               | TokSemiCol loc
               | TokComma loc
               | TokEq loc
               | TokConn loc
               | TokUp loc
               | TokDown loc
               | TokUpDown loc

               | TokLIdent Text loc
               | TokUIdent Text loc
               | TokDouble Rational loc
               | TokInt Int loc

               | TokOp OpApp loc

               | TokEOF
               deriving(Show, Functor)

instance Localized (Token Loc) where
  localize (TokModel   loc ) = loc
  localize (TokLet     loc ) = loc
  localize (TokInit    loc ) = loc
  localize (TokReinit  loc ) = loc
  localize (TokSwitch  loc ) = loc
  localize (TokMode    loc ) = loc
  localize (TokWhen    loc ) = loc
  localize (TokDer     loc ) = loc
  localize (TokArr     loc ) = loc
  localize (TokLParen  loc ) = loc
  localize (TokRParen  loc ) = loc
  localize (TokLBrack  loc ) = loc
  localize (TokRBrack  loc ) = loc
  localize (TokSemiCol loc ) = loc
  localize (TokComma   loc ) = loc
  localize (TokEq      loc ) = loc
  localize (TokConn    loc ) = loc
  localize (TokUp      loc ) = loc
  localize (TokDown    loc ) = loc
  localize (TokUpDown  loc ) = loc

  localize (TokLIdent _ loc) = loc
  localize (TokUIdent _ loc) = loc
  localize (TokDouble _ loc) = loc
  localize (TokInt    _ loc) = loc

  localize (TokOp     _ loc) = loc

  localize TokEOF            = NoLoc

extractionError :: String -> a
extractionError str =
  error
    $  "The parser has a bug. \
          \It has tried to extract "
    ++ str
    ++ " from a token that didn't have that :("

doubleOfTok :: Token loc -> Rational
doubleOfTok (TokDouble d _) = d
doubleOfTok _               = extractionError "a double"

intOfTok :: Token loc -> Int
intOfTok (TokInt i _) = i
intOfTok _            = extractionError "an int"

lidentOfTok :: Token loc -> Text
lidentOfTok (TokLIdent i _) = i
lidentOfTok _               = extractionError "a lower case identifier"

uidentOfTok :: Token loc -> Text
uidentOfTok (TokUIdent u _) = u
uidentOfTok _               = extractionError "an upper case identifier"

unopOfTok :: Token loc -> r -> Op r
unopOfTok (TokOp (Unop op) _) = op
unopOfTok _                   = extractionError "a unary operator"

binopOfTok :: Token loc -> r -> r -> Op r
binopOfTok (TokOp (Binop _ op) _) = op
binopOfTok _                      = extractionError "a binary operator"

instance Pretty (Token loc) where
  pretty TokModel{}       = "model"
  pretty TokLet{}         = "let"
  pretty TokInit{}        = "init"
  pretty TokReinit{}      = "reinit"
  pretty TokSwitch{}      = "switch"
  pretty TokMode{}        = "mode"
  pretty TokWhen{}        = "when"
  pretty TokDer{}         = "der"
  pretty TokArr{}         = "->"
  pretty TokLParen{}      = "("
  pretty TokRParen{}      = ")"
  pretty TokLBrack{}      = "{"
  pretty TokRBrack{}      = "}"
  pretty TokSemiCol{}     = ";"
  pretty TokComma{}       = ","
  pretty TokEq{}          = "="
  pretty TokConn{}        = "<>"
  pretty TokUp{}          = "up"
  pretty TokDown{}        = "down"
  pretty TokUpDown{}      = "updown"
  pretty (TokLIdent s  _) = "l#" <> pretty s
  pretty (TokUIdent u  _) = "u#" <> pretty u
  pretty (TokDouble d  _) = "d#" <> pretty (PrettyRational d)
  pretty (TokInt    i  _) = "i#" <> pretty i
  pretty (TokOp     op _) = pretty (show op)
  pretty TokEOF{}         = "EOF"

mkTok
  :: Applicative f
  => (posn -> Int -> k)
  -> (Text -> k -> Token k)
  -> (posn, char, bytes, String)
  -> Int
  -> f (Token k)
mkTok mkloc mtok (posn, _, _, s) len =
  pure $ mtok (T.pack (take len s)) (mkloc posn len)

mkConstTok
  :: Applicative f
  => (posn -> Int -> k)
  -> (k -> Token k)
  -> (posn, char, bytes, String)
  -> Int
  -> f (Token k)
mkConstTok mkloc mtok = mkTok mkloc (const mtok)
