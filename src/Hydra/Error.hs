{-# LANGUAGE DataKinds, DefaultSignatures, FlexibleContexts, FlexibleInstances,
             FunctionalDependencies, GADTs, GeneralizedNewtypeDeriving,
             KindSignatures, OverloadedLists, OverloadedStrings, RankNTypes,
             ScopedTypeVariables, StandaloneDeriving, TypeApplications,
             UndecidableInstances #-}

module Hydra.Error where

import           Hydra.Config
import           Hydra.Loc
import           Hydra.Pretty

import           Control.Concurrent.MVar
import           Control.Monad
import           Control.Monad.Except           ( MonadError(..) )
import qualified Control.Monad.Except          as E
import           Control.Monad.Reader
import           Control.Monad.State
import qualified Control.Monad.State.Strict    as Strict
import           Control.Monad.Writer
import           Data.Foldable
import qualified Data.Map                      as M
import           Data.Proxy
import           Data.Text               hiding ( reverse )
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as T
import           Data.Text.Prettyprint.Doc.Render.Terminal
import           System.IO
import           System.IO.Unsafe               ( unsafePerformIO )

data MsgKind = Error | Warning | Info | Debug | Stopped

class IsMsgKind (k :: MsgKind) where
  kindColor :: Proxy k -> MsgKind
  kindHeader :: Proxy k -> Text
  kindEnable :: Proxy k -> Config -> Bool

instance IsMsgKind 'Error where
  kindColor Proxy = Error
  kindHeader Proxy = "error"
  kindEnable Proxy _ = True

instance IsMsgKind 'Warning where
  kindColor Proxy = Warning
  kindHeader Proxy = "warning"
  kindEnable Proxy = enableWarning . debugConf

instance IsMsgKind 'Info where
  kindColor Proxy = Info
  kindHeader Proxy = "info"
  kindEnable Proxy = enableInfo . debugConf

instance IsMsgKind 'Debug where
  kindColor Proxy = Debug
  kindHeader Proxy = "debug"
  kindEnable Proxy = enableDebug . debugConf

instance IsMsgKind 'Stopped where
  kindColor Proxy = Stopped
  kindHeader Proxy = "execution stopped"
  kindEnable Proxy _ = True

-- | A class for messages that can be emitted. A message will be emitted if:
--
-- * it's explicitly enabled,
--
-- * all messages of that kind should be emitted *and* it's not explicitly
-- disabled.
class Message (k :: MsgKind) m | m -> k where
  msgTitle  :: m -> Text

  msgBody   :: (Span -> Doc MsgAnn)
            -> m
            -> [Doc MsgAnn]

  msgLoc :: m -> Loc
  msgLoc _ = NoLoc

  msgEmit   :: m -> Maybe (Config -> Bool)
  msgEmit _ = Nothing

newtype DebugMsg = DebugMsg String

instance Message 'Debug DebugMsg where
  msgTitle _ = ""
  msgBody _ (DebugMsg msg) = [pretty (T.pack msg)]

data MsgAnn = MsgKind MsgKind
            | Header

data ErrMsg where
  ErrMsg ::Message 'Error m => m -> ErrMsg
  StopMsg ::ErrMsg

data Msg where
  Msg ::(IsMsgKind k, Message k m) => m -> Msg

ansiMsgAnn :: MsgAnn -> AnsiStyle
ansiMsgAnn (MsgKind kd) = color $ case kd of
  Error   -> Red
  Warning -> Yellow
  Info    -> Blue
  Debug   -> Magenta
  Stopped -> Cyan
ansiMsgAnn Header = bold

isEnabled :: forall k m . (Message k m, IsMsgKind k) => Config -> m -> Bool
isEnabled conf msg = maybe (kindEnable (Proxy @k) conf) ($ conf) (msgEmit msg)

class Monad m => MonadLog m where
  emit :: Msg -> m ()

  default emit :: ( MonadTrans t
                  , MonadLog n
                  , m ~ t n
                  )
               => Msg
               -> m ()
  emit = lift . emit

instance MonadLog m => MonadLog (StateT s m)
instance MonadLog m => MonadLog (Strict.StateT s m)
instance MonadLog m => MonadLog (ReaderT r m)
instance MonadLog m => MonadLog (ConfigT m)
instance MonadLog m => MonadLog (E.ExceptT e m)

-- Pure logging monad

newtype LogT m a = LogT (StateT [Msg] m a)
                 deriving ( Functor
                          , Applicative
                          , Monad
                          , MonadIO
                          , MonadTrans
                          , MonadFix
                          )

runLogT :: Monad m => LogT m a -> m (a, [Msg])
runLogT (LogT log) = do
  (a, msgs) <- runStateT log []
  pure (a, reverse msgs)

deriving instance MonadWriter s m => MonadWriter s (LogT m)
deriving instance MonadError e m => MonadError e (LogT m)
deriving instance MonadReader w m => MonadReader w (LogT m)
deriving instance MonadConfig m => MonadConfig (LogT m)

instance Monad m => MonadLog (LogT m) where
  emit m = LogT (modify (m :))

instance MonadState s m => MonadState s (LogT m) where
  get = lift get
  put = lift . put

-- Impure logging monad

newtype IOLogT m a = IOLogT (ReaderT (Span -> Doc MsgAnn) m a)
                   deriving ( Functor
                            , Applicative
                            , Monad
                            , MonadFix
                            , MonadIO
                            )

runIOLogT :: MonadIO m => IOLogT m a -> m a
runIOLogT (IOLogT log) = do
  rdrSpan <- liftIO getSpanRenderer
  runReaderT log rdrSpan

deriving instance MonadWriter s m => MonadWriter s (IOLogT m)
deriving instance MonadError e m => MonadError e (IOLogT m)
deriving instance MonadConfig m => MonadConfig (IOLogT m)
deriving instance MonadState s m => MonadState s (IOLogT m)

instance MonadReader w m => MonadReader w (IOLogT m) where
  ask = IOLogT $ ReaderT $ const ask
  local f (IOLogT (ReaderT m)) = IOLogT $ ReaderT $ local f . m

instance MonadTrans IOLogT where
  lift = IOLogT . lift

instance MonadIO m => MonadLog (IOLogT m) where
  emit m = IOLogT $ do
    rdrSpan <- ask
    liftIO (hPutDoc stderr (ansiRender rdrSpan m <> line))

msg :: (MonadConfig m, MonadLog m) => Msg -> m ()
msg msg@(Msg m) = do
  conf <- conf
  when (isEnabled conf m) $ emit msg

warn :: (MonadConfig m, MonadLog m, Message 'Warning w) => w -> m ()
warn = msg . Msg

info :: (MonadConfig m, MonadLog m, Message 'Info w) => w -> m ()
info = msg . Msg

debug :: (MonadConfig m, MonadLog m, Message 'Debug w) => w -> m ()
debug = msg . Msg

data StoppingMsg = StoppingMsg

instance Message 'Stopped StoppingMsg where
  msgTitle _ = ""
  msgBody _ _ = ["The compiler exited early with no error."]

stop :: MonadError ErrMsg m => m a
stop = throwError StopMsg

throw :: (MonadError ErrMsg m, Message 'Error e) => e -> m a
throw = throwError . ErrMsg

renderLoc :: (Span -> Doc ann) -> Loc -> Doc ann
renderLoc _       NoLoc      = "at unknown location."
renderLoc rdrSpan (Loc span) = rdrSpan span

renderMessage
  :: forall k m
   . (IsMsgKind k, Message k m)
  => (Span -> Doc MsgAnn)
  -> m
  -> Doc MsgAnn
renderMessage rdrSpan msg =
  annotate
      Header
      (   headLoc
      <>  annotate (MsgKind (kindColor (Proxy @k)))
                   (pretty (kindHeader (Proxy @k)))
      <>  ":"
      <+> pretty (msgTitle msg)
      )
    $+$ indent ilvl (rdrBody (msgBody rdrSpan msg))
 where
  headLoc = case msgLoc msg of
    NoLoc    -> ""
    Loc span -> pretty $ displayPos (spanStart span) <> ":"

  rdrBody []     = ""
  rdrBody [body] = body
  rdrBody body   = lineBlock body

getSpanRenderer :: IO (Span -> Doc ann)
getSpanRenderer = do
  cache <- newMVar []
  let getCachedFile fp = modifyMVar cache $ \cache -> case M.lookup fp cache of
        Just file -> pure (cache, file)
        Nothing   -> do
          file <- T.readFile fp
          pure (M.insert fp file cache, file)
  pure (pretty . unsafePerformIO . renderSpan getCachedFile)

ansiRender :: (Span -> Doc MsgAnn) -> Msg -> Doc AnsiStyle
ansiRender rdrSpan (Msg m) = reAnnotate ansiMsgAnn (renderMessage rdrSpan m)

putMessages :: Traversable t => t Msg -> IO ()
putMessages msgs = do
  rdrSpan <- getSpanRenderer
  hPutDoc stderr (vcat (toList (fmap (ansiRender rdrSpan) msgs)))
  hPutStrLn stderr ""

internalError :: String -> a
internalError msg = error $ Prelude.unlines
  [ "!!!!"
  , "The compiler encountered an internal error, with message:"
  , msg
  , "!!!!"
  ]
