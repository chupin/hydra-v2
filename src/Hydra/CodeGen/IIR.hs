module Hydra.CodeGen.IIR
  ( iir
  ) where

import           Hydra.CodeGen.IIR.QQ           ( iir )
