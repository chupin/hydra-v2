{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}

{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.CodeGen.RootFinding
  ( rootSetupMethod
  , computeRootsMethod
  ) where

import           Control.Lens
import           Control.Monad.Reader
import           Data.Generics.Labels           ( )
import           Hydra.CodeGen.Method
import           Hydra.CodeGen.Residual
import           Hydra.CodeGen.Type
import qualified Hydra.LLVM                    as LL
import           Hydra.OOIR
import qualified LLVM.IRBuilder.Constant       as LL

-- Method to set the direction of roots

rootSetupMethod :: Rel -> Method () RootSetupState
rootSetupMethod Eqn{} = mempty
rootSetupMethod (Let idts rels) =
  withLocals idts $ foldMap rootSetupMethod rels
rootSetupMethod (ModelCall args model) =
  withCall (rootSetupMethodRepr objectReprIndices) args model
rootSetupMethod (Switch _ branches) = withSwitch go branches
 where
  go rawArgsPtr _ Branch { branchArguments = args, branchBlock = block, branchConditions = conds }
    = withBranchArgs rawArgsPtr args
      $  withBlock rootSetupMethod block
      <> foldMap goCond conds

  goCond (Root { rootKind = dir } :-> _) = Method $ do
    nextRootDir <- use (#userState . #nextRootDir)
    let val = case dir of
          UpRoot   -> 1
          DownRoot -> -1
          AnyRoot  -> 0
    assign (#userState . #nextRootDir)
      =<< LL.storeAndStep nextRootDir (LL.int32 val)

-- Method for computing the values of the monitored signals

computeRootsMethod :: Rel -> Method ComputeRootsInput ComputeRootsState
computeRootsMethod Eqn{} = mempty
computeRootsMethod (Let idts rels) =
  withLocals idts (foldMap computeRootsMethod rels)
computeRootsMethod (ModelCall args model) =
  withCall (computeRootsMethodRepr objectReprIndices) args model
computeRootsMethod (Switch _ branches) = withSwitch go branches
 where
  go rawArgsPtr _ Branch { branchArguments = args, branchBlock = block, branchConditions = conds }
    = withBranchArgs
      rawArgsPtr
      args
      (withBlock computeRootsMethod block <> foldMap goCond conds)

  goCond (Root { watchedSig = sig } :-> _) = Method $ do
    scope <- view #methodScope
    input <- view (#userInput . #inputSignals)
    rs    <- use (#userState . #rootResultPtr)
    val   <- lift $ compileSig scope input sig
    assign (#userState . #rootResultPtr) =<< LL.storeAndStep rs val
