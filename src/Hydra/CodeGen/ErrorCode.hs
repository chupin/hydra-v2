-- This module defines the error codes that the generated functions may return
-- upon error
module Hydra.CodeGen.ErrorCode where

divisionByZero, tooManyDifferentiations :: Integral i => i
divisionByZero = -1

tooManyDifferentiations = -2
