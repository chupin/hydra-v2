{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.CodeGen.Type where

import           Control.Monad.Identity
import           Data.Foldable
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import           GHC.Generics
import           Hydra.CodeGen.Monad
import           Hydra.Error                    ( internalError )
import           Hydra.Flatten.AST              ( TSig(..) )
import           Hydra.Ident
import qualified Hydra.LLVM.Utils              as LL
import           Hydra.OOIR
import           Hydra.Pretty
import           LLVM.AST.Constant              ( Constant )
import qualified LLVM.AST.Constant             as LLC
import           LLVM.AST.Name                  ( Name(..) )
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.ParameterAttribute   as LL
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.AST.Typed                as LLT
import           LLVM.Pretty                    ( )

paramNameList :: Foldable f => f (Maybe Ident) -> [LL.ParameterName]
paramNameList =
  fmap (maybe LL.NoParameterName (LL.ParameterName . LL.fromIdent)) . toList

data ModelMethodArg i s = ModelMethodArg
  { modelEnvPtr            :: Operand
  , modelState             :: Operand
  , firstBlockActivation   :: Operand
  , nextVarStorage         :: LL.StoreOf Operand
  , inputArgs              :: i
  , stateArgs              :: LL.StoreOf s
  , interfaceSignalIndices :: Seq Operand
  }

instance (LL.Argument i, LL.Argument s) => LL.Argument (ModelMethodArg i s) where
  fromArg ModelMethodArg { modelEnvPtr = modelEnvPtr, modelState = modelState, firstBlockActivation = firstBlockActivation, nextVarStorage = next, inputArgs = input, stateArgs = state, interfaceSignalIndices = iface }
    = LL.fromArg modelEnvPtr
      ++ LL.fromArg modelState
      ++ LL.fromArg firstBlockActivation
      ++ LL.fromArg next
      ++ LL.fromArg input
      ++ LL.fromArg state
      ++ toList iface

  toArg ops = do
    (modelEnvPtr         , ops) <- LL.toArg ops
    (modelState          , ops) <- LL.toArg ops
    (firstBlockActivation, ops) <- LL.toArg ops
    (nextVar             , ops) <- LL.toArg ops
    (input               , ops) <- LL.toArg ops
    (state               , ops) <- LL.toArg ops
    pure
      ( ModelMethodArg { modelEnvPtr            = modelEnvPtr
                       , modelState             = modelState
                       , firstBlockActivation   = firstBlockActivation
                       , nextVarStorage         = nextVar
                       , inputArgs              = input
                       , stateArgs              = state
                       , interfaceSignalIndices = Seq.fromList ops
                       }
      , []
      )

data ModelMethod i s = ModelMethod
  { interfaceSignals :: Seq Ident
  , otherMethodArgs  :: [LL.FunArgument]
  }

instance (LL.Argument i, LL.Argument s) => LL.Function (ModelMethod i s) (ModelMethodArg i s) ModuleGenM where
  functionType ModelMethod { interfaceSignals = signals, otherMethodArgs = otherArgs }
    = pure $ LL.FunType ity LLT.i32
   where
    ity =
      LL.FunArgument (LLT.ptr LLT.i8) "model.env" []
        :  LL.FunArgument (LLT.ptr LLT.i8) "model.state" []
        :  LL.FunArgument LLT.i1 "block.activated" []
        :  LL.FunArgument (LLT.ptr LLT.i64) "next.var.store" []
        :  otherArgs
        ++ fmap
             (\idt ->
               LL.FunArgument LLT.i64 (LL.ParameterName (LL.fromIdent idt)) []
             )
             (toList signals)

data ConstrArg = ConstrArg
  { modelEnvPtr            :: Operand
  , nextVarStorage         :: LL.StoreOf Operand
  , structuralInfoStorage  :: LL.StoreOf StructuralInfo
  , modelStateStorage      :: LL.StoreOf Operand
  , interfaceSignalIndices :: Seq Operand
  }
  deriving Generic

instance LL.Argument ConstrArg where
  fromArg ConstrArg { modelEnvPtr = modelEnvPtr, nextVarStorage = nextVarStorage, structuralInfoStorage = structuralInfoStorage, modelStateStorage = modelStateStorage, interfaceSignalIndices = iface }
    = LL.fromArg modelEnvPtr
      ++ LL.fromArg nextVarStorage
      ++ LL.fromArg modelStateStorage
      ++ LL.fromArg structuralInfoStorage
      ++ toList iface

  toArg ops = do
    (modelEnvPtr          , ops) <- LL.toArg ops
    (nextVarStorage       , ops) <- LL.toArg ops
    (modelStateStorage    , ops) <- LL.toArg ops
    (structuralInfoStorage, ops) <- LL.toArg ops
    pure
      ( ConstrArg { modelEnvPtr            = modelEnvPtr
                  , nextVarStorage         = nextVarStorage
                  , modelStateStorage      = modelStateStorage
                  , structuralInfoStorage  = structuralInfoStorage
                  , interfaceSignalIndices = Seq.fromList ops
                  }
      , []
      )

newtype ConstrFun = ConstrFun (Seq Ident)

instance LL.Function ConstrFun ConstrArg ModuleGenM where
  functionType (ConstrFun out) = pure $ LL.FunType ity LLT.i32
   where
    ity =
      [ LL.FunArgument (LLT.ptr LLT.i8) "model.env.ptr" [LL.ReadOnly]
        , LL.FunArgument (LLT.ptr LLT.i64) "next.var.store" []
        , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i8)) "model.state.store" []
        ]
        ++ structuralInfoFunArgs
        ++ [ LL.FunArgument LLT.i64 (LL.ParameterName (LL.fromIdent idt)) []
           | idt <- toList out
           ]

-- A compiled model is represented as a kind of object. The object contains
-- methods used that can be used for the three functions the object is used for:
-- computing structural information, running the simulation and monitoring and
-- handling events and mode changes. We may had a role 0 (at the end, for
-- clarity) that relates to providing some informations about the model before
-- all these steps, in order to help allocation and memory management.  For
-- these reasons, the model exports the following methods:
--
-- # Role 1: structural information
--
--  * A single method is able to compute all structural informations by
--    computing the signature matrix.
--
-- # Role 2: running the simulation
--
--  * A method that computes the residue of the model at initialisation and
--    during the simulation
--
-- # Role 3: handling events
--
--  * A method that, given an array of the right size, returns the direction of
--    the roots to monitor (up, down or up and down)
--  * A method that computes the values of the signal being monitored for roots
--  * A method that, given the list of triggered roots, computes the new state
--    the model is in
--
-- # Role 0: allocation
--
-- Most of the above require some form of working memory that must be allocated
-- by the user of the model, if we want to avoid having to perform allocation in
-- these functions. In general, we must perform some form of static allocation
-- (that is, the memory allocated does not depend on the mode the model is in),
-- some dynamic allocation that is not dependant on index-reduction (the size to
-- be allocated depends on the mode) and some dynamic allocation that depends on
-- index-reduction.  This leads to the following 3 methods:
--
--  * A function that returns the size of the model state, always big enough to
--    contain the state the model can be in in all possible modes.
--  * A function to compute size informations on a per-mode basis. In particular
--    this function computes:
--      * the number of relations,
--      * the number of entries in the signature matrix,
--      * the number of roots to monitor.
--  * A function to compute the minimum space requirement for computing the
--    residual.

data ObjectRepr constr method = ObjectRepr
  { constructorFunRepr      :: constr
  , signatureMethodRepr     :: method () FillSigMatState
  , residualMethodRepr      :: method ResidualInput ResidualState
  , rootSetupMethodRepr     :: method () RootSetupState
  , computeRootsMethodRepr  :: method ComputeRootsInput ComputeRootsState
  , eventHandlerMethodRepr  :: method EventHandlerInput EventHandlerState
  , setSignalInfoMethodRepr :: method SetSignalInfoInput SetSignalInfoState
  , computeWorkSpaceMethodRepr
      :: method ComputeWorkingSpaceInput ComputeWorkingSpaceState
  , copyInputsMethodRepr :: method CopyInputsInput CopyInputsState
  }

newtype MethodIndex r i s = MethodIndex { getMethodIndex :: r }

objectReprIndices :: Integral i => ObjectRepr i (MethodIndex i)
objectReprIndices = ObjectRepr { constructorFunRepr         = 0
                               , signatureMethodRepr        = MethodIndex 1
                               , residualMethodRepr         = MethodIndex 2
                               , rootSetupMethodRepr        = MethodIndex 3
                               , computeRootsMethodRepr     = MethodIndex 4
                               , eventHandlerMethodRepr     = MethodIndex 5
                               , setSignalInfoMethodRepr    = MethodIndex 6
                               , computeWorkSpaceMethodRepr = MethodIndex 7
                               , copyInputsMethodRepr       = MethodIndex 8
                               }

newtype MethodType i s = MethodType { getMethodType :: LLT.Type }

objectReprList
  :: Monad m
  => (constr -> m k)
  -> (  forall i s
      . (LL.Argument i, LL.Argument s, LL.Storable s)
     => method i s
     -> m k
     )
  -> ObjectRepr constr method
  -> m [k]
objectReprList constr method (ObjectRepr constructorFunRepr signatureMethodRepr residualMethodRepr rootSetupMethodRepr computeRootsMethodRepr eventHandlerMethodRepr setSignalInfoMethodRepr computeWorkSpaceMethodRepr copyInputsMethodRepr)
  = sequence
    [ constr constructorFunRepr
    , method signatureMethodRepr
    , method residualMethodRepr
    , method rootSetupMethodRepr
    , method computeRootsMethodRepr
    , method eventHandlerMethodRepr
    , method setSignalInfoMethodRepr
    , method computeWorkSpaceMethodRepr
    , method copyInputsMethodRepr
    ]

objectReprMethods :: Seq Ident -> ObjectRepr ConstrFun ModelMethod
objectReprMethods output = ObjectRepr
  { constructorFunRepr         = ConstrFun output
  , signatureMethodRepr        = mkSignatureMethod output
  , residualMethodRepr         = mkResidualMethod output
  , rootSetupMethodRepr        = mkRootSetupMethod output
  , computeRootsMethodRepr     = mkComputeRoots output
  , eventHandlerMethodRepr     = mkEventHandlerState output
  , setSignalInfoMethodRepr    = mkSetSignalInfoMethod output
  , computeWorkSpaceMethodRepr = mkComputeWorkingSpaceMethod output
  , copyInputsMethodRepr       = mkCopyInputsMethod output
  }

objectMethodReprRecordType
  :: ObjectRepr ConstrFun ModelMethod -> ModuleGenM LLT.Type
objectMethodReprRecordType repr =
  LL.structType
    <$> objectReprList (fmap LL.toFunPtrTy . LL.functionType)
                       (fmap LL.toFunPtrTy . LL.functionType)
                       repr


objectMethodRecordType :: Seq ty -> ModuleGenM LLT.Type
objectMethodRecordType =
  objectMethodReprRecordType . objectReprMethods . fmap (const undefined)

newtype MethodFun i s = MethodFun { getLLVMFun :: LL.LLVMFun (ModelMethod i s) (ModelMethodArg i s) }

declareMethodRecord
  :: Seq ty
  -> Name
  -> ObjectRepr (LL.LLVMFun ConstrFun ConstrArg) MethodFun
  -> ModuleGenM Constant
declareMethodRecord outputTy name repr = do
  ModelType { modelFunRecordType = funRecordType } <- declareModelType outputTy
  let LLT.NamedTypeReference nm = funRecordType
  LL.globalConstant
    name
    funRecordType
    (LLC.Struct
      { LLC.structName   = Just nm
      , LLC.memberValues = runIdentity $ objectReprList
                             ( Identity
                             . LL.unsafeFromConstantOp
                             . LL.getLLVMFunPtr
                             )
                             ( Identity
                             . LL.unsafeFromConstantOp
                             . LL.getLLVMFunPtr
                             . getLLVMFun
                             )
                             repr
      , LLC.isPacked     = False
      }
    )

-- MethodTypes for role 1

rowType :: LLT.Type
rowType = LL.structType
  [ LLT.i32 -- Row size
  , LLT.ptr LLT.i32 -- Row indices
  , LLT.ptr LLT.i8 -- Row entries as integers
  , LLT.ptr LLT.double -- Row entries as doubles
  , LLT.ptr LLT.i1 -- Linearity informations for variables
  ]

rowSizeIdx, rowIndicesIdx, rowEntriesIdx, doubleRowEntriesIdx, linearityInfoEntriesIdx
  :: Integral i => i
rowSizeIdx = 0
rowIndicesIdx = 1
rowEntriesIdx = 2
doubleRowEntriesIdx = 3
linearityInfoEntriesIdx = 4

data FillSigMatState = FillSigMatState
  { nextRow, nextEntryIndex, nextEntry, nextDoubleEntry, nextLinearityInfoEntry
      :: Operand
  }
  deriving Generic

instance LL.Argument FillSigMatState

instance LL.Storable FillSigMatState

instance LL.Merge FillSigMatState

mkSignatureMethod :: Seq Ident -> ModelMethod () FillSigMatState
mkSignatureMethod args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  =
    [ LL.FunArgument (LLT.ptr (LLT.ptr rowType)) "next.row" []
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i32)) "next.entry.index" []
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i8)) "next.entry" []
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.double)) "next.double.entry" []
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i1)) "next.linearity.info.entry" []
    ]
  }

-- MethodTypes for role 2

data ResidualInput = ResidualInput
  { atInit       :: Operand
  , eqnHODArray  :: Operand
  , inputSignals :: Operand
  , workingSpace :: Operand
  }
  deriving Generic

instance LL.Argument ResidualInput

data ResidualState = ResidualState
  { resultPtr     :: Operand
  , equationCount :: Operand
  }
  deriving Generic

instance LL.Argument ResidualState
instance LL.Storable ResidualState
instance LL.Merge ResidualState

mkResidualMethod :: Seq Ident -> ModelMethod ResidualInput ResidualState
mkResidualMethod args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  =
    [ LL.FunArgument LLT.i1 "at.init" []
    , LL.FunArgument (LLT.ptr LLT.i64) "equation.hod.array" [LL.ReadOnly]
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.double))
                     "input.signals"
                     [LL.ReadOnly]
    , LL.FunArgument (LLT.ptr LLT.double) "workspace" []
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.double)) "result.ptr.store" []
    , LL.FunArgument (LLT.ptr LLT.i64) "equation.count" []
    ]
  }

newtype ComputeWorkingSpaceInput =
  ComputeWorkingSpaceInput { eqnHODArray :: Operand }
  deriving(Generic)

instance LL.Argument ComputeWorkingSpaceInput

data ComputeWorkingSpaceState = ComputeWorkingSpaceState
  { equationCount  :: Operand
  , currentRequest :: Operand
  }
  deriving Generic

instance LL.Argument ComputeWorkingSpaceState
instance LL.Merge ComputeWorkingSpaceState
instance LL.Storable ComputeWorkingSpaceState

mkComputeWorkingSpaceMethod
  :: Seq Ident -> ModelMethod ComputeWorkingSpaceInput ComputeWorkingSpaceState
mkComputeWorkingSpaceMethod args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  = [ LL.FunArgument (LLT.ptr LLT.i64)
                                        "equation.hod.array"
                                        [LL.ReadOnly]
                       , LL.FunArgument (LLT.ptr LLT.i64) "equation.count" []
                       , LL.FunArgument (LLT.ptr LLT.i64) "current.request" []
                       ]
  }

-- MethodTypes for role 3

newtype RootSetupState = RootSetupState
  { nextRootDir :: Operand
  }
  deriving Generic

instance LL.Argument RootSetupState
instance LL.Merge RootSetupState
instance LL.Storable RootSetupState

mkRootSetupMethod :: Seq Ident -> ModelMethod () RootSetupState
mkRootSetupMethod args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  = [ LL.FunArgument (LLT.ptr (LLT.ptr LLT.i32))
                                        "next.root"
                                        []
                       ]
  }

newtype ComputeRootsInput = ComputeRootsInput
  { inputSignals :: Operand }
  deriving(Generic)

instance LL.Argument ComputeRootsInput

newtype ComputeRootsState = ComputeRootsState
  { rootResultPtr :: Operand
  } deriving(Generic)

instance LL.Argument ComputeRootsState
instance LL.Merge ComputeRootsState
instance LL.Storable ComputeRootsState

mkComputeRoots :: Seq Ident -> ModelMethod ComputeRootsInput ComputeRootsState
mkComputeRoots args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  =
    [ LL.FunArgument (LLT.ptr (LLT.ptr LLT.double))
                     "input.signals"
                     [LL.ReadOnly]
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.double)) "root.ptr.store" []
    ]
  }

data StructuralInfo = StructuralInfo
  { variableCount, relationsCount, initRelationsCount, entriesCount, rootCount
      :: Operand
  }
  deriving Generic

instance LL.Argument StructuralInfo
instance LL.Merge StructuralInfo
instance LL.Storable StructuralInfo

structuralInfoFunArgs :: [LL.FunArgument]
structuralInfoFunArgs =
  [ LL.FunArgument (LLT.ptr LLT.i64) "variable.count" []
  , LL.FunArgument (LLT.ptr LLT.i64) "relations.count" []
  , LL.FunArgument (LLT.ptr LLT.i64) "init.relations.count" []
  , LL.FunArgument (LLT.ptr LLT.i64) "entries.count" []
  , LL.FunArgument (LLT.ptr LLT.i64) "root.count" []
  ]

addStructuralInfo :: StructuralInfo -> StructuralInfo -> CGenM StructuralInfo
addStructuralInfo StructuralInfo { variableCount = vc1, relationsCount = rc1, initRelationsCount = irc1, entriesCount = ec1, rootCount = rt1 } StructuralInfo { variableCount = vc2, relationsCount = rc2, initRelationsCount = irc2, entriesCount = ec2, rootCount = rt2 }
  = do
    vc  <- LL.add vc1 vc2
    rc  <- LL.add rc1 rc2
    irc <- LL.add irc1 irc2
    ec  <- LL.add ec1 ec2
    rt  <- LL.add rt1 rt2
    pure StructuralInfo { variableCount      = vc
                        , relationsCount     = rc
                        , initRelationsCount = irc
                        , entriesCount       = ec
                        , rootCount          = rt
                        }

data EventHandlerInput = EventHandlerInput
  { inputSignals      :: Operand
  , modelStateStorage :: LL.StoreOf Operand
  -- , structuralInfoStorage :: LL.StoreOf StructuralInfo
  }
  deriving Generic

instance LL.Argument EventHandlerInput

data EventHandlerState = EventHandlerState
  { nextRoot, anyChange, structuralChange :: Operand
  , structuralInfo                        :: StructuralInfo
  }
  deriving Generic

instance LL.Argument EventHandlerState
instance LL.Merge EventHandlerState
instance LL.Storable EventHandlerState

mkEventHandlerState
  :: Seq Ident -> ModelMethod EventHandlerInput EventHandlerState
mkEventHandlerState args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  =
    [ LL.FunArgument (LLT.ptr (LLT.ptr LLT.double))
                     "input.signals"
                     [LL.ReadOnly]
      , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i8)) "model.state.storage" []
      , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i32)) "next.root.ptr" []
      , LL.FunArgument (LLT.ptr LLT.i1) "anychange" []
      , LL.FunArgument (LLT.ptr LLT.i1) "structuralchange" []
      ]
      ++ structuralInfoFunArgs
  }

-- -- Role 0 methods

data EventHandlerMemorySize = EventHandlerMemorySize

instance LL.Function EventHandlerMemorySize () ModuleGenM where
  functionType EventHandlerMemorySize = pure $ LL.FunType [] LLT.i64

data EventHandlerStateSize = EventHandlerStateSize

instance LL.Function EventHandlerStateSize () ModuleGenM where
  functionType EventHandlerStateSize = pure $ LL.FunType [] LLT.i64

data SetSignalInfoInput = SetSignalInfoInput
  { reinitSignals, signalNames :: Operand
  }
  deriving Generic

instance LL.Argument SetSignalInfoInput

data SetSignalInfoState = SetSignalInfoState {}
  deriving Generic

instance LL.Argument SetSignalInfoState
instance LL.Storable SetSignalInfoState
instance LL.Merge SetSignalInfoState

mkSetSignalInfoMethod
  :: Seq Ident -> ModelMethod SetSignalInfoInput SetSignalInfoState
mkSetSignalInfoMethod args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  = [ LL.FunArgument (LLT.ptr LLT.i1) "reinit.signals" []
                       , LL.FunArgument (LLT.ptr (LLT.ptr LLT.i8))
                                        "signal.names"
                                        []
                       ]
  }

newtype CountRootsArg =
  CountRootsArg { modelState :: Operand }
  deriving(Generic)

instance LL.Argument CountRootsArg

data CountRoots = CountRoots

instance LL.Function CountRoots CountRootsArg ModuleGenM where
  functionType CountRoots =
    pure $ LL.FunType [LL.FunArgument (LLT.ptr LLT.i8) "model.state" []] LLT.i64

data CopyInputsInput = CopyInputsInput
  { blockExisted, oldHodArray, currHodArray, oldInputSignals, currInputSignals
      :: Operand
  }
  deriving Generic

instance LL.Argument CopyInputsInput

newtype CopyInputsState = CopyInputsState
  { oldNextVar :: Operand
  }
  deriving Generic

instance LL.Argument CopyInputsState
instance LL.Storable CopyInputsState
instance LL.Merge CopyInputsState

mkCopyInputsMethod :: Seq Ident -> ModelMethod CopyInputsInput CopyInputsState
mkCopyInputsMethod args = ModelMethod
  { interfaceSignals = args
  , otherMethodArgs  =
    [ LL.FunArgument LLT.i1 "block.existed" []
    , LL.FunArgument (LLT.ptr LLT.i64) "old.hod.array" [LL.ReadOnly]
    , LL.FunArgument (LLT.ptr LLT.i64) "curr.hod.array" [LL.ReadOnly]
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.double)) "old.input.signals" []
    , LL.FunArgument (LLT.ptr (LLT.ptr LLT.double)) "curr.input.signals" []
    , LL.FunArgument (LLT.ptr LLT.i64) "old.next.var.storage" []
    ]
  }

-- Compiled object types and representation.

-- | Representation of the state of a model
--
-- The state of a model contains all the informations needed to execute the
-- model. In particular, it contains all the values of the captured expressions
-- well as state informations for some of the relations. Consider the following
-- block:

--   [attributes]
--      x : T1
--      y : T2
--      z : T3
--   [relations]
--      a = b
--      c <> x
--      switch
--        mode A -> ...
--        mode B
--      end

-- The representation of the state of that block is a pointer to a two element
-- structure. The first element is a 3-element structure containing the
-- attributes, the second is a linked-list style 2-element structure containing
-- the state of the x signal relation and the state of the switch. All in all,
-- this looks like this:
--
-- {{T1, T2, T3}, {i8*, {{i64, i8*, i8*}, {}}}*
--
-- The state of the model relation is an opaque pointer to an i8, it will be
-- casted by the method of x
--
-- The state of the switch is a struct containing the tag (an i64), the
-- arguments array (an opaque i8, as it is potentially a union of all the
-- possible arguments of the switch) and an i8* to the state of the block
-- defined by each switch mode.
--
-- Note that no state is associated with the equation a = b
--
-- The relations state are stored as a linked-list styled struct. This is
-- because it makes it easier to generate code by, when a relation possessing
-- some state is encountered, essentially « popping-off » its state from the
-- relation states and then just carrying along with the rest of the state for
-- the other relations. The alternative would be to just carry an index into the
-- struct but this solution is simpler in my opinion.

blockStateStruct :: Block -> ModuleGenM LLT.Type
blockStateStruct Block { blockAttributes = attrs, blockRelations = rels } = do
  attrsTy <- traverse (toLLTyp . typeOf) (toList attrs)
  pure
    (LL.structType
      [LL.structType attrsTy, makeRelType (foldMap relStateType rels)]
    )

 where
  makeRelType Empty        = LL.structType []
  makeRelType (ty :<| tys) = LL.structType [ty, makeRelType tys]

relStateType :: Rel -> Seq LLT.Type
relStateType Eqn{}              = []
relStateType (Let       _ rels) = foldMap relStateType rels
relStateType (ModelCall _ _   ) = [LL.voidPtr]
relStateType (Switch    _ _   ) = [switchType]

unConsState :: Operand -> CGenM (Operand, Operand)
unConsState state = case LLT.typeOf state of
  LLT.PointerType { LLT.pointerReferent = LLT.StructureType _ [_, _] } -> do
    hd <- LL.named (LL.ibgep state [LL.int32 0, LL.int32 0]) "this.hd"
    tl <- LL.named (LL.ibgep state [LL.int32 0, LL.int32 1]) "this.tl"
    pure (hd, tl)
  ty -> internalError
    (unwords
      [ "When trying to get the head and tail of the storage pointer"
      , "the storage pointer had type "
      , show (pretty ty)
      , "but was expected to have a type of the form"
      , "{hdty, tlty}*"
      ]
    )

data SwitchState = SwitchState
  { switchIndexPtr              :: Operand
  , switchActivePtr             :: Operand
  , switchPreviousIndexPtr      :: Operand
  , switchPreviousModeNewVarPtr :: Operand
  , switchArgsPtrPtr            :: Operand
  , switchSubBlockStatePtr      :: Operand
  }

switchType :: LLT.Type
switchType =
  LL.structType [LLT.i64, LLT.i1, LLT.i64, LLT.i64, LL.voidPtr, LL.voidPtr]

getSwitchState :: Operand -> CGenM SwitchState
getSwitchState ptr
  | LLT.typeOf ptr == LLT.ptr switchType = do
    indexPtr <- LL.named (LL.ibgep ptr [LL.int32 0, LL.int32 0])
                         "switch.index.ptr"
    activePtr <- LL.named (LL.ibgep ptr [LL.int32 0, LL.int32 1])
                          "switch.active.ptr"
    previousIndexPtr <- LL.named (LL.ibgep ptr [LL.int32 0, LL.int32 2])
                                 "switch.previous.index.ptr"
    previousModeNewVarPtr <- LL.named (LL.ibgep ptr [LL.int32 0, LL.int32 3])
                                      "switch.previous.add.var.ptr"
    argsPtrPtr <- LL.named (LL.ibgep ptr [LL.int32 0, LL.int32 4])
                           "switch.args.ptr"
    subBlockPtr <- LL.named (LL.ibgep ptr [LL.int32 0, LL.int32 5])
                            "switch.subblock.state"
    pure SwitchState { switchIndexPtr              = indexPtr
                     , switchActivePtr             = activePtr
                     , switchPreviousIndexPtr      = previousIndexPtr
                     , switchPreviousModeNewVarPtr = previousModeNewVarPtr
                     , switchArgsPtrPtr            = argsPtrPtr
                     , switchSubBlockStatePtr      = subBlockPtr
                     }
  | otherwise = internalError
    (unwords
      [ "When trying to get state of a switch node,"
      , "the storage pointer had type "
      , show (pretty (LLT.typeOf ptr))
      , "but was expected to have a type of the form"
      , show $ pretty $ LLT.ptr switchType
      ]
    )

data RelState = CallRel Operand -- i8*
              | SwitchRel SwitchState

data BlockState = BlockState
  { blockAttributesState :: Operand
  , blockRelsState       :: Seq RelState
  }

data ModelState = AtEntry Operand | AtBlock BlockState | InRel (Seq Rel)

getModelState :: Operand -> CGenM Operand
getModelState ptr = case LLT.typeOf ptr of
  LLT.PointerType { LLT.pointerReferent = LLT.PointerType{} } -> do
    LL.named (LL.load ptr 0) "model.state.ptr"
  ty -> internalError
    (unwords
      [ "When trying to get state of a model,"
      , "the storage pointer had type "
      , show (pretty ty)
      , "but was expected to have a type of the form"
      , "i8**"
      ]
    )
-- Compiling the functional level

topLevelFunTag, closureTag, partialAppTag :: Integral i => i
topLevelFunTag = 0
closureTag = succ topLevelFunTag
partialAppTag = succ closureTag

funTagIndex, funPtrIndex :: Integral i => i
funTagIndex = 0
funPtrIndex = succ funTagIndex

-- rawModelType :: Seq TSig -> ModelType
-- rawModelType os = ModelType
--   { modelStructType    = LL.structType [LLT.ptr funRecordTy, LL.voidPtr]
--   , modelFunRecordType = funRecordTy
--   }
--   where funRecordTy = objectMethodRecordType (fmap (const undefined) os)

makeModelStructType :: LLT.Type -> LLT.Type
makeModelStructType funRecordTy =
  LL.structType [LLT.ptr funRecordTy, LL.voidPtr]

toRawLLTyp :: TExpr -> ModuleGenM LLT.Type
toRawLLTyp (TSig TReal) = pure LLT.double
toRawLLTyp TArr{}       = pure LL.voidPtr
toRawLLTyp (TModel os)  = do
  ty <- objectMethodRecordType os
  pure $ LLT.ptr $ makeModelStructType ty

declareModelType :: Seq ty -> ModuleGenM ModelType
declareModelType tys = do
  ty <- objectMethodRecordType tys
  getModelType tys ty makeModelStructType

toLLTyp :: TExpr -> ModuleGenM LLT.Type
toLLTyp (TModel os) = LLT.ptr . modelStructType <$> declareModelType os
toLLTyp ty          = toRawLLTyp ty

data InstFun = InstFun (Seq Arg) TExpr

newtype InstArgs = InstArgs (Seq Operand)

instance LL.Argument InstArgs where
  toArg ops = Just (InstArgs (Seq.fromList ops), [])

  fromArg (InstArgs ops) = toList ops

instance LL.Function InstFun InstArgs ModuleGenM where
  functionType (InstFun args ret) = do
    args <- traverse go (toList args)
    ret  <- toLLTyp ret
    pure $ LL.FunType args ret
   where
    go (Arg idt typ) = do
      typ <- toLLTyp typ
      pure $ LL.FunArgument typ (LL.ParameterName (LL.fromIdent idt)) []
