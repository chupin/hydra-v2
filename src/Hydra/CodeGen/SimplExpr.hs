module Hydra.CodeGen.SimplExpr
  ( compileSimplExpr
  ) where

import           Hydra.CodeGen.Scope
import           Hydra.OOIR
import           LLVM.AST.Operand
import qualified LLVM.IRBuilder.Constant       as LL

compileSimplExpr :: ModelScope -> AtomExpr -> Operand
compileSimplExpr _     (Const c         ) = LL.double (fromRational c)
compileSimplExpr scope (LocalExpr attr _) = lookupAttr attr scope
