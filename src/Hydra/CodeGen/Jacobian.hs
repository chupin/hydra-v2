{-# LANGUAGE GeneralizedNewtypeDeriving, TemplateHaskell, FlexibleContexts, OverloadedLists, OverloadedStrings #-}

module Hydra.CodeGen.Jacobian where

import           Prelude                 hiding ( div
                                                , sum
                                                )
import qualified Hydra.CodeGen.FunctionNames   as Name
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.OOIR
import           Hydra.CodeGen.Residual
import           Hydra.Error                    ( internalError )
import           Hydra.Ident
import qualified Hydra.LLVM                    as LL
import qualified Hydra.Op                      as Op
import           Hydra.Op                       ( Op(..) )

import           Control.Arrow
import           Control.Lens            hiding ( Const )
import           Control.Monad.Reader    hiding ( local )
import           Data.List                      ( sortBy )
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import qualified Data.Map.Merge.Lazy           as M
import           Data.Semigroup
import qualified LLVM.AST.IntegerPredicate     as LL
import qualified LLVM.AST.Name                 as LL
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.IRBuilder.Constant       as LL
import qualified LLVM.IRBuilder.Instruction    as LL
import           LLVM.IRBuilder.Module          ( MonadModuleBuilder )
import qualified LLVM.IRBuilder.Module         as LL
import           LLVM.IRBuilder.Monad           ( MonadIRBuilder )
import qualified LLVM.IRBuilder.Monad          as LL
import           LLVM.Pretty                    ( )

signalJacobian :: Sig -> ()
signalJacobian (Local   (LocalSig i k)) = _
signalJacobian (SigExpr _             ) = _
signalJacobian (Op      os            ) = _

-- newtype JacGenM a = JacGenM (ReaderT Operand CGenM a)
--                   deriving(Functor, Applicative, Monad, MonadFix, MonadIRBuilder, MonadModuleBuilder)

-- liftCGenM :: CGenM a -> JacGenM a
-- liftCGenM = JacGenM . lift

-- getJacVarIdx :: JacGenM Operand
-- getJacVarIdx = JacGenM ask

-- runJacGenM :: JacGenM a -> Operand -> CGenM a
-- runJacGenM (JacGenM act) = runReaderT act

-- jacNCompose
--   :: (Operand -> CGenM Operand) -- \i -> f^(i)(g(t))
--   -> (Operand -> CGenM Operand) -- \i -> g^(i)(t)
--   -> (Operand -> CGenM Operand) -- \i -> dg^(i) / dx^(k)
--   -> Operand  -- n
--   -> CGenM Operand
-- jacNCompose callf callg callJg n =
--   IR.compileSimplIR
--     $ IR.If
--         (IR.inject n IR..<= 0)
--         (IR.injectM (callJg (constDiff 0)) * IR.injectM (callf (constDiff 1)))
--     $ IR.Let (IR.injectM (partCount n))
--     $ \partCount -> IR.Let (callJg .$ IR.constWord 0) $ \jg0 ->
--         IR.sum (Range (Including 0) (Excluding partCount)) $ \i ->
--           IR.Let (leadingFactor n .$ i) $ \ci ->
--             IR.Let (coefficientSum n .$ i) $ \di ->
--               let coeff j = IR.injectM $ do
--                     i <- IR.compileSimplIR i
--                     j <- IR.compileSimplIR j
--                     coefficient n i j
--               in
--                 IR.Let (IR.inject n) $ \n ->
--                   ci
--                     * ( jg0
--                       * (callf .$ di + IR.constWord 1)
--                       * IR.prod (Range (Including 0) (Including n))
--                                 (\j -> (callg .$ j) .^ coeff j)

--                       + (callf .$ di)
--                       * IR.sum
--                           (Range (Including 0) (Including n))
--                           (\j -> IR.Let (coeff j) $ \mj -> IR.If
--                             (mj .== 0)
--                             0.0
--                             (IR.Cast mj * (callJg .$ j) .^ (mj - 1) * IR.prod
--                               (Range (Including 1) (Including n))
--                               (\p -> IR.If (p .== j)
--                                            1.0
--                                            ((callg .$ p) .^ coeff p)
--                               )
--                             )
--                           )
--                       )


-- jacMulNDer
--   :: (Operand -> CGenM Operand)
--   -> (Operand -> CGenM Operand)
--   -> (Operand -> CGenM Operand)
--   -> (Operand -> CGenM Operand)
--   -> Operand
--   -> CGenM Operand
-- jacMulNDer p dp q dq n =
--   IR.compileSimplIR
--     $ IR.sum (Range (Including 0) (Including (IR.inject n)))
--     $ \i ->
--         (lookupBinom n .$ i)
--           * ( (dp .$ i)
--             * (q .$ IR.inject n - i)
--             + (p .$ i)
--             * (dq .$ IR.inject n - i)
--             )

-- data LinearityInfo =
--   LinearityInfo { appearsLinearly :: !Bool
--                 , highestOrderAppearance :: !F.Order
--                 }

-- simplCombine
--   :: (Operand -> Operand -> Op Operand)
--   -> JacGenM (Operand -> CGenM Operand)
--   -> JacGenM (Operand -> CGenM Operand)
--   -> JacGenM (Operand -> CGenM Operand)
-- simplCombine op p q = do
--   p <- p
--   q <- q
--   let make n = do
--         p <- p n
--         q <- q n
--         compileOp (op p q)
--   pure make

-- mergeJacOp
--   :: LkpFun CGenM
--   -> Op F.Sig
--   -> LoopGenM (Map Ident (JacGenM (Operand -> CGenM Operand)))
-- mergeJacOp lookup (F.ArithOp op) = case op of
--   F.ArithBinOp F.Add p q -> do
--     jp <- relJacobian lookup p
--     jq <- relJacobian lookup q
--     pure (M.unionWith (simplCombine Op.add) jp jq)
--   F.ArithBinOp F.Sub p q -> do
--     jp <- relJacobian lookup p
--     jq <- relJacobian lookup q
--     let missingLhs _ = fmap (>=> LL.fneg)
--     pure
--       (M.merge M.preserveMissing
--                (M.mapMissing missingLhs)
--                (M.zipWithMatched (const (simplCombine Op.sub)))
--                jp
--                jq
--       )
--   F.ArithBinOp F.Mul p q -> do
--     jp <- relJacobian lookup p
--     jq <- relJacobian lookup q
--     pn <- memoizing (compileSig lookup p)
--     qn <- memoizing (compileSig lookup q)
--     pure
--       (M.merge (M.mapMissing (missing q qn))
--                (M.mapMissing (missing p pn))
--                (M.zipWithMatched (merge pn qn))
--                jp
--                jq
--       )
--    where
--     missing p pn _ dq = mulNDer pn <$> dq
--     merge p q _ dp dq = do
--       dp <- dp
--       dq <- dq
--       pure (jacMulNDer p dp q dq)

--   F.ArithBinOp F.Div p q -> do
--     pn       <- memoizing (compileSig lookup p)
--     (q0, qn) <- do
--       (q0, qn) <- liftGM $ compileSig lookup p
--       qn       <- case q of
--         F.Op{} -> memoize q0 qn
--         _      -> qn
--       pure (q0, qn)
--     (_, iq0, iqn) <- nInvertSig q q0 qn
--     let
--         -- If x doesn't appear in q, then we simply have to divide dp / dx^(k) by q
--       missingDenominator _ dp = do
--         dp <- dp
--         pure (mulNDer dp iqn)


--       denominatorJac dq = do
--         -- We handle the case where p doesn't depend on x^(k). Then we have to
--         -- calcule d D_n / dx^(k), where D_n = (1 / q)^(n). This is given by the
--         -- following formula:
--         --   dDn/dx^(k) = -n!
--         --              * (1 / q)
--         --              * Σ{i = 0; i < n} (f^(n - i) / ((n - i)!i!))
--         --                    * (dDi/dx^(k) + Di * (df^(n - i) / dx^(k)) - 1/f*df/dx^(k))
--         --
--         -- The first term, dD0/dx^(k) is simply -df/dx^(k) / f^2
--         --
--         -- The problem here is that we can't memoize the dDn/dx^(k), because
--         -- of the way the loop go. That's potentially a big problem that would
--         -- need to be addressed at some point.
--         --
--         -- To avoid needless recomputations, we compute all of them up to n
--         -- and store them in an array at the start

--         dq <- dq
--         let
--           make n = do
--             dq0     <- dq (constDiff 0)
--             cst     <- LL.fmul iq0 dq0
--             diq0    <- LL.fneg =<< LL.fdiv dq0 =<< LL.fmul iq0 iq0
--             m       <- LL.add n (constDiff 1)
--             memoDiq <- LL.alloca LLT.double (Just n) 0
--             LL.storeAt memoDiq (constDiff 0) diq0
--             LL.iter (constDiff 1) m $ \n ->
--               LL.storeAt memoDiq n =<< IR.compileSimplIR
--                 (-IR.injectM (lookupFactorial n) * IR.inject iq0 * IR.sum
--                   (Range (Including 0) (Excluding (IR.inject n)))
--                   (\i -> IR.Let (IR.inject n) $ \n ->
--                     (  (qn .$ n - i)
--                       ./ ((lookupFactorial .$ n - i) * (lookupFactorial .$ i))
--                       )
--                       * ( (LL.loadAt memoDiq .$ i)
--                         + (iqn .$ i)
--                         * ((dq .$ n - i) - IR.inject cst)
--                         )
--                   )
--                 )
--             LL.loadAt memoDiq n
--         pure make

--       merge _ dp dq = do
--         dp  <- dp
--         idq <- denominatorJac dq
--         pure (jacMulNDer pn dp iqn idq)


--     jp <- relJacobian lookup p
--     jq <- relJacobian lookup q
--     pure
--       (M.merge (M.mapMissing missingDenominator)
--                (M.mapMissing (const denominatorJac))
--                (M.zipWithMatched merge)
--                jp
--                jq
--       )
--   F.ArithBinOp F.Pow p q -> undefined
--   F.ArithUnOp op p       -> do
--     (p0, pn) <- liftGM $ compileSig lookup p
--     pn       <- case p of
--       F.Op{} -> memoize p0 pn
--       _      -> pn
--     let go = fmap (jacNCompose (unOpDer op p0) pn)
--     fmap go <$> relJacobian lookup p

-- jacVar :: Ident -> Word -> Map Ident (JacGenM (Operand -> CGenM Operand))
-- jacVar i p = M.singleton i make
--  where
--   make = do
--     k <- getJacVarIdx
--     pure
--       (\n -> do
--         n   <- LL.add n (constDiff p)
--         isK <- LL.icmp LL.EQ n k
--         LL.select isK (LL.double 1) (LL.double 0)
--       )

-- linInfo :: F.Sig -> Map Ident LinearityInfo
-- linInfo sig = case sig of
--   F.Expr{} -> []
--   F.Local _ i n ->
--     [ ( i
--       , LinearityInfo { appearsLinearly        = True
--                       , highestOrderAppearance = F.order n
--                       }
--       )
--     ]
--   F.Op (ArithOp (F.ArithUnOp _ p    )) -> nonLin <$> linInfo p
--   F.Op (ArithOp (F.ArithBinOp op p q)) -> case op of
--     F.Add -> M.unionWith mergeSimpl (linInfo p) (linInfo q)
--     F.Sub -> M.unionWith mergeSimpl (linInfo p) (linInfo q)
--     F.Div -> M.merge (M.mapMissing missingDenominator)
--                      M.preserveMissing
--                      (M.zipWithMatched mergeNonLin)
--                      (linInfo p)
--                      (fmap nonLin (linInfo q))
--      where
--       missingDenominator = case q of
--         F.Expr{} -> const id
--         _        -> const nonLin
--     F.Mul -> M.merge (M.mapMissing (missing q))
--                      (M.mapMissing (missing p))
--                      (M.zipWithMatched mergeNonLin)
--                      (linInfo p)
--                      (linInfo q)
--      where
--       missing q = case q of
--         F.Expr{} -> const id
--         _        -> const nonLin
--     F.Pow ->
--       M.unionWith mergeSimpl (nonLin <$> linInfo p) (nonLin <$> linInfo q)
--  where
--   nonLin lin = lin { appearsLinearly = False }
--   mergeSimpl (LinearityInfo l1 hod1) (LinearityInfo l2 hod2) =
--     LinearityInfo (l1 && l2) (hod1 <> hod2)
--   mergeNonLin _ (LinearityInfo _ h1) (LinearityInfo _ h2) =
--     LinearityInfo False (h1 <> h2)

-- relJacobian
--   :: LkpFun CGenM
--   -> F.Sig
--   -> LoopGenM (Map Ident (JacGenM (Operand -> CGenM Operand)))
-- relJacobian lookup sig = case sig of
--   F.Expr{}      -> pure []
--   F.Local _ i n -> pure $ jacVar i n
--   F.Op op       -> mergeJacOp lookup op

-- data JacobianSt s =
--   JacobianSt { _jacEqnCount :: Operand
--              , _jacMatrixState :: s
--              }

-- makeLenses ''JacobianSt

-- data MatrixHandle s m =
--   MatrixHandle { nextLine :: s -> m s
--                , setEntry :: s -> Operand -> Operand -> m s
--                , mergeHandles :: [(s, LL.Name)] -> m s
--                }

-- -- A matrix represented as a big continuous array of doubles. An n × m matrix is
-- -- represented by a n × m long array, where line i starts at address n × i
-- denseMatrixHandle :: MatrixHandle s m
-- denseMatrixHandle = internalError "Dense matrices not supported"

-- data SparseMatrix = SparseMatrix { indexValuePtr :: Operand
--                                  , indexPtrPtr :: Operand
--                                  , sparseDataPtr :: Operand
--                                  , sparseWrittenValues :: Operand
--                                  }

-- sparseMatrixHandle :: MatrixHandle SparseMatrix CGenM
-- sparseMatrixHandle = MatrixHandle { nextLine     = nextLine
--                                   , setEntry     = setEntry
--                                   , mergeHandles = mergeHandles
--                                   }
--  where
--   nextLine matrix@SparseMatrix { indexPtrPtr = indexPtr, sparseWrittenValues = writtenValues }
--     = do
--       -- When we get to the next line, we must simply write at the next indexPtr
--       -- the number of values that have been written so far.
--       indexPtr <- LL.named (LL.nextPtr indexPtr) "index_ptr"
--       LL.store indexPtr 0 writtenValues
--       pure matrix { indexPtrPtr = indexPtr }
--   setEntry matrix@SparseMatrix { indexValuePtr = indexVals, sparseDataPtr = dataPtr, sparseWrittenValues = writtenValues } col val
--     = do
--       indexVals     <- LL.named (LL.storeAndStep indexVals col) "index_vals"
--       dataPtr       <- LL.named (LL.storeAndStep dataPtr val) "matrix_data"
--       writtenValues <- LL.named (LL.incr writtenValues) "writ_val_ct"

--       pure matrix { indexValuePtr       = indexVals
--                   , sparseDataPtr       = dataPtr
--                   , sparseWrittenValues = writtenValues
--                   }

--   mergeHandles branches = do
--     let select f = LL.phi . fmap (first f)
--     indexValue    <- LL.named (select indexValuePtr branches) "index_vals"
--     indexPtr      <- LL.named (select indexPtrPtr branches) "index_ptr"
--     dataPtr       <- LL.named (select sparseDataPtr branches) "matrix_data"
--     writtenValues <- LL.named (select sparseWrittenValues branches)
--                               "writ_val_ct"
--     pure SparseMatrix { indexValuePtr       = indexValue
--                       , indexPtrPtr         = indexPtr
--                       , sparseDataPtr       = dataPtr
--                       , sparseWrittenValues = writtenValues
--                       }

-- data JacTraverserInput s m =
--   JacTraverserInput { matrixHandle :: MatrixHandle s m
--                     , jacResidualInput :: ResidualInput
--                     , lookupFunction :: Scope -> LkpFun m
--                     , shouldConsiderInit :: Bool
--                     , cutVariable :: Scope -> Ident -> CGenM Operand
--                     }

-- cutRes :: Ident -> CGenM Operand
-- cutRes _ = pure (constDiff 0)

-- cutInit :: ResidualInput -> Scope -> Ident -> CGenM Operand
-- cutInit input scope idt = do
--   let var = lookupSig idt scope
--   isReinit <- LL.loadAt (input ^. reinitArray) var
--   headVar  <- LL.loadAt (input ^. headVarArray) var
--   LL.select isReinit (constDiff 0) headVar

-- jacobianTraverser :: JacTraverserInput s CGenM -> Traverser (JacobianSt s)
-- jacobianTraverser JacTraverserInput { cutVariable = cut, jacResidualInput = input, matrixHandle = MatrixHandle { nextLine = nextLine, setEntry = setEntry, mergeHandles = mergeHandles }, lookupFunction = lookup, shouldConsiderInit = shouldConsiderInit }
--   = trivialTraverser
--     { _compileSimpleRel = \st rel -> do
--       let us       = st ^. userState

--           count    = us ^. jacEqnCount
--           mat      = us ^. jacMatrixState
--           hodArray = input ^. eqnHODArray

--           withLinInfo sig = M.merge (M.mapMissing missing)
--                                     (M.mapMissing missing)
--                                     (M.zipWithMatched (const (,)))
--                                     (linInfo sig)
--             where missing = internalError "Jacobian construction is faulty"
--       case rel of
--         ResidualRel lhs rhs -> do
--           let sig = relSig lhs rhs

--           hod           <- LL.loadAt hodArray count
--           hodSucc       <- LL.add hod (constDiff 1)

--           (jac, update) <- runLoopGenM
--             (relJacobian (lookup (st ^. variableScope)) sig)
--             hod
--           (_, mat) <- LL.for (constDiff 0) hodSucc mat mergeHandles $ \n mat ->
--             do
--               update n
--               nextLine =<< consumeJac st n mat (withLinInfo sig jac)

--           count <- LL.named (LL.incr count) "count"
--           pure (set jacEqnCount count (set jacMatrixState mat us))
--         InitRel rel | shouldConsiderInit -> do
--           let active = st ^. newlyEnteredBlock
--               merge ((mth, ctth), th) ((mel, ctel), el) = do
--                 mat   <- mergeHandles [(mth, th), (mel, el)]
--                 count <- LL.phi [(ctth, th), (ctel, el)]
--                 pure (mat, count)
--           (mat, count) <- LL.genChoice
--             merge
--             active
--             (do
--               let sig = case rel of
--                     F.CausalInit idt val ->
--                       relSig (F.localSig F.treal idt 0) (F.realConst val)
--                     F.GenInit lhs rhs -> relSig lhs rhs
--               (jac, update) <- runLoopGenM
--                 (relJacobian (lookup (st ^. variableScope)) sig)
--                 (constDiff 0)

--               update (constDiff 0)
--               mat <- nextLine
--                 =<< consumeJac st (constDiff 0) mat (withLinInfo sig jac)
--               count <- LL.named (LL.incr count) "count"
--               pure (mat, count)
--             )
--             (pure (mat, count))
--           pure (set jacEqnCount count (set jacMatrixState mat us))

--         _ -> pure (st ^. userState)
--     , _mergeStates      = \st states -> do
--       st <- phiMerger [jacEqnCount] st states
--       us <- mergeHandles (fmap (\(s, lab) -> (s ^. jacMatrixState, lab)) states)
--       pure (set jacMatrixState us st)
--     }
--  where
--   consumeJac st n mat jac = do
--     let listJac =
--           sortBy (\(i, _) (j, _) -> scopeOrdering (st ^. variableScope) i j)
--             $ M.toList jac
--     foldM (\mat (idt, linInfo) -> go st n mat idt linInfo) mat listJac
--   go st n mat idt (LinearityInfo appearsLinearly order, compute) = do
--     -- We compute df^(n) / didt^(k), for k going from cut to n + idtHOD, where p
--     -- is the highest-order derivative with which x appears in the expression.
--     -- cut is the lowest-order derivative for which x is a variable (and not a
--     -- constant). While solving for the dynamic system, cut is always 0, but
--     -- when solving for the initialization problem it might be different,
--     -- depending on whether the variable is reinitialized or not.
--     --
--     -- See the comment in countNonZeroTraverser for explanation about the loop bounds

--     let scope = st ^. variableScope
--         p     = F.minOrder order
--         q     = F.maxOrder order
--     r          <- cut scope idt

--     startBound <- LL.named
--       (if appearsLinearly
--         then IR.compileSimplIR
--           (IR.max (IR.inject r) (fromIntegral p + IR.inject n))
--         else pure r
--       )
--       "start_bound"
--     stopBound <- LL.named (LL.incr =<< LL.add n (constDiff q)) "stop_bound"
--     (_, mat)  <- LL.for startBound stopBound mat mergeHandles $ \k mat -> do
--       compute <- runJacGenM compute k
--       val     <- compute n
--       offset  <- lkpLocalSigOffset (lookup scope) idt k
--       setEntry mat offset val
--     pure mat

-- data JacobianMatrixInput = DenseJacobian { denseResultPtr :: Operand }
--                          | SparseJacobian SparseMatrix

-- data JacobianFunArgs =
--   JacobianFunArgs { jacobianInput :: Operand
--                   , jacobianResultMatrix :: JacobianMatrixInput
--                   , jacobianUserData :: Operand
--                   }


-- jacobianSparseFunArgs :: LLT.Type -> [(LLT.Type, LL.ParameterName)]
-- jacobianSparseFunArgs userDataType =
--   [ (LLT.ptr LLT.double  , LL.ParameterName "ys")
--   , (LLT.ptr LLT.i64     , LL.ParameterName "iptr")
--   , (LLT.ptr LLT.i64     , LL.ParameterName "vptr")
--   , (LLT.ptr LLT.double  , LL.ParameterName "res")
--   , (LLT.ptr userDataType, LL.ParameterName "user_data")
--   ]

-- jacobianFunRes :: LLT.Type -> LLT.Type
-- jacobianFunRes _ = LLT.i32

-- packSparseJacobianFunArgs :: [Operand] -> JacobianFunArgs
-- packSparseJacobianFunArgs [ys, iptr, vptr, resptr, userData] = JacobianFunArgs
--   { jacobianInput        = ys
--   , jacobianResultMatrix = SparseJacobian SparseMatrix
--                              { indexValuePtr       = vptr
--                              , indexPtrPtr         = iptr
--                              , sparseDataPtr       = resptr
--                              , sparseWrittenValues = LL.int64 0
--                              }
--   , jacobianUserData     = userData
--   }
-- packSparseJacobianFunArgs args = internalError
--   ("packJacobianFunArgs has been given the wrong number of arguments. \
--                  \Expected 5, got "
--   ++ show (length args)
--   ++ "."
--   )

-- jacobianFun
--   :: (Operand -> Operand -> CGenM (JacTraverserInput SparseMatrix CGenM))
--   -> LL.Name
--   -> LLT.Type
--   -> F.Model
--   -> ModuleGenM Operand
-- jacobianFun makeInput nm userDataType model = codeGenFunction
--   nm
--   (jacobianSparseFunArgs userDataType)
--   (jacobianFunRes userDataType)
--   (makeJacobian . packSparseJacobianFunArgs)
--  where
--   makeJacobian JacobianFunArgs { jacobianInput = ys, jacobianResultMatrix = jac, jacobianUserData = dat }
--     = do
--       let mat = case jac of
--             SparseJacobian jac -> jac -- (jac, sparseMatrixHandle)
--             DenseJacobian{} ->
--               internalError "Dense matrices not supported (yet)"

--           makeJacSt _ = pure JacobianSt { _jacEqnCount    = constDiff 0
--                                         , _jacMatrixState = mat
--                                         }
--       input <- makeInput ys dat
--       _     <- traverseModel makeJacSt (jacobianTraverser input) dat model
--       LL.ret0

-- residualJacobianInput, initJacobianInput
--   :: Operand -> Operand -> CGenM (JacTraverserInput SparseMatrix CGenM)
-- residualJacobianInput ys dat = do
--   input <- makeResidualInput ys dat
--   pure JacTraverserInput { matrixHandle       = sparseMatrixHandle
--                          , lookupFunction = \scope -> residualLookup scope input
--                          , shouldConsiderInit = False
--                          , jacResidualInput   = input
--                          , cutVariable        = const cutRes
--                          }
-- initJacobianInput ys dat = do
--   input <- makeResidualInput ys dat
--   pure JacTraverserInput { matrixHandle       = sparseMatrixHandle
--                          , lookupFunction     = \scope -> initLookup scope input
--                          , shouldConsiderInit = True
--                          , jacResidualInput   = input
--                          , cutVariable        = cutInit input
--                          }

-- residualJacobianFun, initJacobianFun
--   :: LLT.Type -> F.Model -> ModuleGenM Operand
-- residualJacobianFun =
--   jacobianFun residualJacobianInput Name.residualJacobianFun
-- initJacobianFun = jacobianFun initJacobianInput Name.initJacobianFun

-- data CountNonZeroSt = CountNonZeroSt { _nonZeroResEntry :: Operand
--                                      , _nonZeroInitEntry :: Operand
--                                      , _counterEqnCount :: Operand
--                                      }

-- makeLenses ''CountNonZeroSt

-- countNonZeroJacobianTraverser :: ResidualInput -> Traverser CountNonZeroSt
-- countNonZeroJacobianTraverser input = trivialTraverser
--   { _compileSimpleRel = \st rel -> do
--     let us    = st ^. userState
--         scope = st ^. variableScope
--     case rel of
--       ResidualRel lhs rhs -> do
--         let eqnCount = us ^. counterEqnCount
--         eqnHOD <- LL.loadAt (input ^. eqnHODArray) eqnCount
--         let count     = us ^. nonZeroResEntry
--             initCount = us ^. nonZeroInitEntry
--             sig       = relSig lhs rhs
--             info      = linInfo sig

--         resCount  <- withLinInfo cutRes eqnHOD count info
--         initCount <- withLinInfo (cutInit input scope) eqnHOD initCount info
--         eqnCount  <- LL.named (LL.incr eqnCount) "eqn_count"
--         pure
--           (set
--             counterEqnCount
--             eqnCount
--             (set nonZeroResEntry resCount (set nonZeroInitEntry initCount us))
--           )
--       InitRel rel -> do
--         let active    = st ^. newlyEnteredBlock
--             initCount = us ^. nonZeroInitEntry
--             sig       = case rel of
--               F.CausalInit idt val ->
--                 relSig (F.localSig F.treal idt 0) (F.realConst val)
--               F.GenInit lhs rhs -> relSig lhs rhs
--         initCount <- LL.choice
--           active
--           (withLinInfo (cutInit input scope)
--                        (constDiff 0)
--                        initCount
--                        (linInfo sig)
--           )
--           (pure initCount)
--         pure (set nonZeroInitEntry initCount us)
--       _ -> pure us
--   , _mergeStates      = phiMerger
--                           [nonZeroResEntry, nonZeroInitEntry, counterEqnCount]
--   }
--  where
--   withLinInfo cut eqnHOD count linInfo = LL.named
--     (M.foldlWithKey (go cut eqnHOD) (pure count) linInfo)
--     "nonzero_count"
--    where
--     go cut eqnHOD count idt (LinearityInfo linear order) = do
--       count <- count
--       -- Suppose x^(p) is the lowest-order derivative of x appearing in the
--       -- equation, x^(q) is the highest and x^(r) the lowest-order derivative
--       -- being considered a variable.
--       --
--       -- Let's denote by n the number of differentiation for the equation.
--       --
--       -- We are interested in knowing how many slots we must reserve in the
--       -- Jacobian matrix for the various derivatives of x.
--       --
--       -- First, let's consider the following:
--       --   * if x appears linearly in the equation, then in the equation
--       --     differentiated k times, the lowest-order derivative (resp. higher)
--       --     appearing will we x^(p + k) (resp. x^(q + k)). So q - p + 1
--       --     derivatives will appear.
--       --   * if x doesn't appear linearly in the equation, then in the equation
--       --     differentiated k-times, all derivatives of x from x^(0) to x^(q +
--       --     k) may appear, so q + k + 1 derivatives of x will appear.
--       --
--       -- We must remember that derivatives of x less than r are not to be
--       -- considered. This means that if q + k < r, then the equation occupies 0
--       -- slots and otherwise occupies:
--       --    * q + k - max(p + k, r) + 1 if x appears linearly
--       --    * q + k - r + 1 otherwise
--       --
--       -- For all n + 1 equations, the result is thus the sum of all the
--       -- contributions from individual equations:
--       --
--       -- Σ{k = 0; k <= n} if q + k >= r
--       --                      then if linear
--       --                           then q + k - max(p + k, r) + 1
--       --                           else q + k - r + 1
--       --                      else 0
--       --
--       -- We can delete the condition by having the sum start at r - q if r > q, at 0
--       -- otherwise. Let k' = if r > q then r - q else 0.
--       --
--       -- In the linear case, the sum can be further split to avoid the use of
--       -- max. Indeed, we know that p + k >= r when k >= r - p. Let's have k'' =
--       -- if r > p then r - p else k'. This allows us to split the sum into:
--       --
--       --   Σ{k = k'; k < k''} (q + k - r + 1)
--       -- + Σ{k = k''; k <= n} (q - p + 1)
--       --
--       -- The second sum is constant and is (n - k'') * (q - p + 1).

--       -- With C = q - r + 1 we have that the first sum is:
--       -- ((2 * C + (k'' - k')) * (k'' - k' + 1) / 2 In summary, in the
--       -- linear case, we have:
--       --
--       -- let k' = if r > q then r - q else 0
--       --
--       --     k'' = if r > p then r - p else k'
--       --
--       --     C = q - r + 1
--       --
--       -- ((2 * C + (k'' - k')) * (k'' - k' + 1) / 2 + (n - k'') * (q - p + 1)
--       --
--       -- In the non-linear case, posing again C = q - r + 1, we get:
--       --
--       -- let C = q - r + 1
--       --
--       -- ((2 * C + (n - k')) * (n - k' + 1) / 2

--       let p = F.minOrder order
--           q = F.maxOrder order

--           n = eqnHOD
--       r          <- cut idt
--       localCount <- IR.compileSimplIR $ IR.Let (IR.inject n) $ \n ->
--         IR.Let (IR.inject r) $ \r -> IR.Let (IR.inject (constDiff p)) $ \p ->
--           IR.Let (IR.inject (constDiff q)) $ \q ->
--             IR.Let (IR.Select (q IR..< r) (r - q) 0) $ \k' ->
--               IR.Let (q - r + 1) $ \c -> if linear
--                 then IR.Let (IR.Select (p IR..< r) (r - p) k') $ \k'' ->
--                   IR.arithSum (Range (Including k') (Excluding k'')) c 1
--                     + IR.rangeSpan (Range (Including k'') (Including n))
--                     * (q - p + 1)
--                 else IR.arithSum (Range (Including k') (Including n)) c 1
--       LL.add count localCount

-- data CountZeroFunArgs =
--   CountZeroFunArgs { nonZeroResEntryPtr :: Operand
--                    , nonZeroInitEntryPtr :: Operand
--                    , countZeroFunUserData :: Operand
--                    }

-- countNonZeroFunArgs :: LLT.Type -> [(LLT.Type, LL.ParameterName)]
-- countNonZeroFunArgs userDataType =
--   [ (LLT.ptr LLT.i64     , LL.ParameterName "nonzero_res_count")
--   , (LLT.ptr LLT.i64     , LL.ParameterName "nonzero_init_count")
--   , (LLT.ptr userDataType, LL.ParameterName "user_data")
--   ]

-- countNonZeroFunRes :: LLT.Type -> LLT.Type
-- countNonZeroFunRes _ = LLT.i32

-- packCountNonZeroFunArgs :: [Operand] -> CountZeroFunArgs
-- packCountNonZeroFunArgs [resEntry, initEntry, userData] = CountZeroFunArgs
--   { nonZeroResEntryPtr   = resEntry
--   , nonZeroInitEntryPtr  = initEntry
--   , countZeroFunUserData = userData
--   }
-- packCountNonZeroFunArgs args = internalError
--   ("packCountZeroFunArgs has been given the wrong number of arguments. \
--                  \Expected 5, got "
--   ++ show (length args)
--   ++ "."
--   )

-- countNonZeroFun :: LLT.Type -> F.Model -> ModuleGenM Operand
-- countNonZeroFun userDataType model = codeGenFunction
--   Name.countNonZeroFun
--   (countNonZeroFunArgs userDataType)
--   (countNonZeroFunRes userDataType)
--   (makeFun . packCountNonZeroFunArgs)
--  where
--   makeFun CountZeroFunArgs { nonZeroResEntryPtr = nonZeroResEntryPtr, nonZeroInitEntryPtr = nonZeroInitEntryPtr, countZeroFunUserData = dat }
--     = do
--       let mkSt _ = pure CountNonZeroSt { _nonZeroResEntry  = constDiff 0
--                                        , _nonZeroInitEntry = constDiff 0
--                                        , _counterEqnCount  = constDiff 0
--                                        }
--       input <- makeResidualInput
--         (internalError "You weren't supposed to look")
--         dat
--       CountNonZeroSt { _nonZeroInitEntry = nonZeroInitEntry, _nonZeroResEntry = nonZeroResEntry } <-
--         traverseModel mkSt (countNonZeroJacobianTraverser input) dat model
--       LL.store nonZeroResEntryPtr 0 nonZeroResEntry
--       LL.store nonZeroInitEntryPtr 0 nonZeroInitEntry
--       LL.ret0
