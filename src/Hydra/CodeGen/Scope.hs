{-# LANGUAGE DeriveGeneric #-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.CodeGen.Scope where

import           Control.Lens
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Maybe
import           Data.Sequence                  ( Seq((:<|)) )
import           GHC.Generics
import           Hydra.CodeGen.Type
import           Hydra.Error                    ( internalError )
import qualified Hydra.Flatten.AST             as F
import           Hydra.Ident
import qualified Hydra.LLVM.Utils              as LL
import           Hydra.Pretty
import           Hydra.Type              hiding ( TArr
                                                , TExpr
                                                )
import qualified LLVM.AST.Constant             as LLC
import           LLVM.AST.Operand


-- Scopes at the expression level

data GlobalEntry = GlobalEntry
  { globalInstrPtr :: LLC.Constant
  , globalFunPtr   :: LL.LLVMFun InstFun InstArgs
  }

newtype GlobalScope = GlobalScope (Map F.Name (Map (TySubst EVar SVar) GlobalEntry))

data ExprScope = ExprScope
  { localScope  :: Map Ident Operand
  , globalScope :: GlobalScope
  }
  deriving Generic

-- Scopes at the model level

data SigKind = InterfaceSignal Word
             | LocalSignal Word

data SigScopeEntry = SigScopeEntry
  { sigKind :: SigKind
  , sigRepr :: Operand
  }

data ModelScope = ModelScope
  { sigScope   :: Map Ident SigScopeEntry
  , exprScope  :: Map Ident Operand
  , scopeDepth :: Word
  }
  deriving Generic

lookupGlobal :: F.Global -> ExprScope -> GlobalEntry
lookupGlobal global@F.Global { F.globalName = name, F.globalTyInst = tySubst } ExprScope { globalScope = GlobalScope globalScope }
  = case M.lookup name globalScope of
    Nothing -> internalError
      ("Couldn't find global " ++ show (F.pretty global) ++ " in global scope.")
    Just insts -> case M.lookup tySubst insts of
      Nothing -> internalError
        (  "Couldn't find instantiation "
        ++ show (F.pretty tySubst)
        ++ " for global "
        ++ show (F.pretty global)
        ++ "."
        )
      Just entry -> entry

lookupLocalExpr :: Ident -> ExprScope -> Operand
lookupLocalExpr i ExprScope { localScope = scope } = fromMaybe
  (internalError
    (  "Couldn't find local expression "
    ++ show (F.pretty i)
    ++ " in expression scope"
    )
  )
  (M.lookup i scope)

lookupAttr :: Ident -> ModelScope -> Operand
lookupAttr idx ModelScope { exprScope = exprScope } = fromMaybe
  (internalError ("Attribute " ++ show (pretty idx) ++ " is not in scope."))
  (M.lookup idx exprScope)

lookupSigEntry :: Ident -> ModelScope -> SigScopeEntry
lookupSigEntry i ModelScope { sigScope = idtsScope } = fromMaybe
  (internalError ("Signal " ++ show (pretty i) ++ " is not in scope."))
  (M.lookup i idtsScope)

lookupSig :: Ident -> ModelScope -> Operand
lookupSig i scope = sigRepr (lookupSigEntry i scope)

lookupSigKind :: Ident -> ModelScope -> SigKind
lookupSigKind i scope = sigKind (lookupSigEntry i scope)

insertSig :: Ident -> Operand -> ModelScope -> ModelScope
insertSig idt entry scope = over
  #sigScope
  (M.insert idt (SigScopeEntry (LocalSignal (scope ^. #scopeDepth)) entry))
  scope

insertLocals :: Map Ident Operand -> ModelScope -> ModelScope
insertLocals locals scope = over #scopeDepth (+ 1) $ M.foldrWithKey
  (\idt op scope ->
    over #sigScope (M.insert idt (SigScopeEntry (LocalSignal depth) op)) scope
  )
  scope
  locals
  where depth = scopeDepth scope

compareSig :: ModelScope -> Ident -> Ident -> Maybe Ordering
compareSig scope i j = case (lookupSigKind i scope, lookupSigKind j scope) of
  (LocalSignal depthI, LocalSignal depthJ) -> case depthI `compare` depthJ of
    EQ  -> Just (i `compare` j)
    cmp -> Just cmp
  (InterfaceSignal _, LocalSignal _    ) -> Just LT
  (LocalSignal     _, InterfaceSignal _) -> Just GT
  (InterfaceSignal ci, InterfaceSignal cj) | ci == cj  -> Just EQ
                                           | otherwise -> Nothing

-- Given a set of signals and a scope, returns:
--
--  * the set of top signals that cannot be ordered without runtime code
--    generation
--  * a sequence of ordered local signals
inOrder :: Map Ident k -> ModelScope -> (Map Operand k, Seq (Operand, k))
inOrder sigs scope = (tops, foldr (:<|) [] locs)
 where
  (tops, locs) = M.foldrWithKey go ([], []) sigs

  -- Fold the list of signals generating the set of top signals as we go, for
  -- the local signals we generate a map indexed by the depth and the
  -- identifier, so that when we convert it to a sequence the signals are
  -- ordered by depth and then identifier as they should be
  go idt k (tops, locs) = case lvl of
    InterfaceSignal _     -> (M.insert op k tops, locs)
    LocalSignal     depth -> (tops, M.insert (depth, idt) (op, k) locs)
    where SigScopeEntry lvl op = lookupSigEntry idt scope
