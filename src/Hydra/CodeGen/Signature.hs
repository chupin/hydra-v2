{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

{-# LANGUAGE TupleSections #-}

module Hydra.CodeGen.Signature where

import           Control.Lens            hiding ( Empty )
import           Control.Monad.Reader
import           Data.Generics.Labels           ( )
import qualified Data.Map                      as M
import           Data.Map                       ( Map )
import qualified Data.Map.Merge.Lazy           as M
import           Data.Monoid                    ( All(All) )
import           Data.Semigroup                 ( Max(Max) )
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import qualified Data.Set                      as S
import           GHC.Generics
import           Hydra.CodeGen.Method
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.Scope
import           Hydra.CodeGen.Type
import           Hydra.Compiler                 ( internalError )
import qualified Hydra.Flatten.AST             as F
import qualified Hydra.LLVM                    as LL
import           Hydra.OOIR
import qualified LLVM.AST.IntegerPredicate     as LL
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.Type                 as LLT

-- Counts the number of entries contributed by a signal. The number of entries
-- in the signature matrix corresponds to the number of local identifiers
-- appearing (since we know those are distinct) + the number of *distinct*
-- interface signals appearing.
--
-- To count the number of distinct signal interfaces appearing in the model, we
-- proceed like so: given the list of interface signals. Check that if the head
-- appears in the tail of the list, if it does, return 0, if it doesn't return
-- Recurse on the tail and add the result.
countSigEntries :: ModelScope -> Sig -> CGenM Operand
countSigEntries slots sig = do
  iface <- countDistinctIfaceSignals (M.keysSet iface)
  LL.add iface (constDiff (length locals))
 where
  FreeVar fvs _   = freeLocals sig
  (iface, locals) = inOrder fvs slots

  countDistinctIfaceSignals iface = case S.minView iface of
    Nothing      -> pure (constDiff 0)
    Just (i, is) -> do
      iContrib  <- checkDistinct i is
      isContrib <- countDistinctIfaceSignals is
      LL.add iContrib isContrib

  checkDistinct i is = case S.minView is of
    Nothing      -> pure (constDiff 1)
    Just (j, is) -> do
      iEqj <- LL.icmp LL.EQ i j
      LL.ite iEqj (pure (constDiff 0)) (checkDistinct i is)

getMatrixRowField :: Integral i => i -> Operand -> CGenM Operand
getMatrixRowField idx row = do
  ptr <- LL.ibgep row [LL.int32 0, LL.int32 (fromIntegral idx)]
  LL.load ptr 0

getRowSizePtr, getRowIndices, getRowEntries, getDoubleRowEntries, getLinearityInfoEntries
  :: Operand -> CGenM Operand
getRowSizePtr row = LL.ibgep row [LL.int32 0, LL.int32 rowSizeIdx]
getRowIndices = getMatrixRowField rowIndicesIdx
getRowEntries = getMatrixRowField rowEntriesIdx
getDoubleRowEntries = getMatrixRowField doubleRowEntriesIdx
getLinearityInfoEntries = getMatrixRowField linearityInfoEntriesIdx

data RowView = RowView
  { rowIndices, rowEntries, rowDoubleEntries, rowLinearityInfoEntries :: Operand
  }
  deriving Generic

instance LL.Merge RowView

writeSlot :: RowView -> Operand -> Operand -> Operand -> CGenM ()
writeSlot RowView { rowIndices = rowIndices, rowEntries = rowEntries, rowDoubleEntries = rowDoubleEntries, rowLinearityInfoEntries = rowLinearityInfoEntries } index mx linear
  = do
    index <- LL.trunc index LLT.i32
    LL.store rowIndices 0 index
    shortMx <- LL.trunc mx LLT.i8
    LL.store rowEntries 0 shortMx
    doubleMx <- LL.uitofp mx LLT.double
    LL.store rowDoubleEntries 0 doubleMx
    LL.store rowLinearityInfoEntries 0 linear

nextRowView :: RowView -> CGenM RowView
nextRowView RowView { rowIndices = rowIndices, rowEntries = rowEntries, rowDoubleEntries = rowDoubleEntries, rowLinearityInfoEntries = rowLinearityInfoEntries }
  = do
    rowIndices       <- LL.named (LL.nextPtr rowIndices) "row.indices"
    rowEntries       <- LL.named (LL.nextPtr rowEntries) "row.entries"
    rowDoubleEntries <- LL.named (LL.nextPtr rowDoubleEntries)
                                 "row.double.entries"
    rowLinearityInfoEntries <- LL.named (LL.nextPtr rowLinearityInfoEntries)
                                        "row.linearity.info.entries"
    pure RowView { rowIndices              = rowIndices
                 , rowEntries              = rowEntries
                 , rowDoubleEntries        = rowDoubleEntries
                 , rowLinearityInfoEntries = rowLinearityInfoEntries
                 }

-- In general, we know that every local signal can be easily ordered, even in
-- the presence of switches. Indeed, we know that any signal that is declared in
-- a let will at a further index than the signals that already exist. Hence the
-- structure of the SignalSlots type.
--
-- Although the same can be said for output signals (they appear at a lower slot
-- than all locals), but sorting between them is not trivial. Consider the
-- following models:
--
-- let foo = model x, y with
--   x + der y = 0
-- end
--
-- let bar = model x, y with
--   y, x <> foo
-- end
--
-- and suppose we simulate bar. In the simulation, x is variable 0 and y is
-- variable 1, so when we write the signature for the equation in foo, it should
-- be {0, 0}, {1, 1}, but if when we generate the signature function for foo, we
-- assume the index of x is less than the index of y, then we will generate {1,
-- 1}, {0, 0}, which is problematic because the runtime expects the pairs to be
-- sorted by ascending index. There is an even more annoying case, which is that
-- two inputs may actually represent the same signal, for instance in:
--
-- let baz = model x with
--   x, x <> foo
-- end
--
-- then only one slot must be written with the max of the result. In summary,
-- for the example above, the kind of code that must be emitted looks like this:
--
-- if index of x < index of y
--    write {index of x, 0}
--    write {index of y, 1}
-- else if index x > index of y
--    write {index of y, 1}
--    write {index of x, 0}
-- else if index of x == index of y
--    write {index of x, max(0, 1)}
--
-- For ease of implementation, the code that is emitted bubble sorts the inputs.
-- It doesn't eliminate duplicates (that'd be hard I think), duplicates are
-- handled in the next phase.
-- swapPass generates a single pass of bubble sort. It considers the first two
-- entries and swaps them if the first is greater than the second. It then goes
-- on to recurse on the tail. At the end, the last operand in the sequence is
-- guaranteed to be the smallest.
swapPass :: LL.Merge k => Seq (Operand, k) -> CGenM (Seq (Operand, k))
swapPass (n1@(ix1, _) :<| n2@(ix2, _) :<| ns) = do
  cmp      <- LL.icmp LL.ULT ix1 ix2
  (na, nb) <- LL.ite cmp (pure (n1, n2)) (pure (n2, n1))
  ns       <- swapPass (nb :<| ns)
  pure (na :<| ns)
swapPass ops = pure ops

-- Generate code that bubble sorts the sequence of values
bubbleSort :: LL.Merge k => Seq (Operand, k) -> CGenM (Seq (Operand, k))
bubbleSort qs = do
  qs <- swapPass qs
  case qs of
    ns :|> last -> do
      ns <- bubbleSort ns
      pure (ns :|> last)
    Empty -> pure Empty

emitOutputSignature
  :: Map Operand (Max Word, All) -> RowView -> CGenM (Operand, RowView)
emitOutputSignature ops row = do
  -- Start by sorting the interface identifiers
  ops <- bubbleSort $ Seq.fromList $ M.toList $ fmap
    (\(Max n, All b) -> (LL.int64 (fromIntegral n), LL.bool b))
    ops
  let
    go written (Just (prev, prevRow)) current ((index, (n, linear)) :<| ops) =
      do
        cmp                         <- LL.icmp LL.EQ prev index
        (written, prevRow, nextRow) <- LL.ite
          cmp
          (eqPrev written prev prevRow current n linear)
          (neqPrev written current index n linear)
        go written (Just (index, prevRow)) nextRow ops
    go written Nothing current ((index, (n, linear)) :<| ops) = do
      (written, current, next) <- neqPrev written current index n linear
      go written (Just (index, current)) next ops
    go written _ current Empty = pure (written, current)

    neqPrev written current index n linear = do
        -- The current slot
      writeSlot current index n linear
      nextRow <- nextRowView current
      written <- LL.incr written
      pure (written, current, nextRow)

    eqPrev written prevIndex prevRow@RowView { rowEntries = rowEntries } current n linear
      = do
      -- In this case, the current index is the same as the previous one. As a
      -- consequence, we must dereference what has just been written in
      -- rowEntries and rowDoubleEntries and write instead the max of the two
        prevRowEntry <- LL.load rowEntries 0
        prevRowEntry <- LL.zext prevRowEntry LLT.i64
        rowEntry     <- LL.max prevRowEntry n
        writeSlot prevRow prevIndex rowEntry linear
        -- The previous slot is still prevRow and the next slot to write into is
        -- still current
        pure (written, prevRow, current)

  go (LL.int32 0) Nothing row ops

fillSigMethod :: Rel -> Method () FillSigMatState
fillSigMethod (Eqn F.AtInit _  ) = mempty
fillSigMethod (Eqn F.Always sig) = Method $ do
  scope                   <- view #methodScope
  row                     <- use (#userState . #nextRow)
  rowIndices              <- use (#userState . #nextEntryIndex)
  rowEntries              <- use (#userState . #nextEntry)
  rowDoubleEntries        <- use (#userState . #nextDoubleEntry)
  rowLinearityInfoEntries <- use (#userState . #nextLinearityInfoEntry)

  entryIndexPtrPtr        <- LL.ibgep row [LL.int32 0, LL.int32 rowIndicesIdx]
  entryPtrPtr             <- LL.ibgep row [LL.int32 0, LL.int32 rowEntriesIdx]
  doubleEntryPtrPtr <- LL.ibgep row [LL.int32 0, LL.int32 doubleRowEntriesIdx]
  rowLinearityEntryPtrPtr <- LL.ibgep
    row
    [LL.int32 0, LL.int32 linearityInfoEntriesIdx]
  LL.store entryIndexPtrPtr 0 rowIndices
  LL.store entryPtrPtr 0 rowEntries
  LL.store doubleEntryPtrPtr 0 rowDoubleEntries
  LL.store rowLinearityEntryPtrPtr 0 rowLinearityInfoEntries

  -- We store nextEntryIndexPtr, nextEntryPtr and nextDoubleEntryPtr into the
  -- nextRow. We then pass them all to emitOutputSignature


  let
    FreeVar fvs _ = freeLocals sig
    lvs           = linearSignals sig
    catastroph =
      M.mapMissing
        $ internalError
            "A catastrophic mistake must have been made in freeLocals or linearSignals"
    (outs, locals) = inOrder
      (M.merge catastroph catastroph (M.zipWithMatched (const (,))) fvs lvs)
      scope

  rowSizePtr               <- lift $ LL.named (getRowSizePtr row) "row_size_ptr"
    -- rowIndices <- LL.named (getRowIndices row) (LL.getOpName row <> ".indices")
    -- rowEntries <- LL.named (getRowEntries row) (LL.getOpName row <> ".entries")
    -- rowDoubleEntries <- LL.named (getDoubleRowEntries row)
    --                              (LL.getOpName row <> ".real_entries")

  (written, localsRowView) <- lift $ emitOutputSignature
    outs
    RowView { rowEntries              = rowEntries
            , rowIndices              = rowIndices
            , rowDoubleEntries        = rowDoubleEntries
            , rowLinearityInfoEntries = rowLinearityInfoEntries
            }

  (size, _) <- lift $ foldM
    (\(written, rowView) (index, (Max n, All linear)) -> do
      writeSlot rowView index (LL.int64 (fromIntegral n)) (LL.bool linear)
      written     <- LL.incr written
      nextRowView <- nextRowView rowView
      pure (written, nextRowView)
    )
    (written, localsRowView)
    locals

  LL.store rowSizePtr 0 size
  assign (#userState . #nextRow) =<< LL.named (LL.nextPtr row) "row"
  -- Bump all the other pointers by size
  assign (#userState . #nextEntryIndex) =<< LL.ibgep rowIndices [size]
  assign (#userState . #nextEntry) =<< LL.ibgep rowEntries [size]
  assign (#userState . #nextDoubleEntry) =<< LL.ibgep rowDoubleEntries [size]
fillSigMethod (Let idts rels) = withLocals idts (foldMap fillSigMethod rels)
fillSigMethod (ModelCall idts model) =
  withCall (signatureMethodRepr objectReprIndices) idts model
fillSigMethod (Switch _ branches) =
  withSwitch (withSwitchBlock fillSigMethod) branches
