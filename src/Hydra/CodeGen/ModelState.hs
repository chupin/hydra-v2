{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}

{-# LANGUAGE PartialTypeSignatures #-}
module Hydra.CodeGen.ModelState where

-- This module is in charge of handling the construction and manipulation of the
-- model states. In particular at initialisation and after a mode change.
--
-- The representation of state is explained in Hydra.CodeGen.Type.
--
-- Let's consider the case of initialisation first. This case is simple: walk
-- along the AST, allocating everything as needs be (using malloc).
--
-- The case of a mode switch is slightly different but a lot can be reused from
-- the initialisation case. The difficulty lies in the switch. When a switch is
-- triggered, the mode in which the switch switches into must be initialised. If
-- the mode is the same as the one that was just left, then the memory can be
-- reused, but if it's not, the previously allocated memory must be freed and
-- fresh memory allocated.

import           Control.Lens            hiding ( Empty )
import           Control.Monad
import           Control.Monad.RWS.Strict
import           Control.Monad.Trans            ( lift )
import qualified Data.ByteString.Short         as BS
import           Data.Foldable
import qualified Data.Map                      as M
import           Data.Map                       ( Map )
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import qualified Data.Set                      as S
import qualified Data.Text                     as T
import           Hydra.CodeGen.Method
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.Residual         ( compileSig )
import           Hydra.CodeGen.Scope
import           Hydra.CodeGen.Signature        ( countSigEntries )
import           Hydra.CodeGen.SimplExpr
import           Hydra.CodeGen.Type
import           Hydra.Error                    ( internalError )
import           Hydra.Ident                    ( source )
import qualified Hydra.LLVM.Utils              as LL
import           Hydra.OOIR
import           Hydra.Pretty                   ( Pretty(pretty) )
import qualified LLVM.AST.IntegerPredicate     as LL
import qualified LLVM.AST.Name                 as LL
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.AST.Typed                as LLT
import           LLVM.Pretty                    ( )

mkExprScope :: GlobalScope -> ModelScope -> ExprScope
mkExprScope globalScope ModelScope { exprScope = localScope } =
  ExprScope { globalScope = globalScope, localScope = localScope }

eqnStructuralInfo :: Bool -> ModelScope -> Holds -> Sig -> CGenM StructuralInfo
eqnStructuralInfo countInit _ AtInit _ = pure StructuralInfo
  { initRelationsCount = if countInit then constDiff 1 else constDiff 0
  , relationsCount     = constDiff 0
  , rootCount          = constDiff 0
  , entriesCount       = constDiff 0
  , variableCount      = constDiff 0
  }
eqnStructuralInfo _ scope Always sig = do
  entriesCount <- countSigEntries scope sig
  pure StructuralInfo { relationsCount     = constDiff 1
                      , initRelationsCount = constDiff 0
                      , rootCount          = constDiff 0
                      , entriesCount       = entriesCount
                      , variableCount      = constDiff 0
                      }

initializeBlock
  :: (ExprScope -> Expr -> CGenM Operand) -> Block -> Constr Operand
initializeBlock mkExpr block@Block { blockAttributes = attrs, blockRelations = rels }
  = Constr $ do

    -- When entering a block, allocate some memory for its state
    ty                      <- lift $ liftModuleGenM $ blockStateStruct block

    rawPtr                  <- lift (malloc =<< LL.sizeOf ty)
    ptr                     <- LL.bitcast rawPtr (LLT.ptr ty)

    -- Split the state into the state for the attributes and the state of the
    -- relations
    (blockState, relsState) <- lift $ unConsState ptr

    -- Execute the expressions making the attributes
    globalScope             <- view #globalScope
    modelScope              <- view #modelScope

    modelScope              <- lift $ foldM
      (\scope (idx, (idt, expr)) -> do
        expr     <- mkExpr (mkExprScope globalScope scope) expr
        fieldPtr <- LL.ibgep blockState
                             [LL.int32 0, LL.int32 (fromIntegral idx)]
        LL.store fieldPtr 0 expr
        pure (over #exprScope (M.insert idt expr) scope)
      )
      modelScope
      (zip [0 ..] (M.toList attrs))

    locally #modelScope (const modelScope) $ runConstr $ initializeRels
      mkExpr
      relsState
      rels

    -- Finally, set the state back to the rawPtr
    pure rawPtr

initializeRels
  :: (ExprScope -> Expr -> CGenM Operand) -> Operand -> Seq Rel -> Constr ()
initializeRels mkExpr relsState rels = Constr $ foldM_
  (\relsState rel -> do
    ops <- runConstr (initializeRel mkExpr rel)
    foldM
      (\state relState -> do
        (relStatePtr, nextState) <- lift $ unConsState state
        -- relStatePtr <- LL.ibgep relsState [LL.int32 0, LL.int32 idx]
        LL.store relStatePtr 0 relState
        pure nextState
      )
      relsState
      ops
  )
  relsState
  rels

initializeRel
  :: (ExprScope -> Expr -> CGenM Operand) -> Rel -> Constr (Seq Operand)
initializeRel _ (Eqn holds sig) = Constr $ do
  scope      <- view #modelScope
  structInfo <- lift $ eqnStructuralInfo True scope holds sig
  assign #structuralInfo =<< lift . addStructuralInfo structInfo =<< use
    #structuralInfo
  pure []
initializeRel mkExpr (Let idts rels) = Constr $ do
  -- When encountering local identifiers, we just have to set their reinit to 0
  -- and keep going.
  nextVar                            <- use #nextVar
  varCount                           <- use (#structuralInfo . #variableCount)
  (extendedScope, nextVar, varCount) <- foldM
    (\(extendedScope, this, varCount) idt -> do
      next     <- LL.named (LL.incr this) "next.var.ptr"
      varCount <- LL.named (LL.incr varCount) "variable.count"
      pure (M.insert idt this extendedScope, next, varCount)
    )
    ([], nextVar, varCount)
    idts
  #nextVar .= nextVar
  #structuralInfo . #variableCount .= varCount
  locally #modelScope (insertLocals extendedScope)
    $   join
    <$> traverse (runConstr . initializeRel mkExpr) rels
initializeRel _ (ModelCall args index) = Constr $ do
  scope                 <- view #modelScope
  nextVarStorage        <- view #nextVarStorage
  structuralInfoStorage <- view #structuralInfoStorage
  modelStateStorage     <- view #modelStateStorage

  let model = lookupAttr index scope
  -- A model is a record of a struct of functions and a pointer to an
  -- environement. We get the environment and then the function we are
  -- interested in.
  modelEnvPtrPtr <- LL.named (LL.ibgep model [LL.int32 0, LL.int32 1])
                             "model.env.ptr"
  modelEnvPtr     <- LL.named (LL.load modelEnvPtrPtr 0) "model.env"
  funRecordPtrPtr <- LL.named (LL.ibgep model [LL.int32 0, LL.int32 0])
                              "fun.record.ptr.ptr"
  funRecordPtr <- LL.named (LL.load funRecordPtrPtr 0) "fun.record.ptr"
  funPtr       <- LL.named
    (LL.ibgep funRecordPtr
              [LL.int32 0, LL.int32 (constructorFunRepr objectReprIndices)]
    )
    "method.fun.ptr.ptr"
  fun <- LL.named (LL.load funPtr 0) "method.fun.ptr"
  LL.storeIn nextVarStorage =<< use #nextVar
  LL.storeIn structuralInfoStorage =<< use #structuralInfo
  res <- LL.callLLVMFun
    (LL.LLVMFun fun :: LL.LLVMFun ConstrFun ConstrArg)
    ConstrArg { nextVarStorage         = nextVarStorage
              , structuralInfoStorage  = structuralInfoStorage
              , modelStateStorage      = modelStateStorage
              , modelEnvPtr            = modelEnvPtr
              , interfaceSignalIndices = fmap (`lookupSig` scope) args
              }
  LL.assertion (LL.intIs 0 res) res
  assign #nextVar =<< LL.named (LL.loadOut nextVarStorage) "next.var"
  assign #structuralInfo
    =<< LL.named (LL.loadOut structuralInfoStorage) "structural.info"

  thisState <- LL.loadOut modelStateStorage
  pure [thisState]
initializeRel mkExpr (Switch initMode@(Mode initLabel _) branches) =
  Constr $ do
    globalScope <- view #globalScope
    modelScope  <- view #modelScope
    initMode    <- lift
      $ traverse (mkExpr (mkExprScope globalScope modelScope)) initMode
    let branchArgsRequiredSize Branch { branchArguments = args } = do
          tys <- liftModuleGenM $ traverse (toLLTyp . typeOf) args
          LL.sizeOf (LL.structType (toList tys))
    argsSize <- foldM LL.max (constDiff 0)
      =<< lift (traverse branchArgsRequiredSize branches)
    rawArgsPtr                <- lift $ malloc argsSize
    (switchIndex, blockState) <- runConstr
      (initializeBranch mkExpr initMode rawArgsPtr (branches M.! initLabel))
    switchState <- LL.makeStruct
      [switchIndex, LL.true, switchIndex, LL.int64 0, rawArgsPtr, blockState]
    pure [switchState]

initializeBranch
  :: (ExprScope -> Expr -> CGenM Operand)
  -> Mode Operand
  -> Operand
  -> Branch
  -> Constr (Operand, Operand)
initializeBranch mkExpr (Mode Label { labelIndex = index } args) rawArgsPtr Branch { branchConditions = conds, branchArguments = argsNames, branchBlock = block }
  = Constr $ do

    let argsTy = LL.structType (fmap LLT.typeOf (toList args))

    argsArrayPtr <- LL.bitcast rawArgsPtr (LLT.ptr argsTy)

    extScope     <- foldM
      (\extScope (idx, (Arg idt _, val)) -> do
        valPtr <- LL.named (LL.ibgep argsArrayPtr [LL.int32 0, LL.int32 idx])
                           (LL.fromIdent idt <> ".ptr")
        LL.store valPtr 0 val
        lift $ printf "Stored %f at %x\n" [val, valPtr]
        pure (M.insert idt val . extScope)
      )
      id
      (zip [0 ..] (toList (Seq.zip argsNames args)))

    blockState <- locally (#modelScope . #exprScope) extScope
      $ runConstr (initializeBlock mkExpr block)

    assign (#structuralInfo . #rootCount)
      =<< LL.add (constDiff (length conds))
      =<< use (#structuralInfo . #rootCount)
    pure (LL.int64 (fromIntegral index), blockState)
    -- LL.makeStruct
    --   [LL.int64 (fromIntegral index), LL.true, rawArgsPtr, blockState]

initializeObj
  :: (ExprScope -> Expr -> CGenM Operand)
  -> GlobalScope
  -> LL.Name
  -> LLT.Type
  -> ModelObj
  -> ModuleGenM (LL.LLVMFun ConstrFun ConstrArg)
initializeObj mkExpr globalScope name envPtrTy Obj { modelCaptures = captures, modelBlock = block, modelOutput = output }
  =
-- withModelObj globalScope method name modelMethod envPtrTy Obj { modelCaptures = captures, modelBlock = block, modelOutput = output }
    fst <$> codeGenFunction name (ConstrFun output) go
 where
  go ConstrArg { modelEnvPtr = modelEnvPtr, nextVarStorage = nextVarStorage, structuralInfoStorage = structuralInfoStorage, modelStateStorage = modelStateStorage, interfaceSignalIndices = ifaceSignals }
    = do

      -- We start by loading the state and stuff out of their pointers
      nextVar        <- LL.named (LL.loadOut nextVarStorage) "next.var"
      structuralInfo <- LL.named (LL.loadOut structuralInfoStorage)
                                 "structural.info"

      -- Then, we have to make the scope. Initially, the scope only contains the
      -- captured identifiers, which we need to load out of the environment; and
      -- then the output signals
      envPtr    <- LL.bitcast modelEnvPtr envPtrTy
      exprScope <- foldM
        (\scope (idx, Capture { captureRepl = i }) -> do
          valPtr <- LL.ibgep envPtr [LL.int32 0, LL.int32 idx]
          val    <- LL.named (LL.load valPtr 0) (LL.fromIdent i)
          pure (M.insert i val scope)
        )
        []
        (zip [0 ..] (toList captures))

      let sigScope = Seq.foldrWithIndex
            (\idx (out, op) scope -> M.insert
              out
              SigScopeEntry { sigKind = InterfaceSignal (fromIntegral idx)
                            , sigRepr = op
                            }
              scope
            )
            []
            (Seq.zip output ifaceSignals)

          modelScope = ModelScope { sigScope   = sigScope
                                  , exprScope  = exprScope
                                  , scopeDepth = 0
                                  }

      (blockState, ConstrState { nextVar = nextVar, structuralInfo = structuralInfo }, ()) <-
        runRWST
          (runConstr (initializeBlock mkExpr block))
          ConstrInput { globalScope           = globalScope
                      , modelScope            = modelScope
                      , nextVarStorage        = nextVarStorage
                      , structuralInfoStorage = structuralInfoStorage
                      , modelStateStorage     = modelStateStorage
                      }
          ConstrState { nextVar = nextVar, structuralInfo = structuralInfo }
      LL.storeIn nextVarStorage nextVar
      LL.storeIn modelStateStorage blockState
      LL.storeIn structuralInfoStorage structuralInfo
      pure (Just (LL.int32 0), ())

-- Given a set of branches, filter the ones that have no predecessors
filterNoPred :: Map Label Branch -> Map Label Branch
filterNoPred branches = M.restrictKeys branches successors
 where
  successors =
    foldMap (foldMap (S.singleton . successorLabel) . branchConditions) branches

eventHandlerMethod
  :: (ExprScope -> Expr -> CGenM Operand)
  -> Rel
  -> Method EventHandlerInput EventHandlerState
eventHandlerMethod _ (Eqn holds sig) = Method $ do
  scope          <- view #methodScope
  structuralInfo <- lift $ eqnStructuralInfo False scope holds sig
  assign (#userState . #structuralInfo)
    =<< lift
    .   addStructuralInfo structuralInfo
    =<< use (#userState . #structuralInfo)
eventHandlerMethod mkExpr (Let idts rels) = Method $ do
  assign (#userState . #structuralInfo . #variableCount)
    =<< LL.add (constDiff (length idts))
    =<< use (#userState . #structuralInfo . #variableCount)
  runMethod $ withLocals idts $ foldMap (eventHandlerMethod mkExpr) rels
eventHandlerMethod _ (ModelCall args model) =
  withCall (eventHandlerMethodRepr objectReprIndices) args model
eventHandlerMethod mkExpr (Switch _ branches) = Method $ do
  state <- use #modelState
  -- (switchState, _) <- lift $ unConsState state
  -- switchState      <- lift $ getSwitchState switchState
  runMethod (withSwitch handleBranch branches)
  -- So we've encoutered a switch, here is how we proceed. We start by handling
  -- any switch inside the current mode. Doing so is a little bit inefficient
  -- (if this switch switches, all the work we've done is useless), but the
  -- advantage of doing it this way is that it considerably simplifies state
  -- management. In particular, if we were to handle our switch before the inner
  -- switch, we would have to count how many roots the inner-switch monitored.
  -- It's not super complicated to do so but it leads to additional confusing
  -- code. My hope is that LLVM can optimise things anyway (it is quite a small
  -- hope). Anyway, I leave it as future work to improve this state of things.
  --
  -- There is a (very) small difficulty that comes with doing it this way
  -- however: if the switch ends up being triggered. We have to restore nextVar
  -- and structuralInfo to what they were before the switch. We also need to set
  -- nextVar to what it *should have been* after reinitialising the branch,
  -- otherwise things will get confused. But that's easy, we just have to record
  -- what nextVar was when we started handling the conditions.
  --
  -- Once we've handled any change in the current mode, we can handle changes to
  -- this switch.
 where
  handleBranch switchState ourLabel Branch { branchArguments = args, branchBlock = block, branchConditions = conds }
    = Method $ do
      -- oldNextVar/oldStructInfo are the nextVar and structInfo from *before*
      -- the code inside the branch is ran. It is needed if a condition is
      -- triggered, as we need to initialise the branch using these values of
      -- and structInfo
      oldNextVar    <- use #nextVar
      oldStructInfo <- use (#userState . #structuralInfo)
      runMethod $ withBranchArgs switchState args $ Method $ do
        runMethod (withBlock (eventHandlerMethod mkExpr) block)
        -- noChangeNextVar is the nextVar if no condition is triggered. It is
        -- what nextVar should always be set to *after* we've handled the
        -- conditions, since that's what the switches and relations that will
        -- occur after this one except.
        currentNextVar <- use #nextVar
        input          <- ask
        state          <- get
        state          <- lift $ handleConds switchState
                                             oldNextVar
                                             currentNextVar
                                             oldStructInfo
                                             ourLabel
                                             input
                                             state
                                             conds
        put state

  -- handleConds handles all the conditions in the branch. It is in charge of
  -- initializing the switch if a condition is triggered. Note that handleConds
  -- returns is a CGenM, it is passed a userState and returns a userState but
  -- does not modify directly the underlying state.
  --
  -- It takes a few arguments:
  --    * the state of the switch as a SwitchState type,
  --    * the old nextVar value, that is the value nextVar had just before the
  --      switch. When handleConds is entered, the value stored in state is the
  --      value it has *after* the block has been executed. But if our switch is
  --      going to switch, we have to initialise the new branch using the old
  --      value of nextVar
  --    * the current nextVar value, the value of nextVar after the block has
  --      been handled. If we switch, we must set switchPreviousModeNewVarPtr to
  --      currentNextVar - oldNextVar.
  --    * the label of our branch. This is useful if the current condition
  --      switches, to check whether or not this amounts to a structural change.
  --      It is frequent that switches occur that do not cause a structural
  --      change (e.g. in the classic falling ball model). If no structural
  --      change occured, we can skip the index reduction step, and simply reuse
  --      the informations from the previous mode.
  --    * the final root. It corresponds to nextRoot + length conds. If a switch
  --      occurs before the last root, we have to remember to set the nextRoot
  --      pointer to it, otherwise if a switch is placed after this one, it will
  --      not scrutinize the correct root pointer
  --    * the input and state of the method. They are passed explicitly as to
  --      avoid state manipulation issues.
  handleConds SwitchState { switchActivePtr = activePtr } _ _ _ _ _ state Empty
    = do
      LL.store activePtr 0 LL.false
      pure state
  handleConds switchState oldNextVar currentNextVar oldStructuralInfo ourLabel input state ((_ :-> targetMode) :<| conds)
    = do
      let nextRoot = state ^. #userState . #nextRoot
      rootState      <- LL.named (LL.load nextRoot 0) "this.root.state"
      nextRoot       <- LL.named (LL.nextPtr nextRoot) "next.root"
      isZero         <- LL.named (LL.intIs 0 rootState) "is.triggered"
      nextSwitchRoot <- LL.named
        (LL.ibgep nextRoot [constDiff (length conds)])
        "final.root"
      -- If the root has not been triggered, handle the rest of the conditions.
      -- Otherwise, initialize the switch to the new mode and return
      LL.ite
        isZero
        (handleConds switchState
                     oldNextVar
                     currentNextVar
                     oldStructuralInfo
                     ourLabel
                     input
                     (set (#userState . #nextRoot) nextRoot state)
                     conds
        )
        (handleTriggeredCond switchState
                             oldNextVar
                             currentNextVar
                             oldStructuralInfo
                             nextSwitchRoot
                             ourLabel
                             input
                             state
                             targetMode
        )

  handleTriggeredCond
    :: SwitchState
    -> Operand -- oldNextVar: what nextVar should be set to when the condition is triggered
    -> Operand -- currentNextVar: what nextVar is after the sub-block was handled. It is used to set previousModeNexVarPtr but also must be what nextVar is after the condition has been handled
    -> StructuralInfo -- oldStructuralInfo: what structuralInfo should be set to when the condition is triggered
    -> Operand -- nextSwitchRoot: the address of the root of the next switch block
    -> Label -- ourLabel: the label of the branch currently being handled
    -> MethodInput EventHandlerInput EventHandlerState
    -> MethodState EventHandlerState
    -> Mode Sig
    -> CGenM (MethodState EventHandlerState)
  handleTriggeredCond SwitchState { switchIndexPtr = indexPtr, switchActivePtr = activePtr, switchPreviousIndexPtr = previousIndexPtr, switchPreviousModeNewVarPtr = previousModeNewVarPtr, switchArgsPtrPtr = argsPtrPtr, switchSubBlockStatePtr = subBlockStatePtr } oldNextVar currentNextVar oldStructuralInfo nextSwitchRoot ourLabel@Label { labelIndex = previousIndex } input state targetMode@(Mode targetLabel _)
    = do
      let inputSignals = input ^. #userInput . #inputSignals
          scope        = input ^. #methodScope
          setExpr sig = do
            val <- compileSig scope inputSignals sig
            printf (show (pretty sig) ++ " => %f\n") [val]
            pure val
      targetMode <- traverse setExpr targetMode
      argsPtr    <- LL.named (LL.load argsPtrPtr 0) "args.ptr"
      let
        getStructuralInfoStorage (LL.StoreOf EventHandlerState { structuralInfo = structuralInfoStorage })
          = LL.StoreOf structuralInfoStorage
      ((index, subBlockState), ConstrState { nextVar = _, structuralInfo = structuralInfo }, ()) <-
        runRWST
          (runConstr
            (initializeBranch mkExpr
                              targetMode
                              argsPtr
                              (branches M.! targetLabel)
            )
          )
          ConstrInput
            { globalScope           = input ^. #globalScope
            , modelScope            = scope
            , nextVarStorage        = input ^. #nextVarStorage
            , structuralInfoStorage = getStructuralInfoStorage
                                        (input ^. #userStateStorage)
            , modelStateStorage     = input ^. #userInput . #modelStateStorage
            }
          -- We run the constructor using oldNextVar (which is the value of
          -- nextVar just before the switch), the actual nextVar at this point
          -- will have been polluted by the fact that we handled the inner
          -- relations of the branch before this one.
          ConstrState { nextVar        = oldNextVar
                      , structuralInfo = oldStructuralInfo
                      }
      LL.store indexPtr 0 index
      LL.store activePtr 0 LL.true
      LL.store subBlockStatePtr 0 subBlockState
      LL.store previousIndexPtr 0 (constDiff previousIndex)
      previousModeNewVar <- LL.named (LL.sub currentNextVar oldNextVar)
                                     "previous.mode.new.var"
      LL.store previousModeNewVarPtr 0 previousModeNewVar
      -- If we just initialized a branch, there's a few things we need to do
      -- to the state:
      --
      --   * we just skipped a bunch of roots, so we need to adjust the
      --     nextRoot pointer.
      --   * we have to report that a change has happened. If the new mode is
      --     different from the mode the switch was in (i.e. ourLabel /=
      --     targetLabel), we also have to report a structural change
      --     occured
      let structuralChange
            | ourLabel /= targetLabel = LL.true
            | otherwise               = state ^. #userState . #structuralChange
      pure MethodState
        { nextVar    = currentNextVar
        , modelState = LL.nullPtr LLT.i8
        , userState  = EventHandlerState { nextRoot         = nextSwitchRoot
                                         , anyChange        = LL.true
                                         , structuralChange = structuralChange
                                         , structuralInfo   = structuralInfo
                                         }
        }

setSignalInfoMethod :: Rel -> Method SetSignalInfoInput SetSignalInfoState
setSignalInfoMethod Eqn{}           = mempty
setSignalInfoMethod (Let idts rels) = withLocals idts $ Method $ do
  let setVarInfo idt = do
        scope <- view #methodScope
        let index = lookupSig idt scope
        blockActivated <- view #firstBlockActivation
        reinits        <- view (#userInput . #reinitSignals)
        LL.storeAt reinits index blockActivated
        varNames <- view (#userInput . #signalNames)
        str      <- lift $ liftModuleGenM $ do
          idx <- freshGlobal
          LL.globalStringPtr
            (T.unpack (source idt))
            (LL.Name (LL.fromIdent idt <> "_name_" <> LL.fromShow idx))
        LL.storeAt varNames index (LL.constantOp str)
  traverse_ setVarInfo idts
  runMethod $ foldMap setSignalInfoMethod rels
setSignalInfoMethod (ModelCall args model) =
  withCall (setSignalInfoMethodRepr objectReprIndices) args model
setSignalInfoMethod (Switch _ branches) = withSwitch go branches
 where
  go
    :: SwitchState
    -> Label
    -> Branch
    -> Method SetSignalInfoInput SetSignalInfoState
  go state@SwitchState { switchActivePtr = activePtr } _ Branch { branchReinits = reinits, branchArguments = args, branchBlock = block }
    = Method $ do
      active       <- LL.load activePtr 0
      reinitsArray <- view (#userInput . #reinitSignals)
      let setReinit idt = do
            scope <- view #methodScope
            let index = lookupSig idt scope
            LL.storeAt reinitsArray index active
      traverse_ setReinit reinits
      locally #firstBlockActivation (const active)
        $ runMethod
        $ withBranchArgs state args
        $ withBlock setSignalInfoMethod block

-- This method copies the old input vector to the new input vector and sets all
-- the guesses right
copyInputsMethod :: Rel -> Method CopyInputsInput CopyInputsState
copyInputsMethod Eqn{}           = mempty
copyInputsMethod (Let idts rels) = withLocals idts $ Method $ do
  -- If this let-block existed in the previous mode, then we must copy the
  -- values of the signals it introduces into the new input vector, as well as
  -- update oldNextVar when doing so. If it doesn't, we have to initialise the
  -- input vector with a default value, but we don't have to update oldNextVar
  existed          <- view (#userInput . #blockExisted)
  oldHodArray      <- view (#userInput . #oldHodArray)
  currHodArray     <- view (#userInput . #currHodArray)
  currentScope     <- view #methodScope
  currInputSignals <- view (#userInput . #currInputSignals)
  oldInputSignals  <- view (#userInput . #oldInputSignals)
  oldNextVar       <- use (#userState . #oldNextVar)
  let
    setCurrentInput existed oldNextVar = do
      let
        go oldIndex idt = do
          let currIndex = lookupSig idt currentScope
          currHod <- LL.named (LL.loadFrom currHodArray currIndex)
                              ("curr." <> LL.fromIdent idt <> ".hod")
          idtPtr <- LL.named (LL.loadFrom currInputSignals currIndex)
                             ("curr." <> LL.fromIdent idt <> ".value.ptr")

          _ <- LL.for Nothing (constDiff 0) currHod () $ \n () -> do
            -- Set the entry for idt^(n) either to the old value of idt^(n)
            -- if it existed (that is, if n <= oldHod) or to 0
            val <- LL.ite
              existed
              (do
                oldHod <- LL.named (LL.loadFrom oldHodArray oldIndex)
                                   ("old." <> LL.fromIdent idt <> ".hod")
                useOldValue <- LL.named
                  (LL.icmp LL.ULE n oldHod)
                  ("old." <> LL.fromIdent idt <> ".value")
                let fetchOldValue = do
                      oldIdtPtr <- LL.loadFrom oldInputSignals oldIndex
                      LL.loadFrom oldIdtPtr n
                LL.ite useOldValue fetchOldValue (pure (LL.double 0))
              )
              (pure (LL.double 0))
            LL.storeAt idtPtr n val
          LL.named (LL.ite existed (LL.incr oldIndex) (pure oldIndex))
                   "old.next.var"
      foldM go oldNextVar idts

  assign (#userState . #oldNextVar)
    =<< lift (setCurrentInput existed oldNextVar)
  runMethod $ foldMap copyInputsMethod rels
copyInputsMethod (ModelCall args model) =
  withCall (copyInputsMethodRepr objectReprIndices) args model
copyInputsMethod (Switch _ branches) = withSwitch handleBranch branches
  -- When we encounter a switch, we must determine if the subblock existed or
  -- not. The sub-block existed if:
  --    * the block the switch is in existed and,
  --    * the active boolean is set to false (there has not been a structural
  --      change) or,
  --    * previous_index == current_index
  --
  -- If the sub-block existed, we just keep going as normal in the block.
  -- Otherwise, if the block didn't exist, we must compute oldNextVar for after
  -- the block. Note that the behaviour here also depends on wether the block
  -- the switch appears in existed or not. If it didn't, then we don't care
  -- about if because it will never be used and it will be overriden eventually.
  -- If it did (but the sub-block didn't, remember), then oldNextVar is equal
  -- to oldNextVar + switchPreviousModeNewVar.
 where
  handleBranch state@SwitchState { switchActivePtr = activePtr, switchIndexPtr = currentIndexPtr, switchPreviousIndexPtr = previousIndexPtr, switchPreviousModeNewVarPtr = previousModeNewVarPtr } _ Branch { branchArguments = branchArgs, branchBlock = block }
    = Method $ do
      previousOldNextVar <- use (#userState . #oldNextVar)
      blockExisted       <- view (#userInput . #blockExisted)
      isActive           <- LL.named (LL.load activePtr 0) "switch.active"
      currentIndex       <- LL.named (LL.load currentIndexPtr 0) "current.index"
      previousIndex <- LL.named (LL.load previousIndexPtr 0) "previous.index"
      subBlockExisted    <- LL.ite
        blockExisted
        (LL.ite isActive
                (pure LL.true)
                (LL.icmp LL.EQ currentIndex previousIndex)
        )
        (pure LL.false)
      locally (#userInput . #blockExisted) (const subBlockExisted)
        $ runMethod
        $ withBranchArgs state branchArgs
        $ withBlock copyInputsMethod block
      -- After having handled the block, we can now set oldNextVar to the right
      -- If blockExisted but not subBlockExisted, then oldNextVar
      -- should be set to previousOldNextVar + !
      modifyOldNextVar <- LL.named
        (   LL.and blockExisted
        =<< LL.named (LL.not subBlockExisted) "sub.block.didnt.exist"
        )
        "should.modify.old.next.var"
      oldNextVar <- LL.ite
        modifyOldNextVar
        (do
          previousModeNewVar <- LL.named (LL.load previousModeNewVarPtr 0)
                                         "previous.mode.new.var"
          LL.named (LL.add previousOldNextVar previousModeNewVar)
                   "modified.old.next.var"
        )
        (use (#userState . #oldNextVar))
      #userState . #oldNextVar .= oldNextVar
