{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE UndecidableInstances #-}

-- This module defines a common interface for generating (most of) the methods
-- defined on a model. It provides ways to automatically deal with expanding the
-- scope of signals upon encountering a let binding or dealing with switches.
-- This means that for most method, a function that generates code for an
-- equation is the only thing needed. The rest can be defined with functions
-- from this module

module Hydra.CodeGen.Method where

import           Control.Lens
import           Control.Monad
import           Control.Monad.RWS.Strict
import           Data.Foldable
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import           Data.Set                       ( Set )
import           GHC.Generics
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.Scope
import           Hydra.CodeGen.Type
import qualified Hydra.Flatten.AST             as F
import           Hydra.Ident
import qualified Hydra.LLVM.Utils              as LL
import           Hydra.OOIR
import           Hydra.Pretty
import qualified LLVM.AST.Name                 as LL
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.Type                 as LLT
import           LLVM.Pretty                    ( )

-- A method allows access to the scope of signals and to a state that contains:
--  * the signal relation's state
--  * the next available index for a variable

data MethodInput i s = MethodInput
  { methodScope          :: ModelScope
  , globalScope          :: GlobalScope
  , firstBlockActivation :: Operand
  , userInput            :: i
  , nextVarStorage       :: LL.StoreOf Operand
  , userStateStorage     :: LL.StoreOf s
  }
  deriving Generic

data MethodState s = MethodState
  { nextVar    :: Operand
  , modelState :: Operand
  , userState  :: s
  }
  deriving (Show, Generic)

instance LL.Merge s => LL.Merge (MethodState s)

newtype Method i s =
  Method { runMethod :: RWST (MethodInput i s) () (MethodState s) CGenM () }

instance Semigroup (Method i s) where
  Method m1 <> Method m2 = Method (m1 >> m2)

instance Monoid (Method i s) where
  mempty = Method (pure ())

withLocals :: Set Ident -> Method i s -> Method i s
withLocals idts (Method method) = Method $ do
  next <- use #nextVar
  let go (env, this) idt = do
        next <- LL.named (LL.incr this) "next.var"
        pure (M.insert idt this env, next)
  (env, next) <- foldM go ([], next) idts
  #nextVar .= next
  locally #methodScope (insertLocals env) method

withModelObj
  :: (LL.Argument i, LL.Argument s, LL.Merge s, LL.Storable s)
  => GlobalScope
  -> (Rel -> Method i s)
  -> LL.Name
  -> ModelMethod i s
  -> LLT.Type
  -> ModelObj
  -> ModuleGenM
       (LL.LLVMFun (ModelMethod i s) (ModelMethodArg i s))
withModelObj globalScope method name modelMethod envPtrTy Obj { modelCaptures = captures, modelBlock = block, modelOutput = output }
  = fst <$> codeGenFunction name modelMethod go
 where
  go ModelMethodArg { modelEnvPtr = modelEnvPtr, modelState = modelState, firstBlockActivation = firstBlockActivation, nextVarStorage = nextVarStorage, inputArgs = input, stateArgs = userStateStorage, interfaceSignalIndices = ifaceSignals }
    = do

      -- We start by loading the state and stuff out of their pointers
      nextVar   <- LL.named (LL.loadOut nextVarStorage) "next.var"
      userState <- LL.named (LL.loadOut userStateStorage) "user.state"

      -- Then, we have to make the scope. Initially, the scope only contains the
      -- captured identifiers, which we need to load out of the environment; and
      -- then the output signals
      envPtr    <- LL.bitcast modelEnvPtr envPtrTy
      exprScope <- foldM
        (\scope (idx, Capture { captureRepl = i }) -> do
          valPtr <- LL.ibgep envPtr [LL.int32 0, LL.int32 idx]
          val    <- LL.named (LL.load valPtr 0) (LL.fromIdent i)
          pure (M.insert i val scope)
        )
        []
        (zip [0 ..] (toList captures))

      let sigScope = Seq.foldrWithIndex
            (\idx (out, op) scope -> M.insert
              out
              SigScopeEntry { sigKind = InterfaceSignal (fromIntegral idx)
                            , sigRepr = op
                            }
              scope
            )
            []
            (Seq.zip output ifaceSignals)

          modelScope = ModelScope { sigScope   = sigScope
                                  , exprScope  = exprScope
                                  , scopeDepth = 0
                                  }

      ((), MethodState { nextVar = nextVar, userState = userState }, ()) <-
        runRWST
          (runMethod (withBlock method block))
          MethodInput { methodScope          = modelScope
                      , globalScope          = globalScope
                      , firstBlockActivation = firstBlockActivation
                      , userInput            = input
                      , nextVarStorage       = nextVarStorage
                      , userStateStorage     = userStateStorage
                      }
          MethodState { nextVar    = nextVar
                      , modelState = modelState
                      , userState  = userState
                      }
      LL.storeIn userStateStorage userState
      LL.storeIn nextVarStorage nextVar
      pure (Just (LL.int32 0), ())

withCall
  :: (Integral index, LL.Argument i, LL.Argument s, LL.Storable s)
  => MethodIndex index i s
  -> Seq Ident
  -> Ident
  -> Method i s
withCall (MethodIndex funIndex) args index = Method $ do
  ui                     <- view #userInput
  scope                  <- view #methodScope
  nextVarStorage         <- view #nextVarStorage
  userStateStorage       <- view #userStateStorage
  firstBlockActivation   <- view #firstBlockActivation

  userState              <- use #userState
  state                  <- use #modelState
  nextVar                <- use #nextVar
  (modelStatePtr, state) <- lift $ unConsState state
  modelState             <- LL.named (LL.load modelStatePtr 0) "model.state"
  let model = lookupAttr index scope
  -- A model is a record of a struct of functions and a pointer to an
  -- environement. We get the environment and then the function we are
  -- interested in.
  modelEnvPtrPtr <- LL.named (LL.ibgep model [LL.int32 0, LL.int32 1])
                             "model.env.ptr"
  modelEnvPtr     <- LL.named (LL.load modelEnvPtrPtr 0) "model.env"
  funRecordPtrPtr <- LL.named (LL.ibgep model [LL.int32 0, LL.int32 0])
                              "fun.record.ptr.ptr"
  funRecordPtr <- LL.named (LL.load funRecordPtrPtr 0) "fun.record.ptr"
  funPtr       <- LL.named
    (LL.ibgep funRecordPtr [LL.int32 0, LL.int32 (fromIntegral funIndex)])
    "method.fun.ptr.ptr"
  fun <- LL.named (LL.load funPtr 0) "method.fun.ptr"
  LL.storeIn nextVarStorage nextVar
  LL.storeIn userStateStorage userState
  res <- LL.callLLVMFun
    (LL.LLVMFun fun :: LL.LLVMFun (ModelMethod i s) (ModelMethodArg i s))
    ModelMethodArg { modelEnvPtr            = modelEnvPtr
                   , modelState             = modelState
                   , firstBlockActivation   = firstBlockActivation
                   , nextVarStorage         = nextVarStorage
                   , inputArgs              = ui
                   , stateArgs              = userStateStorage
                   , interfaceSignalIndices = fmap (`lookupSig` scope) args
                   }
  LL.assertion (LL.intIs 0 res) res
  us      <- LL.loadOut userStateStorage
  nextVar <- LL.loadOut nextVarStorage
  #modelState .= state
  #nextVar .= nextVar
  #userState .= us

withSwitch
  :: LL.Merge s
  => (SwitchState -> Label -> Branch -> Method i s)
  -> Map F.Label Branch
  -> Method i s
withSwitch handleBranch branches = Method $ do
  state                    <- use #modelState
  (switchState, nextState) <- lift $ unConsState state
  switchState@SwitchState { switchIndexPtr = indexPtr, switchSubBlockStatePtr = blockStatePtr } <-
    lift $ getSwitchState switchState
  blockState <- LL.load blockStatePtr 0
  index      <- LL.named (LL.load indexPtr 0) "switch.index"
  input      <- ask
  state      <- get
  state      <- lift $ LL.switchWithDefault
    index
    Nothing
    (toList
      (compileBranches switchState
                       input
                       (set #modelState blockState state)
                       branches
      )
    )
  put state
  #modelState .= nextState
 where
  compileBranches switchState input state branches = M.mapWithKey
    (\label@F.Label { F.labelIndex = index } branch ->
      ( LL.int64Constant index
      , compileBranch switchState input state label branch
      )
    )
    branches
  compileBranch switchState input state label branch = do
    ((), state, ()) <- runRWST
      (runMethod (handleBranch switchState label branch))
      input
      state
    pure state

withBranchArgs :: SwitchState -> Seq Arg -> Method i s -> Method i s
withBranchArgs SwitchState { switchActivePtr = activePtr, switchArgsPtrPtr = rawArgsPtrPtr } args method
  = Method $ mdo
    argsPtrPtr <- LL.bitcast
      rawArgsPtrPtr
      (LLT.ptr (LLT.ptr (LL.structType (toList argsTy))))
    argsPtr            <- LL.named (LL.load argsPtrPtr 0) "args.ptr"
    (extScope, argsTy) <- foldM
      (\(extScope, argsTys) (idx, Arg idt typ) -> do
        typ    <- lift $ liftModuleGenM $ toLLTyp typ
        valPtr <- LL.ibgep argsPtr [LL.int32 0, LL.int32 (fromIntegral idx)]
        val    <- LL.load valPtr 0
        lift $ printf (show ("Loading" <+> pretty idt <+> "from %x: %f\n"))
                      [valPtr, val]
        pure (M.insert idt val . extScope, argsTys :|> typ)
      )
      (id, [])
      (zip [0 ..] (toList args))
    active <- LL.named (LL.load activePtr 0) "switch.active"
    locally #firstBlockActivation (const active)
      $ locally (#methodScope . #exprScope) extScope
      $ runMethod method

withSwitchBlock
  :: LL.Merge s
  => (Rel -> Method i s)
  -> SwitchState
  -> Label
  -> Branch
  -> Method i s
withSwitchBlock method switchState _ Branch { branchArguments = args, branchBlock = block }
  = withBranchArgs switchState args $ withBlock method block

withBlock :: LL.Merge s => (Rel -> Method i s) -> Block -> Method i s
withBlock mkMethod block@Block { blockAttributes = attributes, blockRelations = rels }
  = Method $ do
    -- Expectation: the state is an i8*
    state <- use #modelState
    blockStateStructTy <- lift $ liftModuleGenM $ blockStateStruct block
    blockState <- LL.bitcast state (LLT.ptr blockStateStructTy)
    (attributesStorage, relsState) <- lift $ unConsState blockState
    extScope <- foldM
      (\extScope (idx, i) -> do
        valPtr <- LL.ibgep attributesStorage [LL.int32 0, LL.int32 idx]
        val    <- LL.named (LL.load valPtr 0) (LL.fromIdent i)
        pure (M.insert i val . extScope)
      )
      id
      (zip [0 ..] (toList (M.keys attributes)))
    #modelState .= relsState
    locally (#methodScope . #exprScope) extScope
      $ runMethod (foldMap mkMethod rels)

data ConstrInput = ConstrInput
  { globalScope           :: GlobalScope
  , modelScope            :: ModelScope
  , nextVarStorage        :: LL.StoreOf Operand
  , structuralInfoStorage :: LL.StoreOf StructuralInfo
  , modelStateStorage     :: LL.StoreOf Operand
  }
  deriving Generic

data ConstrState = ConstrState
  { nextVar        :: Operand
  , structuralInfo :: StructuralInfo
  }
  deriving Generic

newtype Constr k = Constr { runConstr :: RWST ConstrInput () ConstrState CGenM k }

