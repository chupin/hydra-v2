module Hydra.CodeGen.IIR.Token where

import           Hydra.CodeGen.IIR.AST
import           Hydra.Op                       ( ArithBinOp
                                                , ArithUnOp
                                                )

data Token = TokIdent String
           | TokConst Const
           | TokEsc
           | TokInject
           | TokAssig
           | TokBinom
           | TokTanNum
           | TokComma
           | TokLBracket
           | TokRBracket
           | TokLParen
           | TokRParen
           | TokLBrace
           | TokRBrace
           | TokAlloc
           | TokDeref

           | TokAdd
           | TokSub
           | TokMul
           | TokDiv
           | TokPow
           | TokBinOp ArithBinOp
           | TokUnOp ArithUnOp

           | TokIAdd
           | TokISub
           | TokIMul
           | TokIDiv

           | TokBoolEq
           | TokInt2Double
           | TokIf
           | TokElseIf
           | TokElse
           | TokFor
           | TokSum
           | TokProd
           | TokLet
           | TokIn
           | TokSemiCol

           | TokUnit
           | TokBool
           | TokReal
           | TokInt

           | TokFrom
           | TokTo
           | TokStep

           | TokExcluded
           | TokIncluded

           | TokEOF
           deriving(Show)
