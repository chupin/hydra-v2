{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE TemplateHaskell #-}

module Hydra.CodeGen.IIR.QQ where

import Control.Monad.Writer.Strict hiding (Sum)

import           Data.Map                   ( Map )
import qualified Data.Map                   as M

import qualified LLVM.AST.IntegerPredicate  as LL
import           LLVM.AST.Operand           ( Operand )
import qualified LLVM.AST.Type              as LLT
import qualified LLVM.AST.Typed             as LLT
import qualified LLVM.IRBuilder.Constant    as LL

import           Language.Haskell.TH        hiding ( Range, Type, letS )
import           Language.Haskell.TH.Quote

import           Hydra.CodeGen.IIR.AST
import           Hydra.CodeGen.IIR.Lexer
import           Hydra.CodeGen.IIR.Parser
import           Hydra.CodeGen.Monad
import           Hydra.Error                ( internalError )
import qualified Hydra.LLVM.Utils           as LL
import qualified Hydra.Op                   as Op
import           Hydra.Pretty

unit :: Operand
unit = LL.undef LLT.i8

codeGen :: Expr -> Q Exp
codeGen = codeGenPartial []

codeGenPartial :: Map String Name -> Expr -> Q Exp
codeGenPartial env expr = do
  (nm, stmts) <- runWriterT (codeGenAux env expr)
  pure (DoE (stmts ++ [ NoBindS (AppE (VarE 'pure) (VarE nm)) ]))

llvmType :: Type -> LLT.Type
llvmType TIndex = LLT.i64
llvmType TReal = LLT.double
llvmType TBool = LLT.i1
llvmType TUnit = LLT.i8
llvmType (TArray ty) = LLT.ptr (llvmType ty)

assertType :: Type -> LLT.Type -> CGenM ()
assertType ty llty
  | llvmType ty == llty = pure ()
assertType ty llty =
  internalError ("Operand was declared with type " ++ show (pretty ty)
                 ++ " but the operand had LLVM type " ++ show (pretty llty))

bindTo :: String -> Exp -> WriterT [Stmt] Q Name
bindTo src exp = do
  name <- lift $ newName ('_' : src)
  tell [ BindS (VarP name) exp ]
  pure name

letS :: String -> Exp -> WriterT [Stmt] Q Name
letS src exp = do
  name <- lift $ newName ('_' : src)
  tell [ LetS [ ValD (VarP name) (NormalB exp) [] ] ]
  pure name

stmtS :: Exp -> WriterT [Stmt] Q ()
stmtS exp =
  tell [ NoBindS exp ]

codeGenAux :: Map String Name -> Expr -> WriterT [Stmt] Q Name
codeGenAux env (Local nm) = case M.lookup nm env of
  Nothing -> internalError "Name not found"
  Just op -> pure op
codeGenAux _ (Inj nm ty) = do
  mnm <- lift $ lookupValueName nm
  case mnm of
    Just nm -> do
      assert <- lift [| let _ = $(varE nm) :: Operand in assertType ty (LLT.typeOf $(varE nm)) |]
      tell [ NoBindS assert ]
      pure nm
    Nothing -> internalError ("Not in scope " ++ show nm)
codeGenAux _ (Const c) = case c of
  Unit -> pure 'unit
  Index i ->
    letS "index_constant" =<< lift [| LL.int64 (fromIntegral i) |]
  Double d ->
    letS "double_constant" =<< lift [| LL.double d |]
codeGenAux env (Set ptr val) = do
  ptr <- codeGenAux env ptr
  val <- codeGenAux env val
  stmtS =<< lift [| LL.store $(varE ptr) 0 $(varE val) |]
  pure 'unit
codeGenAux env (Binomial n k) = do
  n <- codeGenAux env n
  k <- codeGenAux env k
  bindTo "binomial" =<< lift [| lookupBinom $(varE n) $(varE k) |]
codeGenAux env (TanNum n k) = do
  n <- codeGenAux env n
  k <- codeGenAux env k
  bindTo "binomial" =<< lift [| lookupTangentNumber $(varE n) $(varE k) |]
codeGenAux env (Alloc ty size) = do
  size <- codeGenAux env size
  bindTo "allocate" =<< lift [| LL.alloca (llvmType ty) (Just $(varE size)) 0 |]
codeGenAux env (PtrOffset ptr offset) = do
  ptr <- codeGenAux env ptr
  offset <- codeGenAux env offset
  bindTo "offset" =<< lift [| LL.ibgep $(varE ptr) [ $(varE offset) ] |]
codeGenAux env (Deref ptr) = do
  ptr <- codeGenAux env ptr
  bindTo "deref" =<< lift [| LL.load $(varE ptr) 0 |]
codeGenAux env (Op (Op.ArithUnOp op p)) = do
  p <- codeGenAux env p
  bindTo "unop" =<< lift [| compileOp (Op.unop op $(varE p)) |]
codeGenAux env (Op (Op.ArithBinOp op p q)) = do
  p <- codeGenAux env p
  q <- codeGenAux env q
  bindTo "binop" =<< lift [| compileOp (Op.binop op $(varE p) $(varE q)) |]
codeGenAux env (IAdd lhs rhs) = do
  lhs <- codeGenAux env lhs
  rhs <- codeGenAux env rhs
  bindTo "index_add" =<< lift [| LL.add $(varE lhs) $(varE rhs) |]
codeGenAux env (ISub lhs rhs) = do
  lhs <- codeGenAux env lhs
  rhs <- codeGenAux env rhs
  bindTo "index_sub" =<< lift [| LL.sub $(varE lhs) $(varE rhs) |]
codeGenAux env (IMul lhs rhs) = do
  lhs <- codeGenAux env lhs
  rhs <- codeGenAux env rhs
  bindTo "index_mul" =<< lift [| LL.mul $(varE lhs) $(varE rhs) |]
codeGenAux env (IDiv lhs rhs) = do
  lhs <- codeGenAux env lhs
  rhs <- codeGenAux env rhs
  bindTo "index_udiv" =<< lift [| LL.udiv $(varE lhs) $(varE rhs) |]
codeGenAux env (IEq lhs rhs) = do
  lhs <- codeGenAux env lhs
  rhs <- codeGenAux env rhs
  bindTo "index_eq" =<< lift [| LL.icmp LL.EQ $(varE lhs) $(varE rhs) |]
codeGenAux env (IntToDouble expr) = do
  expr <- codeGenAux env expr
  bindTo "int_to_double" =<< lift [| LL.uitofp $(varE expr) LLT.double |]
codeGenAux env (If cond th el) = do
  cond <- codeGenAux env cond
  bindTo "ite" =<< lift [| LL.ite $(varE cond) $(codeGenPartial env th) $(codeGenPartial env el) |]
codeGenAux env (For i (Range start stop step) body) = do
  start <- codeGenAux env start
  stop <- codeGenAux env stop
  step <- codeGenAux env step
  _ <- bindTo "iir_iter" =<< lift [|
    LL.while (Just "iir_for") $(varE start) (\index -> do
      continue <- LL.icmp LL.ULT index $(varE stop)
      pure (continue, index)) $ \index -> do
        _ <- $(codeGenPartial (M.insert i 'index env) body)
        LL.add index $(varE step) |]
  pure 'unit
codeGenAux env (Sum i (Range start stop step) body) = do
  start <- codeGenAux env start
  stop <- codeGenAux env stop
  step <- codeGenAux env step
  bindTo "iir_sum" =<< lift [|
    snd <$> LL.while (Just "iir.sum") ($(varE start), LL.double 0) (\acc@(index, _) -> do
      continue <- LL.icmp LL.ULT index $(varE stop)
      pure (continue, acc)) (\(index, acc) -> do
        val <- $(codeGenPartial (M.insert i 'index env) body)
        index <- LL.add index $(varE step)
        nacc <- LL.fadd acc val
        pure (index, nacc)) |]
codeGenAux env (Prod i (Range start stop step) body) = do
  start <- codeGenAux env start
  stop <- codeGenAux env stop
  step <- codeGenAux env step
  bindTo "iir_prod" =<< lift [|
    snd <$> LL.while (Just "iir.prod") ($(varE start), LL.double 1) (\acc@(index, _) -> do
      continue <- LL.icmp LL.ULT index $(varE stop)
      pure (continue, acc)) (\(index, acc) -> do
        val <- $(codeGenPartial (M.insert i 'index env) body)
        index <- LL.add index $(varE step)
        nacc <- LL.fmul acc val
        pure (index, nacc)) |]
codeGenAux env (Let i expr body) = do
  expr <- codeGenAux env expr
  codeGenAux (M.insert i expr env) body
codeGenAux env (Seq first next) = do
  _ <- codeGenAux env first
  codeGenAux env next

iir :: QuasiQuoter
iir = QuasiQuoter { quoteExp  = quoteIIR
                  , quotePat  = undefinedFor "patterns"
                  , quoteDec  = undefinedFor "declarations"
                  , quoteType = undefinedFor "types"
                  }
  where
    quoteIIR src = case typeCheck expr of
      Left err -> error (show (pretty err))
      Right _ -> codeGen expr
      where
        toks = alexScanTokens src

        expr = parseExpr toks

    undefinedFor str = error ("iir is not a valid quasiquoter for " ++ str)
-- phiAcc <- case (acc, nextAcc) of
--   (Just (init, _), Just acc) ->
--     Just <$>
--   _ -> pure Nothing
-- continue <- LL.icmp LL.ULT $$(pure index) stop
-- LL.condBr continue stepLabel exitLabel
-- stepLabel <- LL.named LL.block "step"
-- loopAcc <- $$(pure cbody)
-- nextIndex <- LL.add phiIndex step
-- nextAcc <-
--   case (acc, phiAcc) of
--     (Just (_, merge), Just acc) ->
--       Just <$> merge acc loopAcc
--     _ -> pure Nothing
-- stepExitBlock <- LL.currentBlock
-- LL.br testBlock
-- case phiAcc of
--   Nothing -> pure unit
--   Just phiAcc -> pure phiAcc
-- ||]
-- where unit = LL.undef LLT.void
--       go _ env (Local nm) =
--         case M.lookup nm env of
--           Nothing -> internalError "Cannot find name"
--           Just op -> pure op
--       codeGenAux _ (Inj nm _) =
--         case M.lookup nm builtins of
--           Nothing -> internalError "Cannot find builtin"
--           Just op -> pure op
--       go _ _ (Const c) =
--         case c of
--           Unit -> pure unit
--           Index i -> pure (LL.int64 (fromIntegral i))
--           Double d -> pure (LL.double d)
--       codeGenAux env (Set ptr val) = do
--         ptr <- $$(codeGenAux envptr
--         val <- $$(codeGenAux envval
--         LL.store ptr 0 val
--         pure unit
--       -- loop :: Map String Operand
--       --      -> Map String Operand
--       --      -> Operand
--       --      -> (Operand -> Operand -> CGenM Operand)
--       --      -> String
--       --      -> Range
--       --      -> Expr
--       --      -> CGenM Operand
--       -- loop builtins env init merge i (Range start stop step) body = do
--       --   start <- $$(codeGenAux envstart
--       --   stop <- $$(codeGenAux envstop
--       --   step <- $$(codeGenAux envstep
--       --   let test acc@(_, index) = do
--       --         cond <- LL.icmp LL.ULT index stop
--       --         pure (cond, acc)
--       --       loop (acc, index) = do
--       --         incr <- codeGenAux (M.insert i acc env) body
--       --         nacc <- merge acc incr
--       --         nindex <- LL.add index step
--       --         pure (nacc, nindex)
--       --   (acc, _) <- LL.while Nothing (init, start) test loop
--       --   pure acc