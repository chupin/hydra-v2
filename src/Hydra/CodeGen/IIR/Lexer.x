{
{-# OPTIONS_GHC -Wno-all #-}
{-# LANGUAGE DataKinds, MultiParamTypeClasses, OverloadedStrings, FlexibleContexts #-}

module Hydra.CodeGen.IIR.Lexer where

import Hydra.CodeGen.IIR.Token
import Hydra.CodeGen.IIR.AST
import qualified Data.Text as T
import Hydra.Op (ArithBinOp(..), ArithUnOp(..))

}

%wrapper "basic"

$digits = 0-9
$lower = a-z
$upper = A-Z
$alpha = [$lower$upper]
$ascsymb = [\=\<\>\@\^\|\&\~\+\-\*\/\$\%\!\?\.]
$decdigit = [0-9]
$binit = [0-1]
$octit = [0-7]
$hexit = [0-9A-Fa-f]
$white = [\ \t\n\r\f\v]
$charesc = [abfnrtv\\\"\']

@ident = $alpha($alpha|_|'|$decdigit)*

@op1 = [\|]     $ascsymb*
@op2 = [\&]     $ascsymb*
@op3 = [\=\<\>] $ascsymb*
@op4 = [\@\^]   $ascsymb*
@op5 = [\+\-]   $ascsymb*
@op6 = [\*\/\%] $ascsymb*
@mop = [\!\?]   $ascsymb*

@decimal     = $decdigit+
@binary      = $binit+
@octal       = $octit+
@hexadecimal = $hexit+
@exponent    = [eE] [\-\+]? @decimal

@floating_point = @decimal \. @decimal @exponent? | @decimal @exponent

@escape = \\ ($charesc | @decimal | o @octal | x @hexadecimal)
@char = \' ($printable # [\\\'] | $white | @escape) \'
@gap = \\ $white+ \\
@str = \" ($printable # [\\\"] | $white | @escape | @gap)* \"

tokens :-
       $white+  ;
       "--".*   ;
       "$"                  { const TokEsc }
       ":="                 { const TokAssig }
       "tannum"             { const TokTanNum }
       "binom"              { const TokBinom }
       ","                  { const TokComma }
       "["                  { const TokLBracket }
       "]"                  { const TokRBracket }
       "("                  { const TokLParen }
       ")"                  { const TokRParen }
       "{"                  { const TokLBrace }
       "}"                  { const TokRBrace }
       "alloc"              { const TokAlloc }
       "!"                  { const TokDeref }
       ".+"                 { const TokIAdd }
       ".-"                 { const TokISub }
       ".*"                 { const TokIMul }
       "./"                 { const TokIDiv }
       "+"                  { const (TokBinOp Add) }
       "-"                  { const (TokBinOp Sub) }
       "*"                  { const (TokBinOp Mul) }
       "/"                  { const (TokBinOp Div) }
       "^"                  { const (TokBinOp Pow) }

       "sqrt"               { const (TokUnOp Sqrt) }
       "exp"                { const (TokUnOp Exp) }
       "log"                { const (TokUnOp Log) }
       "sin"                { const (TokUnOp Sin) }
       "tan"                { const (TokUnOp Tan) }
       "cos"                { const (TokUnOp Cos) }
       "asin"               { const (TokUnOp Asin) }
       "atan"               { const (TokUnOp Atan) }
       "acos"               { const (TokUnOp Acos) }
       "sinh"               { const (TokUnOp Sinh) }
       "tanh"               { const (TokUnOp Tanh) }
       "cosh"               { const (TokUnOp Cosh) }
       "asinh"              { const (TokUnOp Asinh) }
       "atanh"              { const (TokUnOp Atanh) }
       "acosh"              { const (TokUnOp Acosh) }
       "abs"                { const (TokUnOp Abs) }
       "sgn"                { const (TokUnOp Sgn) }

       "=="                 { const TokBoolEq }
       "int2double"         { const TokInt2Double }
       "if"                 { const TokIf }
       "elseif"             { const TokElseIf }
       "else"               { const TokElse }
       "for"                { const TokFor }
       "sum"                { const TokSum }
       "prod"               { const TokProd }
       "let"                { const TokLet }
       "in"                 { const TokIn }
       ";"                  { const TokSemiCol }

       "unit"               { const TokUnit }
       "bool"               { const TokBool }
       "real"               { const TokReal }
       "int"                { const TokInt }

       "from"               { const TokFrom }
       "to"                 { const TokTo }
       "step"               { const TokStep }

       "excluded"           { const TokExcluded }
       "included"           { const TokIncluded }

       @ident               { TokIdent }
       @decimal |
         0[bB] @binary |
         0[oO] @octal |
         0[xX] @hexadecimal { TokConst . Index . read  }
       @floating_point      { TokConst . Double . read }

{

alexEOF _ = TokEOF

}