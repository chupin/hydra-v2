{-# LANGUAGE MultiParamTypeClasses, OverloadedLists, OverloadedStrings, DeriveLift #-}

module Hydra.CodeGen.IIR.AST where

import           Control.Monad
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Maybe
import           Hydra.Op
import           Hydra.Pretty
import           Language.Haskell.TH.Syntax     ( Lift )

data Type = TUnit | TBool | TReal | TIndex | TArray Type
          deriving(Eq, Ord, Show, Lift)

instance Pretty Type where
  pretty TUnit       = "unit"
  pretty TBool       = "bool"
  pretty TReal       = "real"
  pretty TIndex      = "int"
  pretty (TArray ty) = brackets (pretty ty)

data Const = Unit
           | Index Int
           | Double Double
           deriving(Show, Lift)

instance Pretty Const where
  pretty Unit       = "()"
  pretty (Index  i) = pretty i
  pretty (Double d) = pretty d

data Range = Range
  { rangeStart :: Expr
  , rangeStop  :: Expr
  , rangeStep  :: Expr
  }

instance Pretty Range where
  pretty (Range start stop step) =
    "from" <+> pretty start <+> "to" <+> pretty stop <+> "step" <+> pretty step

data Expr = Local String
          | Inj String Type
          | Const Const
          | Set Expr Expr
          | TanNum Expr Expr
          | Binomial Expr Expr
          | Alloc Type Expr

          | PtrOffset Expr Expr
          | Deref Expr

          | Op (Op Expr)

          | IAdd Expr Expr
          | ISub Expr Expr
          | IMul Expr Expr
          | IDiv Expr Expr

          | IEq Expr Expr

          | IntToDouble Expr

          | If Expr Expr Expr

          | For String Range Expr
          | Sum String Range Expr
          | Prod String Range Expr

          | Let String Expr Expr

          | Seq Expr Expr

instance Pretty Expr where
  pretty (Local nm   ) = pretty nm
  pretty (Inj nm ty  ) = parens ("$" <> pretty nm <> parens (pretty ty))
  pretty (Const c    ) = pretty c
  pretty (Set ptr val) = parens (pretty ptr <+> ":=" <+> pretty val)
  pretty (TanNum n k) =
    parens ("tannum" <> parens (pretty n <> "," <> pretty k))
  pretty (Binomial n k) =
    parens ("binom" <> parens (pretty n <> "," <+> pretty k))
  pretty (Alloc ty expr) =
    parens ("alloc" <+> parens (pretty ty <> "," <+> pretty expr))
  pretty (PtrOffset ptr off) = parens (pretty ptr <> brackets (pretty off))
  pretty (Deref ptr        ) = parens ("!" <> pretty ptr)
  pretty (Op    op         ) = parens (prettyOp (fmap pretty op))
  pretty (IAdd lhs rhs     ) = parens (pretty lhs <+> ".+" <+> pretty rhs)
  pretty (ISub lhs rhs     ) = parens (pretty lhs <+> ".-" <+> pretty rhs)
  pretty (IMul lhs rhs     ) = parens (pretty lhs <+> ".*" <+> pretty rhs)
  pretty (IDiv lhs rhs     ) = parens (pretty lhs <+> "./" <+> pretty rhs)
  pretty (IEq  lhs rhs     ) = parens (pretty lhs <+> "==" <+> pretty rhs)
  pretty (IntToDouble d    ) = parens ("int2double" <+> pretty d)
  pretty (If cond th el    ) = nest
    ilvl
    (parens
      ("if" <+> pretty cond $+$ braces (pretty th) $+$ "else" $+$ braces
        (pretty el)
      )
    )
  pretty (For  nm range body) = prettyLoop Nothing nm range body
  pretty (Sum  nm range body) = prettyLoop (Just "sum") nm range body
  pretty (Prod nm range body) = prettyLoop (Just "prod") nm range body
  pretty (Let i expr body) =
    "let" <+> pretty i <+> ":=" <+> pretty expr <+> "in" $+$ pretty body
  pretty (Seq e1 e2) = nest ilvl (parens (pretty e1 <+> ";" $+$ pretty e2))

prettyLoop :: Maybe (Doc ann) -> String -> Range -> Expr -> Doc ann
prettyLoop head nm range body = nest
  ilvl
  (parens
    (phead <+> "for" <+> pretty nm <+> pretty range $+$ braces (pretty body))
  )
  where phead = fromMaybe "" head

data TypeError = NotInScope String
               | ExpectedArray Expr Type
               | MismatchedTypes Expr Type Type

instance Pretty TypeError where
  pretty (NotInScope nm) = "Identifier" <+> pretty nm <+> "is not in scope."
  pretty (ExpectedArray expr ty) =
    "The expression"
      <+> pretty expr
      <+> "should have an array type."
      <+> "but it has type"
      <+> pretty ty
      <>  "."
  pretty (MismatchedTypes expr ty ty') =
    "The expression"
      <+> pretty expr
      <+> "should have type"
      <+> pretty ty
      <+> "but has type"
      <+> pretty ty'
      <>  "."

unify :: Expr -> Type -> Type -> Either TypeError ()
unify _ ty1 ty2 | ty1 == ty2 = pure ()
unify expr ty1 ty2           = Left (MismatchedTypes expr ty1 ty2)

isArray :: Expr -> Type -> Either TypeError Type
isArray _    (TArray ty) = pure ty
isArray expr ty          = Left (ExpectedArray expr ty)

typeCheck :: Expr -> Either TypeError Type
typeCheck = typeCheckAux []

checkHasType :: Type -> Map String Type -> Expr -> Either TypeError ()
checkHasType ty env expr = do
  ty' <- typeCheckAux env expr
  unify expr ty ty'

typeCheckAux :: Map String Type -> Expr -> Either TypeError Type
typeCheckAux env (Local nm) = case M.lookup nm env of
  Just ty -> pure ty
  Nothing -> Left (NotInScope nm)
typeCheckAux _ (Inj _ ty) = pure ty
typeCheckAux _ (Const c ) = pure $ case c of
  Unit     -> TUnit
  Double _ -> TReal
  Index  _ -> TIndex
typeCheckAux env set@(Set lhs rhs) = do
  pty <- typeCheckAux env lhs
  ty  <- isArray lhs pty
  ty' <- typeCheckAux env rhs
  unify set ty ty'
  pure TUnit
typeCheckAux env (Binomial n k) = do
  checkHasType TIndex env n
  checkHasType TIndex env k
  pure TReal
typeCheckAux env (TanNum n k) = do
  checkHasType TIndex env n
  checkHasType TIndex env k
  pure TReal
typeCheckAux env (Alloc ty size) = do
  checkHasType TIndex env size
  pure (TArray ty)
typeCheckAux env (PtrOffset ptr index) = do
  tptr <- typeCheckAux env ptr
  _    <- isArray ptr tptr
  checkHasType TIndex env index
  pure tptr
typeCheckAux env (Deref ptr) = do
  tptr <- typeCheckAux env ptr
  isArray ptr tptr
typeCheckAux env (Op op) = do
  forM_ op (checkHasType TReal env)
  pure TReal
typeCheckAux env (IAdd lhs rhs) = typeCheckIOp env lhs rhs
typeCheckAux env (ISub lhs rhs) = typeCheckIOp env lhs rhs
typeCheckAux env (IMul lhs rhs) = typeCheckIOp env lhs rhs
typeCheckAux env (IDiv lhs rhs) = typeCheckIOp env lhs rhs
typeCheckAux env (IEq  lhs rhs) = do
  checkHasType TIndex env lhs
  checkHasType TIndex env rhs
  pure TBool
typeCheckAux env (IntToDouble expr) = do
  checkHasType TIndex env expr
  pure TReal
typeCheckAux env ite@(If cond th el) = do
  checkHasType TBool env cond
  th <- typeCheckAux env th
  el <- typeCheckAux env el
  unify ite th el
  pure th
typeCheckAux env (For nm range body) = typeCheckLoop env Nothing nm range body
typeCheckAux env (Sum nm range body) =
  typeCheckLoop env (Just TReal) nm range body
typeCheckAux env (Prod nm range body) =
  typeCheckLoop env (Just TReal) nm range body
typeCheckAux env (Let nm expr body) = do
  ty <- typeCheckAux env expr
  typeCheckAux (M.insert nm ty env) body
typeCheckAux env (Seq e1 e2) = do
  _ <- typeCheckAux env e1
  typeCheckAux env e2

typeCheckIOp :: Map String Type -> Expr -> Expr -> Either TypeError Type
typeCheckIOp env lhs rhs = do
  checkHasType TIndex env lhs
  checkHasType TIndex env rhs
  pure TIndex

typeCheckLoop
  :: Map String Type
  -> Maybe Type
  -> String
  -> Range
  -> Expr
  -> Either TypeError Type
typeCheckLoop env rty nm (Range start stop step) body = do
  checkHasType TIndex env start
  checkHasType TIndex env stop
  checkHasType TIndex env step
  ty <- typeCheckAux (M.insert nm TIndex env) body
  case rty of
    Just rty -> do
      unify body ty rty
      pure rty
    Nothing -> pure TUnit

