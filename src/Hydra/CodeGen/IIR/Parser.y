{

{-# LANGUAGE OverloadedLists, GADTs, DataKinds, MultiParamTypeClasses, OverloadedStrings, TypeFamilies, FlexibleContexts #-}

module Hydra.CodeGen.IIR.Parser (parseExpr) where

import Hydra.CodeGen.IIR.AST
import Hydra.CodeGen.IIR.Token
import Hydra.Error (internalError)
import Hydra.Op (ArithBinOp(..), ArithUnOp)
import qualified Hydra.Op as Op
}

%name parseExpr Expr
%tokentype { Token }

%token
  ident { TokIdent $$ }
  '$' { TokEsc }
  const { TokConst $$ }
  ':=' { TokAssig }
  binom { TokBinom }
  tannum { TokTanNum }
  ','   { TokComma }
  '[' { TokLBracket }
  ']' { TokRBracket }
  '(' { TokLParen }
  ')' { TokRParen }
  '{' { TokLBrace }
  '}' { TokRBrace }
  alloc { TokAlloc }
  '!'   { TokDeref }
  '+'    { TokBinOp Add }
  '-'    { TokBinOp Sub }
  '*'    { TokBinOp Mul }
  '/'    { TokBinOp Div }
  '^'    { TokBinOp Pow }
  unop   { TokUnOp $$ }
  '.+'   { TokIAdd }
  '.-'   { TokISub }
  '.*'   { TokIMul }
  './'   { TokIDiv }
  '=='   { TokBoolEq }
  int2double { TokInt2Double }
  if { TokIf }
  elseif { TokElseIf }
  else { TokElse }
  for { TokFor }
  sum { TokSum }
  prod { TokProd }
  let { TokLet }
  in { TokIn }
  ';' { TokSemiCol }

  unit { TokUnit }
  bool { TokBool }
  real { TokReal }
  int  { TokInt }

  from { TokFrom }
  to   { TokTo }
  step { TokStep }

  excluded { TokExcluded }
  included { TokIncluded }

%right in
%right ';'
%right ':='
%nonassoc '=='
%left '+' '.+' '-' '.-'
%left '*' '.*' '/' './'
%right '^'

%%

Type: unit { TUnit }
    | bool { TBool }
    | real { TReal }
    | int  { TIndex }
    | '[' Type ']' { TArray $2 }
    | '(' Type ')' { $2 }

Inject: '$' { TIndex }
      | '$' '$' { TReal }

ArrayMod: { id } | '[' ArrayMod ']' { TArray . $2 }

Atom:: { Expr }
Atom: ident { Local $1 }
    | Inject ArrayMod ident { Inj $> ($2 $1) }
    |       { Const Unit }
    | const { Const $1 }
    | '(' Expr ')' { $2 }
    | Indexing { let (ptr, n) = $1 in PtrOffset ptr n }

Indexing: Atom '[' Expr ']' { ($1, $3) }
        | Indexing '[' Expr ']' { let (x, n) = $1 in (Deref (PtrOffset x n), $3)
        }

OpExpr:: { Expr }
OpExpr: Atom { $1 }
      | unop '(' Expr ')' { Op (Op.unop $1 $3) }
      | int2double '(' Expr ')' { IntToDouble $3 }

      | alloc '(' Type ',' Expr ')' { Alloc $3 $5 }

      | binom '(' Expr ',' Expr ')' { Binomial $3 $5 }
      | tannum '(' Expr ',' Expr ')' { TanNum $3 $5 }

      | OpExpr '+' OpExpr { Op (Op.binop Add $1 $3) }
      | OpExpr '-' OpExpr { Op (Op.binop Sub $1 $3) }
      | OpExpr '*' OpExpr { Op (Op.binop Mul $1 $3) }
      | OpExpr '/' OpExpr { Op (Op.binop Div $1 $3) }
      | OpExpr '^' OpExpr { Op (Op.binop Pow $1 $3) }

      | OpExpr '.+' OpExpr { IAdd $1 $3 }
      | OpExpr '.-' OpExpr { ISub $1 $3 }
      | OpExpr '.*' OpExpr { IMul $1 $3 }
      | OpExpr './' OpExpr { IDiv $1 $3 }

      | OpExpr '==' OpExpr { IEq $1 $3 }

      | '!' Atom { Deref $2 }

Expr:: { Expr }
Expr: OpExpr { $1 }
    | Expr ':=' Expr { Set $1 $3 }

    | If { $1 }

    | for ident Range '{' Expr '}' { For $2 $3 $5 }
    | sum for ident Range '{' Expr '}' { Sum $3 $4 $6 }
    | prod for ident Range '{' Expr '}' { Prod $3 $4 $6 }

    | let ident ':=' Expr in Expr { Let $2 $4 $6 }

    | Expr ';' Expr { Seq $1 $3 }

Step: { Const (Index 1) }
    | step Expr { $2 }

ExclOrIncl: { Excluded }
          | excluded { Excluded }
          | included { Included }

Range: from Expr to Expr ExclOrIncl Step { Range $2 (stop $5 $4) $6 }

If: if Expr '{' Expr '}' Else { If $2 $4 $6 }

Else: { Const Unit }
    | elseif Expr '{' Expr '}' Else { If $2 $4 $6 }
    | else '{' Expr '}' { $3 }

{

data ExclOrIncl = Excluded | Included

stop Excluded expr = expr
stop Included expr = IAdd expr (Const (Index 1))

happyError toks =
  internalError (unlines ("Failed parsing on tokens" : fmap show toks))

}