{-# LANGUAGE RecursiveDo, OverloadedStrings, NoMonomorphismRestriction  #-}

module Hydra.CodeGen.StaticData
  ( nats
  , facts
  , binoms
  , tangentNumbers
  , Partitions(..)
  , Partition(..)
  , integerPartitions
  -- , matrixRowType
  -- , matrixType
  -- , emitUserData
  -- , userDataType
  -- , getCurrentMode
  -- , getPreviousMode
  -- , getFreshlyEntered
  -- , getArgsPtr
  -- , getChildrenPtr
  -- , getChildSwitch
  -- , getCurrentChild
  -- , setCurrentMode
  -- , setPreviousMode
  -- , setFreshlyEntered
  -- , setArgsPtr
  -- , setChildrenPtr
  -- , getOutputVarCt
  -- , getOutputVarNames
  -- , getPrevVarOffset
  -- , getVarOffset
  -- , getInitVarOffset
  -- , getSolOffset
  -- , getFirstActive
  -- , getSolPtr
  -- , getHeadVarArray
  -- , getVarHODArray
  -- , getReinitArray
  -- , getEqnHODArray
  -- , getPrgmStruct
  -- , getSigMat
  -- , getRootDirArray
  -- , getRootArray
  -- , setOutputVarCt
  -- , setOutputVarNames
  -- , setPrevVarOffset
  -- , setInitVarOffset
  -- , setVarOffset
  -- , setSolOffset
  -- , setFirstActive
  -- , setSolPtr
  -- , setHeadVarArray
  -- , setVarHODArray
  -- , setReinitArray
  -- , setEqnHODArray
  -- , setPrgmStruct
  -- , setSigMat
  -- , setRootDirArray
  -- , setRootArray
  ) where

import qualified Data.IntMap                   as IM
import           Data.Stream.Infinite           ( Stream(..) )
import qualified Data.Stream.Infinite          as Stream
import           Numeric.Natural

nats :: Num i => Stream i
nats = Stream.iterate (+ 1) 0
{-# SPECIALIZE nats :: Stream Int #-}
{-# SPECIALIZE nats :: Stream Word #-}
{-# SPECIALIZE nats :: Stream Integer #-}
{-# SPECIALIZE nats :: Stream Natural #-}

facts :: Num i => Stream i
facts = Stream.scanl (*) 1 (Stream.tail nats)
{-# SPECIALIZE facts :: Stream Int #-}
{-# SPECIALIZE facts :: Stream Word #-}
{-# SPECIALIZE facts :: Stream Integer #-}
{-# SPECIALIZE facts :: Stream Natural #-}

binoms :: Num i => Stream [i]
binoms = Stream.scanl (\xs _ -> 1 : go xs) [1] nats
 where
  go []           = []
  go [x         ] = [x]
  go (x : y : xs) = x + y : go (y : xs)
{-# SPECIALIZE binoms :: Stream [Int] #-}
{-# SPECIALIZE binoms :: Stream [Word] #-}
{-# SPECIALIZE binoms :: Stream [Integer] #-}
{-# SPECIALIZE binoms :: Stream [Natural] #-}

data Partition = Partition
  { partitionSum           :: !Natural
  , partitionLeadingFactor :: !Natural
  }

data Partitions = Partitions
  { partitionsCount    :: Int
  , partitionsData     :: [Partition]
  , subpartitionsCount :: Int
  , subpartitionsStart :: [Int]
  }

integerPartitions :: Stream Partitions
integerPartitions = Stream.unfold go (1, [])
 where
  ext m n (ps : pps) = case ps of
    []    -> pps'
    _ : _ -> fmap (insertInPartition n 1) ps ++ pps'
    where pps' = ext m (n + 1) (removeFromPartition (n + 1) pps)
  ext m _ _ = [IM.singleton m 1]

  insertInPartition _ 0  p = p
  insertInPartition n mn p = IM.insertWith (+) n mn p

  removeFromPartition n pps = fmap (filter go) pps
    where go p = IM.null pln where (pln, _) = IM.split n p

  go (n, ps) =
    ( Partitions { partitionsCount    = length qs
                 , partitionsData     = fmap (mkpart n) qs
                 , subpartitionsCount = length subps
                 , subpartitionsStart = subps
                 }
    , (n + 1, qs : ps)
    )
   where
    qs    = ext n 1 ps
    subps = subparts 1 0 qs

  leadingFactor n ms = facts Stream.!! fromIntegral n `div` product
    (IM.mapWithKey denom ms)
   where
    denom i mi =
      facts Stream.!! fromIntegral mi * (facts Stream.!! fromIntegral i) ^ mi
  mkpart n ms = Partition { partitionSum           = sum ms
                          , partitionLeadingFactor = leadingFactor n ms
                          }

  subparts _ _ [] = []
  subparts n m (p : ps) | IM.null pln = m : subparts (n + 1) m (p : ps)
                        | otherwise   = subparts n (m + 1) ps
    where (pln, _) = IM.split n p

-- The tangent numbers are the numbers that appear in the computation of the
-- n-derivative of tan. The n-th derivative of tan can be expressed as:
--
-- \sum_{i = 0}^{n + 1} b(n, k) * tan(t)^k
--
-- where b(0,0) = 0
--       b(0,1) = 1
--       b(n,k) = 0 if n < 0, k < 0 or k > n
--       b(n,k) = (k - 1) * b(n, k - 1) + (k + 1) * b(n, k + 1)
tangentNumbers :: Stream [Natural]
tangentNumbers = Stream.iterate next [0, 1]
 where
  next bns =
    -- bns are the b(n,k) for the previous level. There are n + 1 of them.
    -- We can quickly compute the next level by zipping two lists:
    --     tail b(n,k) ++ [0,0] which is the list of the b(n, k + 1)
    -- and 0 : b(n,k) which is the list of the b(n, k - 1)
             zipWith (+)
                     (zipWith (*) [1 ..] (tail bns ++ [0, 0]))
                     (0 : zipWith (*) [0 ..] bns) -- Do the zipWith after the 0 : to avoid underflow
-- data SwitchTree =
--   SwitchTree { initialMode :: Int -- F.Mode (F.Sig Ident)
--              , argArrayLength :: Int
--              , subSwitches :: [[SwitchTree]]
--              }

-- generateProgramStructure :: F.Rel -> [SwitchTree]
-- generateProgramStructure (F.Switch (F.Mode imode _) branches) =
--   [ SwitchTree { initialMode    = branchMap M.! imode
--                , argArrayLength = maxArgCount
--                , subSwitches    = toList subStructures
--                }
--   ]
--  where
--   branchMap = M.fromList (zip (M.keys branches) [0 ..])
--   subStructures =
--     fmap (concatMap generateProgramStructure . F.branchRelations) branches
--   maxArgCount =
--     maximum (fmap (\F.Branch { F.branchArgs = args } -> length args) branches)
-- generateProgramStructure (F.Let _ rels) =
--   concatMap generateProgramStructure rels
-- generateProgramStructure _ = []

-- getProgramStructureField
--   :: MonadModuleBuilder m => MonadIRBuilder m => Integer -> Operand -> m Operand
-- getProgramStructureField idx prgmPtr = do
--   fieldPtr <- LL.ibgep prgmPtr [LL.int32 0, LL.int32 idx]
--   LL.load fieldPtr 0

-- setProgramStructureField
--   :: MonadModuleBuilder m
--   => MonadIRBuilder m => Integer -> Operand -> Operand -> m ()
-- setProgramStructureField idx prgmPtr val = do
--   fieldPtr <- LL.ibgep prgmPtr [LL.int32 0, LL.int32 idx]
--   LL.store fieldPtr 0 val

-- getCurrentMode, getPreviousMode, getFreshlyEntered, getArgsPtr, getChildrenPtr
--   :: MonadModuleBuilder m => MonadIRBuilder m => Operand -> m Operand
-- [getCurrentMode, getPreviousMode, getFreshlyEntered, getArgsPtr, getChildrenPtr]
--   = [ getProgramStructureField field | field <- [0 .. 4] ]

-- setCurrentMode, setPreviousMode, setFreshlyEntered, setArgsPtr, setChildrenPtr
--   :: MonadModuleBuilder m => MonadIRBuilder m => Operand -> Operand -> m ()
-- [setCurrentMode, setPreviousMode, setFreshlyEntered, setArgsPtr, setChildrenPtr]
--   = [ setProgramStructureField field | field <- [0 .. 4] ]

-- getChildSwitch
--   :: MonadModuleBuilder m => MonadIRBuilder m => Operand -> Operand -> m Operand
-- getChildSwitch thisSwitch mode = do
--   childrenPtr <- getChildrenPtr thisSwitch
--   childPtr    <- LL.ibgep childrenPtr [mode]
--   LL.load childPtr 0

-- getCurrentChild
--   :: MonadModuleBuilder m => MonadIRBuilder m => Operand -> m Operand
-- getCurrentChild thisSwitch = do
--   currentMode <- getCurrentMode thisSwitch
--   getChildSwitch thisSwitch currentMode

-- prgmStructType :: LLT.Type -> [LLT.Type]
-- prgmStructType self =
--   [LLT.i32, LLT.i32, LLT.i1, LLT.ptr LLT.double, LLT.ptr (LLT.ptr self)]

-- emitProgramStructure
--   :: MonadModuleBuilder m
--   => MonadFix m => Supply Word -> [SwitchTree] -> m LLC.Constant
-- emitProgramStructure supply trees = do
--   let prgm self = LLT.StructureType { LLT.isPacked     = False
--                                     , LLT.elementTypes = prgmStructType self
--                                     }
--       zeroArray nm len ty = LL.globalMut (LL.Name nm)
--                                          aty
--                                          (LLC.AggregateZero aty)
--         where aty = LL.arrayType len ty
--   rec programType <- LL.typedef "prgm.struct" (Just (prgm programType))

--   let
--     LLT.NamedTypeReference programTypeName = programType
--     emitTree supply (SwitchTree mode maxArgLen subTrees) = do
--       subTrees <- traverse (uncurry emitTrees)
--                            (zip (toList (split supply)) subTrees)
--       let idx = extract supply
--       subTreesArray <- LL.globalMut
--         (LL.Name ("prgm.children." <> LL.fromShow idx))
--         (LL.arrayType (length subTrees) (LLT.ptr programType))
--         (LL.arrayConstant (LLT.ptr programType) subTrees)
--       argArray <- zeroArray ("prgm.args." <> LL.fromShow idx)
--                             maxArgLen
--                             LLT.double
--       pure LLC.Struct
--         { LLC.structName   = Just programTypeName
--         , LLC.isPacked     = False
--         , LLC.memberValues = [ LL.int32Constant mode -- Current mode
--                              , LL.int32Constant mode -- Previous mode
--                              , LL.bitConstant True
--                              , LL.constantArrayToPtr argArray
--                              , LL.constantArrayToPtr subTreesArray
--                              ]
--         }
--     emitTrees supply trees = do
--       trees <- traverse (uncurry emitTree) (zip (toList (split supply)) trees)
--       let idx = extract supply
--       treeArray <- LL.globalMut (LL.Name ("prgm.block." <> LL.fromShow idx))
--                                 (LL.arrayType (length trees) programType)
--                                 (LL.arrayConstant programType trees)
--       pure (LL.constantArrayToPtr treeArray)
--   emitTrees supply trees

-- emitModelStructure
--   :: MonadModuleBuilder m
--   => MonadFix m => Supply Word -> F.Model -> m LLC.Constant
-- emitModelStructure supply F.Model { F.modelRelations = rels } =
--   emitProgramStructure supply (concatMap generateProgramStructure rels)

-- modelMaxVar :: F.Model -> Int
-- modelMaxVar F.Model { F.modelOutput = output, F.modelRelations = rels } =
--   length output + go rels
--  where
--   maxVarIntroduced rel = case rel of
--     F.Let    locals rels     -> length locals + go rels
--     F.Switch _      branches -> getMax $ foldMap count branches
--       where count F.Branch { F.branchRelations = rels } = Max (go rels)
--     _ -> 0
--   go = getSum . foldMap (Sum . maxVarIntroduced)

-- modelMaxRoot :: F.Model -> Int
-- modelMaxRoot F.Model { F.modelRelations = rels } = countAllRoots rels
--  where
--   countRoots (F.Switch _ branches) = getMax $ foldMap (Max . count) branches
--    where
--     count F.Branch { F.branchConditions = conds, F.branchRelations = rels } =
--       countAllRoots rels + length conds
--   countRoots (F.Let _ rels) = countAllRoots rels
--   countRoots _              = 0

--   countAllRoots = getSum . foldMap (Sum . countRoots)

-- matrixRowType :: LLT.Type
-- matrixRowType = LL.structType
--   [ LLT.i32 -- row size
--   , LLT.ptr LLT.i32 -- row indices
--   , LLT.ptr LLT.i8 -- row entries (as integers)
--   , LLT.ptr LLT.double -- row entries (as doubles)
--   ]

-- matrixType :: LLT.Type
-- matrixType = LL.structType
--   [ LLT.i32 -- matrix size
--   , LLT.ptr matrixRowType
--   ]

-- eqnStruct :: F.Eqn -> Word
-- eqnStruct (F.GenEqn lhs rhs) = fromIntegral (length (F.freeSigVars lhs <> F.freeSigVars rhs))
-- eqnStruct (F.CausalEqn _ _) = 1
-- eqnStruct (F.NameEqn _ _ ) = 2

-- relStruct :: Seq F.Rel -> [Max Word]
-- relStruct Empty                    = []
-- relStruct (F.Rel F.Always eqn :<| rels) = Max (eqnStruct eqn) : relStruct rels
-- relStruct (F.Let _ lrels :<| rels) = relStruct (lrels <> rels)
-- relStruct (F.Switch { F.switchBranches = branches } :<| rels) = merge
--   []
--   (fmap (++ nextbrs) relbrs)
--  where
--   relbrs = toList $ fmap
--     (\F.Branch { F.branchRelations = brels } -> relStruct brels)
--     branches

--   nextbrs = relStruct rels

--   merge ks ((x : xs) :     (y : ys) : zs) = merge (ys : ks) ((x <> y : xs) : zs)
--   merge ks (xs       :     []       : zs) = merge ks (xs : zs)
--   merge ks ((        x : xs)        : []) = x : merge [] (xs : ks)
--   merge ks ([]                      : ys) = merge ks ys
--   merge [] []                             = []
--   merge ks []                             = merge [] ks
-- relStruct (_ :<| rels) = relStruct rels

-- emitSigMatrix :: MonadModuleBuilder m => MonadFix m => F.Model -> m LLC.Constant
-- emitSigMatrix model = do
--   let rowsMax = relStruct (F.modelRelations model)
--   rows     <- traverse (uncurry emitRow) $ zip [0 ..] rowsMax
--   rowArray <- LL.globalMut "__rows"
--                            (LL.arrayType (length rows) matrixRowType)
--                            (LL.arrayConstant matrixRowType rows)
--   LL.globalMut
--     "__signature_matrix"
--     matrixType
--     LLC.Struct
--       { LLC.structName   = Nothing
--       , LLC.isPacked     = False
--       , LLC.memberValues = [LL.int32Constant 0, LL.constantArrayToPtr rowArray]
--       }
--  where
--   -- maxVar       = modelMaxVar model
--   -- maxVarPerEqn = modelMaxVarPerEqn model

--   emitRow i (Max varct) = do
--     let emitArray nm ty = LL.constantArrayToPtr
--           <$> LL.globalMut nm aty (LLC.AggregateZero aty)
--           where aty = LL.arrayType varct ty
--         makeName nm = LL.Name (nm <> "." <> LL.fromShow i)

--     rowIndices       <- emitArray (makeName "__row_indices") LLT.i32
--     rowEntries       <- emitArray (makeName "__row_entries") LLT.i8
--     rowDoubleEntries <- emitArray (makeName "__row_double_entries") LLT.double
--     pure LLC.Struct
--       { LLC.structName   = Nothing
--       , LLC.isPacked     = False
--       , LLC.memberValues = [ LL.int32Constant 0
--                            , rowIndices
--                            , rowEntries
--                            , rowDoubleEntries
--                            ]
--       }

-- NOTE: could the headVarArray and reinit array be merged into one, if adding a
-- reinitialised variable simply meant setting its head value to 0 ?
-- userDataFields :: LLT.Type -> [(LLT.Type, LL.Name)]
-- userDataFields prgmStructType =
--   [ ( LLT.i32
--     , "out.var.ct"
--     ) -- Number of output variables
--   , ( LLT.ptr (LLT.ptr LLT.i8)
--     , "out.var.names"
--     ) -- Names of output variables
--   , (LLT.ptr LLT.i64, "prev.offset")
--   , (LLT.ptr LLT.i64, "sol.offset")
--   , ( LLT.i1
--     , "first.activation"
--     )  -- is this the first activation?
--   , ( LLT.ptr LLT.double
--     , "sol.vec"
--     ) -- solution vector pointer
--   , ( LLT.ptr LLT.i64
--     , "head.var"
--     ) -- head variable array
--   , ( LLT.ptr LLT.i64
--     , "var.hod"
--     ) -- array containing the value for the
--                     -- index of each head variables of each variable
--   , ( LLT.ptr LLT.i1
--     , "var.reinit"
--     )  -- array informing if a variable has been reinitializd
--   , ( LLT.ptr LLT.i64
--     , "eqn.hod"
--     ) -- equation HOD array
--   , (prgmStructType    , "prgm.type")
--   , (LLT.ptr (internalError "matrix.type"), "sig.matrix")
--   , ( LLT.ptr LLT.i32
--     , "root.dir"
--     ) -- root direction array
--   , (LLT.ptr LLT.i32, "root.vec")
--   ]

-- userDataType :: LLT.Type -> LLT.Type
-- userDataType = LL.structType . fmap fst . userDataFields

-- getUserDataField
--   :: MonadModuleBuilder m
--   => MonadIRBuilder m => Integral i => i -> Operand -> m Operand
-- getUserDataField field residualData = do
--   fieldPtr <- LL.ibgep residualData [LL.int32 0, LL.int32 (fromIntegral field)]
--   LL.load fieldPtr 0

-- setUserDataField
--   :: MonadModuleBuilder m
--   => MonadIRBuilder m => Integral i => i -> Operand -> Operand -> m ()
-- setUserDataField field residualData val = do
--   fieldPtr <- LL.ibgep residualData [LL.int32 0, LL.int32 (fromIntegral field)]
--   LL.store fieldPtr 0 val

-- getOutputVarCt, getOutputVarNames, getPrevVarOffset, getInitVarOffset, getVarOffset, getSolOffset, getFirstActive, getSolPtr, getHeadVarArray, getVarHODArray, getReinitArray, getEqnHODArray, getPrgmStruct, getSigMat, getRootDirArray, getRootArray
--   :: MonadModuleBuilder m => MonadIRBuilder m => Operand -> m Operand
-- [getOutputVarCt, getOutputVarNames, getPrevVarOffset, getInitVarOffset, getVarOffset, getSolOffset, getFirstActive, getSolPtr, getHeadVarArray, getVarHODArray, getReinitArray, getEqnHODArray, getPrgmStruct, getSigMat, getRootDirArray, getRootArray]
--   = [ getUserDataField field | field <- [0 .. 15] ]


-- setOutputVarCt, setOutputVarNames, setPrevVarOffset, setInitVarOffset, setVarOffset, setSolOffset, setFirstActive, setSolPtr, setHeadVarArray, setVarHODArray, setReinitArray, setEqnHODArray, setPrgmStruct, setSigMat, setRootDirArray, setRootArray
--   :: MonadModuleBuilder m => MonadIRBuilder m => Operand -> Operand -> m ()
-- [setOutputVarCt, setOutputVarNames, setPrevVarOffset, setInitVarOffset, setVarOffset, setSolOffset, setFirstActive, setSolPtr, setHeadVarArray, setVarHODArray, setReinitArray, setEqnHODArray, setPrgmStruct, setSigMat, setRootDirArray, setRootArray]
--   = [ setUserDataField field | field <- [0 .. 15] ]

-- emitUserData
--   :: MonadModuleBuilder m => MonadFix m => Supply Word -> F.Model -> m Operand
-- emitUserData supply model@F.Model { F.modelOutput = output } = do
--   let maxVarLenArray nm ty val = LL.constantArrayToPtr <$> LL.globalMut
--         nm
--         (LL.arrayType maxVar ty)
--         (LL.arrayConstant ty (replicate maxVar val))

--       outputVarCt = length output
--   outputVarNamesPtrList <- sequence
--     [ LL.globalStringPtr (T.unpack (F.source out))
--                          (LL.Name ("__output_name." <> LL.fromShow idx))
--     | (idx, out) <- zip [0 ..] (toList output)
--     ]
--   outputVarNames <- LL.constantArrayToPtr <$> LL.globalMut
--     "__output_var_names"
--     (LL.arrayType outputVarCt (LLT.ptr LLT.i8))
--     (LL.arrayConstant (LLT.ptr LLT.i8) outputVarNamesPtrList)
--   prevVarOffset <- maxVarLenArray "__prev_var_offset"
--                                   LLT.i64
--                                   (LL.int64Constant 0)
--   varOffset     <- maxVarLenArray "__var_offset" LLT.i64 (LL.int64Constant 0)
--   initVarOffset <- maxVarLenArray "__init_var_offset"
--                                   LLT.i64
--                                   (LL.int64Constant 0)
--   solOffset <- maxVarLenArray "__sol_offset" LLT.i64 (LL.int64Constant 0)
--   solVector <- -- maxVarLenArray "__sol_vector" LLT.double (LL.doubleConstant 0)
--     let ty = LL.arrayType (10 * maxVar) LLT.double
--     in  LL.constantArrayToPtr
--           <$> LL.globalMut "__sol_vector" ty (LLC.AggregateZero ty)
--   headVar     <- maxVarLenArray "__head_var" LLT.i64 (LL.int64Constant 0)
--   varHOD      <- maxVarLenArray "__var_hod" LLT.i64 (LL.int64Constant 0)
--   reinitArray <- maxVarLenArray "__reinit_array" LLT.i1 LL.trueConstant
--   eqnHOD      <- maxVarLenArray "__eqn_hod" LLT.i64 (LL.int64Constant 0)
--   prgmStruct  <- emitModelStructure supply model
--   sigMatrix   <- emitSigMatrix model
--   let rootArrayType = LL.arrayType maxRoot LLT.i32
--   rootDirArray <- LL.constantArrayToPtr <$> LL.globalMut
--     "__root_dir_array"
--     rootArrayType
--     (LLC.AggregateZero rootArrayType)
--   rootArray <- LL.constantArrayToPtr <$> LL.globalMut
--     "__root_array"
--     rootArrayType
--     (LLC.AggregateZero rootArrayType)
--   let userData = LLC.Struct
--         { LLC.isPacked     = False
--         , LLC.structName   = Nothing
--         , LLC.memberValues = [ LL.int32Constant (length output)
--                              , outputVarNames
--                              , prevVarOffset
--                              , varOffset
--                              , initVarOffset
--                              , solOffset
--                              , LL.bitConstant True
--                              , solVector
--                              , headVar
--                              , varHOD
--                              , reinitArray
--                              , eqnHOD
--                              , prgmStruct
--                              , sigMatrix
--                              , rootDirArray
--                              , rootArray
--                              ]
--         }
--   LL.global Name.userData (LLT.typeOf userData) userData
--  where
--   maxVar  = modelMaxVar model
--   maxRoot = modelMaxRoot model
