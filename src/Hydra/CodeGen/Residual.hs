{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecursiveDo #-}

module Hydra.CodeGen.Residual
  ( compileSig
  , residualMethod
  , requestMethod
  ) where

import           Control.Arrow
import           Control.Lens            hiding ( Const )
import           Control.Monad.Reader    hiding ( local )
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Foldable
import           Data.Generics.Labels           ( )
import           Data.IntMap                    ( IntMap )
import qualified Data.IntMap                   as IM
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Maybe
import           Data.Ratio
import           Data.Sequence                  ( Seq )
import qualified Data.Sequence                 as Seq
import           Debug.Trace
import           GHC.Generics
import           Hydra.CodeGen.ErrorCode
import           Hydra.CodeGen.IIR
import           Hydra.CodeGen.Method
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.Residual.Request
import           Hydra.CodeGen.Scope
import           Hydra.CodeGen.SimplExpr
import           Hydra.CodeGen.Type
import           Hydra.Config
import           Hydra.Error                    ( internalError )
import           Hydra.Flatten.AST              ( Holds(Always, AtInit) )
import qualified Hydra.LLVM                    as LL
import           Hydra.OOIR
import qualified Hydra.Op                      as Op
import           Hydra.Op                       ( ArithBinOp(..)
                                                , ArithUnOp(..)
                                                , Op(..)
                                                , derOp
                                                )
import           Hydra.Pretty
import qualified LLVM.AST.IntegerPredicate     as LL
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.ParameterAttribute   as LL
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.AST.Typed                as LLT
import qualified LLVM.IRBuilder.Constant       as LL
import qualified LLVM.IRBuilder.Monad          as LL
import           LLVM.Pretty                    ( )
import           Numeric.Natural
import           Prelude                 hiding ( div
                                                , sum
                                                )

-- Compile a signal to an operand that computes the result
compileSig :: ModelScope -> Operand -> Sig -> CGenM Operand
compileSig scope _ (SigExpr expr) = pure $ compileSimplExpr scope expr
compileSig scope input (Local (LocalSig i n)) = do
  is <- LL.loadFrom input (lookupSig i scope)
  LL.loadFrom is (constDiff n)
compileSig scope input (Op op) =
  compileOp =<< traverse (compileSig scope input) op

-- A compiled signal is either some constant (the operand is bundled with the
-- Expr node, so as to check if it's a known constant) or a pointer to the
-- computed derivatives of the expression (the 0-th derivative is at index 0,
-- the first at index 1, etc.)
data CompiledSig m = ConstSig AtomExpr (Promise m Operand) | ArraySig (Promise m Operand)

extractNthDer
  :: MonadLoop m => CompiledSig m -> Promise m (Operand -> CGenM Operand)
extractNthDer (ConstSig _ c) =
  withPromise
    $      UseP
             (\c n -> do
               isZero <- LL.icmp LL.EQ n (constDiff 0)
               LL.select isZero c (LL.double 0)
             )
    `AppP` c
extractNthDer (ArraySig ptr) = withPromise $ UseP LL.loadFrom `AppP` ptr

-- Compiles a signal to a CompiledSig. This should only be used for subterms of
-- an expression as to avoid needless allocations
compileParametricDerivativeOfSubTerm :: MonadLoop m => Sig -> m (CompiledSig m)
compileParametricDerivativeOfSubTerm (SigExpr expr) = do
  cexpr <- execCGenM $ \_ scope -> pure $ compileSimplExpr scope expr
  pure (ConstSig expr cexpr)
compileParametricDerivativeOfSubTerm (Local (LocalSig i k)) = do
  ptr <- execCGenM $ \input scope -> do
    let index = lookupSig i scope
    iptr <- LL.loadFrom input index
    case k of
      0 -> pure iptr
      n -> LL.named (LL.ibgep iptr [constDiff n])
                    (LL.fromIdent i <> ".offset." <> LL.fromShow k)
  pure (ArraySig ptr)
compileParametricDerivativeOfSubTerm (Op op) = do
  op <- traverse compileParametricDerivativeOfSubTerm op
  ArraySig <$> memoizing (arithOpParametricDerivative op)

-- Compiled a signal to an action that produces a function that can compute the
-- i-th derivative of an expression. The resulting function can only be used to
-- compute the 0-th derivative, then the first, the the second, etc. No step may
-- be skipped otherwise the state of the intermediate arrays being allocated
-- will not be updated correctly and catastrophe will ensue.
compileParametricDerivative
  :: MonadLoop m => Sig -> m (Promise m (Operand -> CGenM Operand))
compileParametricDerivative (Op op) =
  arithOpParametricDerivative
    =<< traverse compileParametricDerivativeOfSubTerm op
compileParametricDerivative sig =
  extractNthDer <$> compileParametricDerivativeOfSubTerm sig

arithOpParametricDerivative
  :: MonadLoop m
  => Op (CompiledSig m)
  -> m (Promise m (Operand -> CGenM Operand))
arithOpParametricDerivative (ArithUnOp op p) = unOpParametricDerivative op p
arithOpParametricDerivative (ArithBinOp op p q) =
  binOpParametricDerivative op p q

binOpParametricDerivative
  :: MonadLoop m
  => ArithBinOp
  -> CompiledSig m
  -> CompiledSig m
  -> m (Promise m (Operand -> CGenM Operand))
binOpParametricDerivative op (ConstSig _ p) (ConstSig _ q) = pure $ withPromise
  (      UseP
      (\p q n -> do
        expr <- compileOp (Op.binop op p q)
        [iir| if $n == 0 { $$expr } else { 0.0 } |]
      )
  `AppP` p
  `AppP` q
  )
binOpParametricDerivative Add p q = pure
  $ withPromise (UseP addP `AppP` extractNthDer p `AppP` extractNthDer q)
 where
  addP p q n = do
    pn <- p n
    qn <- q n
    compileOp (Op.add pn qn)
binOpParametricDerivative Sub p q = pure
  $ withPromise (UseP subP `AppP` extractNthDer p `AppP` extractNthDer q)
 where
  subP p q n = do
    pn <- p n
    qn <- q n
    compileOp (Op.sub pn qn)
binOpParametricDerivative Mul (ConstSig _ p) (ArraySig q) = pure
  $ withPromise (UseP mulP `AppP` p `AppP` q)
  where mulP p q n = [iir| $$p * !$$[]q[$n] |]
binOpParametricDerivative Mul (ArraySig p) (ConstSig _ q) = pure
  $ withPromise (UseP mulP `AppP` p `AppP` q)
  where mulP p q n = [iir| !$$[]q[$n] * $$p |]
binOpParametricDerivative Mul (ArraySig p) (ArraySig q) = pure
  $ withPromise (UseP mulP `AppP` p `AppP` q)
 where
  mulP p q n = [iir|
    sum for i from 0 to $n included {
      binom($n, i) * !$$[]p[i] * !$$[]q[$n .- i]
    }
    |]
binOpParametricDerivative Div (ArraySig p) (ConstSig _ q) = pure
  $ withPromise (UseP divP `AppP` p `AppP` q)
  where divP p q n = [iir| !$$[]p[$n] / $$q |]
binOpParametricDerivative Div p (ArraySig q) = do
  invQ <- memoizing $ constPowDerivative q (KnownInt (-1))
  binOpParametricDerivative Mul p (ArraySig invQ)
binOpParametricDerivative Pow (ArraySig p) (ConstSig e q) = case e of
  Const ratio | Just q <- isInt ratio -> constPowDerivative p (KnownInt q)
  _ -> constPowDerivative p (OtherExp q)
binOpParametricDerivative Pow (ConstSig _ p) (ArraySig q) = do
  -- The n-th derivative of \t. p^t, where p is a constant is given by
  -- ln(p)^n * p^t
  lnp <- allocate (ConstRequest 1)
  let lnn p q lnp ps n = do
        [iir| if $n == 0 {
                $$[]ps[0] := $$p ^ !$$[]q[0];
              } elseif $n == 1 {
                $$[]lnp := log($$p);
                $$[]ps[1] := !$$[]ps[0] * !$$[]lnp;
              } else {
                $$[]ps[$n] := !$$[]ps[$n .- 1] * !$$[]lnp;
              };
              $$[]ps[$n]
            |]
  ps <- memoizeWith $ withPromise (UseP lnn `AppP` p `AppP` q `AppP` lnp)
  unaryComposition ps q
binOpParametricDerivative Pow p@(ArraySig _) q@(ArraySig _) = do
  -- Compile p ^ q as exp(q * log(p))
  logP  <- ArraySig <$> memoizing (unOpParametricDerivative Log q)
  qLogP <- ArraySig <$> memoizing (binOpParametricDerivative Mul p logP)
  unOpParametricDerivative Exp qLogP

isInt :: Rational -> Maybe Integer
isInt ratio | denominator ratio == 1 = Just (numerator ratio)
            | otherwise              = Nothing

data Exponent m = KnownInt Integer | OtherExp (Promise m Operand)

-- Computes f^{(n)}(g(t)), where f(t) = t^k and k is a constant. The n-th
-- derivative of f is given by product of t^{k - n} and the falling factorial
-- from k to k - n + 1.
--
-- This allows for a nice recursive computation formula: f^{(n)}(g) = (k - n +
--     1) * f^{(n - 1)}(g) / g.
--
-- The problem here is what happens when g(t) is zero. There, the value of the
-- derivative should be 0 but it will be computed as NaN. We could check that g
-- is non-zero, but I'm not too keen on that: floating-point numbers are weird,
-- and g is « not quite zero », we'll just get an enormous value for the
-- derivative, which is not really better.
--
-- There are two alternatives: the first, is to do away with the recursive
-- formula and just compute g^(k - n) at every step but that's SUPER slow (see
-- the Electronics paper), it's a complete no-go to compute the derivative of
-- division or integer powers in general, because that makes the implicit
-- formulation so much slower compared to what one obtains with repeatedly
-- applying first-order automatic differentiation. The alternative then is to do
-- the calculation in reverse, instead of computing f^{(n)} in terms of f^{(n -
-- 1)}, compute f^{(n - 1)} in terms of f^{(n)}. Since we know how many times we
-- have to differentiate, we can just compute g^(k - n) at the start and then
-- multiply back by g to find the correct derivative. The problem is that this
-- goes against the general structure of the code but I don't really see another
-- solution.
--
-- So, what this function does is produce an array that stores all the values of
-- f^{(n)}(g), that is filled partially in reverse to avoid recomputation
-- problems. NOW, one has to be careful: at the point were this function is
-- called, the p array is not filled so p[0] is not necessarily known. For that
-- reason, we do the filling of the array thing when the function that is
-- returned in the CGenM monad is called with index 0.
--
-- In detail, we start by computing p[0]^(k - n), which is the exponent required
-- for k differentiation. As an optimisation, if this function is called with a
-- known natural number, this computation is done with powi (instead of powf)
-- and is not done at all if n >= k, since it will give 1 for n = k and all the
-- derivative after the k-th derivative will be zero. For that reason, after
-- doing that computation we also return the stop bound after which we now that
-- p[0]^(k - n) is zero. It's not n if k is a known natural number less than n,
-- in which case the stop bound is not n but k.
constPowDerivative
  :: MonadLoop m
  => Promise m Operand
  -> Exponent m
  -> m (Promise m (Operand -> CGenM Operand))
constPowDerivative p exponent = do
  n <- getHOD
  let k = case exponent of
        KnownInt k -> cheapPromise (constDiff k)
        OtherExp k -> k
  storage <- memoizeWith (withPromise (UseP step `AppP` n `AppP` p `AppP` k))
  unaryComposition storage p
 where
  step hod p k storage n = do
    isZero <- LL.intIs 0 n
    LL.ite isZero (step0 hod p k storage) [iir|!$$[]storage[$n]|]

  computePkn n k p0 = case exponent of
    -- When k is a known integer, we can compute the p[0]^(k - n) using
    -- powi. Additionally, when k >= 0 and n >= k, we don't even have to use
    -- powi. Since the k-th derivative of t^k is one (and therefore the (k +
    -- 1)-th is 0). This function therefore returns either 1 or p[0]^(k -
    -- n). It also returns the index after which the derivatives of t^k are
    -- known to be 0 (this is n in general, or k if n >= k and k is a
    -- natural number)
    KnownInt k -> do
      let withPowi = do
            kmn  <- LL.sub (constDiff k) n
            pkmn <- powi p0 kmn
            pure (pkmn, n)
      if k >= 0
        then do
          isNgeK <- LL.icmp LL.UGE n (constDiff k)
          LL.ite isNgeK (pure (LL.double 1, constDiff k)) withPowi
        else withPowi
    OtherExp _ -> do
      realN <- LL.uitofp n LLT.double
      kmn   <- LL.fsub k realN
      pkmn  <- compileOp (Op.pow p0 kmn)
      pure (pkmn, n)

  step0 n p k storage = do
    -- This is what is run when we call the function with index 0. There, we
    -- fill the storage array with the successive values of p[0]^(k - n). We
    -- start at the back with the value that is computed with computePkn,
    -- then we fill it up by multiplying with p[0]
    p0           <- LL.loadFrom p (constDiff 0)
    (pkmn, stop) <- computePkn n k p0
    [iir|
      let acc := alloc(real, 1) in
      acc := 1.0;
      for i from 0 to $stop {
        $$[]storage[i] := !acc;
        acc := !acc * int2double($k .- i);
      };
      for i from $stop to $n included {
        $$[]storage[i] := 0.0;
      };
      -- storage[i] now contains the product k * (k - 1) * ... * (k - i)
      -- We just have to multiply that by p[0]^(k - i)
      acc := $$pkmn;
      for i from $stop to 0 included step (0 .- 1) {
        $$[]storage[i] := !$$[]storage[i] * !acc;
        acc := $$p0 * !acc;
      };

      !$$[]storage[0]
    |]

-- This function computes the n-th derivative of f(p) by computing as (n-1)
-- derivatiof f'(p)
withFirstDerivative
  :: MonadLoop m
  => ArithUnOp
  -> Promise m Operand
  -> m (Promise m (Operand -> CGenM Operand))
withFirstDerivative op p = do
  let
    der = derOp op p
    fromOpTree (Op.OpLeaf  leaf ) = pure (ArraySig leaf)
    fromOpTree (Op.OpConst const) = pure
      (ConstSig (Const const) (cheapPromise (LL.double (fromRational const))))
    fromOpTree (Op.OpNode op) = ArraySig
      <$> memoizing (arithOpParametricDerivative =<< traverse fromOpTree op)

  firstDerParametricDerivative <- decrHOD $ fromOpTree der
  let step fd p n = do
        isZero <- LL.intIs 0 n
        LL.ite
          isZero
          (do
            p0 <- LL.loadFrom p (constDiff 0)
            compileOp (Op.unop op p0)
          )
          (fd =<< LL.decr n)
  pure $ withPromise
    (UseP step `AppP` extractNthDer firstDerParametricDerivative `AppP` p)

unOpParametricDerivative
  :: MonadLoop m
  => ArithUnOp
  -> CompiledSig m
  -> m (Promise m (Operand -> CGenM Operand))
unOpParametricDerivative op (ConstSig _ p) = pure
  $ withPromise (UseP step `AppP` p)
 where
  step p n = do
    expr <- compileOp (Op.unop op p)
    [iir| if $n == 0 { $$expr } else { 0.0 } |]
unOpParametricDerivative Exp (ArraySig p) = do
  -- Computing exp(p(t)) can be done using a variant of Fàa di Bruno's formula.
  -- Because the n-th derivative of exp is itself, it can be factored out of the
  -- sum and we just get:
  --
  -- exp(p(t))^(n) = exp(t) * sum_{i = 1}^{n} B_n(p)
  --
  -- Where B_n = \sum_{i = 0}^{n - 1} B_{n - 1 - i}(p) * p^(i), these are the
  -- complete Bell polynomials
  exp0 <- allocate (ConstRequest 1)
  bell <- allocateDefault
  -- LL.storeAt bell (constDiff 0) (LL.double 1)
  let step p exp0 bell n = [iir|
              let B := $$[]bell in
              if $n == 0 {
                B[0] := 1.0;
                $$[]exp0 := exp(!$$[]p[0]);
              };

              B[$n] := sum for i from 1 to $n included {
                binom($n .- 1, i .- 1) * !B[$n .- i] * !$$[]p[i]
              };

              !$$[]exp0 * !B[$n]
            |]
  pure $ withPromise (UseP step `AppP` p `AppP` exp0 `AppP` bell)
unOpParametricDerivative Sqrt (ArraySig p) =
  constPowDerivative p (OtherExp (cheapPromise (LL.double 0.5)))
unOpParametricDerivative Sin (ArraySig p) = do
  let mkSin p sins n = [iir|
              let T := $$[]sins in
              if $n == 0 {
                T[0] := sin(!$$[]p[0]);
              } elseif $n == 1 {
                T[1] := cos(!$$[]p[0]);
              } elseif $n == 2 {
                T[2] := 0.0 - !T[0];
              } elseif $n == 3 {
                T[3] := 0.0 - !T[1];
              } else {
                T[$n] := !T[$n .- 4];
              };
              !T[$n]
            |]
  sins <- memoizeWith (withPromise (UseP mkSin `AppP` p))
  unaryComposition sins p
unOpParametricDerivative Cos (ArraySig p) = do
  let mkCos p coss n = [iir|
        let T := $$[]coss in
        if $n == 0 {
          T[0] := cos(!$$[]p[0]);
        } elseif $n == 1 {
          T[1] := 0.0 - sin(!$$[]p[0]);
        } elseif $n == 2 {
          T[2] := 0.0 - !T[0];
        } elseif $n == 3 {
          T[3] := 0.0 - !T[1];
        } else {
          T[$n] := !T[$n .- 4];
        };
        !T[$n]
      |]
  coss <- memoizeWith (withPromise (UseP mkCos `AppP` p))
  unaryComposition coss p
unOpParametricDerivative Sinh (ArraySig p) = do
  let mkSinH p sinhs n = [iir|
        let T := $$[]sinhs in
        if $n == 0 {
          T[0] := sinh(!$$[]p[0])
        } elseif $n == 1 {
          T[1] := cosh(!$$[]p[0])
        } else {
          T[$n] := !T[$n .- 2]
        };

        !T[$n]
        |]
  sinhs <- memoizeWith (withPromise (UseP mkSinH `AppP` p))
  unaryComposition sinhs p
unOpParametricDerivative Cosh (ArraySig p) = do
  let mkCosH p coshs n = [iir|
        let T := $$[]coshs in
        if $n == 0 {
          T[0] := cosh(!$$[]p[0])
        } elseif $n == 1 {
          T[1] := sinh(!$$[]p[0])
        } else {
          T[$n] := !T[$n .- 2]
        };

        !T[$n]
        |]
  coshs <- memoizeWith (withPromise (UseP mkCosH `AppP` p))
  unaryComposition coshs p
unOpParametricDerivative Tan (ArraySig p) = do
  -- We use the tangent numbers to combute the n-th derivative of tan as a
  -- polynomial of tan(p[0])
  tans <- allocate (RequestDefault <> ConstRequest 1)
  let step tans p n = [iir|
              let T := $$[]tans in
              if $n == 0 {
                T[0] := 1.0;
                T[1] := tan(!$$[]p[0])
              } else {
                T[$n .+ 1] := !T[1] * !T[$n]
              };

              sum for i from 1 to ($n .+ 1) included {
                tannum($n, i) * !T[i]
              }
            |]
  ts <- memoize (withPromise (UseP step `AppP` tans `AppP` p))
  unaryComposition ts p
unOpParametricDerivative Tanh  p            = undefined
unOpParametricDerivative Log   (ArraySig p) = withFirstDerivative Log p
unOpParametricDerivative Asin  (ArraySig p) = withFirstDerivative Asin p
unOpParametricDerivative Acos  (ArraySig p) = withFirstDerivative Acos p
unOpParametricDerivative Atan  (ArraySig p) = withFirstDerivative Atan p
unOpParametricDerivative Acosh (ArraySig p) = withFirstDerivative Acosh p
unOpParametricDerivative Asinh (ArraySig p) = withFirstDerivative Asinh p
unOpParametricDerivative Atanh (ArraySig p) = withFirstDerivative Atanh p
unOpParametricDerivative Abs   (ArraySig p) = do
  let abs p n = [iir|
          if $n == 0 {
            abs(!$$[]p[0])
          } elseif $n == 1 {
            sgn(!$$[]p[0])
          } else {
            0.0
          }
        |]
  absp0 <- memoize (withPromise (UseP abs `AppP` p))
  unaryComposition absp0 p
unOpParametricDerivative Sgn (ArraySig p) = do
  let sgn p n = [iir|
          if $n == 0 {
            sgn(!$$[]p[0])
          } else {
            0.0
          }
        |]
  sgn0 <- memoize (withPromise (UseP sgn `AppP` p))
  unaryComposition sgn0 p

unaryComposition
  :: MonadLoop m
  => Promise m Operand -- \i -> f^(i)(g(t))
  -> Promise m Operand -- \i -> g^(i)
  -> m (Promise m (Operand -> CGenM Operand))
unaryComposition f g = do
  -- Allocates 1 + 2 + ... + (n + 1) slots
  --
  -- To access the bell polynomials for n, one may simply compute 1 + 2 + ... +
  -- i = i * (i - 1) / 2
  bell <- allocateCompose

  let step f g bell n = [iir|
              let f := $$[]f in
              let g := $$[]g in
              let B := $$[]bell in

              if $n == 0 {
                -- B[0][0] := 1
                B[0] := 1.0;
                !$$[]f[0]
              } else {
                -- Retrieve the n-th Bell polynomials
                let Bn := B[($n .* ($n .+ 1)) ./ 2] in

                -- Special case for k = 1, where B[n][1] is known to be g^{(n)}
                Bn[1] := !g[$n];

                let sm := sum for k from 2 to $n included {
                    Bn[k] := sum for i from 1 to ($n .- k .+ 1) included {
                      -- Retrive the (n - i)-th Bell polynomial
                      let Bnmi := B[(($n .- i) .* ($n .- i .+ 1)) ./ 2] in
                      binom($n .- 1, i .- 1) * !g[i] * !Bnmi[k .- 1]
                    };
                    !f[k] * !Bn[k]
                  } in
                !f[1] * !g[$n] + sm
              }
            |]
  pure $ withPromise (UseP step `AppP` f `AppP` g `AppP` bell)

data Unfold = Unfold
  { unfoldOnlyUpTo      :: Maybe Word
  , maxDifferentiations :: Word
  }

-- Given a maximum value for n, given by the Unfold argument, generates code
-- that is able that computes the first n derivatives of an expression given the
-- function in argument. The unfoldOnlyUpTo member in the Unfold argument gives
-- how this computation is performed. If its Just k, then the computation for
-- the derivatives from 0 to k are done explicitly, it then falls back to a for
-- loop for further derivatives. If Nothing is passed in, all the derivatives
-- upto maxDifferentiations are computed with the explicit form. Note that at
-- least one derivative is computed explicitly, since the check that falls back
-- to the implicit form is done further down the line.
unfoldParametricDerivative
  :: Bool
  -> Operand
  -> Operand
  -> (Operand -> CGenM Operand)
  -> Unfold
  -> Operand
  -> Word
  -> CGenM Operand
unfoldParametricDerivative useUODE atInit hod f unfold@(Unfold upto maxHOD) rs n
  = do
    val <- f (constDiff n)
    rs  <- storeResult useUODE (constDiff n) hod atInit rs val
    -- If n corresponds to the maximum number of HOD we can do, we can just stop
    -- there. We should have checked that hod <= maxHOD before being here anyway.
    if n == maxHOD
      then pure rs
      else do
        -- Otherwise, we check if n == hod. If it is, we return rs, otherwise
        -- either we keep unfolding or we switch to parametric mode (in case upto
        -- is Just m with m == n)
        done <- LL.intIs n hod
        LL.ite done (pure rs) $ case upto of
          Just m | m == n ->
            foldedParametricDerivative useUODE atInit hod f rs (n + 1)
          _ ->
            unfoldParametricDerivative useUODE atInit hod f unfold rs (n + 1)

storeResult
  :: Bool
  -> Operand
  -> Operand
  -> Operand
  -> Operand
  -> Operand
  -> CGenM Operand
storeResult False n hod atInit rs val = LL.named (LL.storeAndStep rs val) "rs"
storeResult True  n hod atInit rs val = do
  isLast <- LL.icmp LL.EQ n hod
  saveIt <- LL.or atInit isLast
  LL.ite saveIt (LL.named (LL.storeAndStep rs val) "rs") (pure rs)

foldedParametricDerivative
  :: Bool
  -> Operand
  -> Operand
  -> (Operand -> CGenM Operand)
  -> Operand
  -> Word
  -> CGenM Operand
foldedParametricDerivative useUODE atInit hod f rs n = do
  hodSucc <- LL.named (LL.incr hod) "hod_succ"
  (_, rs) <- LL.for Nothing (constDiff n) hodSucc rs $ \n rs -> do
    val <- f n
    storeResult useUODE n hod atInit rs val
  pure rs

execResidual
  :: ModelScope
  -> Operand
  -> Maybe Operand
  -> Sig
  -> Operand
  -> Operand
  -> Operand
  -> CGenM Operand
execResidual scope atInit higherIndex sig input storage rs = do
  conf <- conf
  -- From higher index, return the value of the HOD (0 if we don't want to
  -- compute n derivatives), maxIndex which is the maximum number of
  -- differentiations that is supported (either 0 when we don't need the n
  -- derivatives or indexLimit which is extracted from the configuration).
  -- Finally, returns an action that checks, in the case where we compute the
  -- derivatives, that hod <= indexLimit. Normally this should never be hit
  -- because the runtime should prevent such cases, but maybe it won't and also
  -- it gives some informations on the range of the hod for the LLVM optimiser.
  let (hod, maxIndex, checkNotTooManyDiff) = case higherIndex of
        Just hod ->
          ( hod
          , limit
          , LL.assertion (LL.icmp LL.ULE hod (constDiff limit))
                         (LL.int32 tooManyDifferentiations)
          )
          where limit = indexLimit (cgConf conf)
        Nothing -> (constDiff 0, 0, pure ())
      unfold | compileDerExplicitly (cgConf conf) = Unfold Nothing maxIndex
             | otherwise                          = Unfold (Just 0) maxIndex

      uode = useUODE (cgConf conf)
  checkNotTooManyDiff
  (LoopPromise exec, update) <- runLoopGenM
    (compileParametricDerivative sig)
    LoopGenEnv { inputSignals = input, modelScope = scope, hodIndex = hod }
    storage
  let step i = update i >> exec i
  unfoldParametricDerivative uode atInit hod step unfold rs 0

requestMethod
  :: Rel -> Method ComputeWorkingSpaceInput ComputeWorkingSpaceState
requestMethod (Eqn AtInit _  ) = mempty
requestMethod (Eqn Always sig) = Method $ do
  eqnHODArray   <- view (#userInput . #eqnHODArray)
  equationCount <- use (#userState . #equationCount)
  hod           <- LL.loadFrom eqnHODArray equationCount
  newRequest    <- lift
    $ requestSize hod (runRequestM (compileParametricDerivative sig))
  currentRequest <- use (#userState . #currentRequest)
  request        <- LL.named (LL.max currentRequest newRequest) "request"
  #userState . #currentRequest .= request
  equationCount <- LL.named (LL.incr equationCount) "equation.count"
  #userState . #equationCount .= equationCount
requestMethod (Let idts rels) = withLocals idts $ foldMap requestMethod rels
requestMethod (ModelCall args model) =
  withCall (computeWorkSpaceMethodRepr objectReprIndices) args model
requestMethod (Switch _ branches) =
  withSwitch (withSwitchBlock requestMethod) branches

residualMethod :: Rel -> Method ResidualInput ResidualState
residualMethod (Eqn AtInit sig) = Method $ do
  atInit         <- view (#userInput . #atInit)
  blockActivated <- view #firstBlockActivation
  counts         <- LL.and atInit blockActivated
  let exec = do
        scope <- view #methodScope
        input <- view (#userInput . #inputSignals)
        val   <- lift $ compileSig scope input sig
        rs    <- use (#userState . #resultPtr)
        lift $ printf ("init " ++ show (pretty sig) ++ " => %f\n") [val]
        LL.storeAndStep rs val
  assign (#userState . #resultPtr)
    =<< LL.ite counts exec (use (#userState . #resultPtr))
residualMethod (Eqn Always sig) = Method $ do
  eqnHODArray   <- view (#userInput . #eqnHODArray)
  equationCount <- use (#userState . #equationCount)
  hod           <- LL.loadFrom eqnHODArray equationCount
  scope         <- view #methodScope
  workingSpace  <- view (#userInput . #workingSpace)
  resultPtr     <- use (#userState . #resultPtr)
  input         <- view (#userInput . #inputSignals)
  atInit        <- view (#userInput . #atInit)
  resultPtr     <- lift
    $ execResidual scope atInit (Just hod) sig input workingSpace resultPtr
  equationCount <- LL.named (LL.incr equationCount) "equation.count"
  #userState . #resultPtr .= resultPtr
  #userState . #equationCount .= equationCount
residualMethod (Let idts rels) = withLocals idts $ foldMap residualMethod rels
residualMethod (ModelCall args model) =
  withCall (residualMethodRepr objectReprIndices) args model
residualMethod (Switch _ branches) =
  withSwitch (withSwitchBlock residualMethod) branches
