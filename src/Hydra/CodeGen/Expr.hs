{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

-- | This module is in charge of compiling model expressions to LLVM.
--
-- Models are compiled to an object (as in objects of OOP). Meaning we generate
-- constructor and a set of methods.
--
-- Everything that's needed is generated in a single pass over the AST
--
--  The object consists in:
--   * an environment pointer, which contains all the captured environment the
--     model needs to execute (typically, if a model is on the right-hand side
--     of a function declaration, the arguments will be captured here)
--   * the size of the environment,
--   * a description of the switching structure of the model,
--   * a pointer for every function needed to compute equations in the model,
--     that is:
--       * a pointer to the residual function,
--       * a pointer to the initial residual function,
--       * a pointer to a function that constructs the signature matrix for the
--         model,
--
-- * Calling residuals
--
-- As a reminder, variables are passed in a double array represented by a
-- double**. If x and y are the first and second variable in a model, then the
-- n-th derivative of x (resp. y) will be found in vars[0][n] (resp.
-- vars[1][n]).
--
-- The input to the residual is organized as follows:
--   * the pointer to the object storage is passed first,
--   * then the pointers to the signal arguments,
--   * a double*** pointer. The pointer contains a double** pointer that points
--     to the next available signals that hasn't been allocated yet. So when we
--     encounter let x in ..., x will be allocated there and the content of the
--     pointer will be bumped by 1. After the residual returns, this pointer
--     contains the pointer to the next available variable
--   * then the storage for the result pointer, of type double**. The pointer
--     contains the value of the pointer the residual needs to write to. After
--     execution of the residual, it contains the pointer to the next available
--     slot in the result pointer
--   * the storage for the equation count, of type i64*. Similar to the storage
--     for the result pointer, except it contains the number of equations.
--
-- For example, the residual for the model of the form:
--
-- model x, y, z with ....
--
-- will have type:
--
-- int @<name>(double* x, double* y, double* z, double*** vars, double** prs,
-- i64* eqn_count)
module Hydra.CodeGen.Expr
  ( compileModule
  , CompiledModule(..)
  ) where

import           Control.Lens            hiding ( Const
                                                , Empty
                                                )
import           Control.Monad
import           Control.Monad.State
import qualified Data.ByteString.Short         as BS
import           Data.Foldable
import           Data.Generics.Labels           ( )
import qualified Data.Map                      as M
import           Data.Map                       ( Map )
import           Data.Maybe
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import           Debug.Trace
import           GHC.Generics
import           Hydra.CodeGen.Method
import           Hydra.CodeGen.ModelState
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.Residual
import           Hydra.CodeGen.RootFinding
import           Hydra.CodeGen.Scope
import           Hydra.CodeGen.Signature
import           Hydra.CodeGen.SimplExpr
import           Hydra.CodeGen.Type
import           Hydra.Compiler
import qualified Hydra.Flatten.AST             as F
import           Hydra.Flatten.AST              ( TSig(..) )
import           Hydra.Ident
import qualified Hydra.LLVM.Utils              as LL
import           Hydra.Name
import           Hydra.OOIR
import           Hydra.Pretty
import qualified LLVM.AST                      as LL
import qualified LLVM.AST.Constant             as LLC
import           LLVM.AST.Operand        hiding ( Module )
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.AST.Typed                as LLT
import qualified LLVM.IRBuilder                as LL
import           LLVM.Pretty                    ( )

compileAtomExpr :: ExprScope -> AtomExpr -> Operand
compileAtomExpr _     (Const c      ) = LL.double (fromRational c)
compileAtomExpr scope (LocalExpr i _) = lookupLocalExpr i scope

compileNormExpr :: ExprScope -> NormExpr -> CGenM Operand
compileNormExpr scope (Call     call) = compileCall scope call
compileNormExpr scope (AtomExpr expr) = pure $ compileAtomExpr scope expr

compileExpr :: ExprScope -> Expr -> CGenM Operand
compileExpr scope (OpExpr op) = compileOp =<< traverse (compileExpr scope) op
compileExpr scope (NormExpr expr) = compileNormExpr scope expr
compileExpr scope (ModelObj obj) = compileObj scope obj

-- * Compiling calls
--
-- Functions and calls are compiled according to eval-apply model (see the paper
-- How to make a fast curry, by SPJ and Simon Marlowe).

compileCall :: ExprScope -> Call -> CGenM Operand
compileCall scope (KnownCall (F.Saturated sat) global args) = do
  args <- traverse (compileExpr scope) args
  case sat of
    0 -> do
      -- If the call is saturated, load the function pointer and perform a
      -- tail-call
      let funPtr = globalFunPtr (lookupGlobal global scope)
      LL.named (LL.callLLVMFun funPtr (InstArgs args))
               (LL.fromText (F.globalName global))
    n -> internalError "Unsaturated call"
compileCall scope (UnknownCall fun args) = internalError "Unknwon call"

compileInst :: GlobalScope -> Name -> Inst -> ModuleGenM GlobalEntry
compileInst globalScope name Inst { instArgs = args, instBody = body } = do
  idx <- freshGlobal
  let nm = LL.fromText name <> "_" <> LL.fromShow idx
  (fun@(LL.LLVMFun ptrOp), ()) <-
    codeGenFunction (LL.Name (nm <> "_fun")) (InstFun args (typeOf body))
      $ \(InstArgs ops) -> do
          let scope = ExprScope
                { localScope  = foldr
                                  (\(Arg idt _, op) env -> M.insert idt op env)
                                  []
                                  (Seq.zip args ops)
                , globalScope = globalScope
                }
          op <- compileExpr scope body
          pure (Just op, ())


  let LL.ConstantOperand ptr = ptrOp
      tlStruct               = LLC.Struct
        { LLC.structName   = Nothing
        , LLC.isPacked     = False
        , LLC.memberValues = [ LL.int64Constant topLevelFunTag
                             , LLC.BitCast { LLC.operand0 = ptr
                                           , LLC.type'    = LL.voidPtr
                                           }
                             ]
        }
  op <- LL.globalConstant (LL.Name nm) (LLT.typeOf tlStruct) tlStruct

  pure GlobalEntry { globalInstrPtr = op, globalFunPtr = fun }

compileDecl :: Decl -> StateT GlobalScope ModuleGenM ()
compileDecl MonoDecl { declName = nm, declInst = inst } = do
  globalScope <- get
  val         <- lift $ compileInst globalScope nm inst
  modify
    (\(GlobalScope scope) ->
      GlobalScope (M.insert nm (M.singleton mempty val) scope)
    )
compileDecl Template { declName = nm, templateInsts = insts } = do
  globalScope <- get
  insts       <- traverse (lift . compileInst globalScope nm) insts
  modify (\(GlobalScope scope) -> GlobalScope (M.insert nm insts scope))

data CompiledModule = CompiledModule
  { llvmBlob         :: LL.Module
  , boundIdentifiers :: GlobalScope
  }

instance Pretty CompiledModule where
  pretty CompiledModule { llvmBlob = blob, boundIdentifiers = GlobalScope ids }
    = nest ilvl ("[bound identifiers]" $+$ prettyIds ids)
      $+$ (nest ilvl "[LLVM module]" $+$ pretty blob)
   where
    prettyIds =
      M.foldrWithKey (\id insts block -> pinsts id insts $+$ block) ""

    pinsts id insts = nest
      ilvl
      (brackets (pretty id) $+$ lineBlock
        (M.mapWithKey
          (\subst (GlobalEntry op (LL.LLVMFun fun)) ->
            brackets (nest ilvl (pretty op)) $+$ pretty subst
          )
          insts
        )
      )

compileModule :: Module -> CompilerM CompiledModule
compileModule Module { moduleDecls = decls } = do
  (globalScope, llvmModule) <- codeGenModule
    ""
    (execStateT (traverse compileDecl decls) (GlobalScope []))
  pure CompiledModule { llvmBlob = llvmModule, boundIdentifiers = globalScope }

compileObjMethod
  :: GlobalScope -> Name -> LLT.Type -> ModelObj -> ModuleGenM LLC.Constant
compileObjMethod globalScope name envPtrTy obj@Obj { modelOutput = output } =
  do
    idx <- freshGlobal
    let
      mkName nm =
        LL.Name (LL.fromText name <> "_" <> nm <> "_" <> LL.fromShow idx)
      mkMethod
        :: (LL.Argument i, LL.Argument s, LL.Storable s, LL.Merge s)
        => BS.ShortByteString
        -> (Seq Ident -> ModelMethod i s)
        -> (Rel -> Method i s)
        -> ModuleGenM
             (LL.LLVMFun (ModelMethod i s) (ModelMethodArg i s))
      mkMethod nm mkFun method =
        withModelObj globalScope method (mkName nm) (mkFun output) envPtrTy obj

  -- We start by generating all the methods of the object
    constructorLLFun <- initializeObj compileExpr
                                      globalScope
                                      (mkName "constructor")
                                      envPtrTy
                                      obj
    sigMatLLFun       <- mkMethod "signature" mkSignatureMethod fillSigMethod
    residualLLFun     <- mkMethod "residual" mkResidualMethod residualMethod
    rootSetupLLFun    <- mkMethod "root_setup" mkRootSetupMethod rootSetupMethod
    computeRootsLLFun <- mkMethod "compute_roots"
                                  mkComputeRoots
                                  computeRootsMethod
    eventHandlerLLFun <- mkMethod "event_handler"
                                  mkEventHandlerState
                                  (eventHandlerMethod compileExpr)
    setSignalInfoLLFun <- mkMethod "set_signal_info"
                                   mkSetSignalInfoMethod
                                   setSignalInfoMethod
    computeWorkSpaceLLFun <- mkMethod "work_space"
                                      mkComputeWorkingSpaceMethod
                                      requestMethod
    copyInputsLLFun <- mkMethod "copy_inputs"
                                mkCopyInputsMethod
                                copyInputsMethod

  -- The record of function leaves in a global structure, a pointer to which
  -- is stored in the model representation
    declareMethodRecord
      (fmap (const TReal) output)
      (mkName "function_record")
      ObjectRepr { constructorFunRepr         = constructorLLFun
                 , signatureMethodRepr        = MethodFun sigMatLLFun
                 , residualMethodRepr         = MethodFun residualLLFun
                 , rootSetupMethodRepr        = MethodFun rootSetupLLFun
                 , computeRootsMethodRepr     = MethodFun computeRootsLLFun
                 , eventHandlerMethodRepr     = MethodFun eventHandlerLLFun
                 , setSignalInfoMethodRepr    = MethodFun setSignalInfoLLFun
                 , computeWorkSpaceMethodRepr = MethodFun computeWorkSpaceLLFun
                 , copyInputsMethodRepr       = MethodFun copyInputsLLFun
                 }


compileObj :: ExprScope -> ModelObj -> CGenM Operand
compileObj scope obj@Obj { modelCaptures = captures, modelOutput = output } =
  do

    let opEnv       = fmap (`lookupLocalExpr` scope) (M.keys captures)
        envTys      = fmap LLT.typeOf opEnv
        envStructTy = LL.structType envTys
        envPtrTy    = LLT.ptr envStructTy

  -- The record of function leaves in a global structure, a pointer to which
  -- is stored in the model representation
    functionRecord <- liftModuleGenM
      $ compileObjMethod (scope ^. #globalScope) "__anon_obj" envPtrTy obj
  -- Then we allocate the necessary space for the object. The object consists
  -- of a record of function of fixed-size (some 6 or so function pointers)
  -- and a struct containing the local identifiers captured in the model.
    rawEnvPtr <- if null envTys
      then pure (LL.nullPtr LLT.i8)
      else malloc (LL.constantOp (LL.constantSizeOf envStructTy))

    envPtr <- LL.bitcast rawEnvPtr envPtrTy

  -- Get the pointer on the environment struct and fill it
    sequence_
      [ do
          ptr <- LL.ibgep envPtr [LL.int32 0, LL.int32 idx]
          LL.store ptr 0 op
      | (idx, op) <- zip [0 ..] opEnv
      ]

    let outputTy = fmap (const TReal) output

    ModelType { modelStructType = modelStructType } <- liftModuleGenM
      $ declareModelType outputTy

    rawObjPtr <- malloc (LL.constantOp (LL.constantSizeOf modelStructType))

    objPtr    <- LL.bitcast rawObjPtr
      =<< liftModuleGenM (toLLTyp (TModel outputTy))

    funRecordPtrPtr <- LL.ibgep objPtr [LL.int32 0, LL.int32 0]
    LL.store funRecordPtrPtr 0 (LL.constantOp functionRecord)

    objEnvPtr <- LL.ibgep objPtr [LL.int32 0, LL.int32 1]
    LL.store objEnvPtr 0 rawEnvPtr

    pure objPtr

