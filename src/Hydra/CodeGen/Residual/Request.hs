{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}

module Hydra.CodeGen.Residual.Request where

import           Control.Lens
import           Control.Monad.Cont
import           Control.Monad.RWS.Strict
import           Control.Monad.Writer
import           GHC.Generics                   ( Generic )
import           Hydra.CodeGen.Monad
import           Hydra.CodeGen.Scope
import qualified Hydra.LLVM.Utils              as LL
import           LLVM.AST.Operand               ( Operand )

data Request =
    ConstRequest !Word           -- Request a constant amount of space
  | DecrHOD !Request             -- Decrease the value of the HOD by 1
  | RequestDefault               -- Request n + 1 elements
  | RequestCompose               -- Request 1 + 2 + ... + n + 1 elements, useful for composition
  | RequestAdd !Request !Request -- Add to requests together

instance Semigroup Request where
  ConstRequest n <> ConstRequest m = ConstRequest (n + m)
  ConstRequest 0 <> req            = req
  req            <> ConstRequest 0 = req
  r1             <> r2             = RequestAdd r1 r2

instance Monoid Request where
  mempty = ConstRequest 0

requestSize :: Operand -> Request -> CGenM Operand
requestSize _ (ConstRequest n      ) = pure (LL.int64 (fromIntegral n))
requestSize n (DecrHOD      request) = do
  n <- LL.decr n
  requestSize n request
requestSize n RequestDefault = LL.incr n
requestSize n RequestCompose = do
  -- The request here is 1 + 2 + ... + n + 1 = (n + 1) * (n + 2) / 2
  np1 <- LL.incr n
  np2 <- LL.incr np1
  num <- LL.mul np1 np2
  LL.udiv num (LL.int64 2)
requestSize n (RequestAdd p q) = do
  p <- requestSize n p
  q <- requestSize n q
  LL.add p q

-- Consider the expression x * y + z. Naively, one would compute the n-th
-- derivative with something like so (rs is the result array where we store the
-- result of the computation ): for (int i = 0; i <= n; ++i) {double leibniz =
-- 0; rs[i] = leibniz + z[i];
-- }
-- This formulation is fine here, because there is no redundant computations
-- happening. But imagine if we have (x * y) * z. Then, if we didn't store the
-- values of the derivatives of (x * y), we would have to recompute them at
-- every round of the loop: no good.
--
-- So instead we want to (like in first-order automatic differentiation) store
-- the result of these intermediate computations. So do something like this:
-- double xy[n + 1];
--
-- for (int i = 0; i <= n; ++i) {xy[i] = 0; for (int k = 0; k <= i; ++k) {xy[i]
--   += binom(i, k) * x[k] * y[i - k];
--   }
--   rs[i] = xy[i] + z[i]
-- }
-- This is to be able to write this sort of code easily that LoopGenM exists.
-- Well, both make it easy but also enable a high-level of control.
--
-- Indeed, in the example above, we allocated on the stack: that's not
-- necessarily a good idea for instance if the expression is really large or if
-- its expected to be differentiated a large number of times. The space needed
-- isn't always linear, for composition for instance, the space needed is
-- quadratic in the number of differentiation. So that might be an issue. Also,
-- it's not entirely clear to me how much LLVM can reuse stack space, so I'd
-- rather have control over that, since I know precisely when the values stored
-- become useless.
--
-- So allocation in LoopGenM is managed from a pointer, that is bumped by the
-- correct amount when the user requests some memory. So the example above
-- becomes something like:
--
-- double* xy = storage; storage += n + 1;
--
-- LoopGenM naturally also produces the actual size that's needed, as a function
-- of n, the highest-order derivative being considered. This way, the user can
-- emit too functions: one that allocates storage for the correct size, and one
-- that uses that storage.
--
-- Note that after having computed the n-th derivative of an expression, we can
-- reuse the storage, we don't have to allocate new storage at all! For that
-- reason, the only storage we need is the maximum storage space requested by
-- any relation (not the sum of all the storage), which is quite great.
--
-- About the implementation: LoopGenM is a wrapper around a RWST, with the HOD
-- (n) in the reader and memory requests being emitted in the Writer. In the
-- state is contained: the value of the HOD, a pointer to the next available
-- storage (storage in the C pseudo code above), the current size of the memory
-- being needed (using the Request data type) and an updating function.
--
-- What the updating function is needs a quick explanation.  Remember our
-- example x * y + z. When we consider x * y, we generate a function from
-- Operand -> CGenM Operand. Then, one level up, we ask for some space for xy
-- and we would like to use the function we generated to update the values in
-- xy. But we don't really want to have to remember to pass around all these
-- functions. So they are gathered in the updating function, which is in charge,
-- given i, to update the values stored in the intermediate arrays.
data LoopGenSt = LoopGenSt
  { updatingFunction :: Operand -> CGenM ()
  , memoryPointer    :: Operand
  }
  deriving Generic

data LoopGenEnv = LoopGenEnv
  { inputSignals :: Operand
  , modelScope   :: ModelScope
  , hodIndex     :: Operand
  }
  deriving Generic

newtype LoopGenM a = LoopGenM (RWST LoopGenEnv () LoopGenSt CGenM a)
  deriving ( Functor, Applicative, Monad, MonadFix, LL.MonadIRBuilder
           , LL.MonadModuleBuilder )

runLoopGenM
  :: LoopGenM a -> LoopGenEnv -> Operand -> CGenM (a, Operand -> CGenM ())
runLoopGenM (LoopGenM act) env ptr = do
  (a, LoopGenSt { updatingFunction = upd }, ()) <- runRWST
    act
    env
    LoopGenSt { updatingFunction = const (pure ()), memoryPointer = ptr }
  pure (a, upd)

data family Promise (m :: * -> *) a

newtype instance Promise LoopGenM a = LoopPromise a

instance Functor (Promise LoopGenM) where
  fmap f (LoopPromise x) = LoopPromise (f x)

class (Functor (Promise m), Monad m) => MonadLoop m where
  getHOD :: m (Promise m Operand)
  decrHOD :: m a -> m a
  allocate :: Request -> m (Promise m Operand)
  memoizingWith :: m (Promise m (Operand -> Operand -> CGenM Operand)) -> m (Promise m Operand)
  withPromise :: WithPromise m b -> Promise m b
  execCGenM :: (Operand -> ModelScope -> CGenM a) -> m (Promise m a)
  cheapPromise :: a -> Promise m a

data WithPromise m b where
  UseP ::b -> WithPromise m b
  AppP ::WithPromise m (a -> b) -> Promise m a -> WithPromise m b

instance MonadLoop LoopGenM where

  getHOD = LoopPromise <$> LoopGenM (view #hodIndex)

  decrHOD (LoopGenM act) = LoopGenM $ do
    nm1 <- LL.decr =<< view #hodIndex
    locally #hodIndex (const nm1) act

  allocate request = LoopGenM $ do
    n      <- view #hodIndex
    ptr    <- use #memoryPointer
    -- If the next available storage is at ptr, then we return ptr to the caller
    -- and the next available storage will be situated at ptr + requestSize
    -- request
    size   <- lift $ requestSize n request
    newPtr <- LL.ibgep ptr [size]
    #memoryPointer .= newPtr
    pure (LoopPromise ptr)

  withPromise with = LoopPromise (pullFun with)
   where
    pullFun :: WithPromise LoopGenM a -> a
    pullFun (UseP fun                ) = fun
    pullFun (AppP fun (LoopPromise a)) = pullFun fun a

  memoizingWith update = do
    LoopPromise update         <- update
    promise@(LoopPromise memo) <- allocateDefault
    LoopGenM $ modifying #updatingFunction $ \f i -> do
      f i
      pi <- update memo i
      LL.storeAt memo i pi
    pure promise

  execCGenM action = LoopGenM $ do
    input <- view #inputSignals
    scope <- view #modelScope
    LoopPromise <$> lift (action input scope)

  cheapPromise = LoopPromise

newtype RequestM a = RequestM (Writer Request a)
                   deriving(Functor, Applicative, Monad, MonadFix)

runRequestM :: RequestM a -> Request
runRequestM (RequestM req) = execWriter req

data instance Promise RequestM a = ILied

instance Functor (Promise RequestM) where
  fmap _ ILied = ILied

instance MonadLoop RequestM where
  getHOD = pure ILied
  decrHOD (RequestM action) = RequestM (censor DecrHOD action)
  allocate request = RequestM (tell request >> pure ILied)
  memoizingWith update = do
    ILied <- update
    allocate RequestDefault
  withPromise _ = ILied
  execCGenM _ = pure ILied
  cheapPromise _ = ILied

allocateDefault, allocateCompose :: MonadLoop m => m (Promise m Operand)
allocateDefault = allocate RequestDefault
allocateCompose = allocate RequestCompose

memoizeWith
  :: MonadLoop m
  => Promise m (Operand -> Operand -> CGenM Operand)
  -> m (Promise m Operand)
memoizeWith update = memoizingWith (pure update)

memoize
  :: MonadLoop m
  => Promise m (Operand -> CGenM Operand)
  -> m (Promise m Operand)
memoize update = memoizeWith (fmap const update)

memoizing
  :: MonadLoop m
  => m (Promise m (Operand -> CGenM Operand))
  -> m (Promise m Operand)
memoizing update = update >>= memoize

