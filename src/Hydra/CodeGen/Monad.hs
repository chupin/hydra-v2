{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}

{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

{-# LANGUAGE FlexibleContexts #-}
module Hydra.CodeGen.Monad
  ( ModuleGenM
  , CGenM
  , MemOpM
  , liftCompilerM
  , liftModuleGenM
  , liftCGenM
  , runMemOpM
  , memCompileOp
  , freshGlobal
  , codeGenFunction
  , codeGenModule
  , lookupBinom
  , lookupFactorial
  , lookupTangentNumber
  , constDiff
  , diffType
  , compileOp
  , powi
  , printf
  , malloc
  , free
  , wrongNumberOfArguments
  , getModelType
  , ModelType(..)
  ) where

import           Control.Comonad
import           Control.Lens
import           Control.Monad.Fix
import           Control.Monad.RWS.Strict
import           Control.Monad.Reader
import           Control.Monad.State.Strict
import           Data.ByteString.Short          ( ShortByteString )
import           Data.Generics.Labels           ( )
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Sequence                  ( Seq )
import qualified Data.Stream.Infinite          as Stream
import           GHC.Generics                   ( Generic )
import           Hydra.CodeGen.StaticData
import           Hydra.Compiler
import qualified Hydra.LLVM.Utils              as LL
import           Hydra.Op
import qualified LLVM.AST                      as LL
import qualified LLVM.AST.Constant             as LLC
import           LLVM.AST.FloatingPointPredicate
                                                ( FloatingPointPredicate
                                                  ( OEQ
                                                  , OGT
                                                  )
                                                )
import qualified LLVM.AST.Global               as LL
import qualified LLVM.AST.IntegerPredicate     as LL
import           LLVM.AST.Operand               ( Operand )
import qualified LLVM.AST.Type                 as LLT
import qualified LLVM.AST.Typed                as LLT
import           LLVM.IRBuilder.Module
import           LLVM.IRBuilder.Monad

wrongNumberOfArguments :: String -> Int -> [a] -> b
wrongNumberOfArguments function expected args = internalError $ unwords
  [ "Wrong number of arguments passed to function "
  , function
  , ". Expected "
  , show expected
  , " but got "
  , show (length args)
  , "."
  ]

data Powi = Intrinsic !Operand | Inline

data GenCtxt = GenCtxt
  { intFactorialArray         :: !Operand
  , doubleFactorialArray      :: !Operand
  , doubleChooseArray         :: !Operand
  , doubleTangentNumbersArray :: !Operand
  , arithUnOpCall             :: !(ArithUnOp -> Operand)
  , powCall                   :: !Operand
  , powiCall                  :: !Powi
  , printfCall                :: !Operand
  , allocCall                 :: !Operand
  , freeCall                  :: !Operand
  , opaqueModelEnvType        :: !LLT.Type
  , opaqueModelStateType      :: !LLT.Type
  }
  deriving Generic

data ModelType = ModelType
  { modelStructType, modelFunRecordType :: !LLT.Type
  }
  deriving Generic

newtype GenSt = GenSt
  { modelTypes :: Map Word ModelType
  }
  deriving Generic

newtype ModuleGenM a =
  ModuleGenM (StateT GenSt (ReaderT GenCtxt (ModuleBuilderT CompilerM)) a)
  deriving ( Functor, Applicative, Monad, MonadFix, MonadModuleBuilder
           , MonadConfig )

newtype CGenM a = CGenM (IRBuilderT ModuleGenM a)
  deriving ( Functor, Applicative, Monad, MonadFix, MonadIRBuilder
           , MonadModuleBuilder, MonadConfig )

newtype MemOpM a = MemOpM (StateT (Map (Op Operand) Operand) CGenM a)
  deriving ( Functor, Applicative, Monad, MonadFix, MonadIRBuilder
           , MonadModuleBuilder, MonadConfig )

liftCompilerM :: CompilerM a -> ModuleGenM a
liftCompilerM = ModuleGenM . lift . lift . lift

liftModuleGenM :: ModuleGenM a -> CGenM a
liftModuleGenM = CGenM . lift

liftCGenM :: CGenM a -> MemOpM a
liftCGenM = MemOpM . lift

runMemOpM :: MemOpM a -> CGenM a
runMemOpM (MemOpM run) = evalStateT run []

getGenCtxt :: CGenM GenCtxt
getGenCtxt = liftModuleGenM (ModuleGenM ask)

gettingGenCtxt :: (GenCtxt -> a) -> CGenM a
gettingGenCtxt f = f <$> getGenCtxt

compileOp :: Op Operand -> CGenM Operand
compileOp (ArithBinOp op p q) = case op of
  Add -> LL.named (LL.fadd p q) "add"
  Sub -> LL.named (LL.fsub p q) "sub"
  Mul -> LL.named (LL.fmul p q) "mul"
  Div -> LL.named (LL.fdiv p q) "div"
  Pow -> do
    pow <- gettingGenCtxt powCall
    LL.named (LL.call pow [(p, []), (q, [])]) "pow"
compileOp (ArithUnOp op p) = do
  unOpCall <- gettingGenCtxt arithUnOpCall
  LL.named (LL.call (unOpCall op) [(p, [])]) (LL.fromShow op)

memCompileOp :: Op Operand -> MemOpM Operand
memCompileOp op = MemOpM $ do
  memo <- get
  case M.lookup op memo of
    Nothing -> do
      val <- lift $ compileOp op
      put $ M.insert op val memo
      pure val
    Just val -> pure val

genPowi :: Operand -> Operand -> CGenM Operand
genPowi p q = do
  ~(_, result, _) <- LL.while (Just "powi") (q, LL.double 1, p) test step
  pure result
 where
  test (exp, result, base) = do
    continue <- LL.icmp LL.UGT exp (constDiff 0)
    pure (continue, (exp, result, base))

  step (exp, result, base) = do
    expMod2   <- LL.urem exp (constDiff 2)
    isNotMod2 <- LL.icmp LL.NE expMod2 (constDiff 0)
    result    <- LL.select isNotMod2 result =<< LL.fmul result base
    exp       <- LL.lshr exp (constDiff 1)
    base      <- LL.fmul base base
    pure (exp, result, base)

powi :: Operand -> Operand -> CGenM Operand
powi p q = do
  powi <- gettingGenCtxt powiCall
  case powi of
    Intrinsic powi -> do
      q <- LL.trunc q LLT.i32
      LL.call powi [(p, []), (q, [])]
    Inline -> genPowi p q

printf :: String -> [Operand] -> CGenM ()
printf fmt args = do
  idx    <- liftModuleGenM freshGlobal
  printf <- gettingGenCtxt printfCall
  str    <- LL.globalStringPtr fmt (LL.Name ("__fmt" <> LL.fromShow idx))
  _      <- LL.call printf (fmap (, []) (LL.constantOp str : args))
  pure ()

malloc :: Operand -> CGenM Operand
malloc size = do
  malloc <- gettingGenCtxt allocCall
  isZero <- LL.intIs 0 size
  LL.ite isZero (pure (LL.nullPtr LLT.i8)) (LL.call malloc [(size, [])])

free :: Operand -> CGenM Operand
free ptr = do
  free <- gettingGenCtxt freeCall
  LL.call free [(ptr, [])]

lookupFactorial :: Operand -> CGenM Operand
lookupFactorial (LL.ConstantOperand (LLC.Int _ n)) =
  pure (LL.double (fromIntegral (facts Stream.!! fromIntegral n)))
lookupFactorial n = do
  doubleFactorialArrayPtr <- gettingGenCtxt doubleFactorialArray
  factorialPtr            <- LL.ibgep doubleFactorialArrayPtr [LL.int32 0, n]
  LL.load factorialPtr 0

lookupBinom :: Operand -> Operand -> CGenM Operand
lookupBinom (LL.ConstantOperand (LLC.Int _ n)) (LL.ConstantOperand (LLC.Int _ k))
  = pure
    (LL.double
      (fromIntegral ((binoms Stream.!! fromIntegral n) !! fromIntegral k))
    )
lookupBinom n k = do
  binomArrayPtr <- gettingGenCtxt doubleChooseArray
  ksPtr <- LL.named (LL.ibgep binomArrayPtr [LL.int32 0, n]) "n.binomial"
  ks <- LL.named (LL.load ksPtr 0) "n.binom.k.ptr"
  LL.named (LL.loadFrom ks k) "n.binom.k"

lookupTangentNumber :: Operand -> Operand -> CGenM Operand
lookupTangentNumber n k = do
  tangentNumbersArrayPtr <- gettingGenCtxt doubleTangentNumbersArray
  ksPtr <- LL.named (LL.ibgep tangentNumbersArrayPtr [LL.int32 0, n])
                    "n.tan_numbers"
  ks <- LL.named (LL.load ksPtr 0) "n.k.tan_number.ptr"
  LL.named (LL.loadFrom ks k) "n.k.tan_number"

diffType :: LLT.Type
diffType = LLT.i64

constDiff :: Integral i => i -> Operand
constDiff = LL.int64 . fromIntegral

codeGenFunction
  :: LL.Function fun arg ModuleGenM
  => LL.Name
  -> fun
  -> (arg -> CGenM (Maybe Operand, ret))
  -> ModuleGenM (LL.LLVMFun fun arg, ret)
codeGenFunction nm fun body = do
  LL.FunType types _ <- LL.functionType fun
  let (tys, nms, attrs) =
        unzip3 (fmap (\(LL.FunArgument ty nm attr) -> (ty, nm, attr)) types)
  ((rty, paramNames, ret), blocks) <- LL.runIRBuilderT LL.emptyIRBuilder $ do
    paramNames <- traverse
      (\case
        NoParameterName  -> LL.fresh
        ParameterName nm -> LL.named LL.fresh nm
      )
      nms
    let ops = zipWith LL.LocalReference tys paramNames

    case LL.toArg ops of
      Nothing -> internalError
        "Couldn't extract structured arguments from hoisted function call."
      Just (args, _) -> do
        let CGenM act = body args
        (rt, other) <- act
        case rt of
          Nothing -> do
            LL.retVoid
            pure (LLT.void, paramNames, other)
          Just op -> do
            LL.ret op
            pure (LLT.typeOf op, paramNames, other)
  -- when (retty /= rty) $ internalError $ unwords
  --   [ "The declared type of the function was"
  --   , show (pretty retty)
  --   , "but the return value is actually"
  --   , show (pretty rty)
  --   ]
  let def = LL.GlobalDefinition LL.functionDefaults
        { LL.name        = nm
        , LL.parameters  = (zipWith3 LL.Parameter tys paramNames attrs, False)
        , LL.returnType  = rty
        , LL.basicBlocks = blocks
        }
      funty = LLT.ptr $ LL.FunctionType rty tys False
  emitDefn def
  pure (LL.LLVMFun (LL.constantOp $ LLC.GlobalReference funty nm), ret)

initGenCtxt :: MonadFix m => MonadConfig m => Int -> ModuleBuilderT m GenCtxt
initGenCtxt n = do
    -- Here we generate all the static data we need, starting with arrays
    -- containing binomial coefficients and values for factorial
  intFactorialArray <-
    LL.globalConstant "__int_factorial" (LL.arrayType n diffType)
    $ LL.arrayConstant diffType
    $ fmap LL.int64Constant (Stream.take n facts)
  doubleFactorialArray <-
    LL.globalConstant "__double_factorial" (LL.arrayType n LLT.double)
    $ LL.arrayConstant LLT.double
    $ fmap (LL.doubleConstant . fromIntegral) (Stream.take n facts)
  doubleChooseArrays <-
    sequence
      $ [ LL.globalConstant
            (LL.Name ("__choose_array_" <> LL.fromShow n))
            (LL.arrayType (length bn) LLT.double)
            (LL.arrayConstant
              LLT.double
              [ LL.doubleConstant (fromIntegral bnk) | bnk <- bn ]
            )
        | (n, bn) <- zip [0 ..] (Stream.take (n + 1) binoms)
        ]
  doubleChooseArray <-
    LL.globalConstant "__choose_array"
                      (LL.arrayType (n + 1) (LLT.ptr LLT.double))
    $ LL.arrayConstant (LLT.ptr LLT.double)
    $ fmap LL.constantArrayToPtr doubleChooseArrays

  doubleTangentNumberArrays <-
    sequence
      $ [ LL.globalConstant
            (LL.Name ("__tangent_numbers" <> LL.fromShow n))
            (LL.arrayType (length bn) LLT.double)
            (LL.arrayConstant
              LLT.double
              [ LL.doubleConstant (fromIntegral bnk) | bnk <- bn ]
            )
        | (n, bn) <- zip [0 ..] (Stream.take (n + 1) tangentNumbers)
        ]

  doubleTangentNumberArray <-
    LL.globalConstant "__tangent_numbers"
                      (LL.arrayType (n + 1) (LLT.ptr LLT.double))
    $ LL.arrayConstant (LLT.ptr LLT.double)
    $ fmap LL.constantArrayToPtr doubleTangentNumberArrays

  let op args nm = extern nm args LLT.double
      unop  = op [LLT.double]
      binop = op [LLT.double, LLT.double]
  sqrt  <- unop "llvm.sqrt.f64"
  exp   <- unop "llvm.exp.f64"
  log   <- unop "llvm.log.f64"
  sin   <- unop "llvm.sin.f64"
  tan   <- unop "tan"
  cos   <- unop "llvm.cos.f64"
  asin  <- unop "asin"
  acos  <- unop "acos"
  atan  <- unop "atan"
  cosh  <- unop "cosh"
  sinh  <- unop "sinh"
  tanh  <- unop "tanh"
  asinh <- unop "asinh"
  acosh <- unop "acosh"
  atanh <- unop "atanh"
  abs   <- unop "llvm.fabs.f64"
  sgn   <-
    LL.privateFunction "__sdebl_intrinsic_sgn" [(LLT.double, "a")] LLT.double
      $ \case
          [a] -> do
            isZero <- LL.fcmp OEQ a (LL.double 0)
            result <- LL.ite isZero (pure (LL.double 0)) $ do
              isPositive <- LL.fcmp OGT a (LL.double 0)
              LL.select isPositive (LL.double 1) (LL.double (-1))
            LL.ret result
          args -> wrongNumberOfArguments "sgn" 1 args
  conf <- conf
  powi <- if inlinePowi $ cgConf conf
    then pure Inline
    else Intrinsic <$> extern "llvm.powi.f64" [LLT.double, LLT.i32] LLT.double
  pow <- binop "llvm.pow.f64"
  let printfName | usePrintfForDebugging $ cgConf conf = "printf"
                 | otherwise                           = "debug"
  printf               <- externVarArgs printfName [LLT.ptr LLT.i8] LLT.void
  malloc               <- extern "malloc" [LLT.i64] (LLT.ptr LLT.i8)
  free                 <- extern "free" [LLT.ptr LLT.i8] LLT.void
  opaqueModelEnvType   <- LL.typedef "model.env" Nothing
  opaqueModelStateType <- LL.typedef "model.state" Nothing
  pure GenCtxt
    { intFactorialArray         = LL.constantOp intFactorialArray
    , doubleFactorialArray      = LL.constantOp doubleFactorialArray
    , doubleChooseArray         = LL.constantOp doubleChooseArray
    , doubleTangentNumbersArray = LL.constantOp doubleTangentNumberArray
    , arithUnOpCall             = \case
                                    Sqrt  -> sqrt
                                    Exp   -> exp
                                    Log   -> log
                                    Sin   -> sin
                                    Tan   -> tan
                                    Cos   -> cos
                                    Asin  -> asin
                                    Acos  -> acos
                                    Atan  -> atan
                                    Cosh  -> cosh
                                    Sinh  -> sinh
                                    Tanh  -> tanh
                                    Acosh -> acosh
                                    Asinh -> asinh
                                    Atanh -> atanh
                                    Abs   -> abs
                                    Sgn   -> sgn
    , powCall                   = pow
    , powiCall                  = powi
    , printfCall                = printf
    , allocCall                 = malloc
    , freeCall                  = free
    , opaqueModelEnvType        = opaqueModelEnvType
    , opaqueModelStateType      = opaqueModelStateType
    }


freshGlobal :: ModuleGenM Word
freshGlobal = ModuleGenM $ do
  src <- lift $ lift $ lift $ getSupply
  pure (extract src)

getModelType
  :: Seq ty -> LLT.Type -> (LLT.Type -> LLT.Type) -> ModuleGenM ModelType
getModelType out funRecordTy mkModelTy = ModuleGenM $ do
  tys <- use #modelTypes
  let len = fromIntegral (length out)
  case M.lookup len tys of
    Just ty -> pure ty
    Nothing -> do
      src <- lift $ lift $ lift $ getSupplyWith
        (\idx tag -> LL.Name
          (tag <> "_type_" <> LL.fromShow len <> "_" <> LL.fromShow idx)
        )
      funRecordTy <- LL.typedef (extract src "fun_record") (Just funRecordTy)
      modelTy <- LL.typedef (extract src "model") (Just (mkModelTy funRecordTy))
      let modelType = ModelType { modelStructType    = modelTy
                                , modelFunRecordType = funRecordTy
                                }
      modifying #modelTypes (M.insert len modelType)
      pure modelType


codeGenModule :: ShortByteString -> ModuleGenM a -> CompilerM (a, LL.Module)
codeGenModule nm (ModuleGenM act) = do
  (a, llvmDefns) <- runModuleBuilderT emptyModuleBuilder $ do
    conf    <- lift conf
    -- We need to + 1 the indexLimit because initGenCtxt counts from 0
    genCtxt <- initGenCtxt (fromIntegral (indexLimit $ cgConf conf) + 1)
    runReaderT (evalStateT act GenSt { modelTypes = [] }) genCtxt
  pure
    ( a
    , LL.defaultModule { LL.moduleName = nm, LL.moduleDefinitions = llvmDefns }
    )
