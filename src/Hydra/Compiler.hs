{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Hydra.Compiler
  ( module Hydra.Compiler
  , module Hydra.Error
  , module Hydra.Config
  ) where

import           Control.Comonad
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State.Strict

import           Data.Stream.Supply

import           Hydra.Config
import           Hydra.Error

newtype CompilerM a =
  CompilerM (ConfigT (StateT (Supply Word) (ExceptT ErrMsg (IOLogT IO))) a)
  deriving ( Functor, Applicative, Monad, MonadIO, MonadFix, MonadError ErrMsg
           , MonadConfig, MonadLog )

getSupply :: CompilerM (Supply Word)
getSupply = CompilerM $ do
  s <- get
  let (s1, s2) = split2 s
  put s1
  pure s2

getSupplyWith :: (Word -> a) -> CompilerM (Supply a)
getSupplyWith f = extend (f . extract) <$> getSupply

runCompilerT :: CompilerM a -> Config -> Supply Word -> IO (Either ErrMsg a)
runCompilerT (CompilerM cmp) conf supply =
  runIOLogT (runExceptT (evalStateT (runConfigT cmp conf) supply))
