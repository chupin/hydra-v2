{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

{-# OPTIONS_GHC -funbox-small-strict-fields #-}

module Hydra.Config where

import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State
import qualified Control.Monad.State.Strict    as Strict
import           Control.Monad.Writer

import           LLVM.IRBuilder.Module
import           LLVM.IRBuilder.Monad

import           Options.Applicative

import           Hydra.Name

data DebugConf = DebugConf
  { enableInfo    :: !Bool
  , enableWarning :: !Bool
  , enableDebug   :: !Bool
  }

data CGConf = CGConf
  { indexLimit              :: !Word
  , compileRelAsFunction    :: !Bool
  , compileRelDerAsFunction :: !Bool
  , compileDerExplicitly    :: !Bool
  , inlinePowi              :: !Bool
  , noOperatorMemoization   :: !Bool
  , usePrintfForDebugging   :: !Bool
  , useUODE                 :: !Bool
  }

data Config = Config
  { sourceFile :: !FilePath
  , simulate   :: !(Maybe Name)
  , outputFile :: !(Maybe FilePath)
  , debugConf  :: !DebugConf
  , cgConf     :: !CGConf
  }

strWithDef :: Show a => a -> String -> String
strWithDef def str = str ++ " (default value: " ++ show def ++ ")"

debugConfParser :: Parser DebugConf
debugConfParser = do
  enableWarning <-
    not
    <$> switch (long "no-warn" <> help "Disable warning messages")
    <|> switch (long "warn" <> short 'W' <> help "Enable warning messages")
  enableInfo <- switch $ long "info" <> short 'i' <> help
    "Display information messages"
  enableDebug <- switch $ long "debug" <> help "Display debugging messages"
  pure DebugConf { enableWarning = enableWarning
                 , enableInfo    = enableInfo
                 , enableDebug   = enableDebug
                 }

cgConfParser :: Parser CGConf
cgConfParser = do
  let defaultIndexLimit = 10
  indexLimit <-
    option
        auto
        (long "max-index" <> help
          (strWithDef defaultIndexLimit
                      "Sets the highest index for the generated model"
          )
        )
      <|> pure defaultIndexLimit
  compileRelAsFunction <-
    switch
    $  long "hoist-relation-residual"
    <> help
         "Hoist the residual function for a relation in its own function. \
         \In the future this may be used as a way to share code between \
         \similar relations."
  compileRelDerAsFunction <-
    switch
    $  long "hoist-relation-derivatives"
    <> help
         "Hoist the code to compute the arbitrary derivative of a relation in \
         \its own separate function. This can be used if having large code in \
         \between relations is a concern, for instance w.r.t filling the \
         \instruction cache unecessarily."
  compileDerExplicitly <-
    switch
    $  long "explicit-derivatives"
    <> help
         "Instead of using the abstract form of relations derivatives, \
         \compute and compile them explicitly up to max-index."
  inlinePowi <-
    switch
    $  long "inline-powi"
    <> help
         "When toggled, this option produces custom inline code where \
         \instead of using the LLVM intrinsic powi function."
  noOperatorMemoization <-
    switch
    $  long "no-op-memo"
    <> help
         "This flag only has an impact when used with \
         \explicit-derivatives. When toggled, it disables memoizing the \
         \construction of LLVM operations."
  usePrintfForDebugging <- switch $ long "use-printf-for-debug" <> help
    "Use printf for debugging instead of the runtime's debug function."
  useUODE <- switch $ long "uode" <> help
    "Use the underlying ODE for simulation"
  pure CGConf { indexLimit              = indexLimit
              , compileRelAsFunction    = compileRelAsFunction
              , compileRelDerAsFunction = compileRelDerAsFunction
              , compileDerExplicitly    = compileDerExplicitly
              , inlinePowi              = inlinePowi
              , noOperatorMemoization   = noOperatorMemoization
              , usePrintfForDebugging   = usePrintfForDebugging
              , useUODE                 = useUODE
              }

configParser :: Parser Config
configParser = do
  sourceFile <- argument str $ metavar "FILE" <> help "Source file to compile"
  simulate   <-
    Just
    <$> option
          str
          (  long "simulate"
          <> short 's'
          <> help
               "Simulate the specified model. \
                               \The model cannot have arguments."
          )
    <|> pure Nothing
  outputFile <-
    Just
    <$> strOption
          (  short 'o'
          <> long "output"
          <> metavar "OUTPUT"
          <> help
               "Override the default output file for the generated LLVM code. \
         \By default, the output file is derived from the source file's name."
          )
    <|> pure Nothing

  debugConf <- debugConfParser
  cgConf    <- cgConfParser

  pure Config { sourceFile = sourceFile
              , outputFile = outputFile
              , simulate   = simulate
              , debugConf  = debugConf
              , cgConf     = cgConf
              }

class Monad m => MonadConfig m where
  conf :: m Config

  default conf :: (MonadTrans t, MonadConfig n, m ~ t n) => m Config
  conf = lift conf

instance MonadConfig m => MonadConfig (StateT s m)

instance MonadConfig m => MonadConfig (Strict.StateT s m)

instance MonadConfig m => MonadConfig (ReaderT r m)

instance MonadConfig m => MonadConfig (ModuleBuilderT m)

instance MonadConfig m => MonadConfig (IRBuilderT m)

newtype ConfigT m a = ConfigT (ReaderT Config m a)
  deriving ( Functor, Applicative, Monad, MonadTrans, MonadFix, MonadIO )

runConfigT :: ConfigT m a -> Config -> m a
runConfigT (ConfigT ct) = runReaderT ct

deriving instance MonadState s m => MonadState s (ConfigT m)

deriving instance MonadError e m => MonadError e (ConfigT m)

deriving instance MonadWriter w m => MonadWriter w (ConfigT m)

instance Monad m => MonadConfig (ConfigT m) where
  conf = ConfigT ask

instance MonadReader r m => MonadReader r (ConfigT m) where
  ask = lift ask

  local f (ConfigT (ReaderT m)) = ConfigT (ReaderT (local f . m))
