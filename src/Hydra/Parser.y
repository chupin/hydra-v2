{

{-# LANGUAGE OverloadedLists, GADTs, DataKinds, MultiParamTypeClasses, OverloadedStrings, TypeFamilies, FlexibleContexts #-}

module Hydra.Parser (parseStep) where

import Hydra.AST
import Hydra.Tokens
import Hydra.Type
import Hydra.Error
import Hydra.Compiler
import Hydra.Loc
import Hydra.Steps
import Hydra.Pretty

import Data.Void
import qualified Data.Set as S
import qualified Data.Map as M
import Data.Foldable
import Data.Sequence (Seq (Empty, (:<|), (:|>)))

}

%name parseModule Module
%monad { Either [Token Loc] }
%error { Left }
%tokentype { Token Loc }

%token
   model { TokModel _ }
   let   { TokLet _ }
   init  { TokInit _ }
   reinit { TokReinit _ }
   switch { TokSwitch _ }
   mode   { TokMode _ }
   when   { TokWhen _ }
   der    { TokDer _ }
   up     { TokUp _ }
   down   { TokDown _ }
   updown { TokUpDown _ }
   ident  { TokLIdent _ _ }
   uident { TokUIdent _ _ }
   int    { TokInt _ _ }
   double { TokDouble _ _ }
   '->'   { TokArr _ }
   '('    { TokLParen _ }
   ')'    { TokRParen _ }
   '{'    { TokLBrack _ }
   '}'    { TokRBrack _ }
   ';'    { TokSemiCol _ }
   ','    { TokComma _ }
   '='    { TokEq _ }
   '<>'   { TokConn _ }
   '+'    { TokOp (Binop PriorityAdd _) _ }
   '-'    { TokOp (Binop PrioritySub _) _ }
   '*'    { TokOp (Binop PriorityMul _) _ }
   '/'    { TokOp (Binop PriorityDiv _) _ }
   '^'    { TokOp (Binop PriorityPow _) _ }
   unop   { TokOp (Unop _) _ }

%left '+' '-'
%left '*' '/'
%right '^'
%right der
%nonassoc up
%nonassoc down
%nonassoc updown
%right unop

%%

SepListSome(Sep, X): X Sep X FollowSep(Sep, X) { $1 :<| $3 :<| $> }

FollowSep(Sep, X): { [] }
                 | Sep X FollowSep(Sep, X) { $2 :<| $> }

SepList(Sep, X): { [] }
               | X { [$1] }
               | X Sep SepNonEmptyList(Sep, X) { $1 :<| $> }

SepNonEmptyList(Sep, X): X { [$1] }
                       | X Sep SepNonEmptyList(Sep, X) { $1 :<| $> }

CommaList(X): SepList(',', X) { $1 }
CommaListSome(X): SepListSome(',', X) { $1 }

AndList(X): { [] }
          | X { [$1] }
          | X ';' AndList(X) { $1 :<| $> }

SpaceList(X): { [] }
            | X SpaceList(X) { $1 :<| $2 }

Const:: { (Loc, Const) }
Const: double { (localize $1, RealConst (toRational (doubleOfTok $1))) }
     | int    { (localize $1, RealConst (fromIntegral (intOfTok $1))) }

Op(Atom): unop Atom    { Left  ($1, $2) }
       | Atom '+' Atom { Right ($2, Right $1, $3) }
       | Atom '-' Atom { Right ($2, Right $1, $3) }
       | '-' Atom      { Right ($1, Left 0, $2) }
       | Atom '*' Atom { Right ($2, Right $1, $3) }
       | Atom '/' Atom { Right ($2, Right $1, $3) }
       | Atom '^' Atom { Right ($2, Right $1, $3) }

AtomSig:: { ParsedSig }
AtomSig: ident { localSig (localize $1) () (lidentOfTok $1) }
       | der AtomSig { derSig (localize $1 <> localize $2) $2 }
       | SigExpr { exprSig () $1 }
       | Op(AtomSig) { handleOp opSig (\loc c -> Expr loc () (Const loc (RealConst c))) $1 }
       | '(' Sig ')' { $2 { sigLoc = localize $1 <> localize $> } }

Sig:: { ParsedSig }
Sig: AtomSig { $1 }
   | '(' ')' { tupleSig (localize $1 <> localize $>) [] }
   | CommaListSome(AtomSig)
   { let (head :<| (_ :|> tail)) = $1 in
     tupleSig (localize head <> localize tail) $1 }

Event:: { ParsedEvent }
Event: up AtomSig { up (localize $1 <> localize $2) $2 }
     | down AtomSig { down (localize $1 <> localize $2) $2 }
     | updown AtomSig { upDown (localize $1 <> localize $2) $2 }

Mode(X): uident { (Mode (Label (uidentOfTok $1)) [], localize $1) }
       | uident '(' CommaList(X) ')'
       { ( Mode (Label (uidentOfTok $1)) $3, localize $1 <> localize $>) }

SigExpr:: { ParsedExpr }
SigExpr: Const { constExpr (fst $1) (snd $1) }

LitModelExpr: model Pat '{' AndList(Rel) '}'
       { Model { modelOutput = $2
              , modelRelations = $4
              , exprLoc = localize $1 <> localize $3
              }
       }

Expr: Op(AtomExpr) { handleOp opExpr realConstExpr $1 }
    | LitModelExpr { $1 }
    | AppExpr { $1 }

AppExpr:: { ParsedExpr }
AppExpr: AppExpr AtomExpr { app (localize $1 <> localize $>) () $1 $2 }
       | AtomExpr { $1 }

AtomExpr:: { ParsedExpr }
AtomExpr: '(' Expr ')' { $2 { exprLoc = localize $1 <> localize $> } }
        | SigExpr { $1 }
        | ident { localExpr (localize $1) () (lidentOfTok $1) }

AtomPat:: { ParsedPat }
AtomPat: ident { localPat (localize $1) () (lidentOfTok $1) }
       | '(' ')' { tuplePat (localize $1 <> localize $>) [] }
       | '(' Pat ')' { $2 { patLoc = localize $1 <> localize $> } }

Pat:: { ParsedPat }
Pat: AtomPat { $1 }
   | CommaListSome(AtomPat)
   { let (head :<| (_ :|> tail)) = $1 in
     tuplePat (localize head <> localize tail) $1 }

Branch: mode Mode(AtomPat) Reinits  '->' AndList(Rel) SpaceList(Condition)
        { Branch (Located (snd $2) (fst $2)) $3 $6 $5 }

Condition: when Event '->' Mode(AtomSig) { $2 :-> locate (snd $>) (fst $>) }

Reinits: reinit CommaList(ident)
       { (S.fromList (fmap (\tok -> Located (localize tok) (lidentOfTok tok)) (toList $2))) }
       | { [] }

Switch:: { ParsedRel }
Switch: switch '{' SpaceList(Branch) '}'
      { let Branch { branchMode = Located _ (Mode init []) } :<| _ = $3 in Switch (Located NoLoc (Mode init [])) [] $3 }
      | switch init Mode(Expr) '{' SpaceList(Branch) '}'
      { Switch (Located (snd $3) (fst $3)) [] $5 }

Eqn:: { ParsedEqn }
Eqn: Sig '=' Sig { $1 :=: $3 }
   | '(' Eqn ')' { $2 }

Rel:: { ParsedRel }
Rel: Eqn { Eqn $1 }
   | init Eqn { Init $2 }
   | Sig '<>' Expr { $1 :<>: $3 }
   | let CommaList(ident) '{' AndList(Rel) '}'
   { Let (foldr (\idt env -> M.insert (lidentOfTok idt) (Located (localize idt) ()) env) [] $2) $4 }
   | Switch { $1 }

Decl:: { ParsedDecl }
Decl: let ident SpaceList(Pat) '=' Expr
        { Decl (lidentOfTok $2) $3 $5 UnitScheme }

Module:: { ParsedModule }
Module: SpaceList(Decl) { Module { moduleDecls = $1 } }

{

type ParsedSig = Sig Void () () Name
type ParsedEvent = Event Void () () Name
type ParsedExpr = Expr Void () () Name
type ParsedPat = Pat () Name
type ParsedEqn = Eqn Void () () Name
type ParsedRel = Rel Void () () Name
type ParsedDecl = Decl Void () () Name
type ParsedModule = Module Void () () Name

handleOp mkop mkcst op = case op of {
              Left (unop, arg) ->
                     mkop (localize unop <> localize arg) ((unopOfTok unop) arg);
              Right (binop, arg1, arg2) ->
                     case arg1 of {
                            Left cst -> mkop (localize binop <> localize arg2)
                                              ((binopOfTok binop) (mkcst NoLoc cst) arg2);
                            Right arg1 -> mkop (localize arg1 <> localize arg2)
                                                ((binopOfTok binop) arg1 arg2)
                     }
              }

data ParseError = ParseError [Token Loc]

instance Message 'Error ParseError where
  msgTitle _ = "parse error"
  msgBody rdrSpan msg@(ParseError toks) =
    [ "Parse error on tokens"
    , hsep (fmap pretty (take 10 toks))
    , case msgLoc msg of
        NoLoc -> ""
        Loc span -> rdrSpan span
    ]

  msgLoc (ParseError (tok : _)) = localize tok
  msgLoc (ParseError []) = NoLoc

parseStep :: StepSpec CompilerM (VSep (Token Loc)) ParsedModule
parseStep = Step { stepConf = fromStepName "parsing"
                 , stepExec = SingleStep $ \(VSep toks) -> case parseModule toks of
                                Left toks -> throw (ParseError toks)
                                Right model -> pure model
                 }

}
