{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.Flatten.Opt.CopyPropagation
  ( copyPropagationStep
  ) where

import           Control.Applicative
import           Data.Foldable
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Sequence                  ( Seq(..) )
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           Hydra.Error                    ( internalError )
import           Hydra.Flatten.AST
import           Hydra.Steps

data EqInfo = NoEqInfo | MustExist | EqToConst Expr

instance Show EqInfo where
  show NoEqInfo      = "NoEqInfo"
  show MustExist     = "MustExist"
  show (EqToConst e) = "EqToConst " ++ show (pretty e)

instance Semigroup EqInfo where
  NoEqInfo    <> info        = info
  info        <> NoEqInfo    = info
  MustExist   <> _           = MustExist
  _           <> MustExist   = MustExist
  EqToConst{} <> EqToConst{} = MustExist

data EqualSet = EqualSet
  { equalSet  :: Set Ident
  , equalInfo :: EqInfo
  }
  deriving Show

instance Semigroup EqualSet where
  EqualSet xs ixs <> EqualSet ys iys = EqualSet (xs <> ys) (ixs <> iys)

addEqualSet :: EqualSet -> [EqualSet] -> [EqualSet]
addEqualSet eq [] = [eq]
addEqualSet eqset@EqualSet { equalSet = eq } (eqset'@EqualSet { equalSet = eq' } : eqs)
  | any (`S.member` eq') eq
  = mergedSet : eqs
  where !mergedSet = eqset <> eqset'
addEqualSet eq (eq' : eqs) = eq' : eqs' where !eqs' = addEqualSet eq eqs

mergeEqualSets :: [EqualSet] -> [EqualSet] -> [EqualSet]
mergeEqualSets []         eqs  = eqs
mergeEqualSets (eq : eqs) eqs' = mergeEqualSets eqs eqs''
  where !eqs'' = addEqualSet eq eqs'

-- It only makes sense to keep a set if it has more than one element. It's not
-- clear if it makes sense to keep sets that have 1 element in all cases. It
-- certainly does if its equal to a constant, unsure if it just says that an
-- identifier must exist.
keepSet :: EqualSet -> Bool
keepSet EqualSet { equalSet = eq } = length eq > 0

suppressIdents :: Set Ident -> [EqualSet] -> [EqualSet]
suppressIdents _ [] = []
suppressIdents suppress (eqset@EqualSet { equalSet = eq } : eqs)
  | keepSet req = req : next
  | otherwise   = next
 where
  req  = eqset { equalSet = eq S.\\ suppress }

  next = suppressIdents suppress eqs

data ElimInto = Itself | OtherSig Ident | ConstExpr Expr

instance Show ElimInto where
  show Itself        = "Itself"
  show (OtherSig  i) = "OtherSig " ++ show (pretty i)
  show (ConstExpr e) = "ConstExpr " ++ show (pretty e)

data ElimInfo = ElimInfo
  { elimSetIndex :: Word -- the index of the set of the equal set an identifier belongs to
  , elimInto     :: ElimInto
  }
  deriving Show

newtype RemovedSet = RemovedSet (Map Ident ElimInfo)
  deriving ( Show )

instance Substitution RemovedSet where
  isEmptySubst (RemovedSet set) = M.null set

  lookupExprVar _ _ = Nothing

  lookupSigVar i set = case lookupInfo i set of
    Nothing                             -> Nothing
    Just ElimInfo { elimInto = Itself } -> Nothing
    Just ElimInfo { elimInto = OtherSig j } ->
      Just Local { localSig = LocalSig j 0 }
    Just ElimInfo { elimInto = ConstExpr e } -> Just (exprSig e)

  substWithout (RemovedSet set) ids = RemovedSet (set `M.withoutKeys` ids)

eliminatedIdents :: RemovedSet -> Set Ident
eliminatedIdents (RemovedSet set) = M.foldrWithKey
  (\idt ElimInfo { elimInto = into } elims -> case into of
    Itself -> elims
    _      -> S.insert idt elims
  )
  []
  set

forgetIdents :: RemovedSet -> Set Ident -> RemovedSet
forgetIdents (RemovedSet set) ids = RemovedSet (set `M.withoutKeys` ids)

lookupInfo :: Ident -> RemovedSet -> Maybe ElimInfo
lookupInfo i (RemovedSet set) = M.lookup i set

-- We are given two sets of equalities. We must compute the set of equality such
-- that the information from both sets are compatible.
--
-- Suppose we have the sets [[a, b], [c, d], [e, f]] and [[a, c], [b, e, f]],
-- then the only thing we can deduce is that e and f are always equal, so this
-- function should output the set [[e,f]]
agreeingSubset :: [EqualSet] -> [EqualSet] -> [EqualSet]
agreeingSubset xss yss =
  [ zs
  | EqualSet { equalSet = xs, equalInfo = cxs } <- xss
  , EqualSet { equalSet = ys, equalInfo = cys } <- yss
  , let zs = EqualSet { equalSet  = xs `S.intersection` ys
                      , equalInfo = cxs <> cys
                      }
  , keepSet zs
  ]

buildRemovedSet
  :: Set Ident -- removable
  -> [EqualSet]
  -> RemovedSet
buildRemovedSet removable set = RemovedSet (go 0 removable set)
 where
  go _ removable _ | S.null removable = []
  go _ _ []                           = []
  go n removable (EqualSet { equalSet = equalSet, equalInfo = cst } : equals) =
    M.union subst nextSubst
   where
    !nextSubst = go (n + 1) removable equals

    mkInfo into = ElimInfo { elimInto = into, elimSetIndex = n }

    !subst = case cst of
      EqToConst expr -> M.fromSet (\_ -> mkInfo (ConstExpr expr)) equalSet
      _              -> case pivot of
        Just pivot | not (S.null equalRemovables) -> M.insert
          pivot
          (mkInfo Itself)
          (M.fromSet (\_ -> mkInfo (OtherSig pivot)) equalRemovables)
        _ -> M.empty
       where
        pivot = S.lookupMin equalNonRemovables <|> S.lookupMin equalRemovables

        (equalRemovables, equalNonRemovables) =
          S.partition (`S.member` removable) equalSet

removeEqn :: RemovedSet -> Eqn -> Maybe Eqn
removeEqn removedSet eqn@(CausalEqn (LocalSig i n) expr) =
  case lookupInfo i removedSet of
    Nothing                             -> Just eqn
    Just ElimInfo { elimInto = Itself } -> Just eqn
    Just ElimInfo { elimInto = OtherSig j } ->
      Just (CausalEqn (LocalSig j n) expr)
    Just ElimInfo { elimInto = ConstExpr{} } -> Nothing
removeEqn removedSet (NameEqn (LocalSig i _) (LocalSig j _))
  | Just ElimInfo { elimSetIndex = p } <- lookupInfo i removedSet
  , Just ElimInfo { elimSetIndex = q } <- lookupInfo j removedSet
  , p == q
  = Nothing
removeEqn removedSet eqn = Just (substEqn removedSet eqn)

  -- where
  --   subst = toSubst removedSet
  --   avg = if length set == 0
  --         then 0
  --         else maximum (fmap (\ElimInfo{elimSetIndex = n} -> n) set)
localKeeper :: (Rel -> Seq Rel) -> RemovedSet -> Rel -> Seq Rel
localKeeper keeper removedSet (Rel holds eqn) =
  case removeEqn removedSet eqn of
    Nothing  -> []
    Just eqn -> keeper (Rel holds eqn)
localKeeper keeper removedSet (Let locals rels) = keeper (Let locals frels)
 where
  frels = foldr
    (\rel rels ->
      localKeeper keeper (removedSet `forgetIdents` locals) rel <> rels
    )
    []
    rels
localKeeper keeper removedSet (ModelCall output expr) = keeper
  (ModelCall output' expr)
 where
  output' = fmap
    (\idt -> case lookupInfo idt removedSet of
      Nothing                           -> idt
      Just ElimInfo { elimInto = into } -> case into of
        Itself        -> idt
        OtherSig idt' -> idt'
        ConstExpr{}   -> internalError "Forbidden elimination"
    )
    output
localKeeper keeper removedSet (Switch initMode branches) = keeper
  (Switch initMode (fmap go branches))
 where
  go br@Branch { branchArgs = args, branchConditions = conds, branchRelations = rels }
    = br { branchConditions = fmap (substCond restrictedSet) conds
         , branchRelations  = frels
         }
   where
    restrictedSet =
      removedSet `forgetIdents` S.fromList (toList (fmap argName args))

    frels =
      foldr (\rel rels -> localKeeper keeper restrictedSet rel <> rels) [] rels

foldDeletion :: (Rel -> Seq Rel) -> Seq Rel -> ([EqualSet], Seq Rel)
foldDeletion keep = foldr go ([], [])
 where
  go rel (eqs, rels) = (meqs, mrel <> rels)
   where
    (eq, mrel) = copyDeletion keep rel

    !meqs      = mergeEqualSets eq eqs

copyDeletion :: (Rel -> Seq Rel) -> Rel -> ([EqualSet], Seq Rel)

-- If we encounter an equation saying i = j, then we state that i and j form an
-- equal set and we use the function that decide if we'll keep that equation
copyDeletion keeper rel@(Rel Always (NameEqn (LocalSig i 0) (LocalSig j 0))) =
  ([EqualSet { equalSet = [i, j], equalInfo = NoEqInfo }], keeper rel)
copyDeletion keeper rel@(Rel Always (CausalEqn (LocalSig i 0) e)) =
  ([EqualSet { equalSet = [i], equalInfo = EqToConst e }], keeper rel)
-- Signals that are used as arguments to a model call *must* be kept alive or
-- must be replaced by another signal identifier
copyDeletion keeper rel@(ModelCall output _) = (keepSets, keeper rel)
 where
  !keepSets = foldr
    (\idt sets ->
      EqualSet { equalSet = [idt], equalInfo = MustExist } `addEqualSet` sets
    )
    []
    output
-- When encountering a let block, we must gather all equal sets in the
-- relations. Then we try to replace as many of the locals variables
copyDeletion keeper (Let locals rels) =
  ( suppressIdents locals equals
  , keeper (Let (locals S.\\ eliminatedIdents removedSet) eqrels)
  )
 where
  (!equals, eqrels) = foldDeletion (localKeeper keeper removedSet) rels

  !removedSet       = buildRemovedSet locals equals

copyDeletion keeper (Switch initMode branches) =
  (equalities, keeper (Switch initMode fbranches))
 where
  go label br@Branch { branchRelations = rels } (equalities, branches) =
    ( agreeingSubset eqs equalities
    , M.insert label br { branchRelations = frels } branches
    )
    where (eqs, frels) = foldDeletion keeper rels

  (equalities, fbranches) = M.foldrWithKey go ([], []) branches
copyDeletion keeper rel = ([], keeper rel)

copyDeletionExpr :: Expr -> Expr
copyDeletionExpr model@Model { modelRelations = rels } = model
  { modelRelations = foldr (\rel rels -> snd (copyDeletion keep rel) <> rels)
                           []
                           rels
  }
 where
  keep (Let ids rels) = letRel ids (keep =<< rels)
  keep rel            = [rel]
copyDeletionExpr expr = expr

copyDeletionInst :: Inst -> Inst
copyDeletionInst inst@Inst { instBody = body } =
  inst { instBody = copyDeletionExpr body }

copyDeletionDecl :: Decl -> Decl
copyDeletionDecl decl@MonoDecl { declInst = inst } =
  decl { declInst = copyDeletionInst inst }
copyDeletionDecl tmp@Template { templateInsts = insts } =
  tmp { templateInsts = fmap copyDeletionInst insts }

copyDeletionModule :: Module -> Module
copyDeletionModule Module { moduleDecls = decls } =
  Module { moduleDecls = fmap copyDeletionDecl decls }

copyPropagationStep :: Applicative m => StepSpec m Module Module
copyPropagationStep = Step
  { stepConf = StepConf
                 { stepName       = "copy deletion optimisation"
                 , stepStopBefore = ["stop-before-copy-propagation"]
                 , stepStopAfter  = ["stop-after-copy-propagation"]
                 , stepSkip = SkipConf { skipByDefault = DontSkip
                                       , dontSkipFlags = []
                                       , skipFlags     = ["no-copy-propagation"]
                                       }
                 }
  , stepExec = SingleStep (pure . copyDeletionModule)
  }
