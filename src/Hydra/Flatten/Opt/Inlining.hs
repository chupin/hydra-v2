{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.Flatten.Opt.Inlining where

import qualified Data.Map                      as M
import           Data.Map                       ( Map )
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import qualified Data.Set                      as S
import           Data.Set                       ( Set )
import           Hydra.Flatten.AST
import           Hydra.Steps

-- * The compiler's inliner.
--
-- This module is in charge of inlining small models into bigger ones. To do so,
-- every declaration is given a size and is inlined if that size is above some
-- threshold.
--
-- Because recursion is disallowed, the inliner can use a « tying-the-knot »
-- technique. The inliner is passed the dictionnary of all instantiations of
-- models in which inlining has already been performed, as its performing it.
-- This is very useful because it means we don't have to maintain an order in
-- the module or care about any of that. Lazy evaluation takes care of ordering
-- everything for us.
--
-- The size of an expression. It is either some finite integer (smaller is
-- better) or it is infinitely small (which should result in the declaration
-- always being inlined) or infinitely big (which should result in the
-- declaration never being inlined). Note that because the language doesn't
-- support lambdas (yet), partially applied functions are never inlined,
-- regardless of their size.
data Size = InfSmall | Finite !Int | InfBig
  deriving ( Eq, Ord )

instance Semigroup Size where
  InfBig   <> _        = InfBig
  _        <> InfBig   = InfBig
  InfSmall <> size     = size
  size     <> InfSmall = size
  Finite n <> Finite m = Finite (n + m)

instance Monoid Size where
  mempty = Finite 0

-- * Size computations
-- A few « magic numbers »
normalEqnSize :: Size
normalEqnSize = Finite 1

localExprSize :: Size
localExprSize = Finite 0

opExprSize :: Size -> Size
opExprSize = (Finite 1 <>)

causalEqnSize, causalEqnSizeWithOutput :: Size
causalEqnSize = Finite (-5)

causalEqnSizeWithOutput = Finite (-10)

nameEqnSize, nameEqnSizeWithOneOutput, nameEqnSizeWithBothOutput :: Size
nameEqnSize = Finite (-2)

nameEqnSizeWithOneOutput = Finite (-5)

nameEqnSizeWithBothOutput = Finite (-10)

modelCallSize :: Size
modelCallSize = Finite 1

exprSize :: Expr -> Size
exprSize Const{}      = InfSmall
exprSize LocalExpr{}  = localExprSize
exprSize GlobalExpr{} = InfSmall
exprSize Model { modelOutput = output, modelRelations = rels } =
  modelSize output rels
exprSize OpExpr { operExpr = op } = opExprSize (foldMap exprSize op)
exprSize Call { callExpr = call } = callSize call

modelSize :: Seq Ident -> Seq Rel -> Size
modelSize output rels = blockSize (foldr S.insert [] output) rels

callSize :: Call -> Size
callSize (KnownCall (Saturated 0) Global { globalInst = Inst { instBody = body } } args)
  = exprSize body <> foldMap exprSize args
callSize (KnownCall (Saturated _) _ _) = InfBig
callSize (UnknownCall fun args       ) = foldMap exprSize (fun :<| args)

eqnSize :: Set Ident -> Eqn -> Size
eqnSize outputSignals (CausalEqn (LocalSig i _) _)
  | i `S.member` outputSignals = causalEqnSize
  | otherwise                  = causalEqnSizeWithOutput

eqnSize outputSignals (NameEqn (LocalSig i _) (LocalSig j _)) =
  case (i `S.member` outputSignals, j `S.member` outputSignals) of
    (True , True ) -> nameEqnSizeWithBothOutput
    (False, False) -> nameEqnSize
    _              -> nameEqnSizeWithOneOutput
eqnSize _ GenEqn{} = normalEqnSize

relationSize :: Set Ident -> Rel -> Size
relationSize outputSignals (Rel _ eqn) = eqnSize outputSignals eqn
relationSize outputSignals (Let ids rels) =
  blockSize (outputSignals S.\\ ids) rels
relationSize outputSignals (Switch _ branches) =
  foldMap (blockSize outputSignals . branchRelations) branches
relationSize _ ModelCall{} = modelCallSize

blockSize :: Foldable f => Set Ident -> f Rel -> Size
blockSize outputSignals rels
  |
  -- Always inline empty relations. We used to inline relations with just one
  -- relation, but it's not necessarily a good idea when the same relation
  -- appears manytime. See for instance the heat example.
    null rels = InfSmall
  | otherwise = foldMap (relationSize outputSignals) rels

-- Performing inlining
sizeThreshold :: Size
sizeThreshold = Finite 0

lookupInst :: Map Name Decl -> Name -> TySubst EVar SVar -> Inst
lookupInst decls nm tySubst = case decls M.! nm of
  MonoDecl { declInst = inst }       -> inst
  Template { templateInsts = insts } -> insts M.! tySubst

inlineInExpr :: Map Name Decl -> Expr -> Expr
inlineInExpr decls expr@GlobalExpr { globalExpr = global@Global { globalName = nm, globalTyInst = tySubst } }
  = case args of
    Empty | exprSize body < sizeThreshold -> body
    _ -> expr { globalExpr = global { globalInst = inst } }
 where
  inst@Inst { instArgs = args, instBody = body } = lookupInst decls nm tySubst
inlineInExpr decls Call { callExpr = KnownCall (Saturated sat) global@Global { globalName = nm, globalTyInst = tySubst } args }
  = case sat of
    0 | exprSize body < sizeThreshold -> substExpr subst body
    _ -> Call
      { callExpr = functionCall
                     GlobalExpr { globalExpr = global { globalInst = inst } }
                     (fmap (inlineInExpr decls) args)
      }
 where
  inst@Inst { instArgs = instArgs, instBody = body } =
    lookupInst decls nm tySubst

  subst = foldr (\(Arg instArg _, arg) subst -> M.insert instArg arg subst)
                []
                (Seq.zip instArgs args)
inlineInExpr decls Call { callExpr = UnknownCall fun args } = Call
  { callExpr = functionCall (inlineInExpr decls fun)
                            (fmap (inlineInExpr decls) args)
  }
inlineInExpr decls OpExpr { operExpr = op } =
  arithOpExpr (fmap (inlineInExpr decls) op)
inlineInExpr decls model@Model { modelRelations = rels } =
  model { modelRelations = fmap (inlineInRel decls) rels }
inlineInExpr _ expr@LocalExpr{} = expr
inlineInExpr _ expr@Const{}     = expr

inlineInSig :: Map Name Decl -> Sig -> Sig
inlineInSig decls Expr { sigExpr = expr } =
  Expr { sigExpr = inlineInExpr decls expr }
inlineInSig decls Op { operSig = op } = arithOp (fmap (inlineInSig decls) op)
inlineInSig _     sig                 = sig

inlineInEqn :: Map Name Decl -> Eqn -> Eqn
inlineInEqn decls (GenEqn lhs rhs) =
  equation (inlineInSig decls lhs) (inlineInSig decls rhs)
inlineInEqn decls (CausalEqn i e) = CausalEqn i (inlineInExpr decls e)
inlineInEqn _     eqn@NameEqn{}   = eqn

inlineInRel :: Map Name Decl -> Rel -> Rel
inlineInRel decls (Rel when eqn) = Rel when (inlineInEqn decls eqn)
inlineInRel decls (ModelCall input model) =
  modelCall input (inlineInExpr decls model)
inlineInRel decls (Switch mode branches) = Switch
  mode
  (fmap
    (\branch@Branch { branchRelations = rels } ->
      branch { branchRelations = inlineInBlock decls rels }
    )
    branches
  )
inlineInRel decls (Let ids rels) = Let ids (inlineInBlock decls rels)

inlineInBlock :: Map Name Decl -> Seq Rel -> Seq Rel
inlineInBlock decls = fmap (inlineInRel decls)

inlineInInst :: Map Name Decl -> Inst -> Inst
inlineInInst decls inst@Inst { instBody = body } =
  inst { instBody = inlineInExpr decls body }

inlineInDecl :: Map Name Decl -> Decl -> Decl
inlineInDecl idecls decl@MonoDecl { declInst = inst } =
  decl { declInst = inlineInInst idecls inst }
inlineInDecl idecls template@Template { templateInsts = insts } =
  template { templateInsts = fmap (inlineInInst idecls) insts }

inlineInModule :: Module -> Module
inlineInModule Module { moduleDecls = decls } = Module
  { moduleDecls = go [] decls
  }
 where
  go env (decl :<| decls) =
    idecl :<| go (M.insert (declName idecl) decl env) decls
    where idecl = inlineInDecl env decl
  go _ Empty = Empty

inliningStep :: Applicative m => StepSpec m Module Module
inliningStep = Step
  { stepConf = StepConf
                 { stepName       = "inlining"
                 , stepStopBefore = ["stop-before-inlining"]
                 , stepStopAfter  = ["stop-after-inlining"]
                 , stepSkip       = SkipConf { skipFlags     = ["no-inlining"]
                                             , dontSkipFlags = []
                                             , skipByDefault = DontSkip
                                             }
                 }
  , stepExec = SingleStep (pure . inlineInModule)
  }
