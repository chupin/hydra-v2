{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}

{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies #-}

module Hydra.Flatten.AST
  ( module Hydra.Flatten.AST
  , module Hydra.Const
  , module Hydra.Type
  , module Hydra.Ident
  , module Hydra.Op
  , module Hydra.Name
  , module Hydra.Pretty
  , Label(..)
  , RootKind(..)
  , Mode(..)
  ) where

import           Data.Foldable
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import qualified Data.Map.Merge.Lazy           as M
import           Data.Maybe
import           Data.Semigroup          hiding ( Arg )
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           Data.Text                      ( Text )

import           GHC.Generics

import           Prelude                 hiding ( div
                                                , exp
                                                , log
                                                )

import           Hydra.AST                      ( RootKind(..) )
import qualified Hydra.AST                     as T
import           Hydra.Const
import           Hydra.Error                    ( internalError )
import           Hydra.Ident                    ( Ident
                                                , source
                                                )
import           Hydra.Name
import           Hydra.Op                hiding ( add
                                                , binop
                                                , div
                                                , exp
                                                , log
                                                , mul
                                                , pow
                                                , sub
                                                , unop
                                                )
import qualified Hydra.Op                      as Op
import           Hydra.Pretty
import           Hydra.Type                     ( EVar
                                                , PTExpr
                                                , PTSig
                                                , SVar
                                                , Scheme
                                                , TySubst
                                                , Typed(..)
                                                )
import qualified Hydra.Typing                  as T
                                                ( Global )

data TSig = TReal
  deriving (Eq, Ord)

instance Pretty TSig where
  pretty TReal = "real"

data TExpr = TSig TSig | TArr TExpr TExpr | TModel (Seq TSig)
  deriving ( Eq, Ord )

instance Pretty TExpr where
  pretty (TSig ts) = pretty ts
  pretty ty@TArr{} = parr [] ty
   where
    parr args (TArr arg ret) = parr (args :|> arg) ret
    parr args ret            = pargs args ret

    pargs Empty          ret = pretty ret
    pargs (arg :<| args) ret = pretty arg <+> "->" <+> pargs args ret

  pretty (TModel ts) = "model" <+> parens (commaList (fmap pretty ts))

data LocalSig = LocalSig Ident Word

data Sig = Expr { sigExpr :: Expr
                }
         | Local { localSig :: LocalSig
                 }
         | Op { operSig :: Op Sig
              }

data Arg = Arg
  { argName :: Ident
  , argType :: TExpr
  }

instance Typed Arg TExpr where
  typeOf = argType

newtype Arity = KnownArity Word

newtype Saturated = Saturated Word

data Call = KnownCall Saturated Global (Seq Expr) | UnknownCall Expr (Seq Expr)

tarr :: Foldable f => f TExpr -> TExpr -> TExpr
tarr targs tret = foldr TArr tret targs

expectedFunTyp :: TExpr -> a
expectedFunTyp typ = internalError
  ("Expected a function type but got type " ++ show (pretty typ) ++ ".")

typeApp :: TExpr -> Seq TExpr -> TExpr
typeApp ret           Empty        = ret
typeApp (TArr _ tret) (_ :<| args) = typeApp tret args
typeApp ty            (_ :<| _   ) = expectedFunTyp ty

instance Typed Call TExpr where
  typeOf (KnownCall _ Global { globalInst = inst } args) =
    typeApp (typeOf inst) (fmap typeOf args)
  typeOf (UnknownCall fun args) = typeApp (typeOf fun) (fmap typeOf args)

data Global = Global
  { globalName   :: Name
  , globalTyInst :: TySubst EVar SVar
  , globalInst   :: Inst
  }

data Expr =
    Const { constVal :: Const
          }
  | LocalExpr { localExprTyp  :: TExpr
              , localExprName :: Ident
              }
  | GlobalExpr { globalExpr :: Global
               }
  | Model { modelOutput    :: Seq Ident
          , modelRelations :: Seq Rel
          }
  | Call { callExpr :: Call
         }
  | OpExpr { operExpr :: Op Expr
           }

functionCall :: Expr -> Seq Expr -> Call
functionCall Call { callExpr = call } args = go call args
 where
  go call                   Empty = call
  go (UnknownCall fun args) args' = UnknownCall fun (args <> args')
  go call@(KnownCall (Saturated 0) _ _) args'@(_ :<| _) =
    UnknownCall Call { callExpr = call } args'
  go (KnownCall (Saturated missing) fun args) (arg :<| args') =
    go (KnownCall (Saturated (missing - 1)) fun (args :|> arg)) args'
functionCall GlobalExpr { globalExpr = global@Global { globalInst = inst } } args
  = case remainingArgs of
    Empty -> knownCall
    _     -> UnknownCall Call { callExpr = knownCall } remainingArgs
 where
  knownCall = KnownCall
    (Saturated (arity - fromIntegral (length passedArgs)))
    global
    passedArgs

  (passedArgs, remainingArgs) = Seq.splitAt (fromIntegral arity) args

  arity                       = instArity inst
functionCall expr args = UnknownCall expr args

modelCall :: Seq Ident -> Expr -> Rel
modelCall input Model { modelOutput = output, modelRelations = rels } = Let
  (foldr S.insert [] output)
  (  fmap
      (Rel Always)
      (Seq.zipWith (\i j -> NameEqn (LocalSig i 0) (LocalSig j 0)) input output)
  <> rels
  )
modelCall input model = ModelCall input model

class Substitution s where
  isEmptySubst :: s -> Bool
  lookupSigVar :: Ident -> s -> Maybe Sig
  lookupExprVar :: Ident -> s -> Maybe Expr
  substWithout :: s -> Set Ident -> s

instance Substitution (Map Ident Expr) where
  isEmptySubst = M.null

  lookupSigVar _ _ = Nothing

  lookupExprVar = M.lookup

  substWithout  = M.withoutKeys

instance Substitution (Map Ident Sig) where
  isEmptySubst = M.null

  lookupSigVar = M.lookup

  lookupExprVar _ _ = Nothing

  substWithout = M.withoutKeys

substExpr :: Substitution s => s -> Expr -> Expr
substExpr subst expr | isEmptySubst subst = expr
substExpr _ expr@Const{}                  = expr
substExpr _ expr@GlobalExpr{}             = expr
substExpr subst expr@LocalExpr { localExprName = nm } =
  fromMaybe expr (lookupExprVar nm subst)
substExpr subst OpExpr { operExpr = op } =
  arithOpExpr (fmap (substExpr subst) op)
substExpr subst Call { callExpr = KnownCall _ global args } = Call
  { callExpr = functionCall GlobalExpr { globalExpr = global }
                            (fmap (substExpr subst) args)
  }
substExpr subst Call { callExpr = UnknownCall fun args } = Call
  { callExpr = functionCall (substExpr subst fun) (fmap (substExpr subst) args)
  }
substExpr subst Model { modelOutput = output, modelRelations = rels } = Model
  { modelOutput    = output
  , modelRelations = fmap
                       (substRel (subst `substWithout` foldr S.insert [] output)
                       )
                       rels
  }

substSig :: Substitution s => s -> Sig -> Sig
substSig subst sig | isEmptySubst subst = sig
substSig subst Expr { sigExpr = expr } =
  Expr { sigExpr = substExpr subst expr }
substSig subst sig@Local { localSig = LocalSig i n } =
  maybe sig (differentiateN n Time) (lookupSigVar i subst)
substSig subst Op { operSig = op } = Op { operSig = fmap (substSig subst) op }

substEvt :: Substitution s => s -> Event -> Event
substEvt subst evt | isEmptySubst subst = evt
substEvt subst evt@Root { watchedSig = sig } =
  evt { watchedSig = substSig subst sig }

substCond :: Substitution s => s -> Condition -> Condition
substCond subst cond | isEmptySubst subst = cond
substCond subst (evt :-> mode) =
  substEvt subst evt :-> fmap (substSig subst) mode

substEqn :: Substitution s => s -> Eqn -> Eqn
substEqn subst eqn | isEmptySubst subst = eqn
substEqn subst (GenEqn lhs rhs) =
  equation (substSig subst lhs) (substSig subst rhs)
substEqn subst eqn@(NameEqn is@(LocalSig i _) js@(LocalSig j _)) =
  case (lookupSigVar i subst, lookupSigVar j subst) of
    (Nothing, Nothing) -> eqn
    (i', j') -> equation (fromMaybe (Local is) i') (fromMaybe (Local js) j')
substEqn subst (CausalEqn is@(LocalSig i _) e) = equation
  (fromMaybe (Local is) (lookupSigVar i subst))
  (Expr (substExpr subst e))

substRel :: Substitution s => s -> Rel -> Rel
substRel subst rel | isEmptySubst subst = rel
substRel subst (Rel when eqn)           = Rel when (substEqn subst eqn)
substRel subst (Let ids rels) =
  Let ids (fmap (substRel (subst `substWithout` ids)) rels)
substRel subst (ModelCall args expr) = modelCall
  (fmap (substArgs subst) args)
  (substExpr subst expr)
 where
  substArgs subst idt = case lookupSigVar idt subst of
    Nothing -> idt
    Just Local { localSig = LocalSig j 0 } -> j
    Just sig -> internalError
      ("It is forbidden to substitute \
                       \a model call argument with "
      ++ show (pretty sig)
      ++ "."
      )
substRel subst (Switch initMode branches) = Switch
  (fmap (substExpr subst) initMode)
  (fmap (substBranch subst) branches)
 where
  substBranch subst branch@Branch { branchRelations = rels, branchConditions = conds, branchArgs = args }
    | isEmptySubst subst'
    = branch
    | otherwise
    = branch { branchRelations  = fmap (substRel subst') rels
             , branchConditions = fmap (substCond subst') conds
             }
    where subst' = subst `substWithout` foldr (S.insert . argName) [] args

instance Typed Sig TSig where
  typeOf _ = TReal

instance Typed Expr TExpr where
  typeOf Const { constVal = RealConst _ } = TSig TReal
  typeOf Call { callExpr = call }         = typeOf call
  typeOf LocalExpr { localExprTyp = typ } = typ
  typeOf GlobalExpr { globalExpr = Global { globalInst = inst } } = typeOf inst
  typeOf OpExpr{}                         = TSig TReal
  typeOf Model { modelOutput = typ }      = TModel (fmap (const TReal) typ)

data Event = Root
  { rootKind   :: RootKind
  , watchedSig :: Sig
  }

data Merged k = OnlyL k | OnlyR k | Both k k

data MergedOp k = MergedUnOp ArithUnOp k | MergedBinOp ArithBinOp (Merged k)

data Gatherer k where
  Gatherer ::(Ident -> Word -> k) -> (Ident -> MergedOp k -> k) -> Gatherer k
  Combined ::(p -> q -> k) -> Gatherer p -> Gatherer q -> Gatherer k

mergedOpSemigroup :: Semigroup k => MergedOp k -> k
mergedOpSemigroup (MergedUnOp  _ k ) = k
mergedOpSemigroup (MergedBinOp _ mg) = case mg of
  OnlyL k  -> k
  OnlyR k  -> k
  Both p q -> p <> q

gatherInfo :: Gatherer k -> Sig -> Map Ident k
gatherInfo _ Expr{} = []
gatherInfo (Gatherer local _) (Local (LocalSig i n)) =
  M.singleton i (local i n)
gatherInfo gatherer@(Gatherer _ mergeInfo) Op { operSig = op } = case op of
  ArithUnOp op p ->
    M.mapWithKey (\i k -> mergeInfo i (MergedUnOp op k)) (gatherInfo gatherer p)
  ArithBinOp op p q -> M.merge (M.mapMissing missingR)
                               (M.mapMissing missingL)
                               (M.zipWithMatched both)
                               (gatherInfo gatherer p)
                               (gatherInfo gatherer q)
   where
    missingL i q = mergeInfo i (MergedBinOp op (OnlyR q))

    missingR i p = mergeInfo i (MergedBinOp op (OnlyL p))

    both i p q = mergeInfo i (MergedBinOp op (Both p q))
gatherInfo (Combined merge gp gq) sig =
  M.intersectionWith merge (gatherInfo gp sig) (gatherInfo gq sig)

pattern NegExpr :: Expr -> Expr
pattern NegExpr q =
  OpExpr { operExpr = ArithBinOp Sub (Const (RealConst 0)) q }

pattern RealConstSig :: Rational -> Sig
pattern RealConstSig c = Expr { sigExpr = Const { constVal = RealConst c } }

pattern Neg :: Sig -> Sig
pattern Neg q = Op { operSig = ArithBinOp Sub (RealConstSig 0) q }

data Order = Order !(Min Word) !(Max Word)
  deriving (Eq, Ord, Show)

instance Semigroup Order where
  Order mi1 ma1 <> Order mi2 ma2 = Order (mi1 <> mi2) (ma1 <> ma2)

order :: Word -> Order
order n = Order (Min n) (Max n)

minOrder :: Order -> Word
minOrder (Order (Min min) _) = min

maxOrder :: Order -> Word
maxOrder (Order _ (Max max)) = max

gatherOrder :: Gatherer Order
gatherOrder = Gatherer (\_ n -> order n) (\_ op -> mergedOpSemigroup op)

gatherLinearity :: Gatherer Bool
gatherLinearity = Gatherer
  (\_ _ -> True)
  (\_ op -> case op of
    MergedUnOp  _   _                -> False
    MergedBinOp Add (Both True True) -> True
    MergedBinOp Sub (Both True True) -> True
    MergedBinOp _   _                -> False
  )

freeSigVars :: Sig -> Map Ident Order
freeSigVars = gatherInfo gatherOrder

freeExprVars :: Sig -> Set Ident
freeExprVars Expr { sigExpr = expr } = go expr
 where
  go LocalExpr { localExprName = idt } = [idt]
  go Const{}                           = []
  go OpExpr { operExpr = op }          = foldMap go op
  go GlobalExpr{}                      = []
  go Model{}                           = []
  go Call { callExpr = KnownCall _ _ args } = foldMap go args
  go Call { callExpr = UnknownCall fun args } = go fun <> foldMap go args
freeExprVars Local{}             = []
freeExprVars Op { operSig = op } = foldMap freeExprVars op

exprSig :: Expr -> Sig
exprSig expr = Expr { sigExpr = expr }

constExpr :: Const -> Expr
constExpr = Const

realConstExpr :: Rational -> Expr
realConstExpr = constExpr . RealConst

realConst :: Rational -> Sig
realConst = exprSig . realConstExpr

zero :: Sig
zero = realConst 0

one :: Sig
one = realConst 1

ident :: Ident -> Word -> Sig
ident name n = Local { localSig = LocalSig name n }

data Label = Label
  { labelIndex :: Word
  , labelName  :: Text
  }
  deriving (Eq, Ord, Show)

data Mode r = Mode
  { modeLabel   :: Label
  , modePayload :: Seq r
  }
  deriving (Functor, Traversable, Foldable)

data Condition = Event :-> Mode Sig

conditionTargetLabel :: Condition -> Label
conditionTargetLabel (_ :-> Mode { modeLabel = target }) = target

data Branch = Branch
  { branchArgs       :: Seq Arg
  , branchReinits    :: Set Ident
  , branchConditions :: Seq Condition
  , branchRelations  :: Seq Rel
  }

makeLabelMap :: Map Label Branch -> Map Label Int
makeLabelMap branches = M.fromList (zip labels [0 ..])
  where labels = M.keys branches

data Eqn = CausalEqn LocalSig Expr | NameEqn LocalSig LocalSig | GenEqn Sig Sig

equation :: Sig -> Sig -> Eqn
equation (Local s1) (Local s2) = NameEqn s1 s2
equation (Local s1) (Expr e)  = CausalEqn s1 e
equation (Expr e) (Local s1)  = CausalEqn s1 e
equation (Op (ArithBinOp Sub s1 s2)) (RealConstSig 0) = equation s1 s2
equation s1@(RealConstSig 0) s2 = equation s2 s1
equation s1 s2                = GenEqn s1 s2

eqnSig :: Eqn -> Sig
eqnSig (GenEqn    lhs rhs) = lhs `sub` rhs
eqnSig (CausalEqn ls  e  ) = Local ls `sub` Expr e
eqnSig (NameEqn   is  js ) = Local is `sub` Local js

data Holds = Always | AtInit

data Rel = Rel Holds Eqn
         | Let (Set Ident) (Seq Rel)
         | Switch { initialMode    :: Mode Expr
                  , switchBranches :: Map Label Branch
                  }
         | ModelCall (Seq Ident) Expr

letRel :: Set Ident -> Seq Rel -> Seq Rel
letRel ids rels | S.null ids         = rels
letRel ids (Let ids' rels :<| Empty) = letRel (ids' <> ids) rels
letRel ids rels                      = [Let ids rels]


data Inst = Inst
  { instArgs :: Seq Arg
  , instBody :: Expr
  }

instArity :: Integral i => Inst -> i
instArity = fromIntegral . length . instArgs
{-# INLINE instArity #-}

instance Typed Inst TExpr where
  typeOf Inst { instArgs = args, instBody = body } =
    tarr (fmap typeOf args) (typeOf body)

data Decl = MonoDecl { declName :: Name
                     , declInst :: Inst
                     }
          | Template { declName      :: Name
                     , templateType  :: Scheme PTExpr
                     , template      :: T.Decl T.Global PTExpr PTSig Ident
                     , templateInsts :: Map (TySubst EVar SVar) Inst
                     }
  deriving ( Generic )

newtype Module = Module { moduleDecls :: Seq Decl
                        }

instance Pretty Arg where
  pretty (Arg idt typ) = parens (pretty idt <+> ":" <+> pretty typ)

instance Pretty LocalSig where
  pretty (LocalSig i n) = plocal i n
   where
    plocal i 0 = pretty i
    plocal i 1 = "der" <+> pretty i
    plocal i n = "der" <+> parens (plocal i (n - 1))

instance Pretty Sig where
  pretty Expr { sigExpr = expr }   = pretty expr
  pretty Local { localSig = lsig } = pretty lsig
  pretty Op { operSig = op }       = prettyOp (fmap pretty op)

instance Pretty Call where
  pretty (KnownCall (Saturated n) Global { globalName = nm } args) = parens
    ("'KNOWN-" <> pretty n <+> pretty nm <+> spaceList (fmap pretty args))
  pretty (UnknownCall fun args) =
    parens ("'UNKNOWN" <+> parens (pretty fun <+> spaceList (fmap pretty args)))

instance Pretty Global where
  pretty Global { globalName = nm, globalTyInst = inst } =
    parens (pretty nm <+> "@" <+> parens (pretty inst))

instance Pretty Expr where
  pretty Const { constVal = c } = pretty c
  pretty LocalExpr { localExprTyp = typ, localExprName = name } =
    parens (pretty name <+> ":" <+> pretty typ)
  pretty OpExpr { operExpr = op } = parens (prettyOp (fmap pretty op))
  pretty GlobalExpr { globalExpr = global } = pretty global
  pretty Model { modelOutput = output, modelRelations = rels } = nest
    ilvl
    ("model" <+> commaList (fmap pretty output) <+> "with" $+$ vsep
      (fmap pretty (toList rels))
    )
  pretty Call { callExpr = call } = pretty call

instance Pretty Event where
  pretty Root { rootKind = root, watchedSig = evt } =
    pretty root <> parens (pretty evt)

instance Pretty Eqn where
  pretty (CausalEqn sig  expr) = pretty sig <+> ":=" <+> pretty expr
  pretty (NameEqn   sig1 sig2) = pretty sig1 <+> "<=>" <+> pretty sig2
  pretty (GenEqn    lhs  rhs ) = pretty lhs <+> "=" <+> pretty rhs

instance Pretty Label where
  pretty Label { labelIndex = index, labelName = name } =
    pretty name <> "#" <> pretty index

instance Pretty r => Pretty (Mode r) where
  pretty Mode { modeLabel = label, modePayload = payload } =
    pretty label <> parens (commaList (fmap pretty payload))

instance Pretty Rel where
  pretty (Rel holds eqn) = case holds of
    Always -> pretty eqn
    AtInit -> "init" <+> pretty eqn
  pretty (Let locals rels) =
    nest
        ilvl
        (   "let"
        <+> commaList (fmap pretty (toList locals))
        <+> "in"
        $+$ block rels
        )
      $+$ "end"
  pretty (Switch ilab branches) = nest
    ilvl
    ("switch" <+> "init" <+> pretty ilab $+$ lineBlock
      (M.mapWithKey prettyBranch branches)
    )
  pretty (ModelCall output call) =
    commaList (fmap pretty output) <+> "<>" <+> pretty call

prettyBranch :: Label -> Branch -> Doc ann
prettyBranch label (Branch mode reinits conds rels) = nest
  ilvl
  (   "mode"
  <+> pretty label
  <>  parens (commaList (fmap pretty mode))
  <>  (if S.null reinits
        then ""
        else
          space
          <>  "reinit"
          <+> commaList (fmap pretty (toList reinits))
          <>  space
      )
  <>  nest ilvl ("->" $+$ block rels $+$ lineBlock (fmap pretty conds))
  )

instance Pretty Condition where
  pretty (sig :-> mode) = "when" <+> pretty sig <+> "->" <+> pretty mode

instance Pretty Inst where
  pretty Inst { instArgs = args, instBody = body } =
    nest ilvl (spaceList (fmap pretty args) <+> "=" $+$ pretty body)

instance Pretty Decl where
  pretty MonoDecl { declName = nm, declInst = inst } =
    "let [mono]" <+> pretty nm <+> pretty inst
  pretty Template { templateInsts = insts, template = T.Decl { T.declName = nm, T.declParams = args, T.declBody = body } }
    = nest
      ilvl
      (   "let [template]"
      <+> pretty nm
      <+> spaceList (fmap pretty args)
      <+> "="
      $+$ pretty body
      $+$ nest ilvl (prettyInsts insts)
      )
   where
    prettyInst tySubst inst = nest ilvl ("[Subst]" $+$ pretty tySubst)
      $+$ nest ilvl ("[Code]" $+$ pretty inst)

    prettyInsts insts =
      --nest ilvl
      "[instantiations]" $+$ lineBlock (M.mapWithKey prettyInst insts)

instance Pretty Module where
  pretty Module { moduleDecls = decls } =
    lineBlock (fmap pretty (toList decls))

binopExpr :: ArithBinOp -> Expr -> Expr -> Expr
binopExpr op (Const (RealConst p)) (Const (RealConst q)) =
  Const (RealConst (evalOp (Op.binop op p q)))
binopExpr Add (Const (RealConst 0)) q                     = q
binopExpr Add p                     (Const (RealConst 0)) = p
binopExpr Add (NegExpr p)           q                     = binopExpr Sub q p
binopExpr Add p                     (NegExpr q)           = binopExpr Sub p q
binopExpr Mul (Const (RealConst 0)) _                     = Const (RealConst 0)
binopExpr Mul _                     (Const (RealConst 0)) = Const (RealConst 0)
binopExpr Mul (Const (RealConst 1)) q                     = q
binopExpr Mul p                     (Const (RealConst 1)) = p
binopExpr Div p (Const (RealConst c)) =
  binopExpr Mul (Const (RealConst (1 / c))) p
binopExpr Pow _ (Const (RealConst 0)) = Const (RealConst 0)
binopExpr Pow p (Const (RealConst 1)) = p
binopExpr op  p q                     = OpExpr (Op.binop op p q)

unopExpr :: ArithUnOp -> Expr -> Expr
unopExpr op (Const (RealConst p)) = Const (RealConst (evalOp (Op.unop op p)))
unopExpr op p                     = OpExpr (Op.unop op p)

arithOpExpr :: Op Expr -> Expr
arithOpExpr (ArithBinOp op p q) = binopExpr op p q
arithOpExpr (ArithUnOp op p   ) = unopExpr op p

binop :: ArithBinOp -> Sig -> Sig -> Sig
binop op (RealConstSig p) (RealConstSig q) =
  realConst (evalOp (ArithBinOp op p q))
binop Add (RealConstSig 0) p                = p
binop Add p                (RealConstSig 0) = p
binop Add (Neg p)          q                = binop Sub q p
binop Add p                (Neg          q) = binop Sub p q
binop Sub p                (RealConstSig 0) = p
binop Sub p                (Neg          q) = binop Add p q
binop Mul (RealConstSig 0) _                = zero
binop Mul _                (RealConstSig 0) = zero
binop Mul (RealConstSig 1) p                = p
binop Mul p                (RealConstSig 1) = p
binop Div p                (RealConstSig c) = binop Mul (realConst (1 / c)) p
binop Pow _                (RealConstSig 0) = one
binop Pow p                (RealConstSig 1) = p
binop op  (Expr p)         (Expr         q) = Expr (binopExpr op p q)
binop op  p                q                = Op (Op.binop op p q)

unop :: ArithUnOp -> Sig -> Sig
unop op (Expr expr) = Expr (unopExpr op expr)
unop op p           = Op (Op.unop op p)

arithOp :: Op Sig -> Sig
arithOp (ArithBinOp op p q) = binop op p q
arithOp (ArithUnOp op p   ) = unop op p

infixl 6 `add`

infixl 6 `sub`

infixl 7 `mul`

infixl 7 `div`

add :: Sig -> Sig -> Sig
add = binop Add

sub :: Sig -> Sig -> Sig
sub = binop Sub

mul :: Sig -> Sig -> Sig
mul = binop Mul

div :: Sig -> Sig -> Sig
div = binop Div

pow :: Sig -> Sig -> Sig
pow = binop Pow

exp :: Sig -> Sig
exp = unop Exp

log :: Sig -> Sig
log = unop Log

sqrt :: Sig -> Sig
sqrt = unop Sqrt

neg :: Sig -> Sig
neg p = realConst 0 `sub` p

square :: Sig -> Sig
square p = p `mul` p

inv :: Sig -> Sig
inv p = realConst 1 `div` p

data Against = Time | Variable Ident Word

fromOpTree :: OpTree Sig -> Sig
fromOpTree (OpLeaf  sig) = sig
fromOpTree (OpConst c  ) = realConst c
fromOpTree (OpNode  op ) = arithOp (fmap fromOpTree op)

differentiate :: Against -> Sig -> Sig
differentiate _       Expr{}                 = realConst 0
differentiate against (Local (LocalSig i n)) = case against of
  Time -> Local (LocalSig i (n + 1))
  Variable j m | i == j && n == m -> realConst 1
               | otherwise        -> realConst 0
differentiate against (Op op) = case op of
  ArithUnOp op p    -> differentiate against p `mul` fromOpTree (derOp op p)
  ArithBinOp op p q -> case op of
    Add -> p' `add` q'
    Sub -> p' `sub` q'
    Mul -> p' `mul` q `add` p `mul` q'
    Div -> (p' `mul` q `sub` p `mul` q') `div` (q `mul` q)
    Pow -> case q of
      Expr{} ->
        q `mul` differentiate against p `mul` (p `pow` (q `sub` realConst 1))
      _ -> differentiate against (exp (q `mul` log p))
   where
    p' = differentiate against p

    q' = differentiate against q

differentiateN :: Word -> Against -> Sig -> Sig
differentiateN 0 _ sig = sig
differentiateN n against sig =
  differentiate against (differentiateN (n - 1) against sig)
