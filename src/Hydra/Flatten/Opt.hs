{-# LANGUAGE OverloadedStrings #-}

module Hydra.Flatten.Opt
  ( optStep
  ) where

import           Hydra.Flatten.AST              ( Module )
import           Hydra.Flatten.Opt.CopyPropagation
                                                ( copyPropagationStep )
import           Hydra.Flatten.Opt.Inlining     ( inliningStep )
import           Hydra.Steps

optimisationSpellings :: [String]
optimisationSpellings = [ "optimi" ++ s : "ation" | s <- ['s', 'z'] ]

mildOpt :: Applicative m => StepSpec m Module Module
mildOpt = Step
  { stepConf = StepConf
                 { stepName       = "mild optimization"
                 , stepStopBefore = [ "stop-before-mild-" ++ opt
                                    | opt <- optimisationSpellings
                                    ]
                 , stepStopAfter  = [ "stop-after-mild-" ++ opt
                                    | opt <- optimisationSpellings
                                    ]
                 , stepSkip       = DontSkip
                 }
  , stepExec = SubStep (inliningStep `Then` copyPropagationStep)
  }

aggrOpt :: Applicative m => StepSpec m Module Module
aggrOpt = Step
  { stepExec = SubStep (inliningStep `Then` copyPropagationStep)
  , stepConf = StepConf
                 { stepName       = "aggressive optimisation"
                 , stepStopBefore = [ "stop-before-aggressive-" ++ opt
                                    | opt <- optimisationSpellings
                                    ]
                 , stepStopAfter  = [ "stop-after-aggressive-" ++ opt
                                    | opt <- optimisationSpellings
                                    ]
                 , stepSkip       = SkipConf
                                      { skipByDefault = Skip
                                      , dontSkipFlags = [ "aggr-opt"
                                                        , "aggressive-optimisation"
                                                        , "aggressive-optimization"
                                                        ]
                                      , skipFlags = [ "no-aggr-opt"
                                                    , "no-aggressive-optimisation"
                                                    , "no-aggressive-optimization"
                                                    ]
                                      }
                 }
  }

optStep :: Applicative m => StepSpec m Module Module
optStep = Step
  { stepConf = StepConf
                 { stepName       = "optimisation"
                 , stepStopBefore = [ "stop-before-" ++ opt
                                    | opt <- optimisationSpellings
                                    ]
                 , stepStopAfter  = [ "stop-after-" ++ opt
                                    | opt <- optimisationSpellings
                                    ]
                 , stepSkip       = SkipConf
                                      { skipByDefault = Skip
                                      , skipFlags     = [ "no-opt"
                                                        , "no-optimisation"
                                                        , "no-optimization"
                                                        ]
                                      , dontSkipFlags = [ "opt"
                                                        , "optimise"
                                                        , "optimize"
                                                        ]
                                      }
                 }
  , stepExec = SubStep (copyPropagationStep `Then` mildOpt `Then` aggrOpt)
  }
