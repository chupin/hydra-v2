{-# LANGUAGE ForeignFunctionInterface, RecursiveDo, GeneralizedNewtypeDeriving #-}

#include <ida/ida.h>
#include <kinsol/kinsol.h>
#include <sundials/sundials_iterative.h>
#include <sundials/sundials_linearsolver.h>
#include <sundials/sundials_nonlinearsolver.h>
#include <sundials/sundials_nvector.h>
#include <nvector/nvector_serial.h>
#include <sunmatrix/sunmatrix_dense.h>
#include <sunmatrix/sunmatrix_sparse.h>
#include <sunlinsol/sunlinsol_dense.h>
#include <sunlinsol/sunlinsol_klu.h>
#include <sunnonlinsol/sunnonlinsol_newton.h>

module Hydra.Sundials.IDA.C where

import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Storable
import GHC.Int (Int32)
import Foreign.Marshal
import Foreign.C.Types

{#context lib="libsundials_ida.so"#}
{#context lib="libsundials_kinsol"#}
{#context lib="libsundials_sunlinsolklu"#}

-- Sundials types

type RealType = {#type realtype #}
{#typedef realtype RealType #}

sizeRealType :: Int
sizeRealType = {#sizeof realtype#}

type IndexType = {#type sunindextype #}
{#typedef sunindextype IndexType #}

-- Serial NVector


{#pointer *N_Vector as NVector foreign finalizer wrap_n_vdestroy as n_v_destroy newtype#}

#c
void wrap_n_vnew_serial(N_Vector* ptr, sunindextype idx) {
  N_Vector vec = N_VNew_Serial(idx);
  *ptr = vec;
}

void wrap_n_vdestroy(N_Vector* ptr) {
  N_VDestroy(*ptr);
}

sunindextype wrap_nv_length(N_Vector* vec) {
  return ((N_VectorContent_Serial)(*vec)->content)->length;
}

void wrap_n_vset(N_Vector* vec, sunindextype idx, realtype val) {
  ((N_VectorContent_Serial)(*vec)->content)->data[idx] = val;
}

realtype* wrap_n_vcontent(N_Vector* vec) {
  return ((N_VectorContent_Serial)(*vec)->content)->data;
}

realtype wrap_n_vlkp(N_Vector* vec, sunindextype idx) {
  return ((N_VectorContent_Serial)(*vec)->content)->data[idx];
}
#endc

n_vnew_serial :: IndexType -> IO NVector
n_vnew_serial idx = do
  fp <- mallocForeignPtrBytes {#sizeof N_Vector#}
  withForeignPtr fp (\p -> {#call wrap_n_vnew_serial #} p idx)
  pure (NVector fp)

{#fun wrap_n_vnew_serial as n_vnew_serial_imp
   { `NVector'
   , id `IndexType'
   } -> `()' #}

{#fun wrap_nv_length as nv_length
   { `NVector'
   } -> `IndexType' id #}

{#fun wrap_n_vset as n_vset
  { `NVector'
  , id `IndexType'
  , id `RealType'
  } -> `()' #}

{#fun wrap_n_vlkp as n_vlookup
  { `NVector'
  , id `IndexType'
  } -> `RealType' id #}

{#fun wrap_n_vcontent as n_vcontent
  { `NVector'
  } -> `Ptr RealType' id #}

-- IDA

{#enum define IDAResult { IDA_SUCCESS as IDASuccess
                        , IDA_MEM_NULL as IDAMemNull
                        , IDA_MEM_FAIL as IDAMemFail
                        , IDA_ILL_INPUT as IDAIllInput
                        , IDA_NO_MALLOC as IDANoMalloc
                        , IDA_LSETUP_FAIL as IDALinearSolverSetupFail
                        , IDA_LSOLVE_FAIL as IDALinearSolverSolveFail
                        , IDA_BAD_EWT as IDAIllegalErrorWeightVector
                        , IDA_FIRST_RES_FAIL as IDAFirstResidualFunctionCallFail
                        , IDA_RES_FAIL as IDAResidualFunctionIrrecoverableFail
                        , IDA_NO_RECOVERY as IDACalcICCannotRecover
                        , IDA_CONSTR_FAIL as IDACannotSatisfyConstraint
                        , IDA_LINESEARCH_FAIL as IDALinearSearchFail
                        , IDA_CONV_FAIL as IDAFailedToGetNewtonConvergence
                        , IDA_TSTOP_RETURN as IDAReachedStopTime
                        , IDA_ROOT_RETURN as IDAFoundRoot
                        , IDA_TOO_MUCH_WORK as IDATooMuchWork
                        , IDA_ERR_FAIL as IDATooManyErrorTestFailure
                        , IDA_REP_RES_ERR as IDAReachedRecoverableError
                        , IDA_RTFUNC_FAIL as IDANonRecoverableRootFunFail
                        } deriving(Eq, Ord, Show) #}

-- IDA allocation and deallocation functions

foreign import ccall "IDACreate"
  c_idaCreate :: IO (Ptr ())

#c
void c_idaFree(void* mem) {
  void** ptr_mem = &mem;
  IDAFree(ptr_mem);
}
#endc

foreign import ccall "&c_idaFree"
  c_idaFree :: FunPtr (Ptr () -> IO ())

newtype IDAMem = IDAMem { getIDAMemPtr :: ForeignPtr () }

newIDAMem :: IO IDAMem
newIDAMem = do
  ptr <- c_idaCreate
  fp <- newForeignPtr c_idaFree ptr
  pure (IDAMem fp)

withIDAMem :: IDAMem -> (Ptr () -> IO k) -> IO k
withIDAMem (IDAMem fp) cont = withForeignPtr fp cont

{#pointer IDAResFn as IDAResFn#}

#c
typedef struct wrapped_fn {
  int(*res_fn)(realtype, N_Vector*, N_Vector*, N_Vector*);
  int(*jac_fn)(realtype, realtype, N_Vector*, N_Vector*, N_Vector*, SUNMatrix*);
  int(*root_fn)(realtype, N_Vector*, N_Vector*, realtype*);
  int(*init_fn)(N_Vector*, N_Vector*);
  int(*init_jac_fn)(N_Vector*, SUNMatrix*);
} wrapped_fn;

int wrap_res_fn(realtype tt, N_Vector ys, N_Vector yps, N_Vector rr, void* user_data) {
  wrapped_fn* wrapped_fn = user_data;
  int(*hs_fn)(realtype, N_Vector*, N_Vector*, N_Vector*) = wrapped_fn->res_fn;
  return hs_fn(tt, &ys, &yps, &rr);
}

int wrap_jac_fn( realtype tt
               , realtype ct
               , N_Vector ys
               , N_Vector yps
               , N_Vector rr
               , SUNMatrix jac
               , void* user_data
               , N_Vector t1
               , N_Vector t2
               , N_Vector t3
               ) {
  wrapped_fn* wrapped_fn = user_data;
  int(*jac_fn)(realtype, realtype, N_Vector*, N_Vector*, N_Vector*, SUNMatrix*) = wrapped_fn->jac_fn;
  return jac_fn(tt, ct, &ys, &yps, &rr, &jac);
}

int wrap_root_fn(realtype tt, N_Vector ys, N_Vector yps, realtype* gout, void* user_data) {
  wrapped_fn* wrapped_fn = user_data;
  int(*root_fn)(realtype, N_Vector*, N_Vector*, realtype*) = wrapped_fn->root_fn;
  return root_fn(tt, &ys, &yps, gout);
}

int wrap_init_fn(N_Vector u, N_Vector fv, void* user_data)  {
  wrapped_fn* wrapped_fn = user_data;
  int(*init_fn)(N_Vector*, N_Vector*) = wrapped_fn->init_fn;
  return init_fn(&u, &fv);
}

int wrap_init_jac_fn(N_Vector u, N_Vector fu, SUNMatrix J, void* user_data, N_Vector tmp1, N_Vector tmp2) {
  wrapped_fn* wrapped_fn = user_data;
  int(*init_jac_fn)(N_Vector*, SUNMatrix*) = wrapped_fn->init_jac_fn;
  return init_jac_fn(&u, &J);
}

void wrap_funs( wrapped_fn* wrapped_fn
              , void* res_fn
              , void* jac_fn
              , void* root_fn
              , void* init_fn
              , void* init_jac_fn
              ) {
  wrapped_fn->res_fn = res_fn;
  wrapped_fn->jac_fn = jac_fn;
  wrapped_fn->root_fn = root_fn;
  wrapped_fn->init_fn = init_fn;
  wrapped_fn->init_jac_fn = init_jac_fn;
}
#endc

foreign import ccall "&wrap_res_fn"
  wrapResFn :: FunPtr (RealType -> NVector -> NVector -> NVector -> Ptr () -> IO Int32)

foreign import ccall "&wrap_jac_fn"
  wrapJacFn :: FunPtr (RealType -> RealType -> NVector -> NVector -> NVector -> matrix -> Ptr () -> NVector -> NVector -> NVector -> IO Int32)

foreign import ccall "&wrap_root_fn"
  wrapRootFn :: FunPtr (RealType -> NVector -> NVector -> Ptr RealType -> Ptr () -> IO Int32)

foreign import ccall "&wrap_init_fn"
  wrapInitFn :: FunPtr (NVector -> NVector -> Ptr () -> IO Int32)

foreign import ccall "&wrap_init_jac_fn"
  wrapInitJacFn :: FunPtr (NVector -> NVector -> matrix -> Ptr () -> NVector -> NVector -> IO Int32)

wrapFunPtrs :: FunPtr res
            -> FunPtr jac
            -> FunPtr root
            -> FunPtr init
            -> FunPtr jacInit
            -> IO (ForeignPtr ())
wrapFunPtrs resFn jacFn rootFn initFn initJacFn = do
  wrappedFnPtr <- mallocForeignPtrBytes {#sizeof wrapped_fn#}
  withForeignPtr wrappedFnPtr $ \wrapFunsPtr ->
    {#call wrap_funs #} wrapFunsPtr (castFunPtrToPtr resFn)
                                    (castFunPtrToPtr jacFn)
                                    (castFunPtrToPtr rootFn)
                                    (castFunPtrToPtr initFn)
                                    (castFunPtrToPtr initJacFn)
  pure wrappedFnPtr

#c
typedef int res_fn_out;
#endc

type IDAResFunTyp =  RealType
                  -> Ptr NVector
                  -> Ptr NVector
                  -> Ptr NVector
                  -> IO {#type res_fn_out#}

type IDAJacFunTyp m =  RealType
                    -> RealType
                    -> Ptr NVector
                    -> Ptr NVector
                    -> Ptr NVector
                    -> Ptr m
                    -> IO {#type res_fn_out#}

type IDARootFunTyp =  RealType
                   -> RealType
                   -> Ptr NVector
                   -> Ptr NVector
                   -> Ptr RealType
                   -> IO {#type res_fn_out#}

type KinInitFunTyp =  Ptr NVector
                   -> Ptr NVector
                   -> IO {#type res_fn_out#}

type KinInitJacFunTyp m =  Ptr NVector
                        -> Ptr m
                        -> IO {#type res_fn_out#}

foreign import ccall "wrapper"
  allocResFn :: IDAResFunTyp
             -> IO (FunPtr IDAResFunTyp)

foreign import ccall "wrapper"
  allocJacFn :: IDAJacFunTyp m
             -> IO (FunPtr (IDAJacFunTyp m))

foreign import ccall "wrapper"
  allocRootFn :: IDARootFunTyp
              -> IO (FunPtr IDARootFunTyp)

foreign import ccall "wrapper"
  allocInitFn :: KinInitFunTyp
              -> IO (FunPtr KinInitFunTyp)

foreign import ccall "wrapper"
  allocInitJacFn :: KinInitJacFunTyp m
                 -> IO (FunPtr (KinInitJacFunTyp m))

{#fun IDAInit as idaInit
           { withIDAMem* `IDAMem'
           , `IDAResFn'
           , id `RealType'
           , %`NVector'
           , %`NVector'
           } -> `IDAResult' #}

-- Setting tolerances

{#fun IDASStolerances as idaSSTolerances
  { withIDAMem* `IDAMem'
  , `RealType'
  , `RealType'
  } -> `IDAResult' #}

{#fun IDASVtolerances as idaSVTolerances
  { withIDAMem* `IDAMem'
  , `RealType'
  , %`NVector'
  } -> `IDAResult' #}

{#pointer IDAEwtFn as IDAEwtFn newtype#}

{#fun IDAWFtolerances as idaWFTolerances
  { withIDAMem* `IDAMem'
  , `IDAEwtFn'
  } -> `IDAResult' #}

-- Setting user data
{#fun IDASetUserData as idaSetUserData
  { withIDAMem* `IDAMem'
  , `Ptr ()'
  } -> `IDAResult' #}

-- Setting solvers

-- Sundials matrices
{#pointer *SUNMatrix as Matrix foreign finalizer wrap_SUNMatrixDestroy newtype#}

#c
void wrap_SUNMatrixDestroy(SUNMatrix* ptr) {
  SUNMatDestroy(*ptr);
}
#endc

-- Dense matrices
newtype DenseMatrix = DenseMatrix Matrix

withDenseMatrix :: DenseMatrix -> (Ptr Matrix -> IO k) -> IO k
withDenseMatrix (DenseMatrix (Matrix fp)) cont = withForeignPtr fp cont

newDenseMatrix :: IndexType
               -> IndexType
               -> IO DenseMatrix
newDenseMatrix row col = do
  fp <- mallocForeignPtrBytes {#sizeof SUNMatrix#}
  withForeignPtr fp $ \matptr ->
    {#call wrap_SUNDenseMatrix#} matptr row col
  pure (DenseMatrix (Matrix fp))

#c
void wrap_SUNDenseMatrix(SUNMatrix* ptr, sunindextype row, sunindextype col) {
  SUNMatrix mat = SUNDenseMatrix(row, col);
  *ptr = mat;
}

realtype wrap_SUNDenseMatrixGetElt(SUNMatrix* matrix, sunindextype i, sunindextype j) {
  return SM_ELEMENT_D((*matrix), i, j);
}

void wrap_SUNDenseMatrixSetElt(SUNMatrix* matrix, sunindextype i, sunindextype j, realtype v) {
  SM_ELEMENT_D((*matrix), i, j) = v;
}
#endc

{#fun wrap_SUNDenseMatrixSetElt as setDenseMatrixElt
  { withDenseMatrix* `DenseMatrix'
  , id `IndexType'
  , id `IndexType'
  , id `RealType'
  } -> `()' #}

{#fun wrap_SUNDenseMatrixGetElt as getDenseMatrixElt
  { withDenseMatrix* `DenseMatrix'
  , id `IndexType'
  , id `IndexType'
  } -> `RealType' id #}

{#fun SUNDenseMatrix_Rows as getDenseMatrixRowCount
  { withDenseMatrix* %`DenseMatrix'
  } -> `IndexType' id #}

{#fun SUNDenseMatrix_Columns as getDenseMatrixColCount
  { withDenseMatrix* %`DenseMatrix'
  } -> `IndexType' id #}

-- Sparse matrices

newtype SparseMatrix = SparseMatrix { getSparseMatrixPtr :: Matrix }

withSparseMatrix :: SparseMatrix
                 -> (Ptr Matrix -> IO k)
                 -> IO k
withSparseMatrix (SparseMatrix (Matrix fp)) cont = withForeignPtr fp cont

#c
void wrap_SUNSparseMatrix( SUNMatrix* ptr
                         , sunindextype row
                         , sunindextype col
                         , sunindextype nnz
                         , int sparsetype) {
  SUNMatrix mat = SUNSparseMatrix(row, col, nnz, sparsetype);
  *ptr = mat;
}

void wrap_SUNSparseSet(SUNMatrix* matrix, sunindextype row, sunindextype col, realtype val) {

    if (SM_SPARSETYPE_S((*matrix)) == CSC_MAT) {
        sunindextype col_start = SM_INDEXPTRS_S((*matrix))[col];
        sunindextype next_col_start = SM_INDEXPTRS_S((*matrix))[col+1];
        for (sunindextype data_ptr = col_start; data_ptr < next_col_start; ++data_ptr) {
            if (SM_INDEXVALS_S((*matrix))[data_ptr] == row) {
                SM_DATA_S((*matrix))[data_ptr] = val;
                return;
            }
        }
    } else {
        sunindextype row_start = SM_INDEXPTRS_S((*matrix))[row];
        sunindextype next_row_start = SM_INDEXPTRS_S((*matrix))[row+1];
        for (sunindextype data_ptr = row_start; data_ptr < next_row_start; ++data_ptr) {
            if (SM_INDEXVALS_S((*matrix))[data_ptr] == col) {
                SM_DATA_S((*matrix))[data_ptr] = val;
                return;
            }
        }
    }
    printf("CATASTROPHIC ERROR");
    exit(-1);
}

void wrap_SUNSparseSetFast(SUNMatrix* matrix, sunindextype idx, realtype val) {
  SM_DATA_S((*matrix))[idx] = val;
}

void wrap_SUNSparseSetIdxPtrs(SUNMatrix* matrix, sunindextype idx, sunindextype val) {
  SM_INDEXPTRS_S((*matrix))[idx] = val;
}

void wrap_SUNSparseSetIdxVals(SUNMatrix* matrix, sunindextype idx, sunindextype val) {
  SM_INDEXVALS_S((*matrix))[idx] = val;
}
#endc

{#enum define SparseMatrixRepr { CSR_MAT as CompressedSparseRow
                               , CSC_MAT as CompressedSparseCol
                               } deriving(Eq, Ord, Show) #}

newSparseMatrix :: IndexType
                -> IndexType
                -> IndexType
                -> SparseMatrixRepr
                -> IO SparseMatrix
newSparseMatrix row col nnz typ = do
  fp <- mallocForeignPtrBytes {#sizeof SUNMatrix#}
  withForeignPtr fp $ \matptr ->
    {#call wrap_SUNSparseMatrix #} matptr row col nnz (fromIntegral (fromEnum typ))
  pure (SparseMatrix (Matrix fp))

{#fun wrap_SUNSparseSet as setSparseMatrixElt
  { withSparseMatrix* `SparseMatrix'
  , id `IndexType'
  , id `IndexType'
  , id `RealType'
  } -> `()' #}

{#fun wrap_SUNSparseSetIdxPtrs as sparseMatSetIdxPtr
  { withSparseMatrix* `SparseMatrix'
  , id `IndexType'
  , id `IndexType'
  } -> `()' #}

{#fun wrap_SUNSparseSetIdxVals as sparseMatSetIdxVal
  { withSparseMatrix* `SparseMatrix'
  , id `IndexType'
  , id `IndexType'
  } -> `()' #}

-- Linear solver

{#pointer *SUNLinearSolver as LinearSolver foreign finalizer wrap_sunlin_free newtype#}

newtype DenseLinearSolver = DenseLinearSolver LinearSolver

withDenseLinearSolver :: DenseLinearSolver
                      -> (Ptr LinearSolver -> IO k)
                      -> IO k
withDenseLinearSolver (DenseLinearSolver (LinearSolver fp)) cont =
  withForeignPtr fp cont

#c
void wrap_sunlin_dense(SUNLinearSolver* ptr, N_Vector* vec, SUNMatrix* matrix) {
  SUNLinearSolver solver = SUNLinSol_Dense(*vec, *matrix);
  *ptr = solver;
}
void wrap_sunlin_free(SUNLinearSolver* solver) {
  SUNLinSolFree(*solver);
}
#endc

sunLinSolDense :: NVector -> DenseMatrix -> IO DenseLinearSolver
sunLinSolDense (NVector fpvec) (DenseMatrix (Matrix fpmat)) = do
  fp <- mallocForeignPtrBytes {#sizeof SUNLinearSolver#}
  withForeignPtr fp $ \solvptr ->
    withForeignPtr fpvec $ \vecptr ->
    withForeignPtr fpmat $ \matptr ->
    {#call wrap_sunlin_dense #} solvptr vecptr matptr
  pure (DenseLinearSolver (LinearSolver fp))

newtype KLULinearSolver = KLULinearSolver LinearSolver

#c
void wrap_sunlin_klu(SUNLinearSolver* ptr, N_Vector* vec, SUNMatrix* matrix) {
  SUNLinearSolver solver = SUNLinSol_KLU(*vec, *matrix);
  *ptr = solver;
}
#endc

withKLULinearSolver :: KLULinearSolver
                    -> (Ptr LinearSolver -> IO k)
                    -> IO k
withKLULinearSolver (KLULinearSolver (LinearSolver fp)) cont =
  withForeignPtr fp cont

sunLinSolKLU :: NVector -> SparseMatrix -> IO KLULinearSolver
sunLinSolKLU (NVector fpvec) (SparseMatrix (Matrix fpmat)) = do
  fp <- mallocForeignPtrBytes {#sizeof SUNLinearSolver#}
  withForeignPtr fp $ \solvptr ->
    withForeignPtr fpvec $ \vecptr ->
    withForeignPtr fpmat $ \matptr -> do
    {#call wrap_sunlin_klu #} solvptr vecptr matptr
  pure (KLULinearSolver (LinearSolver fp))

{#fun IDASetLinearSolver as idaSetDenseLinearSolver
  { withIDAMem* `IDAMem'
  , withDenseLinearSolver* %`DenseLinearSolver'
  , withDenseMatrix* %`DenseMatrix'
  } -> `IDAResult' #}

{#fun IDASetLinearSolver as idaSetKLULinearSolver
  { withIDAMem* `IDAMem'
  , withKLULinearSolver* %`KLULinearSolver'
  , withSparseMatrix* %`SparseMatrix'
  } -> `IDAResult' #}

{#pointer IDALsJacFn as IDALsJacFn#}

{#fun IDASetJacFn as idaSetJacFn
  { withIDAMem* `IDAMem'
  , `IDALsJacFn'
  } -> `IDAResult' #}

#c

#endc

{#pointer *SUNNonlinearSolver as NonLinearSolver foreign finalizer wrap_sunnonlin_free newtype#}

sunNonLinSolNewton :: NVector -> IO NonLinearSolver
sunNonLinSolNewton (NVector fpvec) = do
  fp <- mallocForeignPtrBytes {#sizeof SUNNonlinearSolver#}
  withForeignPtr fp $ \solvptr ->
    withForeignPtr fpvec $ \vecptr ->
    {#call wrap_sunnonlin_newton #} solvptr vecptr
  pure (NonLinearSolver fp)

#c
void wrap_sunnonlin_newton(SUNNonlinearSolver* ptr, N_Vector* vec) {
  SUNNonlinearSolver solver = SUNNonlinSol_Newton(*vec);
  *ptr = solver;
}
void wrap_sunnonlin_free(SUNNonlinearSolver* solver) {
  SUNNonlinSolFree(*solver);
}
#endc

{#fun IDASetNonlinearSolver as idaSetNonLinearSolver
  { withIDAMem* `IDAMem'
  , %`NonLinearSolver'
  } -> `IDAResult' #}

-- Initial condition calculation function

{#enum define InitConditionOpt
 { IDA_YA_YDP_INIT as ComputeAlgFromDer
 , IDA_Y_INIT as ComputeFromDer
 } deriving(Eq, Ord) #}

{#fun IDACalcIC as idaCalcInitCondition
  { withIDAMem* `IDAMem'
  , `InitConditionOpt'
  , `RealType' } -> `IDAResult' #}

-- Rootfinding initialization function

{#pointer IDARootFn as IDARootFn #}

{#fun IDARootInit as idaRootInit
  { withIDAMem* `IDAMem'
  , `Int'
  , `IDARootFn' } -> `IDAResult' #}

{#fun IDAGetRootInfo as idaRootInfo
  { withIDAMem* `IDAMem'
  , id `Ptr CInt'
  } -> `IDAResult' #}

-- IDA solver function

{#enum define SolverTask
  { IDA_NORMAL as Normal
  , IDA_ONE_STEP as OneStep
  } deriving(Eq, Ord) #}

{#fun IDASolve as idaSolve
  { withIDAMem* `IDAMem'
  , `RealType'
  , alloca- `RealType' peek*
  , %`NVector'
  , %`NVector'
  , `SolverTask' } -> `IDAResult' #}

-- Statistics

{#fun IDAGetIntegratorStats as idaGetIntegratorStats
  { withIDAMem* `IDAMem'
  , alloca- `CLong' peek*
  , alloca- `CLong' peek*
  , alloca- `CLong' peek*
  , alloca- `CLong' peek*
  , alloca- `CInt' peek*
  , alloca- `CInt' peek*
  , alloca- `RealType' peek*
  , alloca- `RealType' peek*
  , alloca- `RealType' peek*
  , alloca- `RealType' peek*
  } -> `IDAResult' #}

{#fun IDAGetNonlinSolvStats as idaGetNonlinSolvStats
  { withIDAMem* `IDAMem'
  , alloca- `CLong' peek*
  , alloca- `CLong' peek*
  } -> `IDAResult' #}

{#fun IDAGetNumGEvals as idaGetNumGEvals
  { withIDAMem* `IDAMem'
  , alloca- `CLong' peek*
  } -> `IDAResult' #}

{#fun IDAGetNumJacEvals as idaGetNumJacEvals
  { withIDAMem* `IDAMem'
  , alloca- `CLong' peek*
  } -> `IDAResult' #}

-- Kinsol


-- Kinsol allocation and deallocation functions

foreign import ccall "KINCreate"
  c_kinCreate :: IO (Ptr ())

#c
void c_kinFree(void* mem) {
  void** ptr_mem = &mem;
  KINFree(ptr_mem);
}
#endc

foreign import ccall "&c_kinFree"
  c_kinFree :: FunPtr (Ptr () -> IO ())

newtype KinMem = KinMem { getKinMemPtr :: ForeignPtr () }

newKinMem :: IO KinMem
newKinMem = do
  ptr <- c_kinCreate
  fp <- newForeignPtr c_kinFree ptr
  pure (KinMem fp)

withKinMem :: KinMem -> (Ptr () -> IO k) -> IO k
withKinMem (KinMem fp) cont = withForeignPtr fp cont


{#enum define KinResult { KIN_SUCCESS as KinSuccess
                        , KINLS_SUCCESS as KinLinSolSuccess
                        , KIN_INITIAL_GUESS_OK as KinInitialGuessOK

                        , KIN_STEP_LT_STPTOL as KinStoppedOnScaledLen
                        , KIN_LINESEARCH_NONCONV as KinLineSearchDoesntConverge
                        , KIN_MAXITER_REACHED as KinMaxIterReached
                        , KIN_MXNEWT_5X_EXCEEDED as Kin5XStepExceeded
                        , KIN_LINESEARCH_BCFAIL as KinLineSearchFailedBetaCondition
                        , KIN_LINSOLV_NO_RECOVERY as KinLinSolNoRecovery
                        , KIN_LINIT_FAIL as KinInitFailed
                        , KIN_LSETUP_FAIL as KinSetupFailed
                        , KIN_LSOLVE_FAIL as KinSolveFailed
                        , KIN_SYSFUNC_FAIL as KinSysFuncFailed
                        , KIN_FIRST_SYSFUNC_ERR as KinSysFuncRecoverableFail
                        , KIN_REPTD_SYSFUNC_ERR as KinSysFuncRepeatedRecovFail

                        , KIN_MEM_NULL as KinMemNull
                        , KIN_MEM_FAIL as KinMemFail
                        , KIN_ILL_INPUT as KinIllInput

                        , KINLS_MEM_NULL as KinLinSolMemNull
                        , KINLS_ILL_INPUT as KinLinSolIllInput
                        , KINLS_SUNLS_FAIL as KinLinSolFail
                        , KINLS_MEM_FAIL as KinLinSolMemFail

                        } deriving(Eq, Ord, Show) #}

{#pointer KINSysFn as KinSysFn #}

{#fun KINInit as kinInit
   { withKinMem* `KinMem'
   , `KinSysFn'
   , %`NVector'
   } -> `KinResult' #}

{#fun KINSetLinearSolver as kinSetDenseLinearSolver
  { withKinMem* `KinMem'
  , withDenseLinearSolver* %`DenseLinearSolver'
  , withDenseMatrix* %`DenseMatrix'
  } -> `KinResult' #}

{#fun KINSetLinearSolver as kinSetKLULinearSolver
  { withKinMem* `KinMem'
  , withKLULinearSolver* %`KLULinearSolver'
  , withSparseMatrix* %`SparseMatrix'
  } -> `KinResult' #}

{#pointer KINLsJacFn as KinLsJacFn #}

{#fun KINSetJacFn as kinSetJacFn
  { withKinMem* `KinMem'
  , `KinLsJacFn'
  } -> `KinResult' #}

{#fun KINSetUserData as kinSetUserData
  { withKinMem* `KinMem'
  , `Ptr ()'
  } -> `KinResult' #}

{#enum define KinSolStrategy { KIN_NONE as NewtonIteration
                             , KIN_LINESEARCH as NewtonWithGlobalization
                             , KIN_FP as FixedPointWithAndersonAcceleration
                             , KIN_PICARD as PicardWithAndersonAcceleration
                             } deriving(Eq, Ord, Show) #}

{#fun KINSol as kinSolve
  { withKinMem* `KinMem'
  , %`NVector'
  , `KinSolStrategy'
  , %`NVector'
  , %`NVector'
  } -> `KinResult' #}
