{-# LANGUAGE FlexibleContexts, OverloadedLists, ScopedTypeVariables #-}
{-# OPTIONS_GHC -funbox-small-strict-fields #-}

module Hydra.Sundials.IDA ( module Hydra.Sundials.IDA
                          , C.SparseMatrixRepr(..)
                          ) where

import           Hydra.Der
import           Hydra.Simulate.Statistics
import           Hydra.Simulate.Success
import qualified Hydra.Sundials.IDA.C      as C

import           Control.Monad
import           Control.Monad.Except
import           Data.Coerce               (coerce)
import           Data.Foldable
import           Data.List                 (nub)
import           Data.Maybe
import qualified Data.Vector               as BV
import qualified Data.Vector.Generic       as V
import qualified Data.Vector.Unboxed       as UV
import           Foreign.C.Types
import           Foreign.ForeignPtr
import           Foreign.Marshal
import           Foreign.Ptr
import           Foreign.Storable

newLinearVector :: ( V.Vector v Double
                   , V.Vector v (Int, Double)
                   )
                => v Double
                -> IO C.NVector
newLinearVector vals = do
  vec <- C.n_vnew_serial (fromIntegral (V.length vals))
  writeLinearVector vec vals
  pure vec
{-# SPECIALISE newLinearVector :: BV.Vector Double -> IO C.NVector #-}
{-# SPECIALISE newLinearVector :: UV.Vector Double -> IO C.NVector #-}

readLinearVector :: C.NVector -> IO (UV.Vector Double)
readLinearVector vec = do
  vlen <- C.nv_length vec
  vec <- V.generateM (fromIntegral vlen) $ \i ->
    coerce <$> C.n_vlookup vec (fromIntegral i)
  pure vec

writeLinearVector :: ( V.Vector v Double
                     , V.Vector v (Int, Double)
                     )
                  => C.NVector
                  -> v Double
                  -> IO ()
writeLinearVector vec vals =
  V.forM_ (V.indexed vals) $
  \(i, v) -> C.n_vset vec (fromIntegral i) (coerce v)
{-# SPECIALISE writeLinearVector :: C.NVector -> BV.Vector Double -> IO () #-}
{-# SPECIALISE writeLinearVector :: C.NVector -> UV.Vector Double -> IO () #-}

type Matrix = BV.Vector (BV.Vector Double)

newSparseMatrix :: Int
                -> Int
                -> C.SparseMatrixRepr
                -> [(Int, Int)]
                -> IO C.SparseMatrix
newSparseMatrix row col sparseType nonzeros = do
  matrix <-
    C.newSparseMatrix (fromIntegral row)
                      (fromIntegral col)
                      (fromIntegral (length (nub nonzeros)))
                      sparseType

  pure matrix

data ResFnErr = Irrecoverable | Recoverable

instance Enum ResFnErr where
  toEnum (-1) = Irrecoverable
  toEnum 1    = Recoverable
  toEnum _    = undefined

  fromEnum Irrecoverable = -1
  fromEnum Recoverable   = 1

data InitVal = InitVal { initVal :: !(Maybe Double)
                       , initDer :: !(Maybe Double)
                       , valTol  :: !Double
                       }

type ResFn     = Double -> UV.Vector (Double, Double) -> Either ResFnErr Double
type JacFn     = Double -> Double -> UV.Vector (Double, Double) -> Either ResFnErr Double
type RootFn    = Double -> UV.Vector (Double, Double) -> Either ResFnErr Double
type InitFn    = UV.Vector Double -> Either ResFnErr Double
type InitJacFn = UV.Vector Double -> Either ResFnErr Double

data IDASolverConf =
  IDASolverConf { idaStartTime       :: !Double
                , idaFirstTargetTime :: !Double

                , idaResFn           :: !(BV.Vector ResFn)
                , idaJacFn           :: !(Jacobian JacFn)
                , idaRootFn          :: !(BV.Vector RootFn)
                , idaInitFns         :: !(Maybe (BV.Vector InitFn, Jacobian InitJacFn))

                , idaRelTol          :: !Double

                , idaMakeInitVec     :: !(UV.Vector Double -> BV.Vector InitVal)
                }

data IDASolverState =
  IDASolverState { idaMem         :: !C.IDAMem
                 , idaResFnPtr    :: !(ForeignPtr ())
                 , idaVarVec      :: !C.NVector
                 , idaDerVec      :: !C.NVector
                 , idaResVec      :: !C.NVector
                 , finalizeHsFuns :: !(IO ())
                 }

data SundialsErr = IDAErr C.IDAResult
                 | KinErr C.KinResult
                 deriving(Show)

checkIDACall :: IO C.IDAResult
             -> ExceptT SundialsErr IO ()
checkIDACall call = do
  res <- liftIO call
  when (res /= C.IDASuccess) $ throwError (IDAErr res)

checkKinCall :: IO C.KinResult
             -> ExceptT SundialsErr IO ()
checkKinCall call = do
  res <- liftIO call
  when (res /= C.KinSuccess &&
        res /= C.KinLinSolSuccess &&
        res /= C.KinInitialGuessOK) $ throwError (KinErr res)

attachLinearSolver :: MonadIO m
                   => Jacobian k
                   -> C.NVector
                   -> (C.DenseMatrix -> C.DenseLinearSolver -> m ())
                   -> (C.SparseMatrix -> C.KLULinearSolver -> m ())
                   -> m ()
attachLinearSolver jacobian iys attachDense attachSparse =
  case jacobian of
    Dense { jacobianSize = size } -> do
      jac <- liftIO $ C.newDenseMatrix (fromIntegral size)
                                       (fromIntegral size)
      linSol <- liftIO $ C.sunLinSolDense iys jac
      attachDense jac linSol
    Sparse { jacobianSize = size
           , nonZeros = nonzeros
           } -> do
      jac <-
        liftIO $ newSparseMatrix (fromIntegral size) (fromIntegral size)
                 C.CompressedSparseCol
                 nonzeros
      linSol <- liftIO $ C.sunLinSolKLU iys jac
      attachSparse jac linSol

idaInit :: IDASolverConf
        -> ExceptT SundialsErr IO (UV.Vector Double, IDASolverState)
idaInit IDASolverConf { idaResFn = resFnMat
                      , idaJacFn = jacFnTyp
                      , idaRootFn = rootFnMat
                      , idaInitFns = optInitSys
                      , idaStartTime = st
                      , idaFirstTargetTime = _
                      , idaMakeInitVec = makeInitVec
                      , idaRelTol = relTol
                      } = do
  let withResults results cont =
        case results of
          Left err ->
            pure $ fromIntegral $ fromEnum err
          Right vals -> do
            _ <- cont vals
            pure 0
      hsResFn tt ys yps rs = do
        ys <- readLinearVector =<< C.NVector <$> newForeignPtr_ ys
        yps <- readLinearVector =<< C.NVector <$> newForeignPtr_ yps
        rs <- C.NVector <$> newForeignPtr_ rs
        withResults
          (traverse (\fn -> fn (coerce tt) (V.zip ys yps)) resFnMat)
          (writeLinearVector rs)
      hsJacFnDense jacFn tt ct ys yps jac = do
        ys <- readLinearVector =<< C.NVector <$> newForeignPtr_ ys
        yps <- readLinearVector =<< C.NVector <$> newForeignPtr_ yps
        rowCt <- C.getDenseMatrixRowCount jac
        colCt <- C.getDenseMatrixColCount jac
        res <-
          runExceptT $
          forM_ ([0..rowCt - 1] :: [C.IndexType]) $ \i ->
          forM_ ([0..colCt - 1] :: [C.IndexType]) $ \j ->
          case jacFn (fromIntegral i) (fromIntegral j) (coerce tt) (coerce ct) (V.zip ys yps) of
            Left err ->
              throwError (fromIntegral (fromEnum err))
            Right val ->
              liftIO (C.setDenseMatrixElt jac i j (coerce val))
        pure (either id (const 0) res)
      hsJacFnSparse sparseType nonzeros jacFn tt ct ys yps jac = do
        ys <- readLinearVector =<< C.NVector <$> newForeignPtr_ ys
        yps <- readLinearVector =<< C.NVector <$> newForeignPtr_ yps
        _ <- setupSparseIndices sparseType nonzeros
              (\i iptr -> C.sparseMatSetIdxPtr jac (fromIntegral i) (fromIntegral iptr))
              (\i ival -> C.sparseMatSetIdxVal jac (fromIntegral i) (fromIntegral ival))
        res <-
          runExceptT $ forM_ nonzeros $ \(i,j) ->
          case jacFn i j (coerce tt) (coerce ct) (V.zip ys yps) of
            Left err ->
              throwError (fromIntegral (fromEnum err))
            Right val -> liftIO $
              C.setSparseMatrixElt jac (fromIntegral i) (fromIntegral j) (coerce val)
        pure (either id (const 0) res)

      hsJacFn =
        case jacFnTyp of
          Sparse _ jacFn nonzeros sparseType ->
            \tt ct ys yps _ jac -> do
              jac <- C.Matrix <$> newForeignPtr_ jac
              hsJacFnSparse sparseType nonzeros jacFn tt ct ys yps (C.SparseMatrix jac)
          Dense _ jacFn ->
            \tt ct ys yps _ jac -> do
              jac <- C.Matrix <$> newForeignPtr_ jac
              hsJacFnDense jacFn tt ct ys yps (C.DenseMatrix jac)
      hsRootFn _ tt ys yps (out :: Ptr C.RealType) = do
        ys <- readLinearVector =<< C.NVector <$> newForeignPtr_ ys
        yps <- readLinearVector =<< C.NVector <$> newForeignPtr_ yps
        let results :: Either ResFnErr (BV.Vector Double)
            results =
              traverse (\fn -> fn (coerce tt) (V.zip ys yps)) rootFnMat
        withResults results $ \vals -> do
          let pokePtr (i,v) =
                poke (plusPtr out (i * C.sizeRealType) :: Ptr C.RealType)
                (coerce v)
          traverse_ pokePtr (V.indexed vals)
      hsInitFn initFnMat is rs = do
        is <- readLinearVector =<< C.NVector <$> newForeignPtr_ is
        rs <- C.NVector <$> newForeignPtr_ rs
        withResults (traverse ($ is) initFnMat) (writeLinearVector rs)
      hsInitJacFn initJacFnTyp =
        case initJacFnTyp of
          Sparse _ jacFn nonzeros sparseType ->
            \ys jac -> do
              jac <- C.Matrix <$> newForeignPtr_ jac
              hsInitJacFnSparse sparseType nonzeros jacFn ys (C.SparseMatrix jac)
          Dense _ jacFn ->
            \ys jac -> do
              jac <- C.Matrix <$> newForeignPtr_ jac
              hsInitJacFnDense jacFn ys (C.DenseMatrix jac)
      hsInitJacFnDense initJacFn ys jac = do
        ys <- readLinearVector =<< C.NVector <$> newForeignPtr_ ys
        rowCt <- C.getDenseMatrixRowCount jac
        colCt <- C.getDenseMatrixColCount jac
        res <-
          runExceptT $
          forM_ ([0..rowCt - 1] :: [C.IndexType]) $ \i ->
          forM_ ([0..colCt - 1] :: [C.IndexType]) $ \j ->
          case initJacFn (fromIntegral i) (fromIntegral j) ys of
            Left err ->
              throwError (fromIntegral (fromEnum err))
            Right val ->
              liftIO (C.setDenseMatrixElt jac i j (coerce val))
        pure (either id (const 0) res)
      hsInitJacFnSparse sparseType nonzeros initJacFn ys jac = do
        ys <- readLinearVector =<< C.NVector <$> newForeignPtr_ ys
        _ <- setupSparseIndices sparseType nonzeros
              (\i iptr -> C.sparseMatSetIdxPtr jac (fromIntegral i) (fromIntegral iptr))
              (\i ival -> C.sparseMatSetIdxVal jac (fromIntegral i) (fromIntegral ival))
        res <-
          runExceptT $ forM nonzeros $ \(i,j) ->
          case initJacFn i j ys of
            Left err ->
              throwError (fromIntegral (fromEnum err))
            Right val ->
              liftIO (C.setSparseMatrixElt jac (fromIntegral i) (fromIntegral j) (coerce val))
        pure (either id (const 0) res)

  hsResFnPtr  <- liftIO $ C.allocResFn hsResFn
  hsJacFnPtr  <- liftIO $ C.allocJacFn hsJacFn
  hsRootFnPtr <- liftIO $ C.allocRootFn hsRootFn
  (hsInitFnPtr, hsInitJacFnPtr, freeInitFunPtrs) <- liftIO $
    case optInitSys of
      Nothing -> pure (nullFunPtr, nullFunPtr, pure ())
      Just (initFnMat, initJacFnTyp) -> do
        hsInitFnPtr <- liftIO $ C.allocInitFn (hsInitFn initFnMat)
        hsInitJacFnPtr <- liftIO $ C.allocInitJacFn (hsInitJacFn initJacFnTyp)
        let freeInitFunPtrs = do
              freeHaskellFunPtr hsInitFnPtr
              freeHaskellFunPtr hsInitJacFnPtr
        pure (hsInitFnPtr, hsInitJacFnPtr, freeInitFunPtrs)
  wrappedFuns <-
    liftIO $ C.wrapFunPtrs hsResFnPtr hsJacFnPtr hsRootFnPtr hsInitFnPtr hsInitJacFnPtr
  let finalizeHsFuns = do
        freeHaskellFunPtr hsResFnPtr
        freeHaskellFunPtr hsJacFnPtr
        freeHaskellFunPtr hsRootFnPtr
        freeInitFunPtrs
  initVals <-
    case optInitSys of
      Nothing -> pure (makeInitVec [])
      Just (initFns, initJacTyp) -> do
        -- Initialize the system
        kinMem <- liftIO C.newKinMem
        let nonInitVars =
              UV.replicate (length initFns) 0
        iys <- liftIO $ newLinearVector nonInitVars
        checkKinCall (C.kinInit kinMem (castFunPtr C.wrapInitFn) iys)
        checkKinCall (withForeignPtr wrappedFuns (C.kinSetUserData kinMem))
        attachLinearSolver
          initJacTyp iys
          (\jac linSol ->
             checkKinCall (C.kinSetDenseLinearSolver kinMem linSol jac))
          (\jac linSol ->
             checkKinCall (C.kinSetKLULinearSolver kinMem linSol jac))
        checkKinCall (C.kinSetJacFn kinMem (castFunPtr C.wrapInitJacFn))
        let ones = UV.replicate (V.length nonInitVars) 1
        us <- liftIO $ newLinearVector ones
        fs <- liftIO $ newLinearVector ones
        checkKinCall (C.kinSolve kinMem iys C.NewtonIteration us fs)
        iys <- liftIO $ readLinearVector iys
        pure (makeInitVec iys)
  let initValues = fmap (fromMaybe 0 . initVal) initVals
      initDerValues = fmap (fromMaybe 0 . initDer) initVals

  ys <- liftIO $ newLinearVector initValues
  yps <- liftIO $ newLinearVector initDerValues
  mem <- liftIO C.newIDAMem
  avtol <- liftIO $ newLinearVector (fmap valTol initVals)
  rs <- liftIO $ newLinearVector (UV.replicate (length resFnMat) 0)
  checkIDACall (C.idaInit mem (castFunPtr C.wrapResFn) (coerce st) ys yps)
  checkIDACall (C.idaSVTolerances mem (coerce relTol) avtol)
  checkIDACall (withForeignPtr wrappedFuns (C.idaSetUserData mem))
  checkIDACall (C.idaRootInit mem (length rootFnMat) (castFunPtr C.wrapRootFn))
  attachLinearSolver
    jacFnTyp
    ys
    (\jac linSol -> checkIDACall (C.idaSetDenseLinearSolver mem linSol jac))
    (\jac linSol -> checkIDACall (C.idaSetKLULinearSolver mem linSol jac))
  checkIDACall (C.idaSetJacFn mem (castFunPtr C.wrapJacFn))
  nonLinSol <- liftIO $ C.sunNonLinSolNewton ys
  checkIDACall (C.idaSetNonLinearSolver mem nonLinSol)

  pure (V.convert initValues, IDASolverState { idaMem = mem
                                             , idaResFnPtr = wrappedFuns
                                             , idaVarVec = ys
                                             , idaDerVec = yps
                                             , idaResVec = rs
                                             , finalizeHsFuns = finalizeHsFuns
                                             })

idaSolve :: C.SolverTask
         -> Double
         -> IDASolverConf
         -> IDASolverState
         -> IO (SolveResult C.IDAResult) -- (ForeignValue C.RealType))
idaSolve task dt conf state = do
  (res, tmax) <-
    liftIO $ C.idaSolve (idaMem state) (coerce dt) (idaVarVec state) (idaDerVec state) task
  case res of
    C.IDASuccess -> pure SolveSuccess { endTime = coerce tmax
                                      , successReason = NoParticularReason
                                      }
    C.IDAFoundRoot -> do
      let rootEqnNum = length (idaRootFn conf)
      allocaBytes (rootEqnNum * sizeOf (undefined :: CInt)) $ \rootArray -> do
        _ <- C.idaRootInfo (idaMem state) rootArray
        rootVec <-
          V.generateM rootEqnNum $ \i -> do
          rfn <- peek (plusPtr rootArray (fromIntegral i) :: Ptr CInt)
          pure (rootCode rfn)
        pure SolveSuccess { endTime = coerce tmax
                          , successReason = FoundRoot rootVec
                          }
    C.IDAReachedStopTime -> pure SolveSuccess { endTime = coerce tmax
                                              , successReason = ReachedStopTime
                                              }
    err -> pure (SolveFailure err)

-- Freeing is actually just a matter of freeing the allocated Haskell functions.
-- Indeed, everything is just foreign pointers with properly set finalizers
idaFree :: IDASolverState
        -> IO ()
idaFree IDASolverState { finalizeHsFuns = finalizeHsFuns } =
  finalizeHsFuns

-- withIDA :: IDASolverConf
--         -> (Double -> SuccessReason -> UV.Vector Double -> SolverStat -> IO (Next k))
--         -> IO (Either SundialsErr k)
-- withIDA conf onSuccess = do
--   mInitRes <- runExceptT $ idaInit conf
--   res <-
--     case mInitRes of
--       Right (initVals, state) -> do
--         let loop t reason out = do
--               stat <- getSolverStat (idaMem state)
--               continue <- onSuccess t reason out stat
--               case continue of
--                 Result res -> pure (Right res)
--                 Continue nt -> do
--                   solveRes <- idaSolve C.Normal nt conf state
--                   case solveRes of
--                     SolveSuccess { endTime = nt
--                                  , successReason = reason
--                                  , resultOut = out
--                                  } -> loop nt reason out
--                     SolveFailure err -> pure (Left (IDAErr err))
--         res <- loop (idaStartTime conf) InitSuccess initVals
--         idaFree state
--         pure res
--       Left err -> pure (Left err)
--   pure res


getSolverStat :: C.IDAMem
              -> IO SolverStat
getSolverStat mem = do
  (   _
    , stepCount
    , resEvalCount
    , _
    , errTestFail
    , _
    , _
    , _
    , _
    , _
    , _
    ) <- C.idaGetIntegratorStats mem
  (_, rootEvalCount) <- C.idaGetNumGEvals mem
  (_, nonLinIter, nonLinConvFails) <- C.idaGetNonlinSolvStats mem
  (_, jacEvalCount) <- C.idaGetNumJacEvals mem
  pure SolverStat { stepCount = fromIntegral stepCount
                  , resEvalCount = fromIntegral resEvalCount
                  , jacEvalCount = fromIntegral jacEvalCount
                  , rootEvalCount = fromIntegral rootEvalCount
                  , nonLinIter = fromIntegral nonLinIter
                  , errTestFail = fromIntegral errTestFail
                  , nonLinConvFails = fromIntegral nonLinConvFails
                  }
