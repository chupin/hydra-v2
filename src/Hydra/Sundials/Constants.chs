module Hydra.Sundials.Constants where

#include <kinsol/kinsol.h>

kinNewton, kinNewtonWithGlob, kinFP, kinPicard :: Num a => a
kinNewton = {#const KIN_NONE #}
kinNewtonWithGlob = {#const KIN_LINESEARCH #}
kinFP = {#const KIN_FP #}
kinPicard = {#const KIN_PICARD #}
