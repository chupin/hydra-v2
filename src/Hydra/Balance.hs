{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeFamilies #-}

module Hydra.Balance where

import           Control.Lens            hiding ( Const )
import           Control.Monad.Reader
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import qualified Data.Map.Merge.Strict         as M
import           Data.Sequence                  ( Seq )
import           Data.Set                       ( Set )
import           GHC.Generics                   ( Generic )
import           Hydra.AST
import           Hydra.Compiler
import           Hydra.Type
import           Hydra.Typing

newtype BalVar = BV Word
               deriving(Eq, Ord)

data BalType = BalModel BalVar | BalArr BalType BalType | BalNone

data instance Scheme BalType = BalForall (Set BalVar) (Set Constraint) BalType

newtype Poly = Poly (Map (Maybe BalVar) Integer)

newtype Constraint = LessThan0 Poly

instance Semigroup Poly where
  Poly p1 <> Poly p2 = Poly $ M.merge M.preserveMissing
                                      M.preserveMissing
                                      (M.zipWithMatched (const (+)))
                                      p1
                                      p2

instance Monoid Poly where
  mempty = Poly M.empty

constPoly :: Integer -> Poly
constPoly 0 = mempty
constPoly n = Poly (M.singleton Nothing n)

data Count = Count
  { countConstraints   :: Seq Constraint
  , interfaceEquations :: Poly
  , mixedEquations     :: Poly
  , localEquations     :: Poly
  }

instance Semigroup Count where
  Count cs1 i1 m1 l1 <> Count cs2 i2 m2 l2 =
    Count (cs1 <> cs2) (i1 <> i2) (m1 <> m2) (l1 <> l2)

instance Monoid Count where
  mempty = Count mempty mempty mempty mempty

data VarKind = LocalVar | InterfaceVar

data EqnKind = InterfaceEqn | LocalEqn | MixedEqn

-- This list of pattern looks a bit weird but it's done so that we can short
-- circuit in getEqnKind once we've found a MixedEqn
mergeVarInEqn :: VarKind -> EqnKind -> EqnKind
mergeVarInEqn _            MixedEqn     = MixedEqn
mergeVarInEqn LocalVar     LocalEqn     = LocalEqn
mergeVarInEqn InterfaceVar InterfaceEqn = InterfaceEqn
mergeVarInEqn _            _            = MixedEqn

data BalChkEnv = BalChkEnv
  { variableScope   :: Map Ident VarKind
  , localExprScope  :: Map Ident BalType
  , globalExprScope :: Map Global (Scheme BalType)
  }
  deriving Generic

type BalChkM = ReaderT BalChkEnv CompilerM

getEqnKind :: Map Ident VarKind -> Set Ident -> Maybe EqnKind
getEqnKind scope fvs = foldr go Nothing (scope `M.restrictKeys` fvs)
 where
  go varKind      (Just eqnKind) = Just (mergeVarInEqn varKind eqnKind)
  go LocalVar     Nothing        = Just LocalEqn
  go InterfaceVar Nothing        = Just InterfaceEqn

countEqn :: Map Ident VarKind -> TypedEqn -> Count
countEqn scope (lhs :=: rhs) = case getEqnKind scope fvs of
  Nothing           -> mempty
  Just InterfaceEqn -> mempty { interfaceEquations = constPoly 1 }
  Just LocalEqn     -> mempty { localEquations = constPoly 1 }
  Just MixedEqn     -> mempty { mixedEquations = constPoly 1 }
  where fvs = M.keysSet (freeSigVar lhs) <> M.keysSet (freeSigVar rhs)

typeExpr :: TypedExpr -> BalChkM BalType
typeExpr Const{}                         = pure BalNone
typeExpr LocalExpr { localExprName = i } = do
  localScope <- view #localExprScope
  pure (localScope M.! i)
typeExpr (Global l tes g)                     = _
typeExpr OpExpr{}                             = pure BalNone
typeExpr App { exprFun = fun, exprArg = arg } = do
  tfun <- typeExpr fun
  let BalArr targ tret = tfun
  targ' <- typeExpr arg
  undefined
typeExpr (Model l ptsi srgtestsi) = _

countRel :: TypedRel -> BalChkM Count
countRel (Eqn eqn) = do
  scope <- view #variableScope
  pure (countEqn scope eqn)
countRel (Init eqn         )                  = pure mempty
countRel (args :<>:   model)                  = undefined
countRel (Let  locals rels )                  = undefined
countRel Switch { switchBranches = branches } = undefined
