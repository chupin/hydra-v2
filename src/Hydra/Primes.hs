{-# LANGUAGE BangPatterns #-}

module Hydra.Primes where

import           Data.Foldable
import           Data.List.NonEmpty             ( NonEmpty(..) )
import           Data.Stream.Infinite           ( Stream(..) )
import qualified Data.Stream.Infinite          as Stream
import           Numeric.Natural

data Queue k v = Queue
  { minKey    :: !k
  , minVal    :: v
  , queueTail :: [Queue k v]
  }

instance Ord k => Semigroup (Queue k v) where
  q1@(Queue k1 v1 ts1) <> q2@(Queue k2 v2 ts2)
    | k1 < k2   = Queue k1 v1 (q2 : ts1)
    | otherwise = Queue k2 v2 (q1 : ts2)

singleton :: k -> v -> Queue k v
singleton k v = Queue { minKey = k, minVal = v, queueTail = [] }

insert :: Ord k => k -> v -> Queue k v -> Queue k v
insert k v queue = singleton k v <> queue

newtype Step v = Step v

primeQueue :: Num i => i -> Queue i (Step i) -- Stream i)
primeQueue p = singleton (p * p) (Step p)

-- Unfold a queue of streams such that all the elements in the queue are
-- superior to the given key
adjust :: (Num i, Ord i) => i -> Queue i (Step i) -> Queue i (Step i)
adjust p queue@Queue { minKey = q, minVal = step@(Step dq), queueTail = qs }
  | q <= p    = adjust p (foldl' (<>) (singleton (q + dq) step) qs)
  | otherwise = queue

wheel2357 :: Num i => Stream i
wheel2357 = Stream.cycle
  (  2
  :| [ 4
     , 2
     , 4
     , 6
     , 2
     , 6
     , 4
     , 2
     , 4
     , 6
     , 6
     , 2
     , 6
     , 4
     , 2
     , 6
     , 4
     , 6
     , 8
     , 4
     , 2
     , 4
     , 2
     , 4
     , 8
     , 6
     , 4
     , 6
     , 2
     , 4
     , 6
     , 2
     , 6
     , 6
     , 4
     , 2
     , 4
     , 6
     , 2
     , 6
     , 4
     , 2
     , 4
     , 2
     , 10
     , 2
     , 10
     ]
  )

spin :: Num i => Stream i -> i -> Stream i
spin (p :> ps) !n = n :> spin ps (n + p)

primes :: Stream Natural
primes = 2 :> 3 :> 5 :> 7 :> sieve (spin wheel2357 11) -- Stream.iterate (+2) 3)
 where
  sieve (p :> ps) = p :> go ps (primeQueue p)

  go (p :> ps) crossed@Queue { minKey = k } = -- if p > 13 then undefined else tracweShow (p, showQueue crossed) $
                                              if k <= p
    then go ps (adjust p crossed)
    else p :> go ps (primeQueue p <> crossed)

