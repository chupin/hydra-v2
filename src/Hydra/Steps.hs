{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}

module Hydra.Steps
  ( StepConf(..)
  , defaultStepConf
  , fromStepName
  , Step(..)
  , StepSpec
  , ExecStep(..)
  , runStep
  , parseSpec
  , Skip(DontSkip, Skip, SkipConf)
  , skipFlags
  , dontSkipFlags
  , skipByDefault
  ) where

import           Control.Monad
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Hydra.Compiler
import           Hydra.Pretty
import           Options.Applicative     hiding ( info )

data StepInfo where
  SingleStepInfo ::Pretty i => Text -> i -> StepInfo
  GroupStepInfo ::Text -> StepInfo

instance Message 'Info StepInfo where
  msgTitle (SingleStepInfo name _) = name
  msgTitle (GroupStepInfo name   ) = name

  msgBody _ (SingleStepInfo _ info) = [pretty info]
  msgBody _ (GroupStepInfo name   ) = ["Group step" <+> pretty name]

data Skip k i j where
  DontSkip ::Skip k i j
  Skip ::Skip Bool i i
  SkipConf ::{ skipByDefault :: Skip Bool i i, dontSkipFlags :: [k], skipFlags :: [k] } -> Skip [k] i i

data StepConf k i j = StepConf
  { stepName       :: Text
  , stepStopBefore :: k
  , stepStopAfter  :: k
  , stepSkip       :: Skip k i j
  }

fromStepName :: Text -> StepConf [String] i j
fromStepName name = StepConf { stepName       = name
                             , stepStopBefore = ["stop-before-" ++ strName]
                             , stepStopAfter  = ["stop-after-" ++ strName]
                             , stepSkip       = DontSkip
                             }
  where strName = T.unpack name

defaultStepConf :: Text -> StepConf [k] i j
defaultStepConf name = StepConf { stepName       = name
                                , stepStopBefore = []
                                , stepStopAfter  = []
                                , stepSkip       = DontSkip
                                }

data ExecStep c m j o where
  SingleStep ::(j -> m o) -> ExecStep c m j o
  SubStep ::Step c m j o -> ExecStep c m j o

infixr 5 `Then`

data Step c m i o where
  Step ::Pretty o
       => { stepConf :: StepConf c i o
          , stepExec    :: ExecStep c m i o
          } -> Step c m i o
  Then ::Step c m i k -> Step c m k o -> Step c m i o

type StepSpec = Step [String]

parseSpec :: StepSpec m i o -> Parser (Step Bool m i o)
parseSpec Step { stepConf = conf, stepExec = SingleStep run } = do
  conf <- parseSingleStepConf conf
  pure Step { stepConf = conf, stepExec = SingleStep run }
parseSpec Step { stepConf = conf, stepExec = SubStep step } = do
  conf <- parseSingleStepConf conf
  step <- parseSpec step
  pure Step { stepConf = conf, stepExec = SubStep step }
parseSpec (Then first second) = Then <$> parseSpec first <*> parseSpec second

-- From a list of options, makes a parser that returns a boolean indicating
-- whether to stop or not. By default we don't stop
stopParser :: String -> [String] -> Parser Bool
stopParser helpStr flags =
  flag' True (foldr (<>) (help helpStr) (fmap long flags)) <|> pure False

-- Constructs a parser that from a specification of the skipping option, returns
-- whether to skip the current step or not.
--
-- When we never skip, we just never skip so that's easy.
skipParser :: String -> String -> Skip [String] i j -> Parser (Skip Bool i j)
skipParser _ _ DontSkip = pure DontSkip
skipParser dontSkipHelpStr skipHelpStr SkipConf { skipByDefault = skipByDefault, dontSkipFlags = dontSkipFlags, skipFlags = skipFlags }
  = flag' DontSkip (foldr (<>) (help dontSkipHelpStr) (fmap long dontSkipFlags))
    <|> flag' Skip (foldr (<>) (help skipHelpStr) (fmap long skipFlags))
    <|> pure skipByDefault

parseSingleStepConf :: StepConf [String] i j -> Parser (StepConf Bool i j)
parseSingleStepConf StepConf { stepName = name, stepStopBefore = stopBefore, stepStopAfter = stopAfter, stepSkip = skip }
  = do
    let strName = T.unpack name
        mkStopHelp info = "Stop " ++ info ++ " the " ++ strName ++ " step"

    stopBefore <- stopParser (mkStopHelp "before") stopBefore
    stopAfter  <- stopParser (mkStopHelp "after") stopAfter
    skip       <- skipParser ("Don't skip the " ++ strName ++ " step")
                             ("Skip the " ++ strName ++ " step")
                             skip
    pure StepConf { stepName       = name
                  , stepStopBefore = stopBefore
                  , stepStopAfter  = stopAfter
                  , stepSkip       = skip
                  }

runStep :: Step Bool CompilerM i o -> i -> CompilerM o
runStep Step { stepConf = StepConf { stepName = name, stepStopBefore = stopBefore, stepStopAfter = stopAfter, stepSkip = skip }, stepExec = exec } input
  = do
    when stopBefore stop
    out <- case (skip, exec) of
      (Skip    , _           ) -> pure input
      (DontSkip, SubStep step) -> do
        info (GroupStepInfo name)
        runStep step input
      (DontSkip, SingleStep run) -> do
        out <- run input
        info (SingleStepInfo name out)
        pure out
    when stopAfter stop
    pure out
runStep (Then first second) input = runStep first input >>= runStep second
