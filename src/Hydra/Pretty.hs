{-# LANGUAGE OverloadedStrings #-}

module Hydra.Pretty
  ( module Hydra.Pretty
  , module Data.Text.Prettyprint.Doc
  ) where

import           Data.Foldable
import           Data.Ratio
import           Data.Text.Prettyprint.Doc

ilvl :: Int
ilvl = 2

($+$) :: Doc a -> Doc a -> Doc a
x $+$ y = x <> line <> y

pList :: Foldable t => Doc a -> t (Doc a) -> Doc a
pList sep = go sep . toList
 where
  go _   []       = ""
  go _   [p     ] = p
  go sep (p : ps) = p <> sep <> go sep ps

lineBlock :: Foldable t => t (Doc a) -> Doc a
lineBlock = pList line

commaList :: Foldable t => t (Doc a) -> Doc a
commaList = pList ", "

spaceList :: Foldable t => t (Doc a) -> Doc a
spaceList = pList " "

block :: (Pretty p, Functor t, Foldable t) => t p -> Doc a
block = lineBlock . addAnd . fmap pretty . toList
 where
  addAnd []       = []
  addAnd [p     ] = [p]
  addAnd (p : ps) = (p <+> "and") : addAnd ps

newtype VSep a = VSep [a]

instance Pretty a => Pretty (VSep a) where
  pretty (VSep as) = hsep (fmap pretty as)

newtype PrettyRational = PrettyRational Rational

instance Pretty PrettyRational where
  pretty (PrettyRational c) = case den of
    1   -> pretty num
    den -> parens (pretty num <> "/" <> pretty den)
   where
    num = numerator c
    den = denominator c
