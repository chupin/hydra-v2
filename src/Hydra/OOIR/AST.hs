{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}

{-# LANGUAGE DeriveGeneric #-}
module Hydra.OOIR.AST
  ( module Hydra.OOIR.AST
  , module Hydra.Ident
  , F.LocalSig(..)
  , F.RootKind(..)
  , F.Mode(..)
  , F.Label(..)
  , F.TExpr(..)
  , F.Arg(..)
  , F.Typed(..)
  , F.Holds(..)
  ) where

import           Data.Foldable
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Sequence                  ( Seq(..) )
import           Data.Set                       ( Set )
import qualified Data.Set                      as S

import           Data.Monoid                    ( All(All) )
import           Data.Semigroup                 ( Max(Max) )
import           GHC.Generics                   ( Generic )
import qualified Hydra.Flatten.AST             as F
import           Hydra.Ident
import           Hydra.Name
import           Hydra.Op
import           Hydra.Pretty
import           Hydra.Type                     ( EVar
                                                , SVar
                                                , TySubst
                                                , Typed(..)
                                                )
-- In this module lies the object-oriented intermediate representation used
-- before code generation to LLVM. This representation tries to be as explicit
-- as possible with regard to the functional level of the language. In
-- particular, it makes all captures at the functional level explicit.

-- * Top-level declarations
--
-- A top-level declaration, represented by the Inst type contains the list of
-- arguments, its type and the body. Insts are lowered to LLVM not to functions
-- directly but rather to structs containing the arity and a function pointer.
-- This allows for the evaluation of partially applied functions.

data Inst = Inst
  { instArgs :: Seq F.Arg
  , instBody :: Expr
  }

instArity :: Inst -> Int
instArity Inst { instArgs = args } = length args

data Decl = MonoDecl { declName :: Name
                     , declInst :: Inst
                     }
          | Template { declName      :: Name
                     , templateInsts :: Map (TySubst EVar SVar) Inst
                     }

newtype Module = Module { moduleDecls :: Seq Decl
                        }

-- * Inst bodies
--
-- A signal relation is compiled to an object that captures some global
-- expressions. We make it so that we can capture whole expressions and not just
-- identifiers, as it might be the case that some expressions can be fully
-- evaluated during the construction of the object, saving work during
-- simulation. A similar trick will be used for closures (when closures are
-- implemented
data ModelObj = Obj
  { modelCaptures :: Map Ident Capture
  , modelOutput   :: Seq Ident
  , modelBlock    :: Block
  }

data Capture = Capture
  { captureRepl :: Ident
  , captureTy   :: F.TExpr
  }

-- * Normal expressions
--
-- Expressions consist of references to global or local expressions, operations
-- on expressions and calls. Calls may either be to known functions or unknown.
-- Finally, they may be signal relations objects

data Expr = NormExpr NormExpr
          | ModelObj ModelObj
          | OpExpr (F.Op Expr)

-- A normalised expression is either a call or, a local reference or a constant.
-- Note that global variables are represented as 0-arity functions, which
-- mirrors how they are implemented in LLVM. It's unclear whether it's necessary
-- but it somewhats simplifies the compiler and I would hope LLVM is able to
-- optimise the call anyway.
data NormExpr = Call Call | AtomExpr AtomExpr

data Call = KnownCall F.Saturated F.Global (Seq Expr)
          | UnknownCall NormExpr (Seq Expr)

data AtomExpr = LocalExpr Ident F.TExpr | Const Rational

-- * Signals
--
-- Signals can either consist of local signals, operations on signals or
-- atomic constant expressions

data Sig = Local F.LocalSig | SigExpr AtomExpr | Op (F.Op Sig)

data FreeVar = FreeVar
  { freeSigVars :: Map Ident (Max Word)
  , freeExprVar :: Map Ident F.TExpr
  }

instance Semigroup FreeVar where
  FreeVar is1 as1 <> FreeVar is2 as2 =
    FreeVar (M.unionWith (<>) is1 is2) (as1 <> as2)

instance Monoid FreeVar where
  mempty = FreeVar [] []

linearSignals :: Sig -> Map Ident All
linearSignals (Local   (F.LocalSig i _)) = M.singleton i (All True)
linearSignals (SigExpr _               ) = []
linearSignals (Op      op              ) = case op of
  ArithBinOp Add p q -> M.unionWith (<>) (linearSignals p) (linearSignals q)
  ArithBinOp Sub p q -> M.unionWith (<>) (linearSignals p) (linearSignals q)
  ArithBinOp Mul SigExpr{} q -> linearSignals q
  ArithBinOp Mul p SigExpr{} -> linearSignals p
  ArithBinOp Div p SigExpr{} -> linearSignals p
  ArithBinOp _ p q ->
    fmap (const (All False)) (linearSignals p <> linearSignals q)
  ArithUnOp _ p -> fmap (const (All False)) (linearSignals p)

freeLocals :: Sig -> FreeVar
freeLocals (Local   (F.LocalSig i n)) = FreeVar (M.singleton i (Max n)) []
freeLocals (SigExpr expr            ) = FreeVar [] (atomExprFreeVar expr)
freeLocals (Op      op              ) = foldMap freeLocals op

atomExprFreeVar :: AtomExpr -> Map Ident F.TExpr
atomExprFreeVar (LocalExpr index typ) = [(index, typ)]
atomExprFreeVar Const{}               = []

callFreeVar :: Call -> Map Ident F.TExpr
callFreeVar (KnownCall _ _ args) = foldMap exprFreeVar args
callFreeVar (UnknownCall fun args) =
  normExprFreeVar fun <> foldMap exprFreeVar args

normExprFreeVar :: NormExpr -> Map Ident F.TExpr
normExprFreeVar (Call     call    ) = callFreeVar call
normExprFreeVar (AtomExpr atomExpr) = atomExprFreeVar atomExpr

exprFreeVar :: Expr -> Map Ident F.TExpr
exprFreeVar (NormExpr normExpr) = normExprFreeVar normExpr
exprFreeVar (ModelObj Obj { modelCaptures = captures }) =
  fmap captureTy captures
exprFreeVar (OpExpr op) = foldMap exprFreeVar op

data Event = Root
  { rootKind   :: F.RootKind
  , watchedSig :: Sig
  }

data Rel = Eqn F.Holds Sig
         | Let (Set Ident) (Seq Rel)
         | ModelCall (Seq Ident) Ident
         | Switch (F.Mode Expr) (Map F.Label Branch)

letRelMerge :: Set Ident -> Seq Rel -> Rel
letRelMerge idts (Let idts' rels :<| Empty) = letRelMerge (idts' <> idts) rels
letRelMerge idts rels                       = Let idts rels

letRel :: Set Ident -> Seq Rel -> Seq Rel
letRel idts rels | S.null idts         = rels
letRel idts (Let idts' rels :<| Empty) = letRel (idts' <> idts) rels
letRel _    Empty                      = Empty
letRel idts rels                       = [Let idts rels]

relFreeVars :: Rel -> FreeVar
relFreeVars (Eqn _    sig ) = freeLocals sig
relFreeVars (Let idts rels) = FreeVar exprVar (sigVar `M.withoutKeys` idts)
  where FreeVar exprVar sigVar = foldMap relFreeVars rels
relFreeVars (ModelCall args model) = FreeVar
  (foldr (\idt -> M.insert idt (Max 0)) [] args)
  [(model, F.TModel (fmap (const F.TReal) args))]
relFreeVars (Switch initMode branches) = FreeVar
  sigBranchesVar
  (initModeVars <> exprBranchesVar)
 where
  freeBranchVars Branch { branchArguments = args, branchReinits = reinits, branchConditions = conds, branchBlock = block }
    = FreeVar
      (blockSigVar <> M.fromSet (const (Max 0)) reinits)
      (blockExprVar `M.withoutKeys` foldr (S.insert . F.argName) [] args)
   where
    FreeVar blockSigVar blockExprVar =
      freeBlockVars block <> foldMap freeCondVar conds

  freeCondVar (Root { watchedSig = sig } :-> targetMode) =
    freeLocals sig <> foldMap freeLocals targetMode

  freeBlockVars Block { blockAttributes = attributes, blockRelations = rels } =
    FreeVar sigVar (attrsVar <> (exprVar M.\\ attributes))
   where
    FreeVar sigVar exprVar = foldMap relFreeVars rels
    attrsVar               = foldMap exprFreeVar attributes M.\\ attributes

  initModeVars                           = foldMap exprFreeVar initMode
  FreeVar sigBranchesVar exprBranchesVar = foldMap freeBranchVars branches

-- A block has, essentially, two lists of attributes. The first are its
-- arguments, they are constructed when a block is activated (typically in a
-- switch), the second are its attributes: typically captured locals and
-- globals.
data Block = Block
  { blockAttributes :: Map Ident Expr
  , blockRelations  :: Seq Rel
  }
  deriving Generic

data Condition = Event :-> F.Mode Sig

successorLabel :: Condition -> F.Label
successorLabel (_ :-> F.Mode { F.modeLabel = label }) = label

-- Unlike in the representation of F.Branch, the Branch doesn't have explicit
-- arguments. Instead the arguments are passed as the first attributes to the
-- block. These arguments are constructed either at initialisation
data Branch = Branch
  { branchArguments  :: Seq F.Arg
  , branchReinits    :: Set Ident
  , branchConditions :: Seq Condition
  , branchBlock      :: Block
  }
  deriving Generic

instance Typed AtomExpr F.TExpr where
  typeOf Const{}           = F.TSig F.TReal
  typeOf (LocalExpr _ typ) = typ

instance Typed Expr F.TExpr where
  typeOf (NormExpr expr) = typeOf expr
  typeOf OpExpr{}        = F.TSig F.TReal
  typeOf (ModelObj Obj { modelOutput = out }) =
    F.TModel (fmap (const F.TReal) out)

instance Typed NormExpr F.TExpr where
  typeOf (Call     call) = typeOf call
  typeOf (AtomExpr expr) = typeOf expr

instance Typed Call F.TExpr where
  typeOf call = case call of
    UnknownCall fun args -> F.typeApp (typeOf fun) (fmap typeOf args) -- match Empty (typeOf fun) args
    KnownCall _ F.Global { F.globalInst = inst } args ->
      F.typeApp (typeOf inst) (fmap typeOf args)

instance Typed Inst F.TExpr where
  typeOf Inst { instArgs = args, instBody = body } =
    F.tarr (fmap typeOf args) (typeOf body)

instance Pretty Sig where
  pretty (SigExpr expr) = pretty expr
  pretty (Local   lsig) = pretty lsig
  pretty (Op      op  ) = parens (prettyOp (fmap pretty op))

instance Pretty AtomExpr where
  pretty (Const c       ) = pretty (PrettyRational c)
  pretty (LocalExpr i ty) = parens (pretty i <+> ":" <+> pretty ty)

instance Pretty NormExpr where
  pretty (Call     call) = pretty call
  pretty (AtomExpr expr) = pretty expr

instance Pretty Call where
  pretty (KnownCall (F.Saturated n) F.Global { F.globalName = nm } args) =
    parens
      ("'KNOWN-" <> pretty n <+> pretty nm <+> spaceList (fmap pretty args))
  pretty (UnknownCall fun args) =
    parens ("'UNKNOWN" <+> parens (pretty fun <+> spaceList (fmap pretty args)))

instance Pretty Expr  where
  pretty (NormExpr expr) = pretty expr
  pretty (OpExpr   op  ) = prettyOp (fmap pretty op)
  pretty (ModelObj obj ) = pretty obj

instance Pretty Block where
  pretty Block { blockAttributes = attrs, blockRelations = rels } =
    nest ilvl ("[attributes]" $+$ lineBlock (M.mapWithKey go attrs))
      $+$ nest ilvl ("[relations]" $+$ lineBlock (fmap pretty rels))
    where go idt attr = pretty idt <+> "=" <+> pretty attr

instance Pretty ModelObj where
  pretty Obj { modelCaptures = captures, modelOutput = output, modelBlock = block }
    = nest ilvl ("[captures]" $+$ lineBlock (M.mapWithKey go captures))
      $+$ nest ilvl ("[interface]" $+$ commaList (fmap pretty output))
      $+$ nest ilvl ("[block]" $+$ pretty block)
    where go i j = "capture" <+> pretty i <+> "as" <+> pretty j

instance Pretty Capture where
  pretty (Capture idt ty) = parens (pretty idt <+> ":" <+> pretty ty)

instance Pretty Event where
  pretty Root { rootKind = root, watchedSig = evt } =
    pretty root <> parens (pretty evt)

instance Pretty Rel where
  pretty (Eqn holds eqn) = case holds of
    F.Always -> pretty eqn
    F.AtInit -> "init" <+> pretty eqn
  pretty (Let locals rels) =
    nest
        ilvl
        (   "let"
        <+> commaList (fmap pretty (toList locals))
        <+> "in"
        $+$ block rels
        )
      $+$ "end"
  pretty (Switch ilab branches) = nest
    ilvl
    ("switch" <+> "init" <+> pretty ilab $+$ lineBlock
      (M.mapWithKey prettyBranch branches)
    )
   where
    prettyBranch label Branch { branchArguments = args, branchReinits = reinits, branchConditions = conds, branchBlock = block }
      = nest
        ilvl
        (   "mode"
        <+> pretty label
        <>  parens (commaList (fmap pretty args))
        <>  (if S.null reinits
              then ""
              else
                space
                <>  "reinit"
                <+> commaList (fmap pretty (toList reinits))
                <>  space
            )
        <>  nest ilvl ("->" $+$ pretty block $+$ lineBlock (fmap pretty conds))
        )
  pretty (ModelCall output call) =
    commaList (fmap pretty output) <+> "<>" <+> pretty call

instance Pretty Condition where
  pretty (sig :-> mode) = "when" <+> pretty sig <+> "->" <+> pretty mode

instance Pretty Inst where
  pretty Inst { instArgs = args, instBody = body } =
    nest ilvl (spaceList (fmap pretty args) <+> "=" $+$ pretty body)

instance Pretty Decl where
  pretty MonoDecl { declName = nm, declInst = inst } =
    "let [mono]" <+> pretty nm <+> pretty inst
  pretty Template { declName = nm, templateInsts = insts } = nest
    ilvl
    ("let [template]" <+> pretty nm $+$ nest ilvl (prettyInsts insts))
   where
    prettyInst tySubst inst = nest ilvl ("[Subst]" $+$ pretty tySubst)
      $+$ nest ilvl ("[Code]" $+$ pretty inst)

    prettyInsts insts =
      "[instantiations]" $+$ lineBlock (M.mapWithKey prettyInst insts)

instance Pretty Module where
  pretty Module { moduleDecls = decls } =
    lineBlock (fmap pretty (toList decls))
