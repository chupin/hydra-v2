{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}

module Hydra.OOIR.MinimizeBand
  ( minimizeBandStep
  ) where

import           Control.Lens            hiding ( Empty )
import qualified Data.Map                      as M
import qualified Data.Map.Strict               as MS
import           Data.Sequence                  ( Seq((:<|), Empty) )
import qualified Data.Sequence                 as Seq
import           Data.Set                       ( Set )
import qualified Data.Set                      as S
import           Hydra.OOIR.AST
import           Hydra.Steps

minimizeBandStep :: Applicative m => StepSpec m Module Module
minimizeBandStep = Step
  { stepConf = StepConf
                 { stepName       = "minimize band width"
                 , stepStopBefore = ["stop-before-band-minimization"]
                 , stepStopAfter  = ["stop-after-band-minimization"]
                 , stepSkip = SkipConf { skipByDefault = DontSkip
                                       , skipFlags = ["no-band-minimization"]
                                       , dontSkipFlags = []
                                       }
                 }
  , stepExec = SingleStep (pure . minimizeModuleBand)
  }

minimizeModuleBand :: Module -> Module
minimizeModuleBand Module { moduleDecls = decls } =
  Module { moduleDecls = fmap minimizeDeclBand decls }

minimizeDeclBand :: Decl -> Decl
minimizeDeclBand MonoDecl { declName = nm, declInst = inst } =
  MonoDecl { declName = nm, declInst = minimizeInstBand inst }
minimizeDeclBand Template { declName = nm, templateInsts = insts } =
  Template { declName = nm, templateInsts = fmap minimizeInstBand insts }

minimizeInstBand :: Inst -> Inst
minimizeInstBand Inst { instArgs = args, instBody = body } =
  Inst { instArgs = args, instBody = minimizeExprBand body }

minimizeExprBand :: Expr -> Expr
minimizeExprBand expr@NormExpr{} = expr
minimizeExprBand (ModelObj obj@Obj { modelBlock = block@Block { blockRelations = rels } })
  = ModelObj obj { modelBlock = block { blockRelations = minimizeBand =<< rels }
                 }
minimizeExprBand (OpExpr op) = OpExpr (fmap minimizeExprBand op)

minimizeBand :: Rel -> Seq Rel
minimizeBand (Let idts rels) = minimizeLetBand idts rels
minimizeBand (Switch initMode branches) =
  [ Switch
      initMode
      (fmap (over (#branchBlock . #blockRelations) (>>= minimizeBand)) branches)
  ]
minimizeBand rel@Eqn{}       = [rel]
minimizeBand rel@ModelCall{} = [rel]

-- The way we try to minimize the width of the band for a let is as follows:
--
-- We start by taking pairing each relation with the variables bound by idts
-- that appear in it. Split the resulting list by disjoint set (sets of
-- relations that share no variables at all).
--
-- For every such disjoint set, we then choose a variable such that it appears
-- in the smallest number of relations in the set. We place the relations in
-- which it appears in a let block that now forms a new relation and repeat
-- until no variables can be eliminated.
minimizeLetBand :: Set Ident -> Seq Rel -> Seq Rel
minimizeLetBand idts rels = minimizeByDisjointSet
  (fmap pairWithFreeSignals rels)
 where
  -- pairWithFreeSignals pairs a relation with the set of variables that appear free in
  -- the relations and in idts
  pairWithFreeSignals rel =
    (rel, M.keysSet (freeSigVars (relFreeVars rel)) `S.intersection` idts)

-- minimizeByDisjointSet splits the list of relations by disjoint sets (see
-- byDisjointSet definition and minimizeLetBand for comments) and then delegate
-- to minimizeInDisjointSet for picking a variable, etc.
minimizeByDisjointSet :: Seq (Rel, Set Ident) -> Seq Rel
minimizeByDisjointSet annRels = byDisjointSet annRels >>= minimizeInDisjointSet

-- minimizeInDisjointSet assumes that annRels forms a joint set (but I don't
-- think it's necessary for it to work). It picks a variable to let-bind (if
-- this fails, it just returns the sequence of relations unchanged) and then
-- calls minimizeByDisjointSet to split the sequence by disjoint set again and
-- keep going.
minimizeInDisjointSet :: Seq (Rel, Set Ident) -> Seq Rel
minimizeInDisjointSet annRels = case pickVariable annRels of
  Nothing      -> fmap fst annRels
  Just annRels -> minimizeByDisjointSet annRels

-- From a sequence of relations annoted with the identifiers that appear in it.
-- Pick a variable such that it appears in the smallest number of relations:
-- let-bind this variable by taking all the relations in which it appears and
-- constructing a let block from it. Add this new relation to the relations in
-- which the selected variable did not appear.
--
-- This function assumes that there is at least one variable to pick, otherwise
-- it returns Nothing
pickVariable :: Seq (Rel, Set Ident) -> Maybe (Seq (Rel, Set Ident))
pickVariable annRels = case choice of
  Nothing       -> Nothing
  Just (idt, _) -> Just (introLet idt annRels)
 where
  go !occurences Empty                = occurences
  go !occurences ((_, idts) :<| rels) = go (extOcc idts occurences) rels

  choice = M.foldrWithKey
    (\key occ best -> case best of
      Nothing                      -> Just (key, occ)
      Just (_, occ') | occ >= occ' -> best
      Just _                       -> Just (key, occ)
    )
    Nothing
    (go [] annRels)

  extOcc idts = MS.unionWith (+) (MS.fromSet (const 1) idts)

-- From an identifier and a set of relations annotated by the variables that
-- appear in them. Split the list of relations depending on whether the
-- identifier passed as argument appear in it or not. Gather the relations in
-- which it appears in a let block binding the identifier in question. Add this
-- new relation to the list of relations the identifier doesn't appear in.
-- Annotate the new relations with the set of free identifiers that appear in it
-- (that is the union of the identifiers that appear in every individual
-- relation, minus the identifier that is now let-bound)
introLet :: Ident -> Seq (Rel, Set Ident) -> Seq (Rel, Set Ident)
introLet i rels = (letRelMerge [i] rs, S.delete i ls) :<| noIs
 where
  (is, noIs) = Seq.partition (\(_, idts) -> i `S.member` idts) rels

  (rs, ls) =
    foldr (\(rel, ls) (rels, is) -> (rel :<| rels, ls <> is)) ([], []) is

-- Group relations by sets of common variables. For instance, the following
-- relations:
--
--    a = b
--    b = c
--    c = d
--    e = f
--    g = h
--
-- should be split as [a = b, b = c, c = d], [e = f], [g = h], since a, b, c and
-- d appear in common relations but don't appear with e, f, g or h.
--
-- This is done by the function go. go carries a Seq (Set Ident, Seq (Rel, Set
-- Ident)) as an accumulator that is returned at the end. When a relation is
-- encountered, we extend the accumulator with the extEncountered function.
-- extEncountered checks if any of the identifiers that appear in the relation
-- already appears in a group of relations. If it does, the relation is added
-- (along with all other identifiers that appear in it) and extEncountered is
-- called on the tail with this new set of relations.
byDisjointSet :: Seq (Rel, Set Ident) -> Seq (Seq (Rel, Set Ident))
byDisjointSet = fmap snd . go Empty
 where
  go encountered Empty = encountered
  go encountered (block@(_, idts) :<| rels) =
    go (extEncountered [block] idts encountered) rels

  extEncountered block idts Empty = Seq.singleton (idts, block)
  extEncountered block idts ((idts', block') :<| rels)
    | idts `S.disjoint` idts' =   (idts', block')
    :<| extEncountered block idts rels
    | otherwise = extEncountered (block Seq.>< block') (idts <> idts') rels
