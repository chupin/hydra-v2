{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}

{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses, RecursiveDo #-}

{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}

module Hydra.Flatten
  ( flattenStep
  ) where

import           Control.Comonad
import           Control.Exception              ( assert )
import           Control.Lens            hiding ( Const
                                                , Empty
                                                )
import           Control.Monad.Reader
import           Control.Monad.State
import           Data.Foldable
import           Data.Generics.Labels           ( )
import           Data.Map                       ( Map )
import qualified Data.Map                      as M
import           Data.Maybe
import           Data.Sequence                  ( Seq(..) )
import qualified Data.Sequence                 as Seq
import qualified Data.Set                      as S
import           Data.Stream.Supply
import qualified Data.Text                     as T
import           GHC.Generics
import           Hydra.AST
import           Hydra.Compiler
import qualified Hydra.Flatten.AST             as F
import           Hydra.Loc                      ( Located(..) )
import           Hydra.Pretty
import           Hydra.Steps
import           Hydra.Type
import           Hydra.Typing                   ( Global(..) )

-- Sort a block of relations so that using band matrices is viable given the way
-- we order variables. The goal is to put the equations in which early variables
-- occur appear early. This means that we define the following ordering on
-- relations:
--    * equations
--    * switches
--    * model calls
--    * let blocks
--
-- The comparator assumes that no equality exists between relations.
sortBlock :: Seq F.Rel -> Seq F.Rel
sortBlock = Seq.unstableSortBy go
 where
  go F.Rel{}       _             = LT
  go _             F.Rel{}       = GT
  go F.ModelCall{} _             = LT
  go _             F.ModelCall{} = GT
  go F.Switch{}    _             = LT
  go _             F.Switch{}    = GT
  go F.Let{}       _             = GT


data IdentSubst ty = IdentSubst ty Ident | ListSubst (Seq (IdentSubst ty))
  deriving ( Functor )

flattenSubst :: IdentSubst ty -> Seq (Ident, ty)
flattenSubst (IdentSubst ty idt) = [(idt, ty)]
flattenSubst (ListSubst ts     ) = ts >>= flattenSubst

identsOfSubst :: IdentSubst ty -> Seq Ident
identsOfSubst = fmap fst . flattenSubst

sigsOfSubst :: IdentSubst () -> Seq F.Sig
sigsOfSubst = fmap (\i -> F.Local (F.LocalSig i 0)) . identsOfSubst

substOfTSig :: Ident -> PTSig -> FlatM (Either () (IdentSubst ()))
substOfTSig _   TReal        = pure (Left ())
substOfTSig idt (TSigVar tv) = do
  tvSubst <- view (#typVarSubst . #svarSubst)
  substOfTSig idt (lookupSubst tv tvSubst)
substOfTSig idt (TSigTuple ts) =
  Right
    .   ListSubst
    <$> Seq.traverseWithIndex
          (\idx typ -> do
            supply <- freshSupply
            let idt' = extract supply (source idt <> "_" <> T.pack (show idx))
            ts <- substOfTSig idt' typ
            case ts of
              Left  _  -> pure $ IdentSubst () idt'
              Right ts -> pure ts
          )
          ts

substOfTExpr :: Ident -> PTExpr -> FlatM (Either F.TExpr (IdentSubst F.TExpr))
substOfTExpr idt (TSig ts) = do
  subst <- substOfTSig idt ts
  case subst of
    Left  _     -> pure (Left (F.TSig F.TReal))
    Right subst -> pure (Right (fmap (\_ -> F.TSig F.TReal) subst))
substOfTExpr idt (TExprVar tv) = do
  evSubst <- view (#typVarSubst . #evarSubst)
  substOfTExpr idt (evSubst M.! tv)
substOfTExpr idt (TModel ts) = do
  ts     <- flatTSig ts
  supply <- freshSupply
  pure $ Right $ IdentSubst (F.TModel ts) (extract supply (source idt))
substOfTExpr idt (TArr it ot) = do
  its    <- flatTExpr it
  ot     <- seqHead <$> flatTExpr ot
  supply <- freshSupply
  pure $ Right $ IdentSubst (F.tarr its ot) (extract supply (source idt))
substOfTExpr idt (TExprTuple ts) =
  Right
    .   ListSubst
    <$> Seq.traverseWithIndex
          (\idx typ -> do
            supply <- freshSupply
            let idt' = extract supply (source idt <> "_" <> T.pack (show idx))
            ts <- substOfTExpr idt' typ
            case ts of
              Left _ -> do
                typ <- flatTExpr typ
                pure $ IdentSubst (seqHead typ) idt'
              Right ts -> pure ts
          )
          ts

lookupSubst :: (Ord tv, Pretty tv, Pretty ty) => tv -> Map tv ty -> ty
lookupSubst tv subst = case M.lookup tv subst of
  Just ty -> ty
  Nothing -> internalError
    (unlines
      ([ "Variable " ++ show (pretty tv) ++ " was not found in a substitution."
       , "The substitution contains the following keys:"
       ]
      ++ fmap (\tv -> "   " ++ show (pretty tv)) (M.keys subst)
      )
    )

flatTExpr :: PTExpr -> FlatM (Seq F.TExpr)
flatTExpr (TExprVar ev) = do
  subst <- view (#typVarSubst . #evarSubst)
  flatTExpr (lookupSubst ev subst)
flatTExpr (TSig ts) = do
  ts <- flatTSig ts
  pure (fmap F.TSig ts)
flatTExpr (TExprTuple ts) = join <$> traverse flatTExpr ts
flatTExpr (TModel     ts) = do
  ts <- flatTSig ts
  pure [F.TModel ts]
flatTExpr (TArr it ot) = do
  its <- flatTExpr it
  ot  <- seqHead <$> flatTExpr ot
  pure [F.tarr its ot]

flatTSig :: PTSig -> FlatM (Seq F.TSig)
flatTSig (TSigVar sv) = do
  subst <- view (#typVarSubst . #svarSubst)
  flatTSig (lookupSubst sv subst)
flatTSig TReal          = pure [F.TReal]
flatTSig (TSigTuple ts) = join <$> traverse flatTSig ts

data FlatSubst = FlatSubst
  { sigVarSubst  :: Map Ident (IdentSubst ())
  , exprVarSubst :: Map Ident (IdentSubst F.TExpr)
  , typVarSubst  :: TySubst EVar SVar
  }
  deriving Generic

newtype FlatSt = FlatSt { globalContext :: Map Name F.Decl
                        }
  deriving ( Generic )

newtype FlatM a = FlatM (ReaderT FlatSubst (StateT FlatSt CompilerM) a)
  deriving ( Functor, Applicative, Monad, MonadReader FlatSubst
           , MonadState FlatSt, MonadFix )

data FlatteningError =
  NonCausalRelation | ModelDoesNotExist Name | NotASelfContainedModel Name

instance Message 'Error FlatteningError where
  msgTitle _ = "flattening error"

  msgBody _ NonCausalRelation =
    ["A relation expected to have been causal, given its type, was not"]
  msgBody _ (ModelDoesNotExist name) =
    ["The model" <+> pretty name <+> "was not found."]
  msgBody _ (NotASelfContainedModel name) =
    [ "The model"
        <+> pretty name
        <+> "cannot be simulated"
        <+> "as it expects arguments."
    ]

freshSupply :: FlatM (Supply (Name -> Ident))
freshSupply = FlatM $ lift $ lift $ getSupplyWith (flip ident)

runFlatM :: FlatM a -> TySubst EVar SVar -> StateT FlatSt CompilerM a
runFlatM (FlatM act) tySubst = runReaderT
  act
  FlatSubst { sigVarSubst = [], exprVarSubst = [], typVarSubst = tySubst }

instantiate
  :: TySubst EVar SVar
  -> Decl Global PTExpr PTSig Ident
  -> StateT FlatSt CompilerM F.Inst
instantiate tySubst Decl { declName = nm, declParams = args, declBody = expr }
  = do
    inst <- runFlatM
      (do
        (args, idtSubst) <- flattenPats substOfTExpr args
        locally #exprVarSubst (idtSubst <>) $ do
          expr <- seqHead <$> flattenExpr expr
          pure F.Inst { F.instArgs = fmap (uncurry F.Arg) args
                      , F.instBody = expr
                      }
      )
      tySubst
    modifying #globalContext $ M.adjust
      (\decl -> case decl of
        F.MonoDecl{} -> decl
        template@F.Template { F.templateInsts = insts } ->
          template { F.templateInsts = M.insert tySubst inst insts }
      )
      nm
    pure inst

flattenExpr :: Expr Global PTExpr PTSig Ident -> FlatM (Seq F.Expr)
flattenExpr Const { constVal = RealConst c } =
  pure [F.Const { F.constVal = RealConst c }]
flattenExpr LocalExpr { localExprName = i, localExprTyp = typ } = do
  subst <- view #exprVarSubst
  case M.lookup i subst of
    Just ss -> pure (go ss)
     where
      go (IdentSubst ty idt) = [F.LocalExpr ty idt]
      go (ListSubst ss     ) = ss >>= go
    Nothing -> do
      typ <- flatTExpr typ
      pure [F.LocalExpr { F.localExprName = i, F.localExprTyp = seqHead typ }]
flattenExpr Global { globalInfo = ModuleBound { globalName = g, globalTypSubst = unComposedTyInst } }
  = do
    globals <- use #globalContext
    tySubst <- view #typVarSubst
    -- tyinst is the instantiation of the type of g, but the instantiation may
    -- contain type variables needing to be substituted from the current
    -- tySubst. Therefore we compose tySubst with tyinst to retrieve the true
    -- instantiation of the type variables of the global g
    let tyinst = composeTySubst tySubst unComposedTyInst
        decl   = fromMaybe
          (internalError
            ("Global " ++ show (pretty g) ++ " has not been found.")
          )
          (M.lookup g globals)
    inst <- case decl of
      F.MonoDecl { F.declInst = inst } -> pure inst
      F.Template { F.templateInsts = insts, F.template = template } ->
        case M.lookup tyinst insts of
          Just inst -> pure inst
          Nothing   -> FlatM $ lift $ instantiate tyinst template
    -- Store the global with the composed tyinst' instead of tyinst
    pure
      [ F.GlobalExpr
          { F.globalExpr = F.Global { F.globalInst   = inst
                                    , F.globalTyInst = tyinst
                                    , F.globalName   = g
                                    }
          }
      ]
flattenExpr OpExpr { operExpr = op } = do
  op <- traverse (fmap seqHead . flattenExpr) op
  pure [F.arithOpExpr op]
flattenExpr App { exprFun = fun, exprArg = arg } = do
  fun  <- seqHead <$> flattenExpr fun
  args <- flattenExpr arg
  pure [F.Call { F.callExpr = F.functionCall fun args }]
flattenExpr Model { modelOutput = output, modelRelations = rels } = do
  (output, tsubst) <- flattenPat substOfTSig output
  locally #sigVarSubst (tsubst <>) $ do
    rels <- traverse flattenRel rels
    pure
      [ F.Model { F.modelOutput    = fmap fst output
                , F.modelRelations = sortBlock (join rels)
                }
      ]

seqHead :: Seq a -> a
seqHead (s :<| Empty) = s
seqHead _             = internalError "Flattening failed."

flattenSig :: Sig Global PTExpr PTSig Ident -> FlatM (Seq F.Sig)
flattenSig Expr { sigExpr = expr } = do
  expr <- flattenExpr expr
  pure (fmap F.Expr expr)
flattenSig Tuple { tuplePayload = ss } = join <$> traverse flattenSig ss
flattenSig Local { localName = nm }    = do
  subst <- view #sigVarSubst
  case M.lookup nm subst of
    Just ss -> pure $ sigsOfSubst ss
    Nothing -> pure [F.Local (F.LocalSig nm 0)]
flattenSig Der { derivedSig = sig } = do
  sig <- flattenSig sig
  pure [F.differentiate F.Time (seqHead sig)]
flattenSig Op { operSig = op } = do
  op <- traverse (fmap seqHead . flattenSig) op
  pure [F.arithOp op]

flattenPat
  :: (Ident -> ty -> FlatM (Either ty' (IdentSubst ty')))
  -> Pat ty Ident
  -> FlatM (Seq (Ident, ty'), Map Ident (IdentSubst ty'))
flattenPat substOfTyp PLocal { localPatTyp = ty, localPatName = nm } = do
  tsubst <- substOfTyp nm ty
  case tsubst of
    Left  ty'    -> pure ([(nm, ty')], [])
    Right tsubst -> pure (flattenSubst tsubst, [(nm, tsubst)])
flattenPat substOfTyp PTuple { tuplePatPayload = ps } =
  flattenPats substOfTyp ps

flattenPats
  :: (Ident -> ty -> FlatM (Either ty' (IdentSubst ty')))
  -> Seq (Pat ty Ident)
  -> FlatM (Seq (Ident, ty'), Map Ident (IdentSubst ty'))
flattenPats substOfTyp = foldM
  (\(ids, ss) pat -> do
    (id, su) <- flattenPat substOfTyp pat
    pure (ids <> id, ss <> su)
  )
  ([], [])

flattenEqn :: Eqn Global PTExpr PTSig Ident -> FlatM (Seq F.Eqn)
flattenEqn (lhs :=: rhs) = do
  lhs <- flattenSig lhs
  rhs <- flattenSig rhs
  assert (length lhs == length rhs) (pure ())
  pure (Seq.zipWith F.equation lhs rhs)

generateCall :: Seq F.Sig -> F.Expr -> FlatM (Seq F.Rel)
generateCall args model = do
  let go (localIds, localRels, args) (F.Local (F.LocalSig i 0)) =
        pure (localIds, localRels, args :|> i)
      go (localIds, localRels, args) sig = do
        supply <- freshSupply
        let i   = extract supply "call_argument"
            rel = F.Rel F.Always (F.equation (F.Local (F.LocalSig i 0)) sig)
        pure (S.insert i localIds, localRels :|> rel, args :|> i)
  (localIds, localRels, args) <- foldM go ([], [], []) args
  let call = F.ModelCall args model
  if S.null localIds
    then pure (localRels :|> call)
    else pure [F.Let localIds (localRels :|> call)]

flattenRel :: Rel Global PTExpr PTSig Ident -> FlatM (Seq F.Rel)
flattenRel (Eqn  eqn      ) = fmap (F.Rel F.Always) <$> flattenEqn eqn
flattenRel (Init eqn      ) = fmap (F.Rel F.AtInit) <$> flattenEqn eqn
flattenRel (sig :<>: model) = do
  model <- seqHead <$> flattenExpr model
  sig   <- flattenSig sig
  generateCall sig model
flattenRel (Let ids rels) = do
  (varSubst, ids) <- foldM
    (\(varSubst, ids) (id, Located _ typ) -> do
      ts <- substOfTSig id typ
      case ts of
        Right ts ->
          pure (M.insert id ts varSubst, foldr S.insert ids (identsOfSubst ts))
        Left _ -> pure (varSubst, S.insert id ids)
    )
    ([], [])
    (M.toList ids)
  locally #sigVarSubst (varSubst <>) $ do
    rels <- join <$> traverse flattenRel rels
    pure [F.Let ids (sortBlock rels)]
flattenRel (Switch (Located _ (Mode initLabel initArgs)) _ branches) = do
  initArgs <- join <$> traverse flattenExpr initArgs
  let
    flattenBranch labelIndices (flatBranches, partialIndices) (index, Branch { branchMode = Located _ (Mode label patterns), branchReinits = reinits, branchConditions = conds, branchRelations = rels })
      = do
        (args, subst) <- flattenPats substOfTExpr patterns
        locally #exprVarSubst (subst <>) $ do
          subst <- view #sigVarSubst
          let
            go (Located _ i) reinits = case M.lookup i subst of
              Nothing -> S.insert i reinits
              Just is -> foldr S.insert reinits (identsOfSubst is)
            newReinits = foldr go [] reinits

            flattenConds (Root { rootKind = kd, watchedSig = sig } :-> Located _ (Mode targetLabel payload))
              = do
                sig     <- seqHead <$> flattenSig sig
                payload <- join <$> traverse flattenSig payload
                pure
                  (F.Root { F.rootKind = kd, F.watchedSig = sig } F.:-> F.Mode
                    { F.modeLabel   = F.Label
                                        { F.labelName  = labelText targetLabel
                                        , F.labelIndex = labelIndices
                                                           M.! targetLabel
                                        }
                    , F.modePayload = payload
                    }
                  )
          conds <- traverse flattenConds conds
          rels  <- join <$> traverse flattenRel rels
          pure
            ( M.insert
              F.Label { F.labelName = labelText label, F.labelIndex = index }
              F.Branch { F.branchArgs       = fmap (uncurry F.Arg) args
                       , F.branchReinits    = newReinits
                       , F.branchConditions = conds
                       , F.branchRelations  = sortBlock rels
                       }
              flatBranches
            , M.insert label index partialIndices
            )
  rec (flatBranches, labelIndices) <- foldM (flattenBranch labelIndices)
                                            ([], [])
                                            (zip [0 ..] (toList branches))
  let initMode = F.Mode
        { F.modeLabel   = F.Label { F.labelName  = labelText initLabel
                                  , F.labelIndex = labelIndices M.! initLabel
                                  }
        , F.modePayload = initArgs
        }
  pure [F.Switch { F.initialMode = initMode, F.switchBranches = flatBranches }]

flattenDecl :: Decl Global PTExpr PTSig Ident -> StateT FlatSt CompilerM F.Decl
flattenDecl decl
  | isMonomorph = do
    decl <- instantiate (TySubst [] []) decl
    pure F.MonoDecl { F.declName = nm, F.declInst = decl }
  | otherwise = pure $ F.Template { F.declName      = nm
                                  , F.templateType  = scheme
                                  , F.template      = decl
                                  , F.templateInsts = []
                                  }
 where
  scheme@(Forall evs fvs _) = typeOf decl

  nm                        = declName decl

  isMonomorph               = S.null evs && S.null fvs

flattenModule :: Module Global PTExpr PTSig Ident -> CompilerM F.Module
flattenModule Module { moduleDecls = decls } = do
  rec (idecls, FlatSt { globalContext = finalContext }) <- runStateT
        (traverse
          (\decl -> do
            fdecl <- flattenDecl decl
            modifying #globalContext (M.insert (declName decl) fdecl)
            -- We must get the final declaration from the
            -- final context to get all instantiations for templates
            pure (finalContext M.! declName decl)
          )
          decls
        )
        FlatSt { globalContext = [] }
  pure F.Module { F.moduleDecls = idecls }

flattenStep :: StepSpec CompilerM (Module Global PTExpr PTSig Ident) F.Module
flattenStep = Step { stepConf = fromStepName "flattening"
                   , stepExec = SingleStep flattenModule
                   }
