# Benchmarking

This folder contains benchmarking code to test our idea of using
order-parametric differentiation.

The code is organised as follow. The folders in `src/` contains two LLVM files.
The file `explicit.ll` corresponds to code able to compute the first
`n`-derivative of an equation by having generated the code that computes each
one of them explicitly. The file `implicit.ll` contains code that is able to
compute any of the derivatives on demand using order-parametric formulæ.
Currently, the benchmarks are for `asin`, `exp`, product, division and squaring.

To run the benchmark, run the script `bench.sh`. For all the modules, it will
compile both LLVM code using `clang` and link it to the benchmarking code in
`bench.cpp`. The output of the benchmark can be found in the `out` folder as CSV
files.
