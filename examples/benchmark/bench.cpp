#include <benchmark/benchmark.h>

#define expl __sdebl_rel1
#define impl __sdebl_rel2

extern "C" {

#if VAR_COUNT == 1
typedef int(fun)(double** prs, double* x, uint64_t hod);
// int impl(double** prs, double* x, uint64_t hod);
// int expl(double** prs, double* x, uint64_t hod);
#elif VAR_COUNT == 2
typedef int(fun)(double** prs, double* x, double* y, uint64_t hod);
#endif

fun impl;
fun expl;
}

static void BM_Generic(fun fun, benchmark::State& state) {

    uint64_t hod = state.range(0);
    double* rs = new double[hod + 1];
    double* prs = rs;
    double** variables = new double*[VAR_COUNT];
    for (int i = 0; i < VAR_COUNT; ++i) {
        variables[i] = new double[hod + 1];
        for (int j = 0; j < hod + 1; ++j) {
            variables[i][j] = (double)rand() / RAND_MAX * 2.0 - 1.0;
        }
    }

    for (auto _ : state) {
#if VAR_COUNT == 1
        fun(&rs, variables[0], hod);
#elif VAR_COUNT == 2
        fun(&rs, variables[0], variables[1], hod);
#endif
        rs = prs;
    }

    delete[] rs;
    for (int i = 0; i < VAR_COUNT; ++i) {
        delete variables[i];
    }
    delete[] variables;
}

static void BM_Implicit(benchmark::State& state) { return BM_Generic(impl, state); }

BENCHMARK(BM_Implicit)->DenseRange(0, 20, 1);

static void BM_Explicit(benchmark::State& state) { return BM_Generic(expl, state); }

BENCHMARK(BM_Explicit)->DenseRange(0, 20, 1);

BENCHMARK_MAIN();