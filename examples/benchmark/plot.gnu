# set datafile separator ","

set autoscale
unset log
unset label
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
# set title "Average running time"
set decimal locale
show locale
set format y "%'g"

set xlabel "Number of differentiations"
set ylabel "Mean time (ns)"
set xrange [0:20]
# set yrange [0:18]
set y2tics # enable second axis
set ytics nomirror # dont show the tics on that side
set y2label "Slowdown $\\left(\\frac{\\text{Implicit}}{\\text{Explicit}}\\right)$" # label for second axis
set y2range [0:]

if (ARG2 eq 'tikz') {
    set terminal tikz size 8.5,6 createstyle
} else {
    set terminal postscript eps enhanced color
}

if (ARG3 eq 'semilog') {
    set logscale y 10
}

set key at graph 1, graph 1 right bottom # bmargin horizontal title
# set key bmargin left
# set yrange [0:1]
# set ytics 0,0.1,1

# set key autotitle columnhead # use the first line as title

types="BM_Implicit BM_Explicit"
header="Implicit Explicit"

# awk_filter(explicit_or_implicit, data)=\
# sprintf("<gawk -F, '{if (match($0, /%s\\/(.*)\"/, a)) {print a[1],$4}}' %s",\
#         explicit_or_implicit, data)

DATA=ARG1
# stats DATA
# N=STATS_columns
plot DATA using 1:2 title "Implicit" with linespoint, \
       '' using 1:3 title "Explicit" with linespoint, \
       '' using 1:($2/$3) title "Slowdown" with linespoint axis x1y2
# plot for[idx=1:words(types)] \
#      awk_filter(word(types, idx), DATA) using 1:2 title word(header, idx) with linespoint
# plot for [i=2:N] DATA using 1:i title columnhead with linespoint