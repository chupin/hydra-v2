#!/bin/sh

COMPILE_FILE () {
  echo "Compiling benchmark " $SRC_DIR

  EXPLICIT_BASE=$SRC_DIR/explicit

  EXPLICIT_SRC=$EXPLICIT_BASE.sdebl
  EXPLICIT_LL=$EXPLICIT_BASE.ll
  EXPLICIT_BC=$EXPLICIT_BASE.bc
  EXPLICIT_OBJ=$EXPLICIT_BASE.o

  IMPLICIT_BASE=$SRC_DIR/implicit

  IMPLICIT_SRC=$IMPLICIT_BASE.sdebl
  IMPLICIT_LL=$IMPLICIT_BASE.ll
  IMPLICIT_BC=$IMPLICIT_BASE.bc
  IMPLICIT_OBJ=$IMPLICIT_BASE.o

  cabal run sdebl -O2 --disable-profiling -- $EXPLICIT_SRC -o $EXPLICIT_LL --explicit-derivatives --max-index 20 --use-printf-for-debug
  echo "Generated " $EXPLICIT_LL
  cabal run sdebl -O2 --disable-profiling -- $IMPLICIT_SRC -o $IMPLICIT_LL --max-index 20 --use-printf-for-debug
  echo "Generated " $IMPLICIT_LL

  # llc -filetype=obj -O3 $EXPLICIT_LL -o $EXPLICIT_OBJ
  # llc -filetype=obj -O3 $IMPLICIT_LL -o $IMPLICIT_OBJ

  opt -O3 $EXPLICIT_LL -o $EXPLICIT_BC
  echo "Optimised" $EXPLICIT_LL "to" $EXPLICIT_BC
  opt -O3 $IMPLICIT_LL -o $IMPLICIT_BC
  echo "Optimised" $IMPLICIT_LL "to" $IMPLICIT_BC

  clang -c -O3 $EXPLICIT_BC -o $EXPLICIT_OBJ
  echo "Code generated" $EXPLICIT_BC "to" $EXPLICIT_OBJ
  clang -c -O3 $IMPLICIT_BC -o $IMPLICIT_OBJ
  echo "Code generated" $IMPLICIT_BC "to" $IMPLICIT_OBJ

  EXEC_NAME=examples/benchmark/bin/`basename $SRC_DIR`
  CSV_OUT=examples/benchmark/out/`basename $SRC_DIR`.csv

  clang++ -DVAR_COUNT=$VAR_COUNT $IMPLICIT_OBJ $EXPLICIT_OBJ \
    examples/benchmark/bench.cpp -std=c++11 -isystem \
    examples/benchmark/benchmark/include \
    -Lexamples/benchmark/benchmark/build/src -lbenchmark -lpthread \
    -o $EXEC_NAME

  echo "Compiled benchmarking executable to" $EXEC_NAME

  ./$EXEC_NAME --benchmark_out_format=csv --benchmark_out=$CSV_OUT

  sed -i 1,9d $CSV_OUT

  TEMP_IMPLICIT_CSV=/tmp/implicit.csv
  TEMP_EXPLICIT_CSV=/tmp/explicit.csv

  echo "n Implicit" > $TEMP_IMPLICIT_CSV
  echo "n Explicit" > $TEMP_EXPLICIT_CSV

  gawk -F, '{if (match($0, /BM_Implicit\/(.*)"/, a)) {print a[1],$4}}' $CSV_OUT >> $TEMP_IMPLICIT_CSV
  gawk -F, '{if (match($0, /BM_Explicit\/(.*)"/, a)) {print a[1],$4}}' $CSV_OUT >> $TEMP_EXPLICIT_CSV

  join -t " " -a 1 -o auto $TEMP_IMPLICIT_CSV $TEMP_EXPLICIT_CSV > $CSV_OUT

  PS_OUT=examples/benchmark/out/`basename $SRC_DIR`.ps
  TIKZ_OUT=examples/benchmark/out/`basename $SRC_DIR`.tikz

  gnuplot -c examples/benchmark/plot.gnu $CSV_OUT > $PS_OUT
  gnuplot -c examples/benchmark/plot.gnu $CSV_OUT tikz > $TIKZ_OUT
}

for VAR_COUNT in {1,2}; do
  for SRC_DIR in ./examples/benchmark/src/bench$VAR_COUNT-*; do
    COMPILE_FILE
  done
done

# for SRC_DIR in ./examples/benchmark/src/bench{}; do
#   VAR_COUNT=2
#   COMPILE_FILE
# done