set datafile separator ","

if (ARG2 eq 'tikz') {
    set terminal tikz size 8.5,6 createstyle
} else {
    set terminal postscript eps enhanced color
}

if (ARG3 eq 'semilog') {
    set logscale y 10
}

set key outside # left bottom
# set yrange [0:1]
# set ytics 0,0.1,1

DATA=ARG1
stats DATA
N=STATS_columns
plot for [i=2:N] DATA using 1:i title columnhead with linespoint